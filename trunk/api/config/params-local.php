<?php
if (YII_ENV_PROD):
    $productImageBaseUrl = 'https://s3.ap-south-1.amazonaws.com/nayashoppy/uploads/';
elseif (YII_ENV_TEST):
    $productImageBaseUrl = 'http://staging.nayashoppy.com/uploads/';
elseif (YII_ENV_DEV):
    $productImageBaseUrl = 'http://staging.nayashoppy.com/uploads/';
endif;

return [
    'parentDomain' => $productImageBaseUrl,
	'2xIconsPath' => 'appimages/category/2x/',
		'3xIconsPath' => 'appimages/category/3x/',
		'hdpiIconsPath' => 'appimages/category/hdpi/',
		'mdpiIconsPath' => 'appimages/category/mdpi/',
		'xxdpiIconsPath' => 'appimages/category/xxdpi/',
	'recordsPerPage' => 8
];

