<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use common\models\Products;
use common\models\Features;

class CatalogController extends Controller {

    public function actionIndex() {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	$request = \Yii::$app->request;
    	$categoryId = (int)$request->get('category_id');
    	$brandsId = (int)$request->get('brand_id');
    	$fetch = $request->get('fetch', null);
    	$sortArray = [];
    	if($fetch === 'popular'){
    		$sortArray[] = 'rating desc';
    	}elseif($fetch === 'newarrival'){
    		$sortArray[] = 'product_id desc';
    	}
    	
    	$data = Products::getProductsByCategoryForAPI($categoryId, $brandsId, $sortArray);
	    
	    $formatedData = Products::createFormatedArrayForAPI($data['data']);
	    if($formatedData['meta']['status'] === 200 )
	    	$formatedData['pagination'] = $data['pagination'];
	    
	    return $formatedData;
    }
    
    public function actionSimilarcatalog(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	$request = \Yii::$app->request;
    	$price = $request->get('price');
    	$categoryId = $request->get('category_id');
    	$similarProducts = Products::getSimilarProductsForAPI($price, $categoryId);
    	return Products::createFormatedArrayForAPI( $similarProducts );
    }
    
    public function actionDetail(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$request = \Yii::$app->request;
    	$productSlug = $request->get('slug');
    	
    	if(empty($productSlug) || !is_string($productSlug)){
    		$data = [];
    	}else{
    		$data = Products::getProductDetailsForAPI($productSlug);
    	}
    	
    	return Products::createFormatedArrayForAPI($data);
    }
    
    /*public function actionProductReviews(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$DealsOfTheDay =\common\models\Suppliers::getDealOfTheDayForAPI();
    	
    	return $DealsOfTheDay;
    }*/

    public function actionFeatureslistbycategory(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	$request = \Yii::$app->request;
    	$categoryId = (int) $request->get('category_id');
    	
    	$features = Features::getFeaturesListByCategory($categoryId);
    	
    	return Products::createFormatedArrayForAPI($features);
    }
    
    public function actionPopularproducts(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$request = \Yii::$app->request;
    	$categoryId = (int)$request->get('category_id');
    	$brandId = (int)$request->get('brand_id');
    	$sortArray = [];
    	$data = Products::getPopularProductsForAPI($categoryId, $brandId);
    	
    	$formatedData = Products::createFormatedArrayForAPI($data['data']);
    	if($formatedData['meta']['status'] === 200 )
    		$formatedData['pagination'] = $data['pagination'];
    	
    	return $formatedData;
    }
    
    public function actionProductindividualstarrating(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$request = \Yii::$app->request;
    	$productId = $request->get('product_id');
    	
    	return Products::createFormatedArrayForAPI(Products::getProductIndividualStarRating($productId));
    }
    
    public function actionAddproductreview(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$request = \Yii::$app->request;
    	
    	$model          = new \common\models\ProductReviews();
    	if(!$request->isPost){
    		return $request->post();
    	}
    	
    	$postData = $request->post();
    	$postData['created'] = date('Y-m-d H:i:s');
    	$model->attributes = $postData;
    	
    	$message = [];
    	$record = '';
    	if ($request->isPost && $model->save(true)) {
    		$message['message'] = 'Record inserted successfully...';
    		$message['record'] = $model->id;
    		
    	}else{
    		$message['message'] = $model->getErrors();
    		$message['isPost'] = $request->isPost;
    	}
    	
    	return Products::createFormatedArrayForAPI($message);
    	
    }
    
    public function actionProductreviews(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$request = \Yii::$app->request;
    	
    	$product_id = $request->get('product_id', null);
    	
    	return Products::getProductReviewsAndItsInfoForAPI($product_id);
    }

}
