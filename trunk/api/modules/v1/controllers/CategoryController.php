<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 04/12/17
 * Time: 1:06 PM
 */

namespace api\modules\v1\controllers;


use common\helpers\CorsCustom;
use common\models\Categories;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;

class CategoryController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => CorsCustom::className(),
        ];
        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actionTop()
    {
        try {
            return [
                "status" => true,
                "data" => Categories::getHomePageCategories(),
                "error" => null
            ];
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }
}