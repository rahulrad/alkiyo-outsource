<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 04/12/17
 * Time: 3:49 PM
 */

namespace api\modules\v1\controllers;


use backend\models\CouponsVendors;
use common\helpers\CorsCustom;
use common\models\Coupons;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use backend\models\CouponsSearch;

class CouponController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => CorsCustom::className(),
        ];
        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actionCouponsAndCashBacks($full = 0)
    {
        try {
            return [
                "status" => true,
                "data" => CouponsVendors::getCouponsAndCashBacks($full),
                "error" => null
            ];
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    public function actionTrendingDeals()
    {
        try {
            return [
                "status" => true,
                "data" => Coupons::getLastestCoupons(),
                "error" => null
            ];
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    public function actionAvailableCouponsVendors($id_vendor)
    {
        $searchModel = new CouponsSearch();
        $searchModel->moved = 0;
        $searchModel->type = 0;
        $paramsdata['id_merchant'] = $id_vendor;
        $paramsdata['expiry_date'] = date('m-d-y');

        return [
            "status" => true,
            "data" => $searchModel->search($paramsdata)->getModels(),
            "error" => null
        ];
    }

}