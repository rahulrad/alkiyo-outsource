<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use common\models\Sliders;
use common\models\Categories;
use common\models\Products;
use common\models\Brands;
use common\models\CouponCategories;
use common\models\Menus;
use dektrium\user\traits\EventTrait;

class DefaultController extends Controller {

	use EventTrait;
	
    public function actionIndex() {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$request = \Yii::$app->request;
    	$categoryId = $request->get('category_id', null);
    	$sliders = Sliders::getSliderForAPI($categoryId);
    	
    	return Products::createFormatedArrayForAPI($sliders);
    }
    
    public function actionTopmenu(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	return Menus::getTopeMenuArrayForAPI();
    }
    
    public function actionCashbackcoupons(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	return CouponCategories::getCouponsAndCashBackForAPI();
    }
    
    public function actionDealsoftheday(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$DealsOfTheDay =\common\models\Suppliers::getDealOfTheDayForAPI();
    	
    	return $DealsOfTheDay;
    }
    
    public function actionNewarrival(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	$request = \Yii::$app->request;
    	$limit = $request->get('limit');
    	if(empty($limit) || $limit > 50)
    		$limit = 6;
    	
    	return Products::createFormatedArrayForAPI( Products::getNewProductsForAPI( $limit ) );
    }
    
    public function actionSignin(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	
    	$message = [];
    	if (!\Yii::$app->user->isGuest) {
    		return $message['meta']['message'] = 'You\'re already logged in';
    	}
		
		if(!\Yii::$app->getRequest()->isPost){
			return ['meta' => ['status' => 404, 'message' => 'Invalid request method']];
		}
    	/** @var LoginForm $model */
    	$model = new \dektrium\user\models\LoginForm(new \dektrium\user\Finder, []);
    	$model = \Yii::createObject(\dektrium\user\models\LoginForm::className());
    	$event = $this->getFormEvent($model);
    	
    	$this->trigger('beforeLogin', $event);
    	
    	if ($model->load(\Yii::$app->getRequest()->post()) && $model->validate() && $model->login()) {
    		$this->trigger('afterLogin', $event);
    		
    		$message['message'] = 'Logged in successfully';
    	}else{
    		$message['status'] = 404;
    		$message['message'] = $model->getErrors();
    	}
    	
    	return $message;
    }

    public function actionPage(){
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	 
    	$request = \Yii::$app->request;
    	$slug = $request->get('slug');
    	$data = \backend\models\Pages::find()->where(['slug' => $slug])->andWhere(['status' => 'active'])->all();
    	if(!empty($data)){
    		$config = \HTMLPurifier_Config::createDefault();
    		$config->set('HTML.AllowedElements', []);
    		$purifyHtml = new \HTMLPurifier($config);
    		$data['descritption'] = trim($purifyHtml->purify($data['descritption']));
    	}
    	$data = Products::createFormatedArrayForAPI($data);
    	return $data;
    }
}
