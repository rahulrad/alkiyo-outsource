<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use common\modules\searchEs\components;
use common\components\Utils;


class SearchController extends Controller {

//    use EventTrait;
    
    public function actionProducts() {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $search = new \common\modules\searchEs\components\ElasticSearchProducts();
        
        $param = [];
        $param['term'] = Yii::$app->request->get('term', null); // string
        $param['category'] = Yii::$app->request->get('category', []); // numeric array
        $param['brand'] = Yii::$app->request->get('brand', []); // numeric array
        $param['attribute'] = Yii::$app->request->get('attribute', []); //numeric  array
        $param['filter_group_item'] = Yii::$app->request->get('featureIds', []); //numeric  array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', 20);
        
        // popular, new, price_high, price_low
        $param['sort'] = Yii::$app->request->get('sort', 'relevance');
        
        $param['price_min'] = Yii::$app->request->get('price_min', null);
        $param['price_max'] = Yii::$app->request->get('price_max', null);
        
        // return facets as well
        $param['facets'] = true;
        
        $result = $search->find($param);
        $featuresArray = [];
        $features = $result['facets']['filters'];
        $index = 0;
        
        foreach($features as $key => &$feature):
        foreach($feature as &$searchFilter):
        	$childIndex = 0;
        	$featuresArray[$index] = $searchFilter;
        	unset($featuresArray[$index]['values']);
        	foreach($searchFilter['values'] as $fkey => $feature_value):
        		$featuresArray[$index]['values'][$childIndex]['key'] = $feature_value['key'];
        		$featuresArray[$index]['values'][$childIndex]['count'] = $feature_value['count'];
        		$featuresArray[$index]['values'][$childIndex]['name'] = $feature_value['name'];
            	$childIndex++;
        	endforeach;
        endforeach;
        $index++;
        endforeach;
        
        unset($result['facets']['filters']);
        $result['facets']['filters'] = $featuresArray;
        
        return $result;
    }
    
}
