<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 17/01/18
 * Time: 12:13 PM
 */

namespace api\modules\v1\controllers;


use common\helpers\CorsCustom;
use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use dektrium\user\Mailer;
use dektrium\user\models\RecoveryForm;
use dektrium\user\models\Token;
use dektrium\user\models\User;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;

class UserController extends Controller
{


    /**
     * @var Mailer
     */
    protected $mailer;

    /** @var Finder */
    protected $finder;

    /**
     * @param string $id
     * @param \yii\base\Module $module
     * @param Finder $finder
     * @param array $config
     */
    public function __construct($id, $module, Mailer $mailer, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        $this->mailer = $mailer;
        parent::__construct($id, $module, $config);
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => CorsCustom::className(),
        ];
        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actionLogin($email, $password)
    {
        try {
            $user = $this->finder->findUserByUsernameOrEmail($email);
            if (!is_null($user) && Password::validate($password, $user->password_hash)) {

                $module = \Yii::$app->getModule('user');
                $confirmationRequired = $module->enableConfirmation
                    && !$module->enableUnconfirmedLogin;
                if ($confirmationRequired && !$user->getIsConfirmed()) {
                    return [
                        "status" => false,
                        "data" => null,
                        "error" => 'You need to confirm your email address'
                    ];
                }
                if ($user->getIsBlocked()) {
                    return [
                        "status" => false,
                        "data" => null,
                        "error" => 'Your account has been blocked'
                    ];
                }
                return [
                    "status" => true,
                    "data" => $user,
                    "error" => null
                ];
            } else {
                return [
                    "status" => false,
                    "data" => null,
                    "error" => "Invalid login or password"
                ];
            }
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    public function actionSocialLogin($email)
    {
        try {
            $user = $this->finder->findUserByUsernameOrEmail($email);
            if (!is_null($user) ) {

                $module = \Yii::$app->getModule('user');
                $confirmationRequired = $module->enableConfirmation
                    && !$module->enableUnconfirmedLogin;
                if ($confirmationRequired && !$user->getIsConfirmed()) {
                    return [
                        "status" => false,
                        "data" => null,
                        "error" => 'You need to confirm your email address'
                    ];
                }
                if ($user->getIsBlocked()) {
                    return [
                        "status" => false,
                        "data" => null,
                        "error" => 'Your account has been blocked'
                    ];
                }
                return [
                    "status" => true,
                    "data" => $user,
                    "error" => null
                ];
            } else {
                return [
                    "status" => false,
                    "data" => null,
                    "error" => "Invalid login or password"
                ];
            }
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }


    public function actionRegister($email, $username, $password = null)
    {
        try {
            $user = \Yii::createObject([
                'class' => User::className(),
                'scenario' => 'create',
                'email' => $email,
                'username' => $username,
                'password' => $password,
            ]);
            if ($user->create()) {
                return [
                    "status" => true,
                    "data" => "User Registered Successfully!",
                    "error" => null
                ];
            } else {
                $registerErrors = [];
                foreach ($user->errors as $errors) {
                    foreach ($errors as $error) {
                        $registerErrors[] = $error;
                    }
                }
                return [
                    "status" => false,
                    "data" => implode("<br>", $registerErrors),
                    "error" => "Please fix errors:"
                ];
            }
        } catch (\Exception $exception) {
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    public function actionRecover($email = null)
    {
        try {
            $model = \Yii::createObject([
                'class' => RecoveryForm::className(),
                'scenario' => RecoveryForm::SCENARIO_REQUEST,
            ]);
            $model->email = $email;
            if ($this->sendRecoveryMessage($model)) {
                return [
                    "status" => true,
                    "data" => "Email has been sent with instructions for resetting your password",
                    "error" => null
                ];
            } else {
                return [
                    "status" => false,
                    "data" => null,
                    "error" => "Invalid Email"
                ];
            }
        } catch (\Exception $exception) {
            error_log(var_dump($exception->getMessage(), true));
            error_log(var_dump($exception->getTraceAsString(), true));
            return [
                "status" => false,
                "data" => null,
                "error" => "Somthing went wrong, Please Try again later."
            ];
        }
    }

    private function sendRecoveryMessage($model)
    {
        if (!$model->validate()) {
            return false;
        }

        $user = $this->finder->findUserByEmail($model->email);

        if ($user instanceof User) {

            $token = \Yii::createObject([
                'class' => Token::className(),
                'user_id' => $user->id,
                'type' => Token::TYPE_RECOVERY,
            ]);

            if (!$token->save(false)) {
                return false;
            }

            if (!$this->mailer->sendRecoveryMessage($user, $token)) {
                return false;
            }
        }

        return true;
    }
}