<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$requestConfig = [
    'class' => 'common\components\Request',
    'web' => '/backend/web',
    'adminUrl' => '/admin'
];
//if (YII_ENV_PROD):
    unset($requestConfig['adminUrl']);
//endif;

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['chandresh'],
            'adminPermission' => 'admin',
            'controllerMap' => [
                'registration' => [
                    'class' => 'dektrium\user\controllers\RegistrationController',
                    'layout' => '@app/views/layouts/loginLayout',
                    'viewPath' => '@app/views/rbac',
                ],
                'security' => [
                    'class' => 'dektrium\user\controllers\SecurityController',
                    'layout' => '@app/views/layouts/loginLayout',
                    'viewPath' => '@app/views/rbac',
                ],
                'recovery' => [
                    'class' => 'dektrium\user\controllers\RecoveryController',
                    'layout' => '@app/views/layouts/loginLayout',
                    'viewPath' => '@app/views/rbac',
                ],
            ],

        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'extraColumns' => [
                        [
                            'attribute' => 'role',
                            'label' => 'Roles',
                            'format'=>'raw',
                            'value' => function ($model, $key, $index, $column) {
                                $manager = Yii::$app->getAuthManager();
                                $assigned = [];
                                foreach ($manager->getAssignments($model->id) as $item) {
                                    $assigned[$item->roleName] = \yii\helpers\Html::tag("span",ucwords($item->roleName),[
                                        "class"=>"label label-success"
                                    ]) ;
                                }
                                return implode(" ",$assigned);
                            },
                        ],
                    ],
                    'searchClass' => 'common\models\UserSearch'
                ],
            ],
        ],

    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@mdm/admin/views/assignment' => '@app/views/assignment'
                ],
            ],
        ],
//        'user' => [
//            'identityClass' => 'common\models\Admin',
//            'enableAutoLogin' => true,
//            'identityCookie' => [
//                'name' => '_backendUser', // unique for backend
//            ]
//        ],
        'session' => [
            'name' => 'PHPBACKSESSID',
            'savePath' => sys_get_temp_dir(),
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '5171829677',
            'csrfParam' => '_backendCSRF',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => $requestConfig,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<id:\d+>/<alias:[A-Za-z0-9 -_.]+>' => 'articles/categories/view',
                '<cat>/<id:\d+>/<alias:[A-Za-z0-9 -_.]+>' => 'articles/items/view',
                'coupon-out-<id>' => '/deals/store-out',
            ]
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'user/registration/*',
            'user/recovery/*',
//            'admin/*',
//            'some-controller/some-action',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],

    'params' => $params,
];

