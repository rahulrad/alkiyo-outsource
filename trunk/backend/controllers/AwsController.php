<?php

namespace backend\controllers;
use yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;
use backend\models\CategoryUrl;
use common\models\ProductsSuppliers;
use backend\models\Logs;
use backend\models\ProductUrls;
use common\models\Categories;
use fedemotta\awssdk\AwsSdk;
use Aws\Sqs\SqsClient;
use common\components\sqs;
use backend\models\BaseProductImporter;
use common\components\Aws;

class AwsController extends \yii\web\Controller
{
	
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
	public function actionIndex(){
		echo 'hello world'; die;
	}
	
	
    public function actionProductScraper()
    {
		
		$aws = \Yii::$app->awssdk->getAwsSdk();
        $sqs = $aws->createSqs();

        $queueUrl = \Yii::$app->awssdk->extra['queueUrl'];

        try {
			//{"id":726,"url":"http://www.flipkart.com?pid=MOBEJJ6KYARZGWJC","store_id":1}
			
			$result = $sqs->receiveMessage(array(
				'QueueUrl' => $queueUrl,
			));
			
			if(!empty($result)){
				$messageData = $result->getPath('Messages');
				if(!empty($messageData) && isset($messageData[0])){
					foreach ($result->getPath('Messages') as $messageBody) {
						// Do something with the message
						$data['MessageId'] = $messageBody['MessageId'];
						$data['ReceiptHandle'] = $messageBody['ReceiptHandle'];
						$data['MD5OfBody'] = $messageBody['MD5OfBody'];
						$data['Body'] = $messageBody['Body'];
						
						$body_data = json_decode($messageBody['Body']);
						$data['id'] = isset($body_data->id) ? $body_data->id : '';
						$data['url'] = isset($body_data->url) ? $body_data->url : '';
						$data['store_id'] = isset($body_data->store_id) ? $body_data->store_id : '';
						$data['type'] = isset($body_data->type) ? $body_data->type : '';
						
						
							if(!empty($data['type']) && ($data['type'] == 'product_scraper')){
								if(!empty($data['store_id']) && !empty($data['store_id'])){
									$this->getProductScraper($data);
								}
							}
					}
				}else{
					echo 'something wrong 1'; exit;
				}
				
					
				
			}else{
				echo 'something wrong 2'; exit;
			}
        } catch (\Exception $e) {
            echo "SQS Not added product scraper queue: ".$e->getMessage();
        }
			
    }
	
	

public function getProductScraper($data=0) {
		
		if(isset($data['store_id']) && !empty($data['store_id'])){
		
				$product_url = ProductUrls::find()->where(['id' => $data['id']])->one();
				
				if(!empty($product_url)){
							
							$categories_urls= Categories::find()->where(['category_id' => $product_url->category_id])->one();
							$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
							
							if($data['store_id'] == 1){
								$objProductScraper = new \backend\models\FlipKartProductScrapper();
								
							}elseif($data['store_id'] == 3){
								$objProductScraper = new \backend\models\SnapDealProductScrapper();
								
							}elseif($data['store_id'] == 5){
								$objProductScraper = new \backend\models\AmazonProductScrapper();
								
							}elseif($data['store_id'] == 6){
								$objProductScraper = new \backend\models\JabongProductScrapper();
								
							}elseif($data['store_id'] == 7){
								$objProductScraper = new \backend\models\EbayProductScrapper();
							}
							
							try{
								$objProductScraper->saveProductData($product_url,$category_name,$data);
							} catch (\Exception $e) {
								echo "SQS Error in scraper: ".$e->getMessage();
							}
							
				}
		}		
}


public function actionCreatePriceMessages(){
		$today = date('Y-m-d');
		$ProSuppliersModel = new ProductsSuppliers();
		$products = $ProSuppliersModel::find()->where(['<', 'created', $today])->select(['id'])->all();
		
		foreach($products as $product){
			$aws = new \common\components\Aws();
			$aws = $aws->priceScraperSendMessage($product,'price_scraper');
		}
		
	
}
public function actionPriceScraper($data=0){
	 	$aws = \Yii::$app->awssdk->getAwsSdk();
        $sqs = $aws->createSqs();

        $queueUrl = \Yii::$app->awssdk->extra['queuePriceUrl'];

        try {
			
			$result = $sqs->receiveMessage(array(
				'QueueUrl' => $queueUrl,
			));
			
			if(!empty($result)){
				foreach ($result->getPath('Messages') as $messageBody){
					// Do something with the message
					$data = array();
					$data['MessageId'] = $messageBody['MessageId'];
					$data['ReceiptHandle'] = $messageBody['ReceiptHandle'];
					$data['MD5OfBody'] = $messageBody['MD5OfBody'];
					$data['Body'] = $messageBody['Body'];
					
					$body_data = json_decode($messageBody['Body']);
					$data['id'] = isset($body_data->id) ? $body_data->id : '';
					$data['type'] = isset($body_data->type) ? $body_data->type : '';
						
						if(!empty($data['type']) && ($data['type'] == 'price_scraper')){
							$product = ProductsSuppliers::find()->select(['id','product_id','url','store_id','price','original_price'])->where(['id'=> $data['id']])->one();
								
							$objPriceProduct = new \backend\models\PriceProductScrapper();
							$result = $objPriceProduct->update_product_prices($product,$data);
						} 
				}
				
			}else{
				echo 'something wrong';
			}
        } catch (\Exception $e) {
            echo "SQS Not added: ".$e->getMessage();
        }
}



}
