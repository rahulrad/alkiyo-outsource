<?php
namespace backend\controllers;

use Yii;
use backend\models\Blogs;
use backend\models\BlogsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Logs;
use common\components\CImage;
use common\models\Products;
use common\models\Brands;
use common\models\Categories;
use common\models\ProductImage;

/**
 * BlogsController implements the CRUD actions for Blogs model.
 */
class BlogsController extends Controller
{
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			 'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','resize'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blogs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blogs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blogs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blogs();

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
			$post_data = Yii::$app->request->post();
			if(!empty($post_data['Blogs']['slug'])){
				
				$model->slug = $post_data['Blogs']['slug'];
			
			}
			
			// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				$model->file->saveAs($path.'uploads/blogs/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/blogs/'.$imageName.'.'.$model->file->extension;
			}
						
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
			
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Blogs','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			
			Yii::$app->session->setFlash('success', 'Blog is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blogs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
			
			$post_data = Yii::$app->request->post();
			if(!empty($post_data['Blogs']['slug'])){
				
				$model->slug = $post_data['Blogs']['slug'];
			
			}
			
			// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				$model->file->saveAs($path.'uploads/blogs/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/blogs/'.$imageName.'.'.$model->file->extension;
			}
			
			
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			//echo $id; die;
			$user_logs = array('model'=>'Blogs','action'=>'Update','activity' => $model->title.' is updated','action_id'=>$id);
			$logs_model->saveUserLogs($user_logs);
			
			 //  Email Send
                if($model)
                {

                $message = Yii::$app->mailer->compose();
                $message->setFrom('rei@domain.com')
                ->setTo('vinayverma158@gmail.com')
                ->setSubject('Test subject')
                ->setTextBody('testing with Plain text content')
                ->setHtmlBody('<b>HTML content</b>')
                ->send();
                //echo '<pre>';
                //print_r($message);
                //die ();
            }
			
			
			Yii::$app->session->setFlash('success', 'Blog is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Blogs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		//$model = $this->findModel($id);
			
       // $this->findModel($id)->delete();
			
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Blogs','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'Blogs is successfully delete');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blogs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blogs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blogs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	function actionResize()
	{
		  /** $sourceFile = 'http://img6a.flixcart.com/image/mobile/c/u/f/samsung-galaxy-s6-edge-sm-g928gzsains-1100x1100-imaeagcbgtsxkqxg.jpeg';
		   $destFile = Yii::getAlias('@frontend') .'/web/uploads/blogs/test2.jpg';
			$destWidth = 150;
			$destHeight = 150;
			$fileType = 'jpg';
			$objImageResize = new \common\components\CImage();
			$response = $objImageResize->imageResize($sourceFile,$destFile,$destWidth,$destHeight,$fileType); **/
			
			/**$productsListing = Products::find()->select(['product_id','image','categories_category_id','brands_brand_id'])->all();
			
			foreach($productsListing as $product){
							$categoryDetail = Categories::findOne(['category_id'=>  $product->categories_category_id]);
							$brandDetail = Brands::findOne(['brand_id'=>  $product->brands_brand_id]);
							
							$path = Yii::getAlias('@frontend') .'/web/';
							
							$logsModel = new Logs();
							$category_slug = $logsModel->getSlugFromName($categoryDetail->category_name);
							$brand_slug =  $logsModel->getSlugFromName($brandDetail->brand_name);
							echo $category_slug.'===>'.$brand_slug.'<br>';
							$folder_path = $path.'uploads/products/'.$category_slug.'/'.$brand_slug;
							
							$thumImageSizes = array('150x200','300x400');
								foreach($thumImageSizes as $size){
									
									if (!file_exists($folder_path)) {
										mkdir($folder_path, 0755, true);
									}
									
									$image  = $path.'uploads/products_img/'.$product->image;
									copy($image, $folder_path.'/'.$product->image);
									//echo 'image=>'.$image.'<br>';
									$thumImageSizes = array('150x200','300x400');
									foreach($thumImageSizes as $size){
									$sizeArr = explode('x',$size);
									
										if (!file_exists($folder_path.'/'.$size)) {
											mkdir($folder_path.'/'.$size, 0755, true);
										}
									$destFile = $folder_path.'/'.$size.'/'.$product->image;
									$mainImage = $image;
									
									$objImageResize = new \common\components\CImage();
									$response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
								}
								
								//die('pass');
							}
				//unlink($image); // correct			
			echo $product->product_id.'<br>';
			} **/
			
			/**
			$productsImagesListing = ProductImage::find()->select(['id','id_product','image','image_path'])->andWhere(['>','id',3842])->andWhere(['<=','id',6000])->all();
			
			foreach($productsImagesListing as $product){
							$productDetail = Products::find()->where(['product_id'=> $product->id_product])->one();
							$categoryDetail = Categories::findOne(['category_id'=>  $productDetail->categories_category_id]);
							$brandDetail = Brands::findOne(['brand_id'=>  $productDetail->brands_brand_id]);
							//echo '<pre>'; print_r($categoryDetail); die;
							$path = Yii::getAlias('@frontend') .'/web/';
							
							$logsModel = new Logs();
							$category_slug = $logsModel->getSlugFromName($categoryDetail->category_name);
							$brand_slug =  $logsModel->getSlugFromName($brandDetail->brand_name);
							echo $category_slug.'===>'.$brand_slug.'<br>';
							$folder_path = $path.'uploads/products/'.$category_slug.'/'.$brand_slug;
							
							$thumImageSizes = array('150x200','300x400');
								foreach($thumImageSizes as $size){
									
									if (!file_exists($folder_path)) {
										mkdir($folder_path, 0755, true);
									}
									
									$image  = $path.'uploads/products_img/'.$product->image;
									echo $image;
									if(file_exists($image)){
										copy($image, $folder_path.'/'.$product->image);
										//echo 'image=>'.$image.'<br>';
										$thumImageSizes = array('150x200','300x400');
										foreach($thumImageSizes as $size){
										$sizeArr = explode('x',$size);
										
											if (!file_exists($folder_path.'/'.$size)) {
												mkdir($folder_path.'/'.$size, 0755, true);
											}
										$destFile = $folder_path.'/'.$size.'/'.$product->image;
										$mainImage = $image;
										
										$objImageResize = new \common\components\CImage();
										$response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
									}
									
								}
								
								//die('pass');
							}
				//unlink($image); // correct			
			echo $product->id_product.'<br>';
			} **/
			
			
								/**$thumImageSizes = array('150x200','300x400');
								foreach($thumImageSizes as $size){
									$sizeArr = explode('x',$size);
									
									$destFile = $path.'uploads/products/'.$size.'/'.$image_name;
									$objImageResize = new \common\components\CImage();
									$response = $objImageResize->imageResize($image,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
								} ***/
			
			
	}
	
}
