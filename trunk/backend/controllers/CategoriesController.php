<?php

namespace backend\controllers;

use Yii;
use common\models\Categories;
use backend\models\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Logs;
use backend\models\CategoriesData;
use backend\models\CategoryUrl;


/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete','sort_order_update','importdata','export','download_blank_csv_format','folder_slug'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new CategoriesSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model'=>$model,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();
		
		
        if ($model->load(Yii::$app->request->post())   && $model->validate()) {
		
				
			$post_data = Yii::$app->request->post();
			
			$model->category_name = trim($post_data['Categories']['category_name']);
			
			if(!empty($post_data['Categories']['slug'])){
				
				$model->slug = $post_data['Categories']['slug'];
			
			}
			
			
			$logsModel = new Logs();
			$model->folder_slug = $logsModel->getSlugFromName($post_data['Categories']['category_name']);
							
			if(!isset($post_data['Categories']['show_home_page'])){
                $model->show_home_page = 0;
            }
			
			if(!isset($post_data['Categories']['compare'])){
                $model->compare = 0;
            }
			
			$model->sort_order = $this->getSortorder();
			
			// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				//$model->file->saveAs('uploads/categories/'.$imageName.'.'.$model->file->extension);
                $model->file->saveAs($path.'uploads/categories/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/categories/'.$imageName.'.'.$model->file->extension;
			}
			
			
			// save category image
			/**$model->file = UploadedFile::getInstance($model,'api_icon_file');
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				//$model->file->saveAs('uploads/categories/'.$imageName.'.'.$model->file->extension);
                $model->file->saveAs($path.'uploads/categories/'.$imageName.'.'.$model->file->extension);
				
				$model->api_icon = 'uploads/categories/'.$imageName.'.'.$model->file->extension;
			} **/
			//echo '<pre>'; print_r($model); die;		
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Categories','action'=>'Create','activity' => $model->category_name.' is added','action_id'=>$model->category_id);
			$logs_model->saveUserLogs($user_logs);
                        Yii::$app->session->setFlash('success', 'Category is successfully saved');
			
            //return $this->redirect(['index', 'id' => $model->category_id]);
			return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post())  && $model->validate()) {
			
			$post_data = Yii::$app->request->post();
			
			$oldCategoryName = Categories::find()->select(['category_name'])->where(['category_id'=>$model->category_id])->one();
			
			$model->category_name = trim($post_data['Categories']['category_name']);
			
			/**$logsModel = new Logs();
			$model->folder_slug = $logsModel->getSlugFromName($post_data['Categories']['category_name']);  **/
			
			if($oldCategoryName->category_name != $model->category_name){
				
				$categoriesDataModel = new CategoriesData();
				$categoriesDataModel->changeCategoryName($oldCategoryName->category_name,$model->category_name);
				
				$categoryUrlModel = new CategoryUrl();
				$categoryUrlModel->changeCategoryName($oldCategoryName->category_name,$model->category_name);
			}
			
			if(!empty($post_data['Categories']['slug'])){
				$model->slug = $post_data['Categories']['slug'];
			}
			 
			 if(!isset($post_data['Categories']['show_home_page'])){
                $model->show_home_page = 0;
            }
			
			 if(!isset($post_data['Categories']['compare'])){
                $model->compare = 0;
            }
            
            
            if(!isset($post_data['Categories']['is_index'])){
                $model->is_index = 0;
            }
			
			// save category image
			$model->file = UploadedFile::getInstance($model,'file');
			if(!empty($model->file)){
				$imageName = $model->file->baseName;
				
				//$path = str_replace('admin/','',Yii::$app->homeUrl);
				
				$path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();

				
				$model->file->saveAs($path.'uploads/categories/'.$imageName.'.'.$model->file->extension);
				
				$model->image = 'uploads/categories/'.$imageName.'.'.$model->file->extension;
			}
			
			
			// save category image
			/**$model->file = UploadedFile::getInstance($model,'api_icon_file');
			if(!empty($model->file)){
				//$imageName = $model->file->baseName;
                $path = Yii::getAlias('@frontend') .'/web/';
                $imageName = time().uniqid();
				
				//$model->file->saveAs('uploads/categories/'.$imageName.'.'.$model->file->extension);
                $model->file->saveAs($path.'uploads/categories/'.$imageName.'.'.$model->file->extension);
				
				$model->api_icon = 'uploads/categories/'.$imageName.'.'.$model->file->extension;
			} **/
			
			$model->created = date('Y-m-d h:i:s');
			$model->save();
			//echo '<pre>'; print_r($model); die;
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Categories','action'=>'Update','activity' => $model->category_name.' is updated','action_id'=>$model->category_id);
			$logs_model->saveUserLogs($user_logs);
                        
                         Yii::$app->session->setFlash('success', 'Category is successfully updated');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$this->deleteRelatedCategory($id);
                    
        Yii::$app->session->setFlash('success', 'Category is successfully delete');

        return $this->redirect(['index']);
    }
	
	public function deleteRelatedCategory($id){
			$category_ids = array();
			$category_ids[$id] = $id;
			$parentCatReleted = Categories::find()->select(['category_id'])->where(['parent_category_id' => $id])->all();
			
			if(!empty($parentCatReleted)){
				foreach($parentCatReleted as $parent_cat){
					$category_ids[$parent_cat->category_id] = $parent_cat->category_id;
					
					$subParentCatReleted = Categories::find()->select(['category_id'])->where(['parent_category_id' => $parent_cat->category_id])->all();
					
					foreach($subParentCatReleted as $sub_parent_cat){
						$category_ids[$sub_parent_cat->category_id] = $sub_parent_cat->category_id;
					}
					
				}
			}
			
			if(!empty($category_ids)){
				foreach($category_ids as $ids){
					$this->deleteCategory($ids);
				}
			}
	}
	public function deleteCategory($id){
			
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Categories','action'=>'Delete','activity' => $model->category_name.' is deleted','action_id'=>$model->category_id);
			$logs_model->saveUserLogs($user_logs);
	}

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
        public function actionSort_order_update(){
            
			$model = new Categories();
            
            $getFormData = Yii::$app->request->get();
            
                
            $catDetail = $this->findModel($getFormData['id']);
			
            
                if($getFormData['type'] == 'down'){
                    
                    $nextRecord = $this->getNext($catDetail->category_id,$catDetail->sort_order); // get next id record
                    
                    if(!empty($nextRecord)){
                    
                        $saveSortOrder = $this->saveSortOrder($catDetail, $nextRecord,$getFormData['type']);
                                                
                                                 Yii::$app->session->setFlash('success', 'Category is successfully Sort');
                        
                    }
                    
                    
                    
                }elseif($getFormData['type'] == 'up'){

                
                    $prevRecord = $this->getPrev($catDetail->category_id,$catDetail->sort_order); // get last record of this id
					//echo '<pre>'; print_r($prevRecord); die;
                    if(!empty($prevRecord)){
                       // die('UP');
                        
                        $saveSortOrder = $this->saveSortOrder($catDetail, $prevRecord,$getFormData['type']);
                                                
                                                Yii::$app->session->setFlash('success', 'Category is successfully Sort');
                        
                    }
                    
                    
                }
                
            
         return $this->redirect(['index']);  
    }


        public function getNext($cat_id, $sort_order) {
            $next = Categories::find()->where(['>', 'sort_order', $sort_order])->orderBy('sort_order asc')->one();
            return $next;
        }
        
        public function getPrev($cat_id, $sort_order) {
            $prev = Categories::find()->where(['<', 'sort_order', $sort_order])->orderBy('sort_order desc')->one();
            return $prev;
        }


        public function saveSortOrder($catDetail, $otherRecord, $type){
            
            if(!empty($catDetail) && !empty($otherRecord)){
                
                
                $othercatModel = $otherRecord;
                $othercatModel->sort_order = $catDetail->sort_order;  
                $othercatModel->save();
                    
                $catModel = $catDetail;
                if($type == 'up'){
                    $catModel->sort_order = $catDetail->sort_order - 1;
                }elseif($type == 'down'){
                    $catModel->sort_order = $catDetail->sort_order + 1;
                }
                if($catModel->sort_order > 0){
                    $catModel->save();
                }
                
              return true;
            }
            
            
    }
	
	
	
    /**
    * Import Category Data
    */

    public function actionImportdata()
    {   
        $model = new Categories();

        if($model->load(Yii::$app->request->post())){
       
        $file = UploadedFile::getInstance($model,'category_csv_file');
        
        $filename = 'Data-'.Date('YmdGis').'.'.$file->extension;
        
        $upload = $file->saveAs('uploads/category_import/'.$filename);
        
        define('CSV_PATH','uploads/category_import/'); 
        $csv_file = CSV_PATH . $filename;
            $row = 1;
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                    
                    if($row != 1){
                        
                        $categoryModel = new Categories();
                        
                        $category_name = isset($data[1]) ? $data[1] : '';
                        
                        $checkResult = $categoryModel::find()->where(['category_name' => $category_name])->one();

						$parent_category_id = $categoryModel::find()->select(['category_id'])->where(['category_name' => $data[2]])->one();
						
						if(!empty($parent_category_id)){
							$parent_id = $parent_category_id->category_id;
						}else{
							$parent_id = NULL;
						}

                        $countRecords = $categoryModel::find()->orderBy('category_id desc')->one();
                        if(!empty($countRecords)){
                            $c_id = $countRecords->category_id + 1;
                        }else{
                            $c_id = 1;
                        }
                        
						
                        if(empty($checkResult)){
                            $categoryModel->category_id = $c_id;
                            //$categoryModel->promo_id = isset($data[0])?$data[0]:'';
                            $categoryModel->category_name = isset($data[1])?$data[1]:'';
							$slug = trim(str_replace(array(' ','(',')'),array('-','-','-'),$data[1]));
							$categoryModel->slug = strtolower($slug);
							$categoryModel->parent_category_id = $parent_id;
                            $categoryModel->category_description = isset($data[3])?$data[3]:'';
                            $categoryModel->show_home_page = isset($data[4])?$data[4]:'';
							$categoryModel->sort_order = $this->getSortorder();
							$categoryModel->created = date('Y-m-d h:i:s');
							$logsModel = new Logs();
							$categoryModel->folder_slug = $logsModel->getSlugFromName($data[1]);
                            $categoryModel->save(false);
                        }
                        
                    }
                    $row++;
                    
                    }
                fclose($handle);
            }
            //  unlink('uploads/category_import/'.$filename);
            Yii::$app->session->setFlash('success','Import Success');
            return $this->redirect(['index']);
        }else{
            return $this->render('importdata',['model'=>$model]);
        }

    }


    /**
    * Export Category Data
    */

    public function actionExport(){
            
            $categoryResults = Categories::find()->All();
            
            
           $filename = 'Data-'.Date('YmdGis').'-Category.csv';
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache");
            header("Expires: 0");


             $file = fopen('php://output', 'w');                              
             fputcsv($file, array('ID', 'Category Name', 'Parent Category', 'Category Description','Show Home Page')); 
			
				 $categoryModel = new Categories();
                foreach($categoryResults as $category){
					
					$parent_category_name = $categoryModel::find()->select(['category_name'])->where(['category_id' => $category->parent_category_id])->one();
					
					if(!empty($parent_category_name)){
						$parent_category = $parent_category_name->category_name;
					}else{
						$parent_category = '';
					}
					
                    $record = array($category->category_id,$category->category_name,$parent_category,$category->category_description,$category->show_home_page);
                    
					fputcsv($file, $record);
                        
                }

        }


    /**
    * Export Blank Category Data csv file
    */

    public function actionDownload_blank_csv_format(){
            
            
            $filename = 'Data-'.Date('YmdGis').'-CategoryBlank.csv';
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache");
            header("Expires: 0");

             $file = fopen('php://output', 'w');                              
             fputcsv($file, array('S.No', 'Category Name', 'Parent Category', 'Category Description','Show Home Page')); 

        }
		
	// vinay 15 july
	 public function getSortorder() {

        $last_sort_order = Categories::find()->select(['sort_order'])->orderBy('sort_order desc')->one();

        if (!empty($last_sort_order)) {
            $next_sort_order = $last_sort_order->sort_order + 1;
        } else {
            $next_sort_order = 1;
        }

        return $next_sort_order;
    }
	
	
	public function actionFolder_slug(){
			$categoriesList = Categories::find()->all();
			foreach($categoriesList as $category){
				$categoryModel = $category;
				$logsModel = new Logs();
				$categoryModel->folder_slug = $logsModel->getSlugFromName($category->category_name);
				$categoryModel->save(false);
			}
	}
	
	
}
