<?php

namespace backend\controllers;

use Yii;
use backend\models\Contacts;
use backend\models\ContactsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;

/**
 * ContactsController implements the CRUD actions for Contacts model.
 */
class ContactsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contacts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contacts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contacts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contacts();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created = date('Y-m-d h:i:s');
            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();
            $user_logs = array('model'=>'Contacts','action'=>'Create','activity' => $model->email.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);
                        
            Yii::$app->session->setFlash('success', 'Contacts is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contacts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();
            
            //echo $id; die;
            $user_logs = array('model'=>'Contacts','action'=>'Update','activity' => $model->email.' is updated','action_id'=>$id);
            $logs_model->saveUserLogs($user_logs);
            
            Yii::$app->session->setFlash('success', 'Contact is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contacts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        	
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Conatct','action'=>'Delete','activity' => $model->name.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'Conatct is successfully delete');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contacts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contacts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
