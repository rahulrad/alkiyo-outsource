<?php
namespace backend\controllers;

use Yii;


use common\models\Coupons;
use backend\models\CouponsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;
use backend\models\DealUrls;
use yii\web\UploadedFile;

/**
 * DealsController implements the CRUD actions for Deals model.
 */
class DealsController extends Controller
{
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


 public function actionOnline()
    {
        $searchModel = new CouponsSearch();
		$searchModel->moved = 1;
		$searchModel->type = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('//coupons//index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



	 public function actionStage()
    {
        $searchModel = new CouponsSearch();
		$searchModel->moved = 0;
		$searchModel->type = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('//coupons/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



	 /**
     * Creates a new Coupons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Coupons();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
				
						
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			
			Yii::$app->session->setFlash('success', 'Coupons is successfully saved');
            return $this->redirect(['index']);
			
        } else {
            return $this->render('//coupons/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Coupons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {	
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons','action'=>'Update','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
			
			Yii::$app->session->setFlash('success', 'Coupons is successfully saved');
			if($model->moved){
            return $this->redirect(['online']);
			}else{
				     return $this->redirect(['stage']);
			}
        } else {
            return $this->render('//coupons/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Coupons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		
		$this->findModel($id)->delete();
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Coupons','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'Blogs is successfully delete');
		if($model->moved){
            return $this->redirect(['online']);
			}else{
				     return $this->redirect(['stage']);
			}
        
    }

  

    /**
     * Displays a single Deals model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    // /**
    //  * Creates a new Deals model.
    //  * If creation is successful, the browser will be redirected to the 'view' page.
    //  * @return mixed
    //  */
    // public function actionCreate()
    // {
    //     $model = new Deals();

    //     if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
	// 		// save main image
	// 		$model->main_image = UploadedFile::getInstance($model,'main_image');
	// 		if(!empty($model->main_image)){
	// 			$imageName = time().uniqid();
				
	// 			$path = Yii::getAlias('@frontend') .'/web/';
				
	// 			$model->main_image->saveAs($path.'uploads/deals/'.$imageName.'.'.$model->main_image->extension);
				
				
	// 			$model->image = $imageName.'.'.$model->main_image->extension;
				
	// 		}
						
	// 		$model->created = date('Y-m-d h:i:s');
			
	// 		$model->save();
			
	// 		// save user logs data in user_logs table
	// 		$logs_model = new Logs();
			
	// 		$user_logs = array('model'=>'Deals','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
	// 		$logs_model->saveUserLogs($user_logs);
			
	// 		Yii::$app->session->setFlash('success', 'Deals is successfully saved');
    //         return $this->redirect(['index']);
			
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    // /**
    //  * Updates an existing Deals model.
    //  * If update is successful, the browser will be redirected to the 'view' page.
    //  * @param integer $id
    //  * @return mixed
    //  */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            	
	// 		// save main image
	// 		$model->main_image = UploadedFile::getInstance($model,'main_image');
			
	// 		if(!empty($model->main_image)){
				
	// 			$imageName = time().uniqid();
				
	// 			$path = Yii::getAlias('@frontend') .'/web/';
				
	// 			$model->main_image->saveAs($path.'uploads/deals/'.$imageName.'.'.$model->main_image->extension);
				
				
	// 			$model->image = $imageName.'.'.$model->main_image->extension;
				
	// 		}
			
	// 		$model->created = date('Y-m-d h:i:s');
			
	// 		$model->save();
			
	// 		// save user logs data in user_logs table
	// 		$logs_model = new Logs();
			
	// 		$user_logs = array('model'=>'Deals','action'=>'Update','activity' => $model->title.' is added','action_id'=>$model->id);
	// 		$logs_model->saveUserLogs($user_logs);
			
	// 		Yii::$app->session->setFlash('success', 'Deals is successfully saved');
    //         return $this->redirect(['index']);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    // /**
    //  * Deletes an existing Deals model.
    //  * If deletion is successful, the browser will be redirected to the 'index' page.
    //  * @param integer $id
    //  * @return mixed
    //  */
    // public function actionDelete($id)
    // {
    //     $model = $this->findModel($id);
		
	// 	$this->findModel($id)->delete();
		
	// 	// save user logs data in user_logs table
	// 		$logs_model = new Logs();
			
	// 		$user_logs = array('model'=>'Deals','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
	// 		$logs_model->saveUserLogs($user_logs);	
                        
    //         Yii::$app->session->setFlash('success', 'Deals is successfully delete');

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Deals model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deals the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coupons::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



	  public function actionMove()
    {
       $selection_data = (array)Yii::$app->request->post('selection');//typecasting

        if (!empty($selection_data)) {
            //echo '<pre>'; print_r($selection_data); die;
            foreach ($selection_data as $id) {

                $Coupons = new Deals();
                $couponsData = $Coupons::find()->where(['id' => $id])->one();

                if (!empty($couponsData)) {
						if($couponsData->moved){
							$couponsData->moved=0;
						}else{
							$couponsData->moved=1;
						}
                   $couponsData->save();

                }

            }
            Yii::$app->session->setFlash('success', 'Deals Moved Successfully');

            return $this->redirect(Yii::$app->request->referrer);

        }
		return $this->redirect(Yii::$app->request->referrer);
        
    }
	
	
    /**
    * Import Product Data
    */
	
	public function actionImportdata()
    {	
	

    	$model = new Deals();

    	if($model->load(Yii::$app->request->post())){
       
        $file = UploadedFile::getInstance($model,'file');
		
        $filename = 'Data-'.Date('YmdGis').'.'.$file->extension;
		
        $upload = $file->saveAs('uploads/deals_import/'.$filename);
		
		define('CSV_PATH','uploads/deals_import/'); 
		$csv_file = CSV_PATH . $filename;
			$row = 1;
			if (($handle = fopen($csv_file, "r")) !== FALSE) {
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					
					if($row != 1){
						
						$dealsModel = new Deals();
						
						$offer_id = isset($data[1]) ? $data[1] : 0;
						
						$checkResult = $dealsModel::find()->where(['offer_id' => $offer_id])->one();
						$countRecords = $dealsModel::find()->orderBy('id desc')->one();
						if(!empty($countRecords)){
							$c_id = $countRecords->id + 1;
						}else{
							$c_id = 1;
						}
						
						if(empty($checkResult)){
							
							$dealsModel->title = isset($data[0])?$data[0]:'';
							$dealsModel->product_id = isset($data[1])?$data[1]:'';
							$dealsModel->discount = isset($data[2])?$data[2]:'';
							$dealsModel->start_date = isset($data[3])?$data[3]:'';
							$dealsModel->end_date = isset($data[4])?$data[4]:'';
							$dealsModel->save(false);
						}
						
					}
					$row++;
					
					}
				fclose($handle);
			}
			//  unlink('uploads/deals_import/'.$filename);
			Yii::$app->session->setFlash('success','Import Success');
			return $this->redirect(['index']);
	    }else{
	        return $this->render('importdata',['model'=>$model]);
	    }
		
	}	
		
		 /**
	* Export Products Data
    */

    public function actionExport(){
			
			$dealsResults = Deals::find()->All();
			
			
			$filename = 'Data-'.Date('YmdGis').'-Deals.csv';
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$filename);
			header("Pragma: no-cache");
			header("Expires: 0");


			 $file = fopen('php://output', 'w');                              
			 fputcsv($file, array('id', 'title', 'product_id', 'discount', 'start_date', 'end_date', 'status', 'created')); 


				foreach($dealsResults as $deals){

					$record = array($deals->id,$deals->title,$deals->product_id,$deals->discount,$deals->start_date,$deals->end_date,$deals->status,$deals->created);
					fputcsv($file, $record);
						
				}

		}
		
		
	  /**
	* Export Products Data
    */

    public function actionDownload_deals_csv_format(){
			
			
			$filename = 'Data-'.Date('YmdGis').'-Deals.csv';
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$filename);
			header("Pragma: no-cache");
			header("Expires: 0");

			 $file = fopen('php://output', 'w');                              
			 fputcsv($file, array( 'id', 'title', 'product_id', 'discount', 'start_date', 'end_date', 'status', 'created')); 

		}
	
	
	// funcation for save deals urls form api
	public function actionScraper(){


			// delete all deals before save new deals
			Deals::deleteAll();
			
			$path = Yii::getAlias('@frontend') .'/web/';
			array_map('unlink', glob($path."uploads/deals/*"));
			
			
			$dealsModel = new Deals();
			
			$dealsModel->getFlipkartDealUrls('nayashopp','2b64692f37a5493c97d0003b4f4a9543');
			
			$dealsModel->getSnapdealDealUrls('46884','4f47e6cc3ea375329136a5b3d3f125');
			
			$dealsModel->getShopcluesDealUrls();
			
			$dealsModel->getEbayDealUrls();
			$dealsModel->getAmazonDealUrls();
			echo 'Saved Deals..'; die;
			
	}


	
//Set up the operation in the request
function ItemSearch($SearchIndex, $Keywords){

			
			
			//Enter your IDs
/**define("Access_Key_ID", "[Your Access Key ID]");
define("Associate_tag", "[Your Associate Tag ID]");

$Access_Key_ID = 'AKIAI32XY7KT5TQRGNEQ';
$Associate_tag = 'nayashoppy-21';

$this->ItemSearch('All','deals of the day'); **/
/**
echo $SearchIndex.'=====>'.$Keywords; die;
//Set the values for some of the parameters
$Operation = "ItemSearch";
$Version = "2013-08-01";
$ResponseGroup = "ItemAttributes,Offers";
//User interface provides values
//for $SearchIndex and $Keywords

//Define the request
$request=
     "http://webservices.amazon.com/onca/xml"
   . "?Service=AWSECommerceService"
   . "&AssociateTag=" . Associate_tag
   . "&AWSAccessKeyId=" . Access_Key_ID
   . "&Operation=" . $Operation
   . "&Version=" . $Version
   . "&SearchIndex=" . $SearchIndex
   . "&Keywords=" . $Keywords
  // . "&Signature=" . [Request Signature]
   . "&ResponseGroup=" . $ResponseGroup;

//Catch the response in the $response object
$response = file_get_contents($request);
$parsed_xml = simplexml_load_string($response);
printSearchResults($parsed_xml, $SearchIndex); **/
}
	
		
		
}