<?php

namespace backend\controllers;

use Yii;
use common\models\EmailTemplates;
use backend\models\EmailTemplatesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;

/**
 * EmailtemplatesController implements the CRUD actions for EmailTemplates model.
 */
class EmailtemplatesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailTemplatesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmailTemplates model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmailTemplates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailTemplates();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			if(!empty($post_data['EmailTemplates']['slug'])){
				
				$model->slug = $post_data['EmailTemplates']['slug'];
			
			}
			
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'EmailTemplates','action'=>'Create','activity' => $model->title.' is added','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'EmailTemplates is successfully saved');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmailTemplates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())    && $model->validate()) {
		
			if(!empty($post_data['EmailTemplates']['slug'])){
				
				$model->slug = $post_data['EmailTemplates']['slug'];
			
			}
			
			$model->save();
			
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'EmailTemplates','action'=>'Update','activity' => $model->title.' is updated','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);
                        
                        Yii::$app->session->setFlash('success', 'EmailTemplates is successfully updated');
			
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EmailTemplates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'EmailTemplates','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);

         Yii::$app->session->setFlash('success', 'EmailTemplates is successfully delete');
        return $this->redirect(['index']);
    }

    /**
     * Finds the EmailTemplates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailTemplates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailTemplates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
