<?php

namespace backend\controllers;

use common\models\Categories;
use Yii;
use common\models\FeatureGroups;
use backend\models\FeatureGroupsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;
use yii\web\UploadedFile;

/**
 * FeatureGroupsController implements the CRUD actions for FeatureGroups model.
 */
class FeaturegroupsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'import-data', 'download-sample-csv'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FeatureGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeatureGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FeatureGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FeatureGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FeatureGroups();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created = date('Y-m-d h:i:s');
            $model->modified = date('Y-m-d h:i:s');
            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();

            $user_logs = array('model' => 'FeatureGroups', 'action' => 'Create', 'activity' => $model->name . ' is added', 'action_id' => $model->id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'FeatureGroups is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FeatureGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->modified = date('Y-m-d h:i:s');
            $model->save();

            // save user logs data in user_logs table
            $logs_model = new Logs();

            $user_logs = array('model' => 'FeatureGroups', 'action' => 'Update', 'activity' => $model->name . ' is updated', 'action_id' => $model->id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'FeatureGroups is successfully update');

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FeatureGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        die('not access');
        $model = $this->findModel($id);
        $this->findModel($id)->delete();

        // save user logs data in user_logs table
        $logs_model = new Logs();

        $user_logs = array('model' => 'FeatureGroups', 'action' => 'Delete', 'activity' => $model->name . ' is deleted', 'action_id' => $model->id);
        $logs_model->saveUserLogs($user_logs);

        Yii::$app->session->setFlash('success', 'FeatureGroups is successfully delete');

        return $this->redirect(['index']);
    }

    /**
     * Finds the FeatureGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FeatureGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FeatureGroups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownloadSampleCsv()
    {
        $filename = 'Data-' . Date('YmdGis') . '-ProductGroups.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        $file = fopen('php://output', 'w');
        fputcsv($file, array('category', 'group_name'));
    }

    public function actionImportData()
    {

        $model = new FeatureGroups();
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'uploadFile');
            if (!is_null($file)) {
                $filename = 'Data-' . Date('YmdGis') . '.' . $file->extension;
                define('CSV_PATH', 'uploads/category_import/');
                $upload = $file->saveAs(CSV_PATH . $filename);
                $csv_file = CSV_PATH . $filename;
                $row = 1;
                if (($handle = fopen($csv_file, "r")) !== FALSE) {

                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                        if ($row != 1 && (isset($data[0]) && !empty($data[0])) && (isset($data[1]) && !empty($data[1]))) {

                            $categoryModel = Categories::find()->where([
                                'category_name' => trim(strtolower($data[0]))
                            ])->one();
                            if (!is_null($categoryModel)) {

                                $groupName = trim(strtolower($data[1]));
                                $model = FeatureGroups::find()->where([
                                    'name' => $groupName,
                                    'category_id' => $categoryModel->category_id
                                ])->one();

                                $max_sort_order = FeatureGroups::find()->orderBy('sort_order DESC')->one();

                                if (is_null($model)) {
                                    $model = new FeatureGroups();
                                    $model->name = ucwords($groupName);
                                    $model->category_id = $categoryModel->category_id;
                                    $model->sort_order = !is_null($max_sort_order) ? $max_sort_order->sort_order + 1 : 1;
                                    $model->created = date('Y-m-d h:i:s');
                                }
                                $model->modified = date('Y-m-d h:i:s');
                                $model->save();
                            }
                        }
                        $row++;
                    }
                    fclose($handle);
                }
                unlink(CSV_PATH . $filename);
                Yii::$app->session->setFlash('success', 'Mappings successfully imprted!');
                return $this->redirect(['index']);
            } else {
                return $this->render('importdata', ['model' => $model]);
            }
        } else {
            return $this->render('importdata', ['model' => $model]);
        }
    }


}
