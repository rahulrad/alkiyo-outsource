<?php

namespace backend\controllers;

use Yii;
use backend\models\FilterGroups;
use common\models\Features;
use backend\models\FilterGroupsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Logs;

/**
 * FilterGroupsController implements the CRUD actions for FilterGroups model.
 */
class FilterGroupsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FilterGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FilterGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new FilterGroupsSearch();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $model,
        ]);
    }

    /**
     * Displays a single FilterGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FilterGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FilterGroups();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			$postData = Yii::$app->request->post();
			
			$model->sort_order = $this->getSortorder($postData['FilterGroups']['category_id']);
			
			$model->created = date('Y-m-d h:i:s');
			
			$model->save();
			
				$logs_model = new Logs();
				$user_logs = array('model'=>'FilterGroup','action'=>'Create ','activity' => $model->title.' is added','action_id'=>$model->id);
				$logs_model->saveUserLogs($user_logs);
			
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FilterGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
				$logs_model = new Logs();
				$user_logs = array('model'=>'FilterGroup','action'=>'Update ','activity' => $model->title.' is updated','action_id'=>$model->id);
				$logs_model->saveUserLogs($user_logs);
				
				
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FilterGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$model = $this->findModel($id);
			
       // $this->findModel($id)->delete();
			
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		

            $name=$model->title;
            $cat=$model->category_id;
            $FeaturesModel = Features::find()->where(['name' => $name,'category_id'=>$cat, 'is_delete'=>0])->one();
            if(isset($FeaturesModel)){
                    $FeaturesModel->is_filter=0;
                    $FeaturesModel->save();

            };

	
			// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'FilterGroup','action'=>'Delete','activity' => $model->title.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);	
                        
            Yii::$app->session->setFlash('success', 'FilterGroup is successfully delete');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the FilterGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FilterGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FilterGroups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionChange_status(){
		$model = new FilterGroups();
		 $getData = Yii::$app->request->get();
		 
			if(!empty($getData) && isset($getData['filter_group_id'])){
				
				$filterGroupsModel = FilterGroups::find()->where(['id' => $getData['filter_group_id']])->one();
				
				if($getData['type'] == 1){
					$status = 1;
				}else{
					$status = 0;
				}
				
				$filterGroupsModel->status = $status;
				$filterGroupsModel->save(false);
				
				$logs_model = new Logs();
				
				$user_logs = array('model'=>'FilterGroup','action'=>'Change FilterGroup Status','activity' => $filterGroupsModel->title.' is change status','action_id'=>$filterGroupsModel->id);
				$logs_model->saveUserLogs($user_logs);
				
				Yii::$app->session->setFlash('success', 'Filter Group Status changed successfully saved');
			
				return $this->redirect(['index']);
				
			}
	}
	
	public function actionSave_sort_order(){
			
			if(isset($_POST['sort_order']) && !empty($_POST['sort_order'])){
					
					$this->save_order($_POST['sort_order']);
					
					Yii::$app->session->setFlash('success', 'Filter Group Sort Order successfully saved');
				
					return $this->redirect(Yii::$app->request->referrer);
				
			}
			
	}
	
	public function save_order($sortData){
			
			if(!empty($sortData)){
				
				foreach($sortData as $key => $data){
					
						$filterGroupsModel = FilterGroups::findOne($key);
						$filterGroupsModel->sort_order = $data;
						$filterGroupsModel->save(false);
						
						$logs_model = new Logs();
						$user_logs = array('model'=>'FilterGroup','action'=>'Change Save_sort_order','activity' => $filterGroupsModel->title.' is change status','action_id'=>$filterGroupsModel->id);
						$logs_model->saveUserLogs($user_logs);
					
				}
				
			}
		
	}
	
	public function getSortorder($category_id){
		
			if(!empty($category_id)){
				
				$result = FilterGroups::find()->where(['category_id' => $category_id])->orderBy('sort_order desc')->one();
				
				if (!empty($result)) {
					$sort_order = $result->sort_order + 1;
				} else {
					$sort_order = 1;
				}
			}else{
				$sort_order = 1;
			}
			
			return $sort_order;
	}
	
}
