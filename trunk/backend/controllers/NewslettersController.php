<?php

namespace backend\controllers;

use Yii;
use common\models\Newsletters;
use backend\models\NewslettersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Logs;

/**
 * NewslettersController implements the CRUD actions for Newsletters model.
 */
class NewslettersController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Newsletters models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewslettersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Newsletters model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Newsletters model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Newsletters();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created = date('Y-m-d h:i:s');

            $model->save();

            // save user logs data in user_logs table

            $logs_model = new Logs();
            $user_logs = array('model'=>'Newsletters','action'=>'Create','activity' => $model->email.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Newsletter is successfully saved');
            return $this->redirect(['index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Newsletters model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            // save user logs data in user_logs table

            $logs_model = new Logs();
            $user_logs = array('model'=>'Newsletters','action'=>'Create','activity' => $model->email.' is added','action_id'=>$model->id);
            $logs_model->saveUserLogs($user_logs);

            Yii::$app->session->setFlash('success', 'Newsletter is successfully saved');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Newsletters model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
			$model = $this->findModel($id);
			$modelDelete = $model;
			$modelDelete->is_delete = 1;
			$modelDelete->save(false);
		
		// save user logs data in user_logs table
			$logs_model = new Logs();
			
			$user_logs = array('model'=>'Newsletter','action'=>'Delete','activity' => $model->email.' is deleted','action_id'=>$model->id);
			$logs_model->saveUserLogs($user_logs);

         Yii::$app->session->setFlash('success', 'Newsletter is successfully delete');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Newsletters model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Newsletters the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Newsletters::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
