<?php
namespace backend\controllers;

use Yii;
use common\models\Admin;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\AdminLoginForm;
use yii\filters\VerbFilter;
use backend\models\Users;
use common\models\Products;
use common\models\Suppliers;
use common\models\EmailTemplates;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','forget-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
		/*use backend\models\Users;
		use backend\models\Products;
		use backend\models\Suppilers;*/
		$data = array();
		
		$UsersModel = new Admin();
		$users = $UsersModel::find()->all();
		$data['users'] = count($users);
		
		$ProductsModel = new Products();
		$products = $ProductsModel::find()->where(['is_delete'=>0])->all();
		$data['products'] = count($products);
		
		$SuppliersModel = new Suppliers();
		$suppliers = $SuppliersModel::find()->all();
		$data['suppliers'] = count($suppliers);
		
		
		
        return $this->render('index',['data'=>$data]);
    }

    public function actionLogin()
    {
	
		$this->layout = 'loginLayout';
		
		
		if (!\Yii::$app->user->isGuest){
			return $this->goHome();
		}
		
		
		
		$model = new AdminLoginForm();
		
		if ($model->load(Yii::$app->request->post()) && $model->login()){
			
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionForgetPassword()
    {
	
		$this->layout = 'loginLayout';
		
		
		if (!\Yii::$app->user->isGuest){
			return $this->goHome();
		}
		
		
	
		$model = new Admin();
		
		if ($model->load(Yii::$app->request->post())){
			$postData = Yii::$app->request->post();
			// we are checking user exit or not
			$exitUser  = Admin::find()->where(['email' => $postData['Admin']['email'],'username' => $postData['Admin']['username']])->one();
			if(!empty($exitUser)){
				
				$AdminModel = $exitUser;
				$password = uniqid();
				$AdminModel->last_name = $password;
				$AdminModel->password_hash = Yii::$app->security->generatePasswordHash($password);
				//$model->setPassword($postData['Admin']['password_hash']);
				$AdminModel->save(false);
				
				
				// getting email format for price_alert email
				$emailTemplatesModel = new EmailTemplates();
				$emailTemplatesDetail = $emailTemplatesModel->getEmailTemplateBySlug('forget-password');
				
				// we are getting email actual content
				$emailData = array('USERNAME' => $exitUser->username,'EMAIL' =>$exitUser->email,'PASSWORD'=> $password);
				$message = $emailTemplatesModel->getUserEmailMessage($emailTemplatesDetail->content, $emailData);
				
				$Email =Yii::$app->mailer->compose()
									->setFrom('vinayverma158@gmail.com')
									->setTo($exitUser->email)
									->setSubject($emailTemplatesDetail->subject)
									//->setTextBody('Plain text content')
									->setHtmlBody($message)
									->send();
									
				//echo '<prE>'; print_r($Email); die;
				Yii::$app->session->setFlash('success', 'We sent you an email for your new password: '.$password);
				return $this->redirect(['/site/login']);
				
			}else{
				Yii::$app->session->setFlash('error', 'Invalid check user name and email');
				return $this->redirect(['/site/forget-password']);
			}
		} else {
			return $this->render('forget_password', [
				'model' => $model,
			]);
		}
	}
	
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
