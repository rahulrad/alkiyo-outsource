<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class AmazonProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName = "")
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);

        $retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url);
        $this->arr = $retrnarr;
//        $html->clear();

        unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {
        $objHttpService = new \common\components\HttpService();
        // $response = $objHttpService->getResponse($strurl);

        $response = $objHttpService->getAmazonProductDetail($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url)
    {
        $scraperLog = new Logs();
//        echo $url;
        $siteName = 'http://www.amazon.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
        $colors = array();
        $sizes = array();
        $offers = array();
        $instock = true;

        $product_unique_id = '';
        $productDeliverd = '';
        $productReturnPolicy = '';
        $productFeatures = '';
        $productCOD = '';


        $arr_merchant = '';
        // product title
        if ($html->find('#productTitle', 0)) {
            $productNamehtml = $html->find('#productTitle', 0)->plaintext;
            $productNamehtml = trim($productNamehtml);
        } else {
            $scraperLog->saveScraperLog('amazon scraper', 'AmazonProductDetailScrapper', '.pdp-e-i-head class not found', 'Not Found', __LINE__);
        }


        if ($html->find('#priceblock_saleprice', 0)) {
            error_log("fetcing from sale price");
            $productPrice = $html->find('#priceblock_saleprice', 0)->plaintext;
            $productPrice = $this->convet_price_string($productPrice);
        }

        // product price
        if (empty($productPrice) && $html->find('#priceblock_ourprice', 0)) {
            error_log("fetcing from out price");
            $productPrice = $html->find('#priceblock_ourprice', 0)->plaintext;
            $productPrice = $this->convet_price_string($productPrice);
        } else {
            $scraperLog->saveScraperLog('amazon scraper', 'AmazonProductDetailScrapper', '.payBlkBig class not found', 'Not Found', __LINE__);
        }

        // product main image
        if ($html->find('#imgTagWrapperId', 0)) {
            if ($html->find('#landingImage', 0)) {
                $productImgSrchtml = $html->find('#landingImage', 0)->getAttribute('data-old-hires');
            }
        }

        if (empty($productImgSrchtml) && $html->find('#imgTagWrapperId', 0)) {
            if ($html->find('#landingImage', 0)) {
                $productImgSrchtml = $html->find('#landingImage', 0)->getAttribute('data-a-dynamic-image');
                $productImgSrchtml = htmlspecialchars_decode($productImgSrchtml);
                $productImgSrchtml = json_decode($productImgSrchtml, true);
                $productImgSrchtml = key($productImgSrchtml);
            }
        }



        /**echo $productImgSrchtml; die;
         * $main_img_anchors = $html->find('#bx-pager-left-image-panel a');
         * $i_num = 0;
         * foreach( $main_img_anchors as $i => $anchor ) {
         *
         * $img = explode('"',$anchor->innertext);
         *
         * if(isset($img[5]) && !empty($img[5]) && $i_num == 0){
         * $productImgSrchtml = $img[5];
         * }
         *
         * $i_num++;
         * } **/


        // product unique id
        $product_unique_id = '';
        $productUrl = explode('dp/', $url);
        if (isset($productUrl[1]) && !empty($productUrl[1])) {
            $unique_id = explode('%3', $productUrl[1]);
            $product_unique_id = isset($unique_id[0]) ? $unique_id[0] : '';
        }


        $arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
        // $arr_merchant[] = array();

        /* if ($html->find('.proDescript', 0)) {
              $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
         }*/


        // product details
        $product_detail = array();

        // product category
        $product_detail['Category'] = $categoryName;
        $product_detail['cash_on_delivery'] = '';
        if ($html->find('#cod_eligible_message', 0)) {
            $product_detail['cash_on_delivery'] = trim($html->find('#cod_eligible_message', 0)->plaintext);
        }

        $product_detail['shipping'] = 'Free Shipping';
        $product_detail['return_policy'] = '100% Purchase Protection | Original Products | Easy Returns | Secure Payments';

        $pro_title = explode(' ', $productNamehtml);
        $product_detail['brand'] = $pro_title[0];


        $product_detail['model_number'] = trim(str_replace($pro_title[0], '', $productNamehtml));


        // product original price
        $product_detail['original_price'] = $productPrice;
        if ($html->find('#price', 0)) {
            $priceDiv = $html->find('#price', 0);
            if ($priceDiv->find('.a-text-strike', 0)) {
                $productOriginalPrice = $priceDiv->find('.a-text-strike', 0)->plaintext;
                $product_detail['original_price'] = $this->convet_price_string($productOriginalPrice);
            }
        }

        if (empty($productPrice) && !empty($product_detail['original_price'])) {
            $productPrice = $product_detail['original_price'];
        }

        if (empty($productPrice) && $html->find('#olp_feature_div', 0)) {
            $priceDiv = $html->find('#olp_feature_div', 0);
            if ($priceDiv->find('.a-color-price', 0)) {
                $productOriginalPrice = $priceDiv->find('.a-color-price', 0)->plaintext;
                $productPrice = $this->convet_price_string($productOriginalPrice);
                if (!empty($productPrice) && empty($product_detail['original_price'])) {
                    $product_detail['original_price'] = $productPrice;
                }
            }
        }

        // product count users for product rating

        $product_detail['rating_user_count'] = '';
        if ($html->find('#acrCustomerReviewText', 0)) {
            $productReviews = trim($html->find('#acrCustomerReviewText', 0)->plaintext);
            $product_detail['rating_user_count'] = $this->convet_price_string($productReviews);
        }


        // product model number
        if ($html->find('.product-details', 0)) {
            if ($html->find('.title-wrap', 0)) {
                $productModelHtml = $html->find('.title-wrap', 0)->getAttribute('data-evar2');
                $productModelHtml = str_replace($productBrandHtml, '', $productModelHtml);
                $product_detail['model_number'] = trim($productModelHtml);
            }
        }


        /*******New filds*******/

        // product rating
        $product_detail['rating'] = '';
        if ($html->find('#averageCustomerReviewRating', 0)) {
            if ($html->find('#averageCustomerReviewRating', 0)) {
                $productRatingHtml = $html->find('#averageCustomerReviewRating', 0)->plaintext;
                $product_detail['rating'] = trim(str_replace('out of 5 stars', '', $productRatingHtml));
            } else {
                $scraperLog->saveScraperLog('amazon scraper', 'AmazonProductDetailScrapper', '.title-wrap class not found', 'Not Found', __LINE__);
            }

        }

        // product reviews
        $product_detail['reviews'] = '';
        if ($html->find('#acrCustomerReviewText', 0)) {
            $productReviews = trim($html->find('#acrCustomerReviewText', 0)->plaintext);
            $product_detail['reviews'] = $this->convet_price_string($productReviews);
        }


        $product_detail['discount'] = '';
        if ($html->find('#regularprice_savings', 0)) {
            $discountText = trim($html->find('#regularprice_savings', 0)->plaintext);
            $discountText = explode('(', $discountText);
            if (isset($discountText[1]) && !empty($discountText[1])) {
                $product_detail['discount'] = str_replace('%)', '', $discountText[1]);
            }
        }


        $product_detail['product_delivery'] = '';
        if ($html->find('#ddmDeliveryMessage', 0)) {
            if ($html->find('#ddmDeliveryMessage', 0)) {
                $product_detail['product_delivery'] = trim($html->find('#ddmDeliveryMessage', 0)->innertext);

            }
        }

        $product_detail['emi'] = '';
        if ($html->find('#inemi_feature_div', 0)) {
            $emi = trim($html->find('#inemi_feature_div', 0)->plaintext);
            $emiText = explode('per month.', $emi);
            if (isset($emiText[0]) && !empty($emiText[0])) {
                $product_detail['emi'] = $emiText[0] . ' per month.';
            }
        }

        // product description
        /**$description = '';
         * if ($html->find('.pdp-e-i-keyfeatures ul', 0)) {
         *
         * $ulData = $html->find('.pdp-e-i-keyfeatures ul', 0);
         * $li = 1;
         * foreach ($ulData->find("li") as $skey => $sval) {
         * if($li <= 5){
         * $description .= $sval->plaintext.',';
         * }
         * $li++;
         * }
         *
         * }
         * $product_detail['descritpion'] = $description; **/

        $product_detail['descritpion'] = '';
        if ($html->find('#feature-bullets', 0)) {
            $descData = $html->find('#feature-bullets', 0);
            if ($descData->find('.a-vertical', 0)) {
                $productDetailhtml = $descData->find('.a-vertical', 0)->outertext;
                $product_detail['descritpion'] = trim($productDetailhtml);
            }
        }

        if ($html->find('#variation_color_name', 0)) {
            $colorDiv = $html->find('#variation_color_name', 0);
            $colorLI = $colorDiv->find('li');
            foreach ($colorLI as $color) {
                if (isset($color->find('img', 0)->attr['alt'])) {
                    $colors[] = $color->find('img', 0)->attr['alt'];
                }
            }
        }

        if ($html->find('#variation_size_name', 0)) {
            $sizeDiv = $html->find('#variation_size_name', 0);
            $sizeOptions = $sizeDiv->find('select > option');
            foreach ($sizeOptions as $size) {
                if (is_numeric(trim($size->plaintext))) {
                    $sizes[] = trim($size->plaintext);
                }
            }
        }


        // product feature details
        $arr_product_images = array();
        if ($html->find('#altImages', 0)) {
            $multImagesBox = $html->find('#altImages', 0);
            $anchors = $multImagesBox->find('li');
            // loop and print nodes content
            $img_num = 0;
            foreach ($anchors as $i => $anchor) {
                if ($img_num <= 3) {
                    if ($anchor->find('img', 0)) {
                        $image_Name = $anchor->find('img', 0)->getAttribute('src');
                        $pattern = '/(?<=)._.*?._(?=)/';
                        $arr_product_images[] = preg_replace($pattern, "", $image_Name);
                    }

                }
                $img_num++;
            }
        }


        // product feature details
        $arr_product_features = array();
        $groupIndex = 0;
        if ($html->find('#prodDetails', 0)) {
            $productFeaturesDiv = $html->find('#prodDetails', 0);
            foreach ($productFeaturesDiv->find('div.col1') as $productFeaturesColumn) {
                foreach ($productFeaturesColumn->find('div.section') as $productFeaturesSection) {
                    $arr_product_features[$groupIndex]['group'] = $productFeaturesSection->find('div.secHeader > span', 0)->innertext;
                    foreach ($productFeaturesSection->find('div.content > table > tr') as $feature) {
                        if ($feature->find('td.label', 0) && $feature->find('td.value', 0)) {
                            $featureName = $feature->find('td.label', 0)->innertext;
                            $featureValue = $feature->find('td.value', 0)->innertext;
                            $arr_product_features[$groupIndex]['feature'][0][] = array(
                                'feature' => $featureName,
                                'feature_value' => $featureValue);
                        }

                    }
                    $groupIndex++;
                }
            }
        }

        if ($html->find('#olp_feature_div', 0)) {
            $offers[] = trim($html->find('#olp_feature_div', 0)->plaintext);
        }

        if ($html->find('#availability', 0)) {
            $availablityDiv = $html->find('#availability', 0);
            if (stripos($availablityDiv->find('span.a-size-medium', 0)->plaintext, "Currently unavailable") !== false) {
                $instock = false;
            }
        }

        $returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $arr_product_images;
        $returnarray['product_price'] = $productPrice;
        $returnarray['product_features'] = $arr_product_features;
        $returnarray['product_colors'] = implode("##", $colors);
        $returnarray['product_sizes'] = implode("##", $sizes);
        $returnarray['product_offers'] = implode("##", $offers);
        $returnarray['instock'] = $instock;
        return $returnarray;
    }

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return ($result);
    }


}
