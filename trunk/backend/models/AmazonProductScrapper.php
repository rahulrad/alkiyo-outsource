<?php
namespace backend\models;
use common\models\Brands;
use common\models\Products;
use common\models\ProductsSuppliers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\simple_html_dom;
use common\components\HttpService;
use common\models\Categories;
use backend\models\ProductUrls;
use common\components\AmazonECS;
use common\components\Aws;
use backend\models\MongoProducts;

//require_once 'simple_html_dom.php';
//require_once 'JungleeProductDetailScrapper.php';
//require_once('Net/URL2.php');

class AmazonProductScrapper {

    /**
     * get All Paging url from Junglee Site and save in DB All product pages Url.
     */
    public function getAllPagingUrl($url) {
        $strurl = str_replace('{pg}', 1, $url);

//        $response = $this->fetchContentUsingProxy($strurl);
//
//        if (!$response)
//            return false;
//
//        $html = $this->LoadHTMLContent($response);
//        if ($html->find('#resultCount')) {
//            $totalProductStr = $html->find('#resultCount span', 0)->plaintext;
//            $arr_records = explode('of', $totalProductStr);
//            $productPerPage = 24;
//            $totalProduct = $this->convert_total_pages_string($arr_records[1]);
//            $noOfPages = ceil($totalProduct / $productPerPage);
//        }
        $productPerPage = 20;
        $noOfPages = ceil(100 / $productPerPage);
        $arr_url = array();
        for ($i = 1; $i <= 8360; $i+=20) {
            $strurl = str_replace('{pg}', $i, $url);
            $arr_url[] = $strurl;
        }
        return $arr_url;
    }

    /**
     * Save All Product Pages Url in Data Base.
     */
    public function saveAllPagingUrl($arr_url, $categoryName, $categoryType) {
        $connection = Yii::$app->db;
        foreach ($arr_url as $key => $value) {
            $query = "SELECT site_url from category_url where site_url='" . $value . "'";
            $command = $connection->createCommand($query);
            $arr_result = $command->queryAll();
            $no_of_records = count($arr_result);

            if ($no_of_records == 0) {
                $insert_query = "INSERT INTO category_url (category_name,site_url, category_type) VALUES(:category_name, :site_url, :category_type)";
                $insert_command = $connection->createCommand($insert_query);
                $category_name = $categoryName;
                $insert_command->bindParam(':category_name', $category_name);
                $insert_command->bindParam(':site_url', $value);
                $insert_command->bindParam(':category_type', $categoryType);
                $insert_command->execute();
            }
        }
    }

    /**
     * Save Product In DB.
     */
    public function saveProduct($categoryName, $arr_department_type, $categoryType, $latest_product = 0, $store_name) {
		//echo $categoryName; die;
        $connection = Yii::$app->db;
        /* This condition indicate that if new product add on this category then first we set all the status as 1 and after that we updated status column as 0 of top 2 pages and save new product in our DB. */
        /* if ($latest_product == 1) {

          $update_sql= "update junglee_category_url set status=1 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url";
          $update_result = yii::app()->db->createCommand($update_sql);
          $update_result->execute();

          $update_query = "update junglee_category_url set status=0 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url LIMIT 2";
          $update_query = yii::app()->db->createCommand($update_query);
          $update_query->execute();
          } */

        $objAmazonProduct = new \backend\models\AmazonProductScrapper();
        $query = "SELECT id_category_url,category_name,site_url from category_url where status=0 and category_type='" . $categoryType . "' and category_name='" . $categoryName . "' and store_name='" . $store_name . "' order by id_category_url limit 1";
        $command = $connection->createCommand($query);
        $arr_data = $command->queryAll();
//        $arr_data[] = array('site_url'=>'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007');



        if (is_array($arr_data)) {
            foreach ($arr_data as $key => $value) {
                //if ($key == 0) {
				//echo $value['site_url'].'=======>'.$categoryName; die;
                $data = $objAmazonProduct->getAllProductDetailsOnPage($value['site_url'], $categoryName);


                try {
                    foreach ($data as $pkey => $pval) {


                        $name = $pval['name'];
//                        if (!is_array($pval['merchant_arr']))
//                            continue;
//                        else
//                            $price = isset($pval['merchant_arr'][0]['mprice']) ? $pval['merchant_arr'][0]['mprice'] : '';
                        $price = $pval['product_price'];
                        $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                        $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
						$description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
						$emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
						$cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
						$return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
						$imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
						$murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
						$m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';

						$discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
						$rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
						$product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new

						$reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new
						$shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
						$original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
						$rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new

						// $model = \common\components\Tools::removeBrand($name, $brand);

                        //$importer = new BaseProductImporter();
                        //$importer->setName($name);

                        $sandle_cat_name = 'Sandal';
                        $slipper_cat_name = 'Slipper';

                        if (stristr($name, $sandle_cat_name)) {
                            //$importer->setCategory('Sandals');
                            $importer = new BaseAmazonProductImporter('Sandals', $name);
                        } elseif (stristr($name, $slipper_cat_name)) {
                            //$importer->setCategory('Slippers');
                            $importer = new BaseAmazonProductImporter('Slippers', $name);
                        } else {
                            //$importer->setCategory($value['category_name']);
                            $importer = new BaseAmazonProductImporter($value['category_name'], $name);

                        }

                        $importer->setLowestPrice($price);
                        $importer->setLinkRewrite($name);
                        $importer->setModel($model);
                        $importer->setBrand($brand);
						$importer->setdescription($description);
                        $importer->setProductEMI(strip_tags($emi));
						$importer->setCategory($value['category_name']);

						$importer->setCOD($cash_on_delivery);
						$importer->setReturnPolicy($return_policy);
						$importer->setMainImages($imgpath);
						$importer->setProducturl($murl);
						$importer->setProductUniqueId($m_unique_id);

						$importer->setProductDiscount($discount);
						$importer->setProductRating($rating);
						$importer->setProductDelivery($product_delivery);

						$importer->setProductReviews($reviews);
						$importer->setProductShipping($shipping);
						$importer->setProductOriginalPrice($original_price);
						$importer->setProductRatingUserCount($rating_user_count);


						$importer->setMultipleImages($pval['product_img_arr']);
						$importer->setProductFeature($pval['product_features']);


                        $importer->save();
                    }

                    $q = "update category_url SET status = '1' where id_category_url = '" . $value['id_category_url'] . "'";
                    echo $q;
                    $comm = $connection->createCommand($q);
                    $comm->bindParam(':id_category_url', $key);
                    $comm->execute();
                } catch (Exception $e) {
                    echo "wwwwwwwwwww".$e->getMessage().'::'.$e->getLine().'::'.$e->getTraceAsString();exit;
                    continue;
                }
            }
        }
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getProductUrlOnPage($url, $categoryName) {

		//$crlUrl = 'http://localhost/nayashoppy/frontend/web/amazon_api/index.php?category=Electronics&sub_category=mobiles&page_number=2';

		//$crlUrl = Yii::getAlias('@frontend') .'/web/'.$url;

		$crlUrl = 'http://localhost/nayashoppy/frontend/web/'.$url;

		//echo $crlUrl; die;
		$arr_product = array();

		if(!empty($crlUrl)){


			$headers = array(
				 "Content-Type: application/json",
				 "Accept: application/json",
				 "Access-Control-Request-Method: GET"
			 );
			   $ch = curl_init();
			   curl_setopt($ch, CURLOPT_URL, $crlUrl);
			   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
			   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			   $result = curl_exec($ch);
			   $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			    $productUrls = json_decode($result, true);

				   if(!empty($productUrls)){
					   $arr_product = $productUrls;
						//$arr_product[] ='http://www.amazon.in/YU-Yuphoria-YU5010A-Buffed-Steel/dp/B015W4UQJS/ref=sr_1_6?s=electronics&ie=UTF8&qid=1471586380&sr=1-6&keywords=mobile';
				   }

		}
		//echo '<pre>'; print_r($arr_product); die;
		return $arr_product;
    }


    private function getAllProductDetailsOnPage($url, $categoryName) {
        $product_urls = $this->getProductUrlOnPage($url, $categoryName);
        $product_details = array();
        if (is_array($product_urls)) {
            foreach ($product_urls as $key => $value) {
                $product_details[] = $this->getProductDetail($value, $categoryName);
            }
        }


        return $product_details;
    }

    private function getProductDetail($url, $category) {

        $objAmazon_product_detail = new \backend\models\AmazonProductDetailScrapper($url, $category);

        return $objAmazon_product_detail->StoreDataInArray();
    }



    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl) {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content) {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Convert No of Total Pages in a string.
     */
    private function convert_total_pages_string($userString) {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }



	// save products Detail New code 28 sep

	public function saveProductData($productUrlData,$categoryName,$queueMessageData = null){

				$exitProduct = MongoProducts::find()->where(['url' => $productUrlData->url])->one();

				if(!empty($exitProduct)){
					$ProductUrlsModel = ProductUrls::findOne($productUrlData->id);
					$ProductUrlsModel->status = 1;
					$ProductUrlsModel->save(false);


					if(!empty($queueMessageData)){
						$aws = new \common\components\Aws();
						$aws = $aws->deleteQueueMessage($queueMessageData,'product_scraper');

					}
				}


			$connection = Yii::$app->db;


			$objflipkartProduct = new \backend\models\AmazonProductScrapper();

			if ($productUrlData) {

					$pval = $objflipkartProduct->getProductDetail($productUrlData->url, $categoryName);
				   //echo '<pre>pval'; print_r($pval); die;
				   try {

						if(!empty($pval)){
							//echo '<pre>'; print_r($pval); die;
							$name = $pval['name'];
							$price = $pval['product_price'];
							$model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
							$brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
							$description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
							$emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
                            $colors = isset($pval['product_colors']) ? $pval['product_colors'] : '';
                            $sizes = isset($pval['product_sizes']) ? $pval['product_sizes'] : '';
                            $offers = isset($pval['product_offers']) ? $pval['product_offers'] : '';
                            $instock = (isset($pval['instock']) && $pval['instock']) ? 1 : 0;
							$cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
							$return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
							$imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
							$murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
							$m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';

							$discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
							$rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
							$product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
							$reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new

							$shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
							$original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
							$rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new

							// $model = \common\components\Tools::removeBrand($name, $brand);

							//$importer = new BaseProductImporter();
							//$importer->setName($name);

							$importer = new BaseAmazonProductImporter($categoryName, $name);

							//echo $price; die;
							$importer->setLowestPrice($price);
							$importer->setLinkRewrite($name);
							$importer->setModel($model);
							$importer->setBrand($brand);
							$importer->setdescription($description);
							$importer->setProductEMI($emi);
                            $importer->setProductColors($colors);
                            $importer->setProductSizes($sizes);
                            $importer->setProductOffers($offers);
                            $importer->setProductInstock($instock);
							$importer->setCategory($categoryName);

							$importer->setCOD($cash_on_delivery);
							$importer->setReturnPolicy($return_policy);
							$importer->setMainImages($imgpath);
							$importer->setProducturl($murl);
							$importer->setProductUniqueId($m_unique_id);

							$importer->setProductDiscount($discount);
							$importer->setProductRating($rating);
							$importer->setProductDelivery($product_delivery);
							$importer->setProductReviews($reviews);
							$importer->setProductShipping($shipping);
							$importer->setProductOriginalPrice($original_price);
							$importer->setProductRatingUserCount($rating_user_count);

							$importer->setMultipleImages($pval['product_img_arr']);
							$importer->setProductFeature($pval['product_features']);

						   // $importer->setReferenceNumber($ref_no);
							$importer->setProductUrlId($productUrlData->id);

							$importer->setQueueMessageData($queueMessageData);// vinay 13 oct
							$front_image = array();
							$product_images = array();

							$importer->save();

							/**$q = "update product_urls SET status = '1' where id = '" . $productUrlData->id . "'";
							echo $q.'</br>';
							$comm = $connection->createCommand($q);
							$comm->bindParam(':id_category_url', $key);
							$comm->execute(); **/
						}




					} catch (\Exception $e) {
                       error_log(var_export($e->getMessage(), true));
                       error_log(var_export($e->getTraceAsString(), true));
                       throw  $e;
					}

			}

	}

    public function saveProductSupplierData(ProductsSuppliers $productSupplier, $categoryName)
    {
        try {
            $pval = $this->getProductDetail($productSupplier->url, $categoryName);
            error_log(var_export($pval, true));
            if (!empty($pval)) {
                $name = $pval['name'];
                $price = $pval['product_price'];
                $model = isset($pval['product_detail']['model_number']) ? $pval['product_detail']['model_number'] : '';
                $brand = isset($pval['product_detail']['brand']) ? $pval['product_detail']['brand'] : '';
                $description = isset($pval['product_detail']['descritpion']) ? $pval['product_detail']['descritpion'] : '';
                $emi = isset($pval['product_detail']['emi']) ? $pval['product_detail']['emi'] : '';
                $colors = isset($pval['product_colors']) ? $pval['product_colors'] : '';
                $sizes = isset($pval['product_sizes']) ? $pval['product_sizes'] : '';
                $offers = isset($pval['product_offers']) ? $pval['product_offers'] : '';
                $instock = (isset($pval['instock']) && $pval['instock']) ? 1 : 0;
                $cash_on_delivery = isset($pval['product_detail']['cash_on_delivery']) ? $pval['product_detail']['cash_on_delivery'] : '';
                $return_policy = isset($pval['product_detail']['return_policy']) ? $pval['product_detail']['return_policy'] : '';
                $imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';
                $murl = isset($pval['merchant_arr']['murl']) ? $pval['merchant_arr']['murl'] : '';
                $m_unique_id = isset($pval['merchant_arr']['m_unique_id']) ? $pval['merchant_arr']['m_unique_id'] : '';

                $discount = isset($pval['product_detail']['discount']) ? $pval['product_detail']['discount'] : '';// vinay new
                $rating = isset($pval['product_detail']['rating']) ? $pval['product_detail']['rating'] : '';// vinay new
                $product_delivery = isset($pval['product_detail']['product_delivery']) ? $pval['product_detail']['product_delivery'] : '';// vinay new
                $reviews = isset($pval['product_detail']['reviews']) ? $pval['product_detail']['reviews'] : '';// vinay new

                $shipping = isset($pval['product_detail']['shipping']) ? $pval['product_detail']['shipping'] : '';// vinay new
                $original_price = isset($pval['product_detail']['original_price']) ? $pval['product_detail']['original_price'] : '';// vinay new
                $rating_user_count = isset($pval['product_detail']['rating_user_count']) ? $pval['product_detail']['rating_user_count'] : '';// vinay new


                $importer = new BaseProductImporter($categoryName, $name);

                $importer->setLowestPrice($price);
                $importer->setLinkRewrite($name);
                $importer->setModel($model);
                $importer->setBrand($brand);
                $importer->setdescription($description);
                $importer->setProductEMI(strip_tags($emi));
                $importer->setProductColors($colors);
                $importer->setProductSizes($sizes);
                $importer->setProductOffers($offers);
                $importer->setProductInstock($instock);
                $importer->setCategory($categoryName);
                $importer->setCOD($cash_on_delivery);
                $importer->setReturnPolicy($return_policy);
                $importer->setMainImages($imgpath);
                $importer->setProducturl($productSupplier->url);
                $importer->setProductUniqueId($m_unique_id);
                $importer->setProductDiscount($discount);
                $importer->setProductRating($rating);
                $importer->setProductDelivery(strip_tags($product_delivery));
                $importer->setProductReviews($reviews);
                $importer->setProductShipping($shipping);
                $importer->setProductOriginalPrice($original_price);
                $importer->setProductRatingUserCount($rating_user_count);
                $importer->setMultipleImages($pval['product_img_arr']);
                $importer->setProductFeature($pval['product_features']);
                $importer->setProductUrlId($productSupplier->id);
                $importer->updateProductSupplier($productSupplier);

            }

        } catch (\Exception $e) {
            error_log(var_export($e->getMessage(), true));
            error_log(var_export($e->getTraceAsString(), true));
            throw  $e;
        }
    }

    public function saveProductUrls($response,$categories_url_data){

        $categoryData = Categories::find()->where(['category_name' => $categories_url_data->category_name])->one();
        $category_id = !empty($categoryData) ? $categoryData->category_id : 0;

        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
        $arr_product = array();

        if ($html->find('#s-results-list-atf', 0)) {
            foreach ($html->find(".s-result-item") as $key => $pval) {

                if ($pval->find('.a-spacing-base a', 0)) {
                    $productUrl = $pval->find('.a-spacing-base a', 0)->getAttribute('href');
                    if (strpos($productUrl, 'www.amazon.in') !== false) {
                        error_log($productUrl);
                        $parts = parse_url($productUrl);
                        parse_str($parts['query'], $query);
                        if(isset($query['url'])){
                            $productUrl = $query['url'];
                        }
                        $arr_product[] = $productUrl;
                        $product_url = $productUrl;
                        $exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 3])->one();
                        if (empty($exitProductUrl)) {

                            $productUrlsModel = new ProductUrls();
                            $productUrlsModel->store_id = 5;
                            $productUrlsModel->cateogry_url_id = $categories_url_data->id_category_url;
                            $productUrlsModel->category_id = $category_id;
                            $productUrlsModel->status = 0;
                            $productUrlsModel->url = $product_url;
                            $productUrlsModel->save();

                            $exitProductUrl = ProductUrls::find()->where(['url' => $product_url, 'store_id' => 3])->one();
                            $aws = new \common\components\Aws();
                            $aws = $aws->sendMessage($exitProductUrl, 'product_scraper');
                        }
                    }

                }
            }
            if (!empty($arr_product)) {
                //echo '<pre>'; print_r($productUrl);
                $CategoryUrlModel = CategoryUrl::findOne($categories_url_data->id_category_url);
                $CategoryUrlModel->status = 1;
                $CategoryUrlModel->save(false);
                echo 'save category url id=>' . $CategoryUrlModel->id_category_url . '<br>';
            }
        }else{
            $CategoryUrlModel = CategoryUrl::findOne($categories_url_data->id_category_url);
            $CategoryUrlModel->status = 1;
            $CategoryUrlModel->save(false);
            echo 'save category url but no records in it id=>'.$CategoryUrlModel->id_category_url.'<br>';
        }
    }

    public function updateProductImages(Products $product, Categories $category, Brands $brand)
    {
        try {
            $pval = $this->getProductDetail($product->url, $category->category_name);
            if (!empty($pval)) {
                $name = $pval['name'];
                $imgpath = isset($pval['imgpath']) ? $pval['imgpath'] : '';

                $importer = new BaseProductImporter($category->category_name, $name);
                $importer->setMainImages($imgpath);
                $importer->setMultipleImages($pval['product_img_arr']);
                $importer->updateProductImages($product, $category, $brand);
            }
        } catch (\Exception $e) {
            error_log(var_export($e->getMessage(), true));
            error_log(var_export($e->getTraceAsString(), true));
            throw  $e;
        }
    }
}
