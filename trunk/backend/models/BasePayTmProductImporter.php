<?php
/**
 * Description of BasePayTmProductImporter
 *
 * @author Vikash
 */
 

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Brands;
use common\models\Categories;
use common\models\ProductImage;
use common\models\FeatureGroups;
use common\models\Features;
use common\models\FeatureValues;
use common\models\ProductFeature;
use backend\models\Mobile;
use backend\models\Computer;
use backend\models\MongoProducts;

class BasePayTmProductImporter {
    
    private $_product;
    private $id_category;
    private $name;
    private $description;
    private $model;
    private $lowest_price;
    private $length;
    private $width;
    private $height;
    private $package_content;
    private $warranty;
    private $featured;
    private $link_rewrite;
    private $brand;
    private $category;
    private $features = array();
    private $images = array();
    private $tags = array();
    private $suppliers = array();
    private $active;
    private $video = array();
    private $launch_date;
    private $product_model;
    private $reference_number;
    private $suppliercount;
    private $keyField;
    private $keyValue;

    public function __construct($category_name, $name = '', $key_field = NULL, $key_value = NULL) {
        $this->setName($name);
        $this->setCategory($category_name);
        $this->setKeyField($key_field);
        $this->setKeyValue($key_value);        
        $this->init();
    }
    
    private function init() {
        $category = $this->saveCategory();
		
        if (is_object($category)) {
		    $this->category_id = $category->category_id;
			$this->category_name = $category->category_name;
//            if ($this->keyField != NULL)            
//                $this->_product = Product::model()->findByAttributes(array($this->keyField => $this->keyValue, 'id_category' => $category->id_category));            
//            else
//                $this->_product = Product::model()->findByAttributes(array('name' => $this->name));
//                //$this->_product = Product::model()->findByAttributes(array('name' => $this->name, 'id_category' => $category->id_category));
//            if (!is_object($this->_product)) {
//                $this->_product = new Product();
//                $this->_product->active = 1;
//            }
        } 
		 
		/* $cat_name = ucfirst($category->category_name);  
		 if($cat_name == 'Computer'){
       		 $this->_product = new Computer();
		 } elseif($cat_name == 'Mobile'){
			$this->_product = new Mobile();
		 }*/
		 
		 
		 $this->_product = new MongoProducts();
		 
		
        
    }
    
    public function setName($name) {
        if (!is_string($name))
            throw new CException("Invalid name");
        $this->name = \common\components\Tools::cleanString($name);
    }

    public function setdescription($description) {
        if (!is_string($description))
            throw new CException("Invalid description");
        $this->description = $description;
    }

    public function setModel($model) {
	
        if (!is_string($model))
            throw new CException("Invalid model");
        $this->model = \common\components\Tools::cleanString($model);
    }

    public function setLowestPrice($lowest_price) {
		 $this->price = $lowest_price;
    }

    public function setWidth($width) {
        if (!is_string($width))
            throw new CException("Invalid width");
        $this->width = $width;
    }

    public function setHeight($height) {
        if (!is_string($height))
            throw new CException("Invalid height");
        $this->height = $height;
    }

    public function setPackageContent($package_content) {
        if (!is_string($package_content))
            throw new CException("Invalid package content");
        $this->package_content = $package_content;
    }

    public function setWarranty($warranty) {
        if (!is_string($warranty))
            throw new CException("Invalid warranty");
        $this->package_content = $warranty;
    }

    public function setFeatured($featured) {
        if (is_int($featured))
            $this->featured = $featured;
    }

    public function setLinkRewrite($name) {
        if (!is_string($name))
            throw new CException("Invalid name");
        $this->link_rewrite = \common\components\Tools::link_rewrite($name);
    }

    public function setBrand($brand) {
        if (!is_string($brand))
            throw new CException("Invalid brand");
        $this->brand = $brand;
    }

    public function setTags($tag) {
        if (!is_array($tag))
            throw new CException("Invalid Tag");
        $this->tags = $tag;
    }

    public function setCategory($category) {
        if (!is_string($category))
            throw new CException("Invalid category");
        $this->category = $category;
    }

    public function setFeatures($features) {
        if (!is_array($features))
            throw new CException("Invalid Features");
        $this->features = $features;
    }

    public function setImages($images) {
			//echo $images; die;
        if (!is_array($images))
            throw new CException("Invalid images");
        $this->images = $images;
    }

    public function setSuppliers($suppliers) {
        if (!is_array($suppliers))
            throw new CException("Invalid suppliers");
        $this->suppliers = $suppliers;
    }

    public function setActive($active=1) {
        if (!is_integer($active))
            throw new CException("Invalid active parameter");
        $this->active = $active;
    }

    public function setVideos($video) {
        if (!is_array($video))
            throw new CException("Invalid Videos");
        $this->video = $video;
    }

    public function setLaunchDate($launch_date) {
        if (!is_string($launch_date))
            throw new CException("Invalid Launch Date");
        $this->launch_date = $launch_date;
    }

    public function setProductModel($name) {
        if (!is_string($name))
            throw new CException("Invalid model name");
        $this->product_model = $name;
    }

    public function setReferenceNumber($reference_number) {
        //if (!is_string($reference_number))
        //    throw new CException("Invalid Reference Number");
        $this->reference_number = $reference_number;
    }

    public function setSupplierCount($suppliercount=1) {
        if (!is_integer($suppliercount))
            throw new CException("Invalid active parameter");
        $this->suppliercount = $suppliercount;
    }

    public function setKeyField($keyfield) {
        $this->keyField = $keyfield;
    }

    public function setKeyValue($keyvalue) {
        $this->keyValue = $keyvalue;
    }
	
	
	
	
	
	
    
    public function getName() {
        return (isset($this->name) AND $this->name) ? $this->name : $this->_product->product_name;
    }
    
    public function getModel() {
        return (isset($this->model) AND $this->model) ? $this->model : $this->_product->model_number;
    }
    
    public function getLowestPrice() {
		
        return (isset($this->price) AND $this->price) ? $this->price : $this->_product->price;
    }
    
    public function getDescription() {
        return (isset($this->description) AND $this->description) ? $this->description : $this->_product->product_description;
    }
    
    public function getHeight() {
        return (isset($this->height) AND $this->height) ? $this->height : $this->_product->height;
    }
    
    public function getLength() {
        return (isset($this->length) AND $this->length) ? $this->length : $this->_product->length;
    }
    
    public function getLinkRewrite() {
        return (isset($this->link_rewrite) AND $this->link_rewrite) ? $this->link_rewrite : $this->_product->link_rewrite;
    }
    
    public function getWarranty() {
        return (isset($this->warranty) AND $this->warranty) ? $this->warranty : $this->_product->warranty;
    }
    
    public function getWidth() {
        return (isset($this->width) AND $this->width) ? $this->width : $this->_product->width;
    }
    
    public function getActive() {
        return isset($this->active) ? $this->active : $this->_product->active;
    }
    
    public function getDateAdd() {
        return isset($this->_product->date_add) ? $this->_product->date_add : time();
    }
    
    public function getDateUpdate() {
        return date('Y-m-d');
    }
    
    public function getDateLaunch() {
        return (isset($this->launch_date) AND $this->launch_date) ? $this->launch_date : $this->_product->date_launch;
    }
    
    public function getReferenceNumber() {
        return (isset($this->reference_number) AND $this->reference_number) ? $this->reference_number : $this->_product->reference_number;
    }
    
    public function getSupplierCount() {
        return (isset($this->suppliercount) AND $this->suppliercount) ? $this->suppliercount : $this->_product->suppliercount;
    }
    
    public function getIdBrand() {        
        $brand = $this->saveBrand();
		//echo '<pre>'; print_r($brand->brand_name); die;
        return (isset($brand->brand_name) AND $brand->brand_name) ? $brand->brand_name : $this->_product->brand_name;       
    }
    
    public function getIdCategory() {
        return (isset($this->category_name) AND $this->category_name) ? $this->category_name : $this->_product->category_name;
    }
	
	
	// vinay New Fields
	
	 public function setMainImages($image) {
        $this->main_image = $image;
    }
	
	public function getMainImages() {
        return (isset($this->main_image) AND $this->main_image) ? $this->main_image : $this->_product->image;
    }
	
	 public function setCOD($cod) {
        $this->cash_on_delivery = $cod;
    }
	
	public function getCOD() {
        return (isset($this->cash_on_delivery) AND $this->cash_on_delivery) ? $this->cash_on_delivery : $this->_product->cod;
    }
	
	 public function setReturnPolicy($return_policy) {
        $this->return_policy = $return_policy;
    }
	
	public function getReturnPolicy() {
        return (isset($this->return_policy) AND $this->return_policy) ? $this->return_policy : $this->_product->return_policy;
    }
	
	 public function setProducturl($url) {
        $this->url = $url;
    }
	
	public function getProducturl() {
        return (isset($this->url) AND $this->url) ? $this->url : $this->_product->url;
    }
	
	 public function setProductUniqueId($product_unique_id) {
        $this->unique_id = $product_unique_id;
    }
	
	public function getProductUniqueId() {
        return (isset($this->unique_id) AND $this->unique_id) ? $this->unique_id : $this->_product->unique_id;
    }
	
	
	public function setMultipleImages($all_images) {
        $this->all_images = $all_images;
    }
	
	public function getMultipleImages() {
        return (isset($this->all_images) AND $this->all_images) ? $this->all_images : '';
    }
	
    
	public function setProductFeature($product_features) {
        $this->productFeature = $product_features;
    }
	
	public function getProductFeature() {
        return (isset($this->productFeature) AND $this->productFeature) ? $this->productFeature : '';
    }
	
	
	 
	public function setProductEMI($emi) {
        $this->emi = $emi;
    }
	
	public function getProductEMI() {
        return (isset($this->emi) AND $this->emi) ? $this->emi : '';
    }
	
	public function setProductDiscount($discount) {
        $this->discount = $discount;
    }
	
	public function getProductDiscount() {
        return (isset($this->discount) AND $this->discount) ? $this->discount : '';
    }
	
    
	public function setProductRating($rating) {
        $this->rating = $rating;
    }
	
	public function getProductRating() {
        return (isset($this->rating) AND $this->rating) ? $this->rating : '';
    }
	
	public function setProductDelivery($product_delivery) {
        $this->delivery = $product_delivery;
    }
	
	public function getProductDelivery() {
        return (isset($this->delivery) AND $this->delivery) ? $this->delivery : '';
    }
	
	public function setProductReviews($reviews) {
        $this->reviews = $reviews;
    }
	
	public function getProductReviews() {
        return (isset($this->reviews) AND $this->reviews) ? $this->reviews : '';
    }


	
    
    
    public function save($refresh=false, $is_product_log = false) {  
        if (isset($this->_product)) {
            if ($this->_product->_id != NULL && $refresh === false) {
                if ($is_product_log)
                    return $this->_product->_id;
                return FALSE;
            }

            try {  
				
				//$mobile = new Mobile();
				$pro_name = $this->getName();
				
				

				/*$exit_product = Mobile::find()->where(['product_name' => $pro_name])->one();
				if(!empty($exit_product)){
					echo '<pre>vinay result'; print_r($exit_product); die;
				}*/
				
				
				$this->_product->store_name = 'paytm';
				$this->_product->store_id = 2;
				$this->_product->brand_name = $this->getIdBrand();
				$this->_product->category_name = $this->getIdCategory();
				$this->_product->product_name = $this->getName();
				$this->_product->product_description = $this->getDescription();
				$this->_product->model_number =  $this->getModel();
				$this->_product->price =  $this->getLowestPrice();
				//$this->_product->cod = $this->getCOD();
				
				$this->_product->emi = $this->getProductEMI();
				$this->_product->cod = $this->getCOD();
				$this->_product->return_policy = $this->getReturnPolicy();
				$this->_product->url = $this->getProducturl();
				$this->_product->unique_id = $this->getProductUniqueId();
				$this->_product->product_description = $this->getDescription();
				$this->_product->discount = $this->getProductDiscount();
				$this->_product->delivery = $this->getProductDelivery();
				$this->_product->rating = $this->getProductRating();
				$this->_product->reviews = $this->getProductReviews();
				$this->_product->image = $this->getMainImages();
				$this->_product->status = 1;
				$all_images = $this->getMultipleImages();
				if(!empty($all_images)){	
					$this->_product->other_images = serialize($all_images);
				}
				
				/*foreach($all_images as $p_image){
					
					$image_field = 'image_'.$i;
					$this->_product->$image_field = $p_image;
					
					break;
					
				$i++;
				}*/
				
				//echo '<pre>'; print_r($this->_product); die;
				/*$this->_product->product_description = $this->getDescription();
				$this->_product->product_name = $this->getName();
                $this->_product->model_number = $this->getModel();   
				$this->_product->active = $this->getActive();      
                $this->_product->brands_brand_id = $this->getIdBrand();
                $this->_product->categories_category_id = $this->getIdCategory();
				$this->_product->emi = $this->getProductEMI();
				
				//echo '<pre>'; print_r($this->_product); die;
				// vinay new code ;
				 $this->_product->cod = $this->getCOD();
				 $this->_product->return_policy = $this->getReturnPolicy();
				 $this->_product->url = $this->getProducturl();
				 $this->_product->unique_id = $this->getProductUniqueId();//getMainImages
				 
				 
				  	 $image = str_replace('400x400','1100x1100',$this->getMainImages());
					 $image_name = time().'.jpg';
					if(!empty($image)){
						copy($image, 'uploads/products/'.$image_name);
						$this->_product->image = $image_name;
					}*/
					
//                $this->_product->tags = $this->tags;
				//echo '<pre>'; print_r($this->_product); die;
				// echo vinay;
				
                if ($this->_product->validate()) {            
                    $this->_product->save();
				//	echo '<pre>'; print_r($this->_product); die;
					
					echo $this->_product->_id.' This Product is saved<br>'; 
						
						// save product feature
						//$productFeatures = $this->getProductFeature();
						//$this->saveProductFeatureGroup($this->_product,$productFeatures);
						
						                       
//                    $this->saveFeatures($this->_product);
//                    $this->saveTags($this->_product);
					//$all_images = \common\components\CImage::getImages($this->_product->product_id);
					//$all_images = $this->getMultipleImages();
                   // if (!count($all_images))
				   
						//$all_images = $this->getMultipleImages();
					//	$this->saveMultipleImages($this->_product->product_id,$all_images);
						
                      // $this->saveImages($this->_product);
	                  // $this->saveSuppliers($this->_product);
//                    $this->saveProductmodelProduct($this->_product);
                    return $this->_product->_id;
                }else {
                    var_dump($this->_product->errors);
                    return false;
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                echo '<pre>';
                                print_r($message);exit;
                $subject = 'Error while saving the product';
                //Tools::alertMail($message, $subject);
            }
        }
        return false;       
    }
	
	
	public function saveProductFeatureGroup($product,$productFeatures){
				
				foreach($productFeatures as $productFeature){
					
					$feature_group = FeatureGroups::findOne(['name'=>  $productFeature['group']]);
					
					$max_sort_order = FeatureGroups::find()->orderBy('sort_order DESC')->one();
					
					if(!empty($max_sort_order)){
					
						$sort_order = $max_sort_order->sort_order + 1;
					
					}else{
					
						$sort_order = 1;
					}
					
					//echo '<pre>'; print_r($max_sort_order); 
					
					if ($feature_group == null) {
					  
					// Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
								$feature_group_model = new FeatureGroups();
								$feature_group_model->category_id = $product->categories_category_id;
								$feature_group_model->name = $productFeature['group'];
								$feature_group_model->sort_order = $sort_order;
								$feature_group_model->created = date('Y-m-d h:i:s');
								$feature_group_model->modified = date('Y-m-d h:i:s');
								$feature_group_model->save();
								
								$feature_group = FeatureGroups::findOne(['name'=>  $productFeature['group']]);
									
					}  
					
					if(isset($feature_group->id) && !empty($feature_group->id)){
						
						$this->saveProductFeatureData($product->product_id,$product->categories_category_id,$feature_group->id,$productFeature);
					
					}
				} 
				
				return $feature_group;
	}
	
	
	public function saveProductFeatureData($product_id, $category_id, $feature_group_id,$product_feature){
			
			$feature_data = '';
			foreach($product_feature['feature'] as $features){
					
					foreach($features as $feature){
						
							if(isset($feature['feature']) && !empty($feature['feature'])){
							
							
								$feature_data = Features::findOne(['name'=>  $feature['feature'], 'category_id' => $category_id, 'feature_group_id' => $feature_group_id]);
								
								$max_sort_order = Features::find()->orderBy('sort_order DESC')->one();
								
								if(!empty($max_sort_order)){
								
									$sort_order = $max_sort_order->sort_order + 1;
								
								}else{
								
									$sort_order = 1;
								}
							
									if ($feature_data == null) {
									  
									//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
												$feature_model = new Features();
												$feature_model->category_id = $category_id;
												$feature_model->feature_group_id = $feature_group_id;
												$feature_model->name = $feature['feature'];
												$feature_model->display_name = $feature['feature'];
												$feature_model->sort_order = $sort_order;
												$feature_model->status = 1;
												$feature_model->created = date('Y-m-d h:i:s');
												$feature_model->modified = date('Y-m-d h:i:s');
												$feature_model->save();
												
												$feature_data = Features::findOne(['name'=>  $feature['feature']]);
													
									}  
							
							if(isset($feature_data->id) && !empty($feature_data->id)){
							
								$this->saveProductFeatureValue($product_id,$feature_data->id,$product_feature);
							
							}
						}	
					} 
				}
			return $feature_data;
	
	}
	
	
	public function saveProductFeatureValue($product_id, $feature_id, $product_feature_values){
			
			$feature_value_data = '';
			foreach($product_feature_values['feature'] as $feature_values){
					
					foreach($feature_values as $feature_value){
						
						if(isset($feature_value['feature_value']) && !empty($feature_value['feature_value'])){
						
							$feature_value_multi = explode(',',$feature_value['feature_value']);
								//echo '<pre>'; print_r($feature_value_datas); die;
								foreach($feature_value_multi as $feature_value_value){
									//echo $feature_value_value.'<br>===>';
									if(isset($feature_value_value) && !empty($feature_value_value)){
									
									$feature_value_data = FeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_id]);
									
									if ($feature_value_data == null) {
									  
									//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
												$feature_value_model = new FeatureValues();
												$feature_value_model->feature_id = $feature_id;
												$feature_value_model->value = $feature_value_value;
												$feature_value_model->created = date('Y-m-d h:i:s');
												$feature_value_model->modified = date('Y-m-d h:i:s');
												$feature_value_model->save();
												
												$feature_value_data = FeatureValues::findOne(['value'=>  $feature_value_value]);
													
									}  
								//echo '<pre>'; print_r($feature_value_data); echo '<br>===>'.$feature_value_data->id;  die;
							
								if(isset($feature_value_data->id) && !empty($feature_value_data->id)){
							
									$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
							
								}
							}
						}
					}
				} 
			}
		//echo '<pre>'; print_r($feature_value_data); 
		return $feature_value_data;
	}
	
	
	public function saveProductFeature($product_id, $feature_id,$feature_value_id){
				
				if(!empty($product_id) && !empty($feature_id) && !empty($feature_value_id)){
						
						$product_feature = ProductFeature::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						
						if ($product_feature == null) {
						  
						//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
									$product_feature_model = new ProductFeature();
									$product_feature_model->id_product = $product_id;
									$product_feature_model->id_feature = $feature_id;
									$product_feature_model->id_feature_value = $feature_value_id;
									$product_feature_model->save();
									
									$feature_value_data = ProductFeature::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						}  
						//$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
				
				return $product_feature;
				}	
		}
	
	
	
	public function saveMultipleImages($product_id, $images){
			
			if(!empty($images)){
				$i=1;
				foreach($images as $image){
					 if(!empty($image)){
						 $product_image_model = new ProductImage;
						 
						 $image = str_replace('400x400','1100x1100',$image);
						 $image_name = time().$i.'.jpg';
						 $product_image_model->id_product = $product_id;
						 $product_image_model->image_path = $image;
						 $product_image_model->image = $image_name;
						 
						 $product_image_model->created = date('Y-m-d h:i:s');
						 $product_image_model->save();
						 
						 copy($image, 'uploads/products/'.$image_name);
					 }
				$i++;
				}
			}
	}
	

    public function saveBrand() {
        try {
//            $brand = Brand::model()->findByAttributes(array('name' => $this->brand));
            $brand = Brands::findOne(['brand_name'=>  $this->brand]);
            if ($brand == null) {
               //	echo $this->brand; die;
//                if (!empty($this->brand))
//                   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
						$brand_model = new Brands();
						$brand_model->brand_name = $this->brand;
						$brand_model->brand_description = $this->brand;
						$brand_model->status = 'active';
						$brand_model->created = date('Y-m-d h:i:s');
						$brand_model->save();
						
						// Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vinayverma158@gmail.com','Vinay');  
						 
						$brand = Brands::findOne(array('brand_name' => $this->brand));
				        
            }  
			//echo '<pre>'; print_r($brand); die;          
            return $brand;
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            //$subject = 'Error while saving brand in BaseProductImporter Class';        
            //Tools::alertMail($message, $subject, 'vikashkumar@girnarsoft.com');
        }
    }

    public function saveCategory() {
        try {
//            $category = Category::model()->findByAttributes(array('name' => $this->category));
            $category = Categories::findOne(['category_name'=>  $this->category]);
			
            if ($category == null) {
					    $category_model = new Categories();
						$category_model->category_name = $this->category;
						$category_model->category_description = $this->category;
						$category_model->category_status = 'active';
						$category_model->created = date('Y-m-d h:i:s');
						$category_model->save();
						
						 //Tools::alertMail("Create a new category named $this->category", "Create Category", 'vinayverma158@gmail.com','Vinay');
						 
						$category = Categories::findOne(['category_name'=>  $this->category]);
//                \common\components\Tools::alertMail("Create a new category named $this->category", "Create Category", 'vikashkumar@girnarsoft.com');
            }
            return $category;
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            //$subject = 'Error while saving category in BaseProductImporter Class';
            //Tools::alertMail($message, $subject, 'vikashkumar@girnarsoft.com');
        }
    }

    public function saveFeatures($product) {
        $product->features = $this->features;
        $product->saveFeatures();
    }

    public function saveTags($product) {
        $product->tags = $this->tags;
        $product->saveTags(true);
    }

    public function saveImages($product) {
        $product->images = $this->images;
        $product->saveImage();
    }

    public function saveSuppliers($product) {
		//echo '<pre>'; print_r($this->suppliers); die;
        foreach ($this->suppliers as $sup) {
            //get supplier id
            $supplier_name = (string) $sup['storename'];
            $supplier = Supplier::model()->findByAttributes(array('name' => (string) $supplier_name));
            if ($supplier == null) {
                $supplier = new Supplier();
                $supplier->name = $supplier_name;
                $supplier->active = 1;
                $supplier->save();
            }
            
            $supplier_product = SupplierProduct::model()->findByAttributes(array('id_product' => $product->id_product, 'id_supplier' => $supplier->id_supplier));
            if ($supplier_product == null)
                $supplier_product = new SupplierProduct ();
            $sup_url = (string) $sup['sellerurl'];
            if ($sup && !stristr($sup_url, 'mysmartprice')) {
                $supplier_product->id_supplier = (int) $supplier->id_supplier;
                $supplier_product->id_product = (int) $product->id_product;
                $supplier_product->url = (string) $sup['sellerurl'];
                $supplier_product->price = isset($sup['price']) ? (string) $sup['price'] : 0;
                $supplier_product->stock = (isset($sup['stock']) AND $sup['stock']) ? $sup['stock']: "";
                $supplier_product->deal_price = (isset($sup['deal_price']) AND $sup['deal_price']) ? $sup['deal_price'] : 0; 
                $supplier_product->save();
            }
        }
    }

    public function saveProductmodelProduct($product) {
        echo "\n id_product::" . $product->id_product . "\t model_number" . $this->product_model;
        if ($this->product_model) {
            $product_model = $this->saveProductModel($product);

            if ($product_model->id_model)
                ProductModel::saveProductmodelproducts($product_model->id_model, $product->id_product);
        }
    }

    private function saveProductModel($product) {
        $link_rewrite = \common\components\Tools::link_rewrite($this->product_model);
        $product_model = ProductModel::model()->findByAttributes(array('link_rewrite' => $link_rewrite, 'id_category' => $product->id_category));
        if ($product_model == NULL)
            $product_model = new ProductModel();

        $product_model->name = $this->product_model;
        $product_model->display_name = $this->product_model;
        $product_model->link_rewrite = $link_rewrite;
        $product_model->id_brand = $product->id_brand;
        $product_model->id_category = $product->id_category;

        if ($product_model->getIsNewRecord())
            $product_model->date_add = date('Y-m-d');
        $product_model->date_update = date('Y-m-d');


        $product_model->save();

        return $product_model;
    }

    public function saveVideos() {
        $date = date('Y-m-d');
        $insert_video = "INSERT INTO product_video (id_product,title,video,short_description,description,upload_date,deleted,content_type)
                                VALUES (:id,:title,:video,:short_desc,:desc,:date,:del,:content_type)";
        $cmd = Yii::app()->db->createCommand($insert_video);
        $del = 0;

        $id_product = $this->video['id_product'];
        $content_type = $this->video['content_type'];
        $desc = $this->video['description'];
        $short_desc = "";
        $title = $this->video['title'];
        if (!$title)
            $title = Product::model()->findByPk($id_product)->name;

        $video = $this->video['video'];

        if ($this->getVideos($id_product, $video))
            return;

        $cmd->bindParam(':id', $id_product);
        $cmd->bindParam(':title', $title);
        $cmd->bindParam(':video', $video);
        $cmd->bindParam(':short_desc', $short_desc);
        $cmd->bindParam(':desc', $desc);
        $cmd->bindParam(':date', $date);
        $cmd->bindParam(':del', $del);
        $cmd->bindParam(':content_type', $content_type);

        $cmd->execute();
    }

    public function getVideos($id_product, $curr_video) {
        $query = "SELECT video from product_video where id_product=:id";
        $con = Yii::app()->db;
        $flag = 0;
        $cmd = $con->createCommand($query);
        $cmd->bindParam(':id', $id_product);
        $result = $cmd->queryAll();

        foreach ($result as $video) {
            if (!strcasecmp(trim($video['video']), trim($curr_video))) {

                return TRUE;
            }
        }
        return FALSE;        
    }

}

?>
