<?php

/**
 * Description of BaseProductImporter
 *
 * @author Vikash
 */
 

namespace backend\models;

use common\models\ProductFeatureMappings;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Brands;
use common\models\Categories;
use common\models\ProductImage;
use common\models\FeatureGroups;
use common\models\Features;
use common\models\FeatureValues;
use common\models\ProductFeature;
use common\models\ProductsSuppliers;
use common\components\CImage;
use backend\models\Logs;
use backend\models\ProductUrls;
use common\components\AmazonECS;
use common\components\Aws;

class BaseProductImporter {
    
    private $_product;
    private $id_category;
    private $name;
    private $description;
    private $model;
    private $lowest_price;
    private $length;
    private $width;
    private $height;
    private $package_content;
    private $warranty;
    private $featured;
    private $link_rewrite;
    private $brand;
    private $category;
    private $features = array();
    private $images = array();
    private $tags = array();
    private $suppliers = array();
    private $active;
    private $video = array();
    private $launch_date;
    private $product_model;
    private $reference_number;
    private $suppliercount;
    private $keyField;
    private $keyValue;
    private $instock = 1;

    public function __construct($category_name, $name = '', $key_field = NULL, $key_value = NULL) {
        $this->setName($name);
        $this->setCategory($category_name);
        $this->setKeyField($key_field);
        $this->setKeyValue($key_value);        
        $this->init();
    }
    
    private function init() {
        $category = $this->saveCategory();
		//echo '<pre>'; print_r($category); die;
        if (is_object($category)) {
            $this->category_id = $category->category_id;
//            if ($this->keyField != NULL)            
//                $this->_product = Product::model()->findByAttributes(array($this->keyField => $this->keyValue, 'id_category' => $category->id_category));            
//            else
//                $this->_product = Product::model()->findByAttributes(array('name' => $this->name));
//                //$this->_product = Product::model()->findByAttributes(array('name' => $this->name, 'id_category' => $category->id_category));
//            if (!is_object($this->_product)) {
//                $this->_product = new Product();
//                $this->_product->active = 1;
//            }
        }    
        $this->_product = new Products();
        $this->_product->active = 'inactive';
		
        
    }
    
    public function setName($name) {
        $this->name = $name;
    }

    public function setdescription($description) {
        $this->description = $description;
    }

    public function setModel($model) {
        $this->model = $model;
    }

    public function setLowestPrice($lowest_price) {
        $this->lowest_price = $lowest_price;
    }

    /**public function setWidth($width) {
        if (!is_string($width))
            throw new CException("Invalid width");
        $this->width = $width;
    }

    public function setHeight($height) {
        if (!is_string($height))
            throw new CException("Invalid height");
        $this->height = $height;
    } **/

    public function setPackageContent($package_content) {
        
        $this->package_content = $package_content;
    }

    public function setWarranty($warranty) {
        $this->package_content = $warranty;
    }

    public function setFeatured($featured) {
            $this->featured = $featured;
    }

   public function setLinkRewrite($name) {
         $this->link_rewrite = $name;
    } 

    public function setBrand($brand) {
        $this->brand = $brand;
    }

    public function setTags($tag) {
         $this->tags = $tag;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function setFeatures($features) {
        $this->features = $features;
    }

    public function setImages($images) {
		$this->images = $images;
    }

    public function setSuppliers($suppliers) {
        $this->suppliers = $suppliers;
    }

    public function setActive($active=1) {
        $this->active = $active;
    }

    public function setVideos($video) {
        $this->video = $video;
    }

    public function setLaunchDate($launch_date) {
        $this->launch_date = $launch_date;
    }

    public function setProductModel($name) {
        $this->product_model = $name;
    }

    public function setReferenceNumber($reference_number) {
        //if (!is_string($reference_number))
        //    throw new CException("Invalid Reference Number");
        $this->reference_number = $reference_number;
    }

    public function setSupplierCount($suppliercount=1) {
        $this->suppliercount = $suppliercount;
    }

    public function setKeyField($keyfield) {
        $this->keyField = $keyfield;
    }

    public function setKeyValue($keyvalue) {
        $this->keyValue = $keyvalue;
    }
	
	
	
	
	
	
    
    public function getName() {
        return (isset($this->name) AND $this->name) ? $this->name : $this->_product->product_name;
    }
    
    public function getModel() {
        return (isset($this->model) AND $this->model) ? $this->model : $this->_product->model_number;
    }
    
    public function getLowestPrice() {
        return (isset($this->lowest_price) AND $this->lowest_price) ? $this->lowest_price : 0;
    }
    
    public function getDescription() {
        return (isset($this->description) AND $this->description) ? $this->description : '';
    }
   /** 
    public function getHeight() {
        return (isset($this->height) AND $this->height) ? $this->height : $this->_product->height;
    }
    
    public function getLength() {
        return (isset($this->length) AND $this->length) ? $this->length : $this->_product->length;
    } 
    **/
    public function getLinkRewrite() {
        return (isset($this->link_rewrite) AND $this->link_rewrite) ? $this->link_rewrite : $this->_product->link_rewrite;
    }
    
    public function getWarranty() {
        return (isset($this->warranty) AND $this->warranty) ? $this->warranty : $this->_product->warranty;
    }
    
    public function getWidth() {
        return (isset($this->width) AND $this->width) ? $this->width : $this->_product->width;
    }
    
    public function getActive() {
        return isset($this->active) ? $this->active : $this->_product->active;
    }
    
    public function getDateAdd() {
        return isset($this->_product->date_add) ? $this->_product->date_add : time();
    }
    
    public function getDateUpdate() {
        return date('Y-m-d');
    }
    
    public function getDateLaunch() {
        return (isset($this->launch_date) AND $this->launch_date) ? $this->launch_date : $this->_product->date_launch;
    }
    
    public function getReferenceNumber() {
        return (isset($this->reference_number) AND $this->reference_number) ? $this->reference_number : $this->_product->reference_number;
    }
    
    public function getSupplierCount() {
        return (isset($this->suppliercount) AND $this->suppliercount) ? $this->suppliercount : $this->_product->suppliercount;
    }
    
    public function getIdBrand() {        
        $brand = $this->saveBrand();
		//echo '<pre>'; print_r($brand); die;
        return (isset($brand->brand_id) AND $brand->brand_id) ? $brand->brand_id : $this->_product->brands_brand_id;        
    }
    
    public function getIdCategory() {
        return (isset($this->category_id) AND $this->category_id) ? $this->category_id : $this->_product->categories_category_id;
    }
	
	
	// vinay New Fields
	
	 public function setMainImages($image) {
        $this->main_image = $image;
    }
	
	public function getMainImages() {
        return (isset($this->main_image) AND $this->main_image) ? $this->main_image : $this->_product->image;
    }
	
	 public function setCOD($cod) {
        $this->cash_on_delivery = $cod;
    }
	
	public function getCOD() {
        return (isset($this->cash_on_delivery) AND $this->cash_on_delivery) ? $this->cash_on_delivery : $this->_product->cod;
    }
	
	 public function setReturnPolicy($return_policy) {
        $this->return_policy = $return_policy;
    }
	
	public function getReturnPolicy() {
        return (isset($this->return_policy) AND $this->return_policy) ? $this->return_policy : $this->_product->return_policy;
    }
	
	 public function setProducturl($url) {
        $this->url = $url;
    }
	
	public function getProducturl() {
        return (isset($this->url) AND $this->url) ? $this->url : $this->_product->url;
    }
	
	 public function setProductUniqueId($product_unique_id) {
        $this->unique_id = $product_unique_id;
    }
	
	public function getProductUniqueId() {
        return (isset($this->unique_id) AND $this->unique_id) ? $this->unique_id : $this->_product->unique_id;
    }
	
	
	public function setMultipleImages($all_images) {
        $this->all_images = $all_images;
    }
	
	public function getMultipleImages() {
        return (isset($this->all_images) AND $this->all_images) ? $this->all_images : '';
    }
	
    
	public function setProductFeature($product_features) {
        $this->productFeature = $product_features;
    }
	
	public function getProductFeature() {
        return (isset($this->productFeature) AND $this->productFeature) ? $this->productFeature : '';
    }
	
	
	 
	public function setProductEMI($emi) {
        $this->emi = $emi;
    }
	
	public function getProductEMI() {
        return (isset($this->emi) AND $this->emi) ? $this->emi : '';
    }

    public function setProductColors($colors)
    {
        $this->colors = $colors;
    }

    public function getProductColors()
    {
        return (isset($this->colors) AND $this->colors) ? $this->colors : '';
    }

    public function setProductSizes($sizes)
    {
        $this->sizes = $sizes;
    }

    public function getProductSizes()
    {
        return (isset($this->sizes) AND $this->sizes) ? $this->sizes : '';
    }

    public function setProductOffers($offers)
    {
        $this->offers = $offers;
    }

    public function getProductOffers()
    {
        return (isset($this->offers) AND $this->offers) ? $this->offers : '';
    }

    public function setProductInstock($instock)
    {
        $this->instock = $instock;
    }

    public function getProductInstock()
    {
        return $this->instock;
    }
	
	public function setProductDiscount($discount) {
        $this->discount = $discount;
    }
	
	public function getProductDiscount() {
        return (isset($this->discount) AND $this->discount) ? $this->discount : '';
    }
	
	public function setProductRating($rating) {
        $this->rating = $rating;
    }
	
	public function getProductRating() {
        return (isset($this->rating) AND $this->rating) ? $this->rating : '';
    }
	
	public function setProductDelivery($product_delivery) {
        $this->delivery = $product_delivery;
    }
	
	public function getProductDelivery() {
        return (isset($this->delivery) AND $this->delivery) ? $this->delivery : '';
    }
	
    public function setProductReviews($reviews) {
        $this->reviews = $reviews;
    }
	
	public function getProductReviews() {
        return (isset($this->reviews) AND $this->reviews) ? $this->reviews : '';
    }
	
	public function setProductShipping($shipping) {
        $this->shipping = $shipping;
    }
	
	public function getProductShipping() {
        return (isset($this->shipping) AND $this->shipping) ? $this->shipping : '';
    }
    
	public function setProductOriginalPrice($original_price) {
        $this->original_price = $original_price;
    }
	
	public function getProductOriginalPrice() {
        return (isset($this->original_price) AND $this->original_price) ? $this->original_price : '';
    }
    
	
	public function setProductRatingUserCount($rating_user_count) {
        $this->rating_user_count = $rating_user_count;
    }
	
	public function getProductRatingUserCount() {
        return (isset($this->rating_user_count) AND $this->rating_user_count) ? $this->rating_user_count : '';
    }
   
	public function setProductUrlId($product_url_id) {
		
		$this->product_url_id = $product_url_id;
    }
	
	public function getProductUrlId() {
        return $this->product_url_id;
    }
     
// vinay 13 oct	 
    public function setQueueMessageData($queueMessageData = null) {
		$this->queueMessageData = $queueMessageData;
	}
	
	public function getQueueMessageData() {
		return $this->queueMessageData;
    }
    
	
	
    public function save($refresh=false, $is_product_log = false) {  
        if (isset($this->_product)) {
            if ($this->_product->product_id != NULL && $refresh === false) {
                if ($is_product_log)
                    return $this->_product->product_id;
                return FALSE;
            }

		
		$exit_product =	Products::find()->where(['store_product_name' => $this->getName(), 'categories_category_id'=>$this->getIdCategory(),'brands_brand_id'=>$this->getIdBrand()])->one();
		//echo '<pre>exit_product=>'; print_r($exit_product);
		if(empty($exit_product)){
            try { 
				$productPrice = $this->getLowestPrice();
				$productName = $this->getName();
				if(!empty($productPrice) && !empty($productName)){
					$this->_product->store_name = 'flipkart';
					$this->_product->store_id = 1;
					$this->_product->lowest_price = $this->getLowestPrice();
					$this->_product->product_description = $this->getDescription();
					$this->_product->product_name = $this->getName();
					$this->_product->store_product_name = $this->getName();
					$this->_product->model_number = $this->getModel();   
					$this->_product->active = $this->getActive();      
					$this->_product->brands_brand_id = $this->getIdBrand();
					$this->_product->categories_category_id = $this->getIdCategory();
					$this->_product->emi = $this->getProductEMI();
					 $this->_product->cod = $this->getCOD();
					 $this->_product->return_policy = $this->getReturnPolicy();
					 $this->_product->url = $this->getProducturl();
					 $this->_product->unique_id = $this->getProductUniqueId();//getMainImages
					 
					 
					 $this->_product->created = date('Y-m-d h:i:s');
					 $this->_product->updated = date('Y-m-d h:i:s');
					 $this->_product->discount = $this->getProductDiscount();
					  
					 $this->_product->rating = $this->getProductRating();
					 $this->_product->delivery = $this->getProductDelivery();
					 $this->_product->reviews = $this->getProductReviews();
					 $this->_product->shipping = $this->getProductShipping();
					 
					 $this->_product->original_price = $this->getProductOriginalPrice();
					 $this->_product->rating_user_count = $this->getProductRatingUserCount();
					 
					 $this->_product->is_approved = 0;
					 
						$main_image = $this->getMainImages();
						
						if(!empty($main_image)){
							
							 $image_name = time().'.jpg';
							 $image = str_replace('400x400','1100x1100',$this->getMainImages());
							 $this->_product->image = $image_name;
							 $this->_product->image_path = $image;
							 
							/**if(!empty($image)){
								
								$path = Yii::getAlias('@frontend') .'/web/';
								
								$categoryDetail = Categories::findOne(['category_id'=>  $this->_product->categories_category_id]);
								$brandDetail = Brands::findOne(['brand_id'=>  $this->_product->brands_brand_id]);
								
								$folder_path = $path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug;
								if (!file_exists($folder_path)) {
									mkdir($folder_path, 0755, true);
								}
								
								copy($image, $folder_path.'/'.$image_name);
								$this->_product->image = $image_name;
								
								
								$thumImageSizes = array('150x200','300x400');
								foreach($thumImageSizes as $size){
									$sizeArr = explode('x',$size);
									
										if (!file_exists($folder_path.'/'.$size)) {
											mkdir($folder_path.'/'.$size, 0755, true);
										}
									$destFile = $folder_path.'/'.$size.'/'.$image_name;
									$mainImage = $folder_path.'/'.$image_name;
									$objImageResize = new \common\components\CImage();
									$response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
								}
								
							} **/
						} //die;
						
						
					//echo '<pre>product detail before save=>'; print_r($this->_product); 	
					//$productFeatures = $this->getProductFeature();
					
					$this->_product->save(); 
						//echo '<pre>'; print_r($this->_product); die;
							$ProductUrlsModel = ProductUrls::findOne($this->getProductUrlId());
							//echo '<pre>ProductUrlsModel'; print_r($ProductUrlsModel);
							$ProductUrlsModel->status = 1;
							$ProductUrlsModel->save(false);
							
							// vinay 13 oct
							$queueMessageData = $this->getQueueMessageData();
							if(!empty($queueMessageData)){
								$aws = new \common\components\Aws();
								$aws = $aws->deleteQueueMessage($queueMessageData,'product_scraper');
							}
							/**$queueMessage  = $this->getQueueMessageData();
							echo '<pre>===>'; print_r($queueMessage); die;
							if(!empty($queueMessage)){
								$this->deleteQueueMessage($queueMessage);
							} **/
							
							
							$this->saveProductSupplierData($this->_product);
							
							
							// save product feature
							$productFeatures = $this->getProductFeature();
							//echo '<pre>'; print_r($productFeatures);
							$this->saveProductFeatureGroup($this->_product,$productFeatures);
							
							
							$this->saveProductFeature($this->_product->product_id,$this->_product->categories_category_id,$productFeatures);
						
							$all_images = $this->getMultipleImages();
							$this->saveMultipleImages($this->_product->product_id,$all_images);
							
						
						return $this->_product->product_id;
				}
				
            } catch (Exception $e) {
                $message = $e->getMessage();
                echo '<pre>';
                                print_r($message);exit;
                $subject = 'Error while saving the product';
                //Tools::alertMail($message, $subject);
            }
		  }
        }
        return false;       
    }
	
	
	// this function for save products data  in product supllier table
	public function saveProductSupplierData($productData){
		//echo '<pre>'; print_r($productData); die;
				$ProductsSuppliersModel = new ProductsSuppliers();
				
				// we are checking exiting product in product supplier table
				$exit_product_supplier = $ProductsSuppliersModel::find()->where(['product_id' => $productData->product_id, 'store_id'=>$productData->store_id])->one();
				
				// if this record not are in our product suppllier then we will be do new entry 
				if(empty($exit_product_supplier)){
						$ProductsSuppliersModel->product_id = $productData->product_id;
						$ProductsSuppliersModel->store_id = $productData->store_id;
						$ProductsSuppliersModel->store_name = $productData->store_name;
						$ProductsSuppliersModel->price = $productData->lowest_price;
						$ProductsSuppliersModel->emi = $productData->emi;
						$ProductsSuppliersModel->cod = $productData->cod;
						$ProductsSuppliersModel->return_policy = $productData->return_policy;
						$ProductsSuppliersModel->url = $productData->url;
						$ProductsSuppliersModel->discount = $productData->discount;
				 		$ProductsSuppliersModel->rating = $productData->rating;
						$ProductsSuppliersModel->delivery = $productData->delivery;
						$ProductsSuppliersModel->reviews = $productData->reviews;
						$ProductsSuppliersModel->shipping = $productData->shipping;
						$ProductsSuppliersModel->active = 'active';
						
						$ProductsSuppliersModel->original_price = $productData->original_price;
						$ProductsSuppliersModel->rating_user_count = $productData->rating_user_count;
						$ProductsSuppliersModel->created = date('Y-m-d h:i:s');
						$ProductsSuppliersModel->save();
						//echo '<pre>exit_product_supplier'; print_r($ProductsSuppliersModel); die;
						$exit_product_supplier = $ProductsSuppliersModel::find()->where(['product_id' => $productData->product_id, 'store_id'=>$productData->store_id])->one();
				
				}
				
				//return $exit_product_supplier;
		
		
	
	}
	
	
	
	// save product features to prodcuts data
	public function saveProductFeatureGroup($product,$productFeatures){
				if(!empty($productFeatures)){
					
					foreach($productFeatures as $productFeature){
					
					$feature_group = FeatureGroups::findOne(['name'=>  $productFeature['group']]);
					
					$max_sort_order = FeatureGroups::find()->orderBy('sort_order DESC')->one();
					
					if(!empty($max_sort_order)){
					
						$sort_order = $max_sort_order->sort_order + 1;
					
					}else{
					
						$sort_order = 1;
					}
					
					//echo '<pre>'; print_r($max_sort_order); 
					
					if ($feature_group == null) {
					  
					 //Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
								$feature_group_model = new FeatureGroups();
								$feature_group_model->category_id = $product->categories_category_id;
								$feature_group_model->name = $productFeature['group'];
								$feature_group_model->sort_order = $sort_order;
								$feature_group_model->created = date('Y-m-d h:i:s');
								$feature_group_model->modified = date('Y-m-d h:i:s');
								$feature_group_model->save();
								
								$feature_group = FeatureGroups::findOne(['name'=>  $productFeature['group']]);
									
					}  
					
					if(isset($feature_group->id) && !empty($feature_group->id)){
						
						$this->saveProductFeatureData($product->product_id,$product->categories_category_id,$feature_group->id,$productFeature);
					
					}
				} 
				
				return $feature_group;
				
				}
				
	}
	
	
	public function saveProductFeatureData($product_id, $category_id, $feature_group_id,$product_feature){
			
			if(!empty($product_feature)){
				$feature_data = '';
					foreach($product_feature['feature'] as $features){
							
							foreach($features as $feature){
								
									if(isset($feature['feature']) && !empty($feature['feature'])){
									
									
									//	$feature_data = Features::findOne(['name'=>  $feature['feature'], 'category_id' => $category_id, 'feature_group_id' => $feature_group_id]);
										
										$feature_data = Features::findOne(['name'=>  $feature['feature'], 'category_id' => $category_id]);
										//echo '<pre>'; print_r($feature_data); die;
										$max_sort_order = Features::find()->orderBy('sort_order DESC')->one();
										
										if(!empty($max_sort_order)){
										
											$sort_order = $max_sort_order->sort_order + 1;
										
										}else{
										
											$sort_order = 1;
										}
										
										
									
											if (empty($feature_data)) {
											  
											//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
														$feature_model = new Features();
														$feature_model->category_id = $category_id;
														$feature_model->feature_group_id = $feature_group_id;
														$feature_model->name = $feature['feature'];
														$feature_model->display_name = $feature['feature'];
														$feature_model->sort_order = $sort_order;
														$feature_model->status = 1;
														$feature_model->created = date('Y-m-d h:i:s');
														$feature_model->modified = date('Y-m-d h:i:s');
														$feature_model->save();
														
														$feature_data = Features::findOne(['name'=>  $feature['feature'], 'category_id' => $category_id]);
															
											}  
									
									if(!empty($feature_data->id)){
										
										if(isset($feature['feature_value']) && !empty($feature['feature_value'])){
											//echo 'feature_id=>'.$feature_data->id.'<br>';
											$this->saveProductFeatureValue($product_id,$feature_data->id,$feature['feature_value']);
										}
										
									
									}
								}	
							} 
						}
					return $feature_data;
			}
			
	
	}
	
	
	public function saveProductFeatureValue($product_id, $feature_id, $product_feature_values){
			$feature_value_data = '';
			if(!empty($product_feature_values)){
						
							$feature_value_multi = explode(',',$product_feature_values);
							
								foreach($feature_value_multi as $feature_value_value){
									//echo $feature_value_value.'<br>===>';
									if(!empty($feature_value_value)){
											
											$feature_value_data = FeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_id]);
											
											if (empty($feature_value_data)) {
											  
											//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
														$feature_value_model = new FeatureValues();
														$feature_value_model->feature_id = $feature_id;
														$feature_value_model->value = $feature_value_value;
														$feature_value_model->display_name = $feature_value_value;
														$feature_value_model->created = date('Y-m-d h:i:s');
														$feature_value_model->save();
														
														$feature_value_data = FeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_id]);
															
											
											if(!empty($feature_value_data->id)){
													//echo $feature_value_data->id; die;
													//echo 'feature_value_id=>'.$feature_value_data->id.'<br>';
													//$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
									
										} //echo 'fail'; die;
									
									}  
								//echo '<pre>'; print_r($feature_value_data); echo '<br>===>'.$feature_value_data->id;  die;
							
								
							}
						}
					}
			
		//echo '<pre>'; print_r($feature_value_data); 
		return $feature_value_data;
	}
	
	
	
	public function saveProductFeature($product_id, $category_id,$productFeaturesData){
		//echo $product_id.'===>'.$category_id.'===>';
		
			if(!empty($productFeaturesData)){
					foreach($productFeaturesData as $productFeatures){
							
							$feature_group = FeatureGroups::findOne(['name'=>  $productFeatures['group']]);
							//echo '<pre>first record'; print_r($productFeatures);
							//echo '<pre>second record'; print_r($feature_group);
							if(isset($productFeatures['feature'][0])){
								foreach($productFeatures['feature'][0] as $features){
									//echo '<pre>third record'; print_r($features);
									//foreach($productFeature as $features){
										
											if(isset($features['feature']) && isset($features['feature_value'])){
												
												$feature_data = Features::findOne(['name'=>  $features['feature'], 'category_id' => $category_id, 'feature_group_id' => $feature_group->id]);
												//echo '<pre>fourth record'; print_r($feature_data);
												$feature_value_multi = explode(',',$features['feature_value']);
												
												foreach($feature_value_multi as $feature_value_value){
													//echo $feature_value_value.'<br>===>';
													if(!empty($feature_value_value) && !empty($feature_data)){
														$feature_value_data = FeatureValues::findOne(['value'=>  $feature_value_value, 'feature_id' => $feature_data->id]);
														
														if(!empty($feature_data) && !empty($feature_value_data)){
															//echo $feature_data->id.'===>'.$feature_value_data->id.'<br>';
															$product_feature_model = new ProductFeature();
															$product_feature_model->id_product = $product_id;
															$product_feature_model->id_feature = $feature_data->id;
															$product_feature_model->id_feature_value = $feature_value_data->id;
															$product_feature_model->save();
														}
														//echo '<pre>five record'; print_r($feature_value_data);
													}
												}
											}
								//	}
									
								}
						}
					}
				//die('exit');
			}
			return true;
		
	}
	
	/**
	public function saveProductFeature($product_id, $feature_id,$feature_value_id){
				//echo $product_id.'===>'.$feature_id.'===>'.$feature_value_id.'<br>';
				if(!empty($product_id) && !empty($feature_id) && !empty($feature_value_id)){
						
						$product_feature_data = ProductFeature::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						
						if (empty($product_feature_data)) {
						  
						//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
									$product_feature_model = new ProductFeature();
									$product_feature_model->id_product = $product_id;
									$product_feature_model->id_feature = $feature_id;
									$product_feature_model->id_feature_value = $feature_value_id;
									$product_feature_model->save();
									
									$product_feature_data = ProductFeature::findOne(['id_product'=>  $product_id, 'id_feature' => $feature_id,'id_feature_value'=> $feature_value_id]);
						}  
						//$this->saveProductFeature($product_id, $feature_id,$feature_value_data->id);
				
					return $product_feature_data;
				}	
		} **/
	
	
	
	public function saveMultipleImages($product_id, $images){
			
			if(!empty($images)){
				$i=1;
				foreach($images as $image){
					 if(!empty($image)){
						 $product_image_model = new ProductImage;
						 
						 $image = str_replace('400x400','1100x1100',$image);
						 $image_name = time().$i.'.jpg';
						 $product_image_model->id_product = $product_id;
						 $product_image_model->image_path = $image;
						 $product_image_model->image = $image_name;
						 
						 $product_image_model->created = date('Y-m-d h:i:s');
						 $product_image_model->save();
						
						 //copy($image, 'uploads/products/'.$image_name);
						/**	$path = Yii::getAlias('@frontend') .'/web/';
							copy($image, $path.'uploads/products/'.$category_slug.'/'.$brand_slug.'/'.$image_name);
							
								$thumImageSizes = array('150x200','300x400');
								foreach($thumImageSizes as $size){
									$sizeArr = explode('x',$size);
									
									$destFile = $path.'uploads/products/'.$category_slug.'/'.$brand_slug.'/'.$size.'/'.$image_name;
									$objImageResize = new \common\components\CImage();
									$response = $objImageResize->imageResize($image,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
								} **/
					 }
				$i++;
				}
			}
	}
	

    public function saveBrand() {
        try {
//            $brand = Brand::model()->findByAttributes(array('name' => $this->brand));
            $brand = Brands::findOne(['brand_name'=>  $this->brand]);
            if ($brand == null) {
               //	echo $this->brand; die;
//                if (!empty($this->brand))
                 // Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vinayverma158@gmail.com','Vinay');   
						$brand_model = new Brands();
						$brand_model->brand_name = $this->brand;
						$brand_model->brand_description = $this->brand;
						$brand_model->status = 'active';
						$brand_model->created = date('Y-m-d h:i:s');
						$logsModel = new Logs();
						$brand_model->folder_slug = $logsModel->getSlugFromName($this->brand);
						$brand_model->save();
						
						$brand = Brands::findOne(array('brand_name' => $this->brand));
				        
            }  
			//echo '<pre>'; print_r($brand); die;          
            return $brand;
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            //$subject = 'Error while saving brand in BaseProductImporter Class';        
            //Tools::alertMail($message, $subject, 'vikashkumar@girnarsoft.com');
        }
    }

    public function saveCategory() {
        try {
//            $category = Category::model()->findByAttributes(array('name' => $this->category));
            $category = Categories::findOne(['category_name'=>  $this->category]);
			
            if ($category == null) {
				
						
					    $category_model = new Categories();
						$category_model->category_name = $this->category;
						$category_model->category_description = $this->category;
						$category_model->status = 'active';
						$category_model->created = date('Y-m-d h:i:s');
						$logsModel = new Logs();
						$category_model->folder_slug = $logsModel->getSlugFromName($this->category);
						$category_model->save();
						
						$category = Categories::findOne(['category_name'=>  $this->category]);
						
						//Tools::alertMail("Create a new category named $this->category", "Create Category", 'vinayverma158@gmail.com','Vinay');   
//                \common\components\Tools::alertMail("Create a new category named $this->category", "Create Category", 'vikashkumar@girnarsoft.com');
            }
            return $category;
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            //$subject = 'Error while saving category in BaseProductImporter Class';
            //Tools::alertMail($message, $subject, 'vikashkumar@girnarsoft.com');
        }
    }

    public function saveFeatures($product) {
        $product->features = $this->features;
        $product->saveFeatures();
    }

    public function saveTags($product) {
        $product->tags = $this->tags;
        $product->saveTags(true);
    }

    public function saveImages($product) {
        $product->images = $this->images;
        $product->saveImage();
    }

    public function updateProductSupplier(ProductsSuppliers $productsSupplier)
    {
        error_log("product supplier data...");
        error_log(var_export($this->getProductInstock(), true));
        error_log(var_export($this->getName(), true));
        error_log(var_export($this->getLowestPrice(), true));
        if ($this->getProductInstock()) {
            if (!empty($this->getName()) && !empty($this->getLowestPrice())) {
                $productsSupplier->price = $this->getLowestPrice();
                $productsSupplier->emi = $this->getProductEMI() ? substr($this->getProductEMI(), 0, 255) : '';
                $productsSupplier->colors = $this->getProductColors();
                $productsSupplier->sizes = $this->getProductSizes();
                $productsSupplier->offers = $this->getProductOffers();
                $productsSupplier->instock = $this->getProductInstock();
                $productsSupplier->cod = $this->getCOD();
                $productsSupplier->return_policy = $this->getReturnPolicy();
                $productsSupplier->discount = $this->getProductDiscount();
                $productsSupplier->rating = $this->getProductRating();
                $productsSupplier->delivery = $this->getProductDelivery() ? substr($this->getProductDelivery(), 0, 255) : '';
                $productsSupplier->reviews = $this->getProductReviews();
                $productsSupplier->shipping = $this->getProductShipping();
                $productsSupplier->active = 'active';
                $productsSupplier->original_price = $this->getProductOriginalPrice();
                $productsSupplier->rating_user_count = $this->getProductRatingUserCount();
                $productsSupplier->updated = date('Y-m-d h:i:s');
            } else {
                throw  new Exception("Invalid data while updating product supplier data");
            }
        } else {
            $productsSupplier->instock = $this->getProductInstock();
        }
        if (!$productsSupplier->save()) {
            error_log(var_export($productsSupplier->errors, true));
            throw  new Exception("Invalid data while updating product supplier data");
        } else {
            $this->updateProductFeatures($productsSupplier->product, $productsSupplier, $this->getProductFeature());
        }
    }

    private function updateProductFeatures($product, $productsSupplier, $productFeaturesData)
    {
        $baseProductSupplier = $product->baseProductSupplier;
        error_log(var_export("--base Product Supplier--", true));
        error_log(var_export($baseProductSupplier->id, true));
        error_log(var_export("--compare Product Supplier--", true));
        error_log(var_export($productsSupplier->id, true));
        if ($product->feature_scrapped == 0 && !empty($baseProductSupplier) && ($baseProductSupplier->id == $productsSupplier->id)) {
            $product->deleteAllProductFeatureValues();
            $this->saveIndividualProductFeatures($product, $productFeaturesData);
            $product->feature_scrapped = 1;
            $product->save(false);
        }
    }

    public function saveIndividualProductFeatures($product, $productFeaturesData)
    {
        foreach ($productFeaturesData as $productFeatures) {

            if (isset($productFeatures['feature'][0])) {
                foreach ($productFeatures['feature'][0] as $features) {

                    if (isset($features['feature']) && isset($features['feature_value'])) {

                        $model = ProductFeatureMappings::find()->where([
                            'id_category' => $product->categories_category_id,
                            'store_feature_name' => trim($features['feature'])
                        ])->one();

                        if (!is_null($model)) {
                            $feature_data = $model->feature;
                            $feature_value_multi = explode(',', $features['feature_value']);
                            foreach ($feature_value_multi as $feature_value_value) {
                                if (!empty($feature_value_value) && !empty($feature_data)) {
                                    $feature_value_data = $this->saveProductFeatureValueInDB($feature_data, $feature_value_value);
                                    if (!is_null($feature_value_data)) {
                                        $this->saveProductFeatureDB($product->product_id, $feature_data->id, $feature_value_data->id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function saveProductFeatureValueInDB(Features $featureDB, $featureValue)
    {
        $feature_value_data = FeatureValues::findOne(['value' => $featureValue, 'feature_id' => $featureDB->id]);
        if (is_null($feature_value_data)) {
            $feature_value_model = new FeatureValues();
            $feature_value_model->feature_id = $featureDB->id;
            $feature_value_model->value = trim(ucwords($featureValue));
            $feature_value_model->display_name = trim(ucwords($featureValue));
            $feature_value_model->created = date('Y-m-d h:i:s');
            $feature_value_model->modified = $feature_value_model->created;
            if ($feature_value_model->save()) {
                return $feature_value_model;
            } else {
                return null;
            }
        }
        return $feature_value_data;
    }

    public function saveProductFeatureDB($productId, $featureId, $featureValueId)
    {
        $feature_value_data = ProductFeature::findOne(['id_product' => $productId, 'id_feature' => $featureId, 'id_feature_value' => $featureValueId]);
        if (is_null($feature_value_data)) {
            $product_feature_model = new ProductFeature();
            $product_feature_model->id_product = $productId;
            $product_feature_model->id_feature = $featureId;
            $product_feature_model->id_feature_value = $featureValueId;
            if ($product_feature_model->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function updateProductImages(Products $product, Categories $category, Brands $brand)
    {
        if (!empty($this->getMainImages())) {
            $product->image = Products::saveProductImage($category, $brand, $this->getMainImages());
            $product->image_path = $this->getMainImages();
            $product->save(false);
        }

        if (!empty($this->getMultipleImages())) {
            ProductImage::deleteAll('id_product = :id_product', [':id_product' => $product->product_id]);
            $product->saveMultipleImages($this->getMultipleImages(), $product->product_id, $category, $brand);
        }
    }
}

?>
