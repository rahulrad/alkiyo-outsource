<?php

namespace backend\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "blogs".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $status
 * @property string $created
 */
class Blogs extends \yii\db\ActiveRecord
{
   
   public $file;
	
	const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;
	
	public function behaviors()
    {
        return [
		 
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','meta_title','meta_keyword','meta_desc'], 'string'],
            [['title'], 'required'],
            [['created','is_delete'], 'safe'],
            [['title', 'image'], 'string', 'max' => 255],
			[['file'], 'file'],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'status' => 'Status',
            'created' => 'Created',
			'file' => 'Image'
        ];
    }
}
