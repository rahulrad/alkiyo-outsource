<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Categories;

/**
 * CategoriesSearch represents the model behind the search form about `backend\models\Categories`.
 */
class CategoriesSearch extends Categories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['category_name', 'category_description', 'status', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categories::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 100,
			],
			'sort'=> ['defaultOrder' => ['category_id'=>SORT_DESC]]
			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$categoryArr = array();
		$categoryArr[] = $this->category_id;
		$categoryLists = Categories::find()->select(['category_id','parent_category_id'])->where(['parent_category_id' => $this->category_id])->all();
		
		if(!empty($categoryLists)){
			foreach($categoryLists as $cat){
				$categoryArr[] = $cat->category_id;
				$childCategoryLists = Categories::find()->select(['category_id','parent_category_id'])->where(['parent_category_id' => $cat->category_id])->all();
				if(!empty($childCategoryLists)){
					foreach($childCategoryLists as $childCat){
						$categoryArr[] = $childCat->category_id;
					}
				}
			}
		}
		
		if(isset($categoryArr[0]) && !empty($categoryArr[0])){
			 $query->andFilterWhere([
				'category_id' => $categoryArr,
			]);	
		}
       

        $query->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'category_description', $this->category_description])
            ->andFilterWhere(['like', 'status', $this->status]);
			
	$query->andFilterWhere(['!=', 'is_delete', 1]);

        return $dataProvider;
    }
}
