<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CategoryUrl;

/**
 * CategoryUrlSearch represents the model behind the search form about `app\models\CategoryUrl`.
 */
class CategoryUrlSearch extends CategoryUrl
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category_url', 'status'], 'integer'],
            [['category_name', 'site_url', 'category_type', 'createdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoryUrl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_category_url' => $this->id_category_url,
            'status' => $this->status,
            'createdate' => $this->createdate,
        ]);

        $query->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'site_url', $this->site_url])
            ->andFilterWhere(['like', 'category_type', $this->category_type]);

        return $dataProvider;
    }
}
