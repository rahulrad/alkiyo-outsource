<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Coupons;

/**
 * CouponsSearch represents the model behind the search form about `backend\models\Coupons`.
 */
class CouponsSearch extends Coupons
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['merchant', 'category', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coupons::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 100,
			],
			'sort'=> ['defaultOrder' => ['created'=>SORT_DESC]]
        ]);
        $this->load($params);

    

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
        ]);

        
   $this->expiry_date=date('Y-m-d');
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'offer', $this->offer])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'valid', $this->valid])
            ->andFilterWhere(['like', 'status', $this->status])
           
             ->andFilterWhere(['>', 'expiry_date', $this->expiry_date])

->andFilterWhere(['=', 'category', $this->category])


 ->andFilterWhere(['=', 'merchant', $this->merchant]);
            if(isset($params) && !empty( $params) && array_key_exists('id_merchant',$params) && !empty($params['id_merchant'])){
               $query ->andFilterWhere(['=', 'merchant', $params['id_merchant']]);
            }

             if(isset($params) && !empty( $params) && array_key_exists('category',$params)  && !empty($params['category'])){
               $query ->andFilterWhere(['in', 'category', explode('-',$params['category'])]);
            }

              if(isset($params) && !empty( $params) && !empty($params['discount'])){
               $query ->andFilterWhere(['>=', 'discount', $params['discount']*10]);
            }

            //    if(isset($params) && !empty( $params) && array_key_exists('type',$params)  && !empty($params['type'])){
            //    $query ->andFilterWhere(['in', 'type', explode('-',$params['type'])]);
            // }

            	//$query->andFilterWhere(['=', 'moved', 1]);

        return $dataProvider;
    }
}
