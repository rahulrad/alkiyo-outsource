<?php

namespace backend\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "coupons_vendors".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
  * @property string $slug
   * @property string $title
    * @property string $short_description
 * @property string $image_name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_descrption
 * @property integer $show_home
 * @property integer $google_index
 * @property string $status
 * @property integer $is_delete
 * @property string $created
 */
class CouponsVendors extends \yii\db\ActiveRecord
{

public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupons_vendors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'name','short_description','title','slug'], 'string'],
            [[ 'meta_descrption','short_description','title','slug'], 'required'],
            [['show_home'], 'integer'],
            [['file'], 'file'],
            [['created','is_delete','google_index','is_popular'], 'safe'],
            [['name', 'image', 'image_name', 'meta_title', 'meta_keyword'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'slug' => 'Slug',
            'short_description' => 'Short Description',
            'slug' => 'Slug',
            'image' => 'Image',
            'image_name' => 'Image Name',
            'description' => 'Description',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_descrption' => 'Meta Descrption',
            'show_home' => 'Show Home',
            'status' => 'Status',
            'is_delete' => 'Is Delete',
             'google_index' => 'Google Index',
            'created' => 'Created',
        ];
    }


    	public static function getLists(){
			$categoryData = Self::find()->select(['id','name'])->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all();
			$categories = array();
			if(!empty($categoryData)){
				foreach($categoryData as $category){
				
					$categories[$category->id] = $category->name;
				}
			}
		return $categories;
	}

    	public static function getListswithSlug(){
			$categoryData = Self::find()->select(['name','slug'])->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all();
			$categories = array();
			if(!empty($categoryData)){
				foreach($categoryData as $category){
				
					$categories[$category->slug] = $category->name;
				}
			}
		return $categories;
	}


    public static function getVendorsList(){

    $sql="select * from coupons_vendors where is_delete=0 and status='active' order by id desc ";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            $sql");
        $result = $command->queryAll();
                return $result;

        }


 public static function getPopularVendorsList(){

    $sql="select * from coupons_vendors where is_delete=0 and status='active' and is_popular=1 order by id desc ";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            $sql");
        $result = $command->queryAll();
                return $result;

        }


        public static function getVendorObj($slug){
             $connection = Yii::$app->getDb();
 $sql="select * from coupons_vendors where is_delete=0 and status='active'  and slug=:slug";
$command = $connection->createCommand($sql, [':slug' => $slug]) ;
        $result = $command->queryAll();
                return $result;

        }

    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        return $dataProvider;
    }

    public static function getCouponsAndCashBacks($full=0)
    {
        $popular="";
        if(!$full)
        $popular="  and is_popular=1 ";

        $sql = "select cv.name,cv.id, image ,count(c.id) offer_count, discount, 1 as selected from coupons_vendors cv inner join coupons c on cv.id=c.merchant  where is_delete=0 and cv.status='active' " .$popular. "group by cv.name order by discount  desc";
      
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        return $result;
    }
}
