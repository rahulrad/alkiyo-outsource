<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "deal_urls".
 *
 * @property integer $id
 * @property string $url
 * @property integer $store_id
 * @property string $status
 * @property string $created
 */
class DealUrls extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal_urls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created'], 'safe'],
            [['url'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'store_id' => 'Store ID',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	
	
}
