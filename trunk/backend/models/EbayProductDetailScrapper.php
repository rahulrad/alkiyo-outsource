<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class EbayProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName="",$productDetail)
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
		
        $retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url,$productDetail);
        $this->arr = $retrnarr;
        $html->clear();
		
        unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url,$productDetail)
    {
		$scraperLog = new Logs();
		//echo $url.'<br>';
		$siteName = 'http://www.ebay.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
		
		$product_unique_id = '';
		$productDeliverd = '';
		$productReturnPolicy = '';
		$productFeatures = '';
		$productCOD = '';


        $arr_merchant = '';
		// product title
       /** if ($html->find('.pdp-e-i-head', 0)) {
            $productNamehtml = $html->find('.pdp-e-i-head', 0)->plaintext;
			$productNamehtml = trim($productNamehtml);
        } **/
		
		if(isset($productDetail['title']) && !empty($productDetail['title'])){
			$productNamehtml = $productDetail['title'];
		}
		
		
		// product price
       /** if ($html->find('.payBlkBig', 0)) {
            if ($html->find('.payBlkBig', 0)) {
                $productPrice = $html->find('.payBlkBig', 0)->plaintext;
                $productPrice = $this->convet_price_string($productPrice);
            }
        } **/
		
		if ($html->find('#prcIsum', 0)) {
                $productPrice = $html->find('#prcIsum', 0)->plaintext;
                $productPrice = $this->convet_price_string($productPrice);
        }else{
			if(isset($productDetail['price']) && !empty($productDetail['price'])){
				$productPrice = $productDetail['price'];
			}
		}
		
		
		// product main image
      if ($html->find('#icImg', 0)) {
			$productImgSrchtml = $html->find('#icImg', 0)->getAttribute('src');
        }
		
		
		
		
		// product unique id
		    if ($html->find('#descItemNumber', 0)) {
                $product_unique_id = $html->find('#descItemNumber', 0)->plaintext;
            }
        
		
		$arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
       // $arr_merchant[] = array();

       /* if ($html->find('.proDescript', 0)) {
             $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
        }*/
		
		
		// product details 
		$product_detail = array();
		 
		 // product category
		 $product_detail['Category'] = $categoryName;
		 
		 if(isset($productDetail['cod']) && !empty($productDetail['cod'])){
			$product_detail['cash_on_delivery'] = $productDetail['cod'];
		}
		 
		 $product_detail['shipping'] = 'Free Shipping';
		 $product_detail['return_policy'] = '';
		
		 $pro_title = explode(' ',$productNamehtml);
		 $product_detail['brand'] = html_entity_decode($pro_title[0]);
		 
		 $product_detail['model_number'] = trim(str_replace($pro_title[0],'',$productNamehtml));
		 
		 $product_detail['original_price'] = $productPrice;
		 $product_detail['rating_user_count'] = '';
		
		// product model number
		if ($html->find('.product-details', 0)) {
			if($html->find('.title-wrap', 0)){
				$productModelHtml = $html->find('.title-wrap', 0)->getAttribute('data-evar2');
				$productModelHtml = str_replace($productBrandHtml,'',$productModelHtml);
				$product_detail['model_number'] = trim($productModelHtml);
			}
        }
		
		
		/*******New filds*******/
		 
		 // product rating
		$product_detail['rating'] = '';
		 if ($html->find('.pdp-e-i-ratereviewQA', 0)) {
			 if ($html->find('.pdp-e-i-ratings div', 0)) {
				$productRatingHtml = $html->find('.pdp-e-i-ratings div', 0)->getAttribute('ratings');
				$product_detail['rating'] = trim($productRatingHtml);
			 }else{
					$scraperLog->saveScraperLog('ebay scraper','EbayProductDetailScrapper','.pdp-e-i-ratings div class not found','Not Found',__LINE__);
			}
		}
		
		 // product reviews
		$product_detail['reviews'] = '';
		 if ($html->find('.productWriteReview', 0)) {
			 if ($html->find('.productWriteReview  a', 0)) {
				$productReviews = trim($html->find('.productWriteReview  a', 0)->plaintext);
				$product_detail['reviews'] = (int)$productReviews;
			 }else{
					$scraperLog->saveScraperLog('ebay scraper','EbayProductDetailScrapper','.productWriteReview  a class not found','Not Found',__LINE__);
			}
			
        }
		
		
		
		 $product_detail['discount'] = '';
		 if ($html->find('.pdp-e-i-PAY-r', 0)) {
            if ($html->find('.pdpDiscount ', 0)) {
                $productDiscount = $html->find('.pdpDiscount span', 0)->plaintext;
				$product_detail['discount'] = $productDiscount;
            }
        }
		
		
		$product_detail['product_delivery'] = '';
		if ($html->find('.check-avail-pin-inner', 0)) {
            if ($html->find('.check-avail-pin-info', 0)) {
                $product_detail['product_delivery']  = trim($html->find('.check-avail-pin-info p', 0)->plaintext);
               
            }
        }
		
		$product_detail['emi'] = '';
		if ($html->find('.elecPriceTile', 0)) {
            if ($html->find('.pdp-emi', 0)) {
                $product_detail['emi']  = trim($html->find('.pdp-emi', 0)->plaintext);
			}else{
					$scraperLog->saveScraperLog('ebay scraper','EbayProductDetailScrapper','.pdp-emi  a class not found','Not Found',__LINE__);
			}
        }
		
		// product description
		$description = '';
		if ($html->find('.itemAttr', 0)) {
			
			$descData = $html->find('.itemAttr', 0);
			
			$description = $descData->find('.section', 0)->outertext;
			

        }
		$product_detail['descritpion'] = $description;
		
		
		
		
		// product feature details		
	$arr_product_images = array();
		$anchors = $html->find('#vi_main_img_fs_slider li');
			// loop and print nodes content
		$img_num = 0;
		foreach( $anchors as $i => $anchor ) {
				
				if($img_num <= 4){
					$arr_product_images[] = $anchor->find('img', 0)->getAttribute('src');
				}
				
			$img_num++;
		}
		
	
	
		// product feature details		
		$arr_product_features = array();
	
		 if ($html->find('.itemAttr', 0)) {
			/**
			$g = 0;
            foreach ($html->find(".itemAttr",$g) as $skey => $sval) {
				
				$table = $html->find(".itemAttr",$g); // table record

				// initialize empty array to store the data array from each row
				$theData = array();
				
				// loop over rows
				$n = 0;
				if(!empty($html->find('.attrLabels', $g)->plaintext)){
				
						foreach($table->find('tr') as $row) { // loop for tr for get product group head or features
						
							//$arr_product_features[$g]['group'] = $html->find('.attrLabels', $g)->plaintext;
							// initialize array to store the cell data from each row
							$f = 0;
							foreach($row->find('td.attrLabels') as $cell) {  // loop for get product feature name
								 $feature_name = trim($cell->innertext);
								 $arr_product_features[$g]['feature'][$f][$n]['feature'] = $feature_name;
							
							$f++;
							}
							
							$v= 0;
							foreach($row->find('td.specsValue') as $cell) { // loop for get product feature value
						
								// push the cell's text to the array
								$feature_value = trim($cell->innertext);
								$arr_product_features[$g]['feature'][$v][$n]['feature_value'] = $feature_value;
							
							$v++;
							}
						
						$n++;	
						} 
					}
			 $g++;	
				} **/
           
			}
		
		
        $returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $arr_product_images;
        $returnarray['product_price'] = $productPrice;
        $returnarray['product_features'] = $arr_product_features;
		//echo '<pre>returnarray'; print_r($returnarray); die;
        return $returnarray;
    }

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
}
