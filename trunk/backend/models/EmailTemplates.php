<?php

namespace backend\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "email_templates".
 *
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $content
 * @property string $status
 * @property string $created
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
   
   const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;
	
	public function behaviors()
    {
        return [
		 
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['title','subject'], 'required'],
            [['created'], 'safe'],
            [['title', 'subject'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'subject' => 'Subject',
            'content' => 'Content',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
}
