<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Features;

/**
 * FeaturesSearch represents the model behind the search form about `app\models\Features`.
 */
class FeaturesSearch extends Features
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'feature_group_id', 'is_filter', 'is_required', 'sort_order', 'status'], 'integer'],
            [['name', 'slug', 'type', 'unit', 'display_text', 'created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Features::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 100,
			],
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'feature_group_id' => $this->feature_group_id,
            'is_filter' => $this->is_filter,
            'is_required' => $this->is_required,
            'sort_order' => $this->sort_order,
            'status' => $this->status,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'unit', $this->unit])
  ->andFilterWhere(['>', 'id', Features::FEATURE_LAST_ID])
            ->andFilterWhere(['like', 'display_text', $this->display_text]);
			
		

        return $dataProvider;
    }
}
