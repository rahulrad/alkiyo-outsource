<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "filter_group_values".
 *
 * @property integer $id
 * @property integer $group_item_id
 * @property integer $value_id
 * @property integer $status
 * @property integer $is_delete
 * @property string $created
 */
class FilterGroupValues extends \yii\db\ActiveRecord
{
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_group_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_item_id', 'value_id', 'status', 'is_delete'], 'integer'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_item_id' => 'Group Item ID',
            'value_id' => 'Value ID',
            'status' => 'Status',
            'is_delete' => 'Is Delete',
            'created' => 'Created',
        ];
    }
}
