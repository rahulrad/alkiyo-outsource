<?php

namespace backend\models;

use Yii;
use common\models\Categories;

/**
 * This is the model class for table "filter_groups".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property integer $status
 * @property integer $is_delete
 * @property string $created
 */
class FilterGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'status', 'is_delete','sort_order'], 'integer'],
            [['created'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'title' => 'Title',
            'status' => 'Status',
            'is_delete' => 'Is Delete',
            'created' => 'Created',
        ];
    }
	
	
	/**
     * @return yii\db\ActiveQuery
     */
    public function getCategoriesName() {
        return $this::hasOne(Categories::className(), ['category_id' => 'category_id']);
    }
	
	
	
    
    /**
     * @todo Add cache
     * @return type
     */
    public static function loadAll(){
        $query = new \yii\db\Query();
        return $query->select(['id','title','sort_order'])->from(self::tableName())->where(['status'=>1])->indexBy('id')->all();
    }
	
 public static function getFilterGroupValuebyfeaturevalue($featureid){

        $sql = "select concat(group_id,'_',group_item_id) as list from filter_group_values v inner join filter_group_items i on i.id=v.group_item_id where value_id=$featureid limit 1";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql) ;
        return $list = $command->queryOne();
   
    }	
	
}
