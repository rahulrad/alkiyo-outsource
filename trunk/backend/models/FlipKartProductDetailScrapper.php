<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\Tools;
use common\components\simple_html_dom;


class FlipKartProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName = "")
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $retrnarr = $this->getProductData($response, $categoryName, $url);
        //$html = $this->LoadHTMLContent($response);
        //echo '<pre>html'; print_r($html); die;
        // $retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url);
        $this->arr = $retrnarr;
        //$html->clear();

        // unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {

        $objHttpService = new \common\components\HttpService();
        // $response = $objHttpService->getResponse($strurl);

        $response = $objHttpService->getFlipkartProductDetail($strurl);

        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }


    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url)
    {

        $scraperLog = new Logs();
        echo $url;
        $siteName = 'http://www.flipkart.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';

        $product_unique_id = '';
        $productDeliverd = '';
        $productReturnPolicy = '';
        $productFeatures = '';
        $productCOD = '';

        $arr_merchant = '';
        // product title
        if ($html->find('.title-wrap', 0)) {
            if ($html->find('.title-wrap h1', 0)) {
                $productNamehtml = $html->find('.title-wrap h1', 0)->plaintext;
                $productNamehtml = trim($productNamehtml);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.title-wrap h1 class not found', 'Not Found', __LINE__);
            }

        } else {
            $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.title-wrap class not found', 'Not Found', __LINE__);
        }


        // product price
        if ($html->find('.pricing', 0)) {
            if ($html->find('.selling-price', 0)) {
                $productPrice = $html->find('.selling-price', 0)->plaintext;
                $productPrice = $this->convet_price_string($productPrice);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.selling-price class not found', 'Not Found', __LINE__);
            }
        } else {
            $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.selling-price class not found', 'Not Found', __LINE__);
        }

        // product main image
        if ($html->find('.imgWrapper', 0)) {
//            echo $html->find(".imgWrapper img", 0);exit;
//            $productImgSrchtml = $html->find('.imgWrapper img', 0)->getAttribute('data-src');
            $productImgSrchtml = $html->find('.imgWrapper img', 0)->getAttribute('data-src');
        } else {
            $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.imgWrapper class not found', 'Not Found', __LINE__);
        }

        // product unique id---
        if ($html->find('.btn-buy-now', 0)) {
            if ($html->find('.btn-buy-now', 0)) {
                $product_unique_id = $html->find('.btn-buy-now', 0)->getAttribute('data-buy-listing-id');
            }
        } else {
            $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.btn-buy-now class not found', 'Not Found', __LINE__);
        }

        $arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
        // $arr_merchant[] = array();

        /* if ($html->find('.proDescript', 0)) {
              $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
         }*/


        // product details
        $product_detail = array();

        // product category
        $product_detail['Category'] = $categoryName;

        $product_detail['shipping'] = 'Free Shipping';

        // product discount
        $product_detail['discount'] = '';
        if ($html->find('.pricing', 0)) {
            if ($html->find('.discount', 0)) {
                $productDiscount = $html->find('.discount', 0)->plaintext;
                $product_detail['discount'] = $this->convet_price_string($productDiscount);
            }
        }

        // product original price
        $product_detail['original_price'] = $productPrice;
        if ($html->find('.pricing', 0)) {
            if ($html->find('.price', 0)) {
                $productOriginalPrice = $html->find('.price', 0)->plaintext;
                $product_detail['original_price'] = $this->convet_price_string($productOriginalPrice);
            }
        }

        // product count users for product rating
        $product_detail['rating_user_count'] = '';
        if ($html->find('.ratingHistogram', 0)) {
            if ($html->find('.subText', 1)) {
                $productRatingUserHtml = $html->find('.subText', 1)->plaintext;
                $product_detail['rating_user_count'] = $this->convet_price_string($productRatingUserHtml);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.subText class not found', 'Not Found', __LINE__);
            }
        }


        //$product_detail['reviews'] = '';
        $product_detail['reviews'] = '';
        if ($html->find('.helpfulReviews', 0)) {
            if ($html->find('.helpfulReviews a', 0)) {
                $productReviewsHtml = $html->find('.helpfulReviews a', 0)->innertext;
                $product_detail['reviews'] = $this->convet_price_string($productReviewsHtml);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.helpfulReviews a class not found', 'Not Found', __LINE__);
            }
        }

        // product rating
        $product_detail['rating'] = '';
        if ($html->find('.ratingHistogram', 0)) {
            if ($html->find('.bigStar', 0)) {
                $productRatingHtml = $html->find('.bigStar', 0)->plaintext;
                $product_detail['rating'] = $productRatingHtml;
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.bigStar class not found', 'Not Found', __LINE__);
            }
        }
        //echo '<pre>'; print_r($product_detail); die;

        // product brand
        if ($html->find('.product-details', 0)) {
            if ($html->find('.title-wrap', 0)) {
                $productBrandHtml = $html->find('.title-wrap', 0)->getAttribute('data-prop41');
                $product_detail['brand'] = trim($productBrandHtml);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.title-wrap class not found', 'Not Found', __LINE__);
            }
        } else {
            $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.product-details class not found', 'Not Found', __LINE__);
        }

        // product model number
        if ($html->find('.product-details', 0)) {
            if ($html->find('.title-wrap', 0)) {
                $productModelHtml = $html->find('.title-wrap', 0)->getAttribute('data-evar2');
                $productModelHtml = str_replace($productBrandHtml, '', $productModelHtml);
                $product_detail['model_number'] = trim($productModelHtml);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.title-wrap class not found', 'Not Found', __LINE__);
            }
        } else {
            $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.product-details class not found', 'Not Found', __LINE__);
        }

        // product description
        $product_detail['descritpion'] = '';
        if ($html->find('.specifications-warranty-wrap', 0)) {
            $productDetailhtml = $html->find('.specifications-warranty-wrap', 0)->outertext;
            $product_detail['descritpion'] = trim($productDetailhtml);
        }

        // product cash on delevery detail
        $product_detail['cash_on_delivery'] = '';
        if ($html->find('.cash-on-delivery-header', 0)) {
            if ($html->find('.cash-on-delivery-header', 0)) {
                $cash_on_delivery = $html->find('.cash-on-delivery-header', 0)->plaintext;
                $product_detail['cash_on_delivery'] = trim($cash_on_delivery);
                //$productDelivered = $this->convet_price_string($productPrice);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.cash-on-delivery-header class not found', 'Not Found', __LINE__);
            }
        }

        // product EMI details
        $product_detail['emi'] = '';
        if ($html->find('.emi-details', 0)) {
            if ($html->find('.emi-text', 0)) {
                $product_detail['emi'] = trim($html->find('.emi-text', 0)->plaintext);

            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.emi-text class not found', 'Not Found', __LINE__);
            }
        }

        // product return policy
        $product_detail['return_policy'] = '';
        if ($html->find('.return-policy', 0)) {
            if ($html->find('.return-text', 0)) {
                $product_detail['return_policy'] = trim($html->find('.return-text', 0)->plaintext);
                //$productDelivered = $this->convet_price_string($productPrice);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.return-text class not found', 'Not Found', __LINE__);
            }
        }

        $product_detail['product_delivery'] = '';
        if ($html->find('.delivery-info', 0)) {
            if ($html->find('.delivery', 0)) {
                $product_detail['product_delivery'] = trim($html->find('.delivery', 0)->plaintext);
                //$productDelivered = $this->convet_price_string($productPrice);
            } else {
                $scraperLog->saveScraperLog('flipkart scraper', 'FlipKartProductDetailScrapper', '.delivery class not found', 'Not Found', __LINE__);
            }
        }

        //// product short features
        $product_detail['productFeatures'] = '';
        if ($html->find('#veiwMoreSpecifications', 0)) {
            if ($html->find('.keyFeaturesList', 0)) {
                $product_detail['productFeatures'] = $html->find('.keyFeaturesList', 0)->outertext;
            }   //$productDelivered = $this->convet_price_string($productPrice);
        }


        // product multiple images
        $arr_product_images = array();
        if ($html->find('.imgWrapper', 0)) {
            $im = 1;
            foreach ($html->find("img") as $skey => $sval) {

                if ($sval->getAttribute('data-src') != "") {
                    //echo $im.'<br>';
                    $arr_product_images[] = $sval->getAttribute('data-src');

                }
                //}
                $im++;
            }
        }

        $product_images = array();
        if (!empty($arr_product_images)) {
            $im = 1;
            foreach ($arr_product_images as $pro_images) {
                if ($im <= 4) {
                    $product_images[] = $pro_images;
                }
                $im++;
            }

        }


        // product feature details
        $arr_product_features = array();
        if ($html->find('.specTable', 0)) {
            //$g = 0;

            for ($g = 0; $g <= 20; $g++) {

                $table = $html->find(".specTable", $g); // table record

                // initialize empty array to store the data array from each row
                $theData = array();

                // loop over rows
                $n = 0;

                if (!empty($html->find('.groupHead', $g)->plaintext)) {

                    foreach ($table->find('tr') as $row) { // loop for tr for get product group head or features

                        $arr_product_features[$g]['group'] = trim($html->find('.groupHead', $g)->plaintext);
                        // initialize array to store the cell data from each row
                        $f = 0;
                        foreach ($row->find('td.specsKey') as $cell) {  // loop for get product feature name
                            $feature_name = trim($cell->innertext);
                            $arr_product_features[$g]['feature'][$f][$n]['feature'] = $feature_name;

                            $f++;
                        }

                        $v = 0;
                        foreach ($row->find('td.specsValue') as $cell) { // loop for get product feature value

                            // push the cell's text to the array
                            $feature_value = trim($cell->innertext);
                            $arr_product_features[$g]['feature'][$v][$n]['feature_value'] = $feature_value;

                            $v++;
                        }

                        $n++;
                    }
                }

                // $g++;
            }


        }
        //die('dsaf');
        //echo '<pre>'; print_r($arr_product_features); die;

        $returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $product_images;
        $returnarray['product_price'] = $productPrice;
        $returnarray['product_features'] = $arr_product_features;
        return $returnarray;
    }


    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return ($result);
    }


    //////////// New code for flipkart product detail

    public function getProductData($response, $categoryName, $url)
    {
        $scraperLog = new Logs();

        $siteName = 'http://www.flipkart.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';
        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
        $product_unique_id = '';
        $productDeliverd = '';
        $productReturnPolicy = '';
        $productFeatures = '';
        $productCOD = '';
        $colors = [];
        $sizes = [];
        $offers = [];
        $instock = true;
        $product_detail['model_number'] = "";

        if (!empty($response)) {
            $productNamehtml = isset($response->RESPONSE->data->product_summary_1->data[0]->value->title) ? $response->RESPONSE->data->product_summary_1->data[0]->value->title : '';

            $productPrice = '';
            if (isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->pricing->value->finalPrice->value) && isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->pricing->value->deliveryCharge->value)) {
                $finalPrice = $response->RESPONSE->data->product_seller_detail_1->data[0]->value->pricing->value->finalPrice->value;
                $deliveryCharges = $response->RESPONSE->data->product_seller_detail_1->data[0]->value->pricing->value->deliveryCharge->value;
                $productPrice = $finalPrice - $deliveryCharges;
            }

            $product_unique_id = isset($response->RESPONSE->data->product_rating_default_1->data[0]->action->params->productId) ? $response->RESPONSE->data->product_rating_default_1->data[0]->action->params->productId : '';
            if (empty($product_unique_id)) {
                $product_unique_id = isset($response->RESPONSE->data->product_seller_announcement_1->data[0]->action->params->productId) ? $response->RESPONSE->data->product_seller_announcement_1->data[0]->action->params->productId : '';
            }

            $productImgSrchtml = isset($response->RESPONSE->data->product_multimedia_1->data[0]->value->url) ? str_replace(array('{@width}', '{@height}', '{@quality}'), array('1500', '1500', '100'), $response->RESPONSE->data->product_multimedia_1->data[0]->value->url) : '';

            $url = isset($response->RESPONSE->pageContext->smartUrl) ? $response->RESPONSE->pageContext->smartUrl : $url;
            $arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);

            $product_detail = array();

            $product_detail['Category'] = $categoryName;

            $product_detail['shipping'] = 'Free Shipping';

            $product_detail['discount'] = isset($response->RESPONSE->data->product_summary_1->data[0]->value->pricing->totalDiscount) ? $response->RESPONSE->data->product_summary_1->data[0]->value->pricing->totalDiscount : '';

            $product_detail['original_price'] = '';
            if (isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->pricing->value->prices)) {
                foreach ($response->RESPONSE->data->product_seller_detail_1->data[0]->value->pricing->value->prices as $price) {
                    if ($price->name == "MRP") {
                        $product_detail['original_price'] = $price->value;
                    }
                }
            }
//            $product_detail['original_price'] = isset($response->RESPONSE->data->product_summary_1->data[0]->value->pricing->prices[0]->value) ? $response->RESPONSE->data->product_summary_1->data[0]->value->pricing->prices[0]->value : '';

            $product_detail['rating_user_count'] = isset($response->RESPONSE->data->product_summary_1->data[0]->value->rating->count) ? $response->RESPONSE->data->product_summary_1->data[0]->value->rating->count : 0;

            $product_detail['reviews'] = isset($response->RESPONSE->data->product_summary_1->data[0]->value->reviewCount) ? $response->RESPONSE->data->product_summary_1->data[0]->value->reviewCount : 0;

            $product_detail['rating'] = isset($response->RESPONSE->data->product_summary_1->data[0]->value->rating->average) ? $response->RESPONSE->data->product_summary_1->data[0]->value->rating->average : 0;

            $pro_title = explode(' ', $productNamehtml);
            $product_detail['brand'] = isset($response->RESPONSE->pageContext->brand) ? $response->RESPONSE->pageContext->brand : $pro_title[0];

            $product_detail['descritpion'] = isset($response->RESPONSE->data->rich_product_description->data[0]->value->richProductDescription->overview->description->text) ? $response->RESPONSE->data->rich_product_description->data[0]->value->richProductDescription->overview->description->text : '';


            $product_detail['emi'] = isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->emiInfo->minPrice->value) ? 'EMIs from Rs.' . $response->RESPONSE->data->product_seller_detail_1->data[0]->value->emiInfo->minPrice->value . '/month' : '';

            if (isset($response->RESPONSE->data->product_swatches_1->data[0]->value->attributes)) {
                foreach ($response->RESPONSE->data->product_swatches_1->data[0]->value->attributes as $attributeKey => $attribute) {
                    if (isset($response->RESPONSE->data->product_swatches_1->data[0]->value->attributeOptions[$attributeKey])) {
                        foreach ($response->RESPONSE->data->product_swatches_1->data[0]->value->attributeOptions[$attributeKey] as $attributeObj) {
                            if ($attribute->id == "color") {
                                $colors[] = $attributeObj->value;
                            } else if ($attribute->id == "size") {
                                $sizes[] = $attributeObj->value;
                            }
                        }
                    }

                }
            }

            if (isset($response->RESPONSE->data->product_specification_1->data)) {
                foreach ($response->RESPONSE->data->product_specification_1->data as $specification) {
                    if (isset($specification->value->attributes)) {
                        foreach ($specification->value->attributes as $attribute) {
                            if ($attribute->name == "Model Number") {
                                $product_detail['model_number'] = $attribute->values[0];
                            }
                        }
                    }
                }
            }

            if (empty($product_detail['model_number']) && isset($response->RESPONSE->data->product_specification_1->data)) {
                foreach ($response->RESPONSE->data->product_specification_1->data as $specification) {
                    if (isset($specification->value->attributes)) {
                        foreach ($specification->value->attributes as $attribute) {
                            if ($attribute->name == "Model Name") {
                                $product_detail['model_number'] = $attribute->values[0];
                            }
                        }
                    }
                }
            }

            if (empty($product_detail['model_number'])) {
                $product_detail['model_number'] = isset($pro_title[1]) ? $pro_title[1] : '';
                $product_detail['model_number'] .= isset($pro_title[2]) ? ' ' . $pro_title[2] : '';
            }

            //$product_detail['cash_on_delivery'] = isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[0]) ? $response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[0] : '';
            //$product_detail['return_policy']  = isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[1]) ? $response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[1] : '';

            $product_detail['cash_on_delivery'] = '';
            $product_detail['return_policy'] = '';
            if (isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts)) {
                $callOuts = $response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts;

                $countcallOuts = count($callOuts);
                $product_detail['cash_on_delivery'] = isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[$countcallOuts - 2]) ? $response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[$countcallOuts - 2] : '';
                $product_detail['return_policy'] = isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[$countcallOuts - 1]) ? $response->RESPONSE->data->product_seller_detail_1->data[0]->value->callOutsV2->value->callOuts[$countcallOuts - 1] : '';

            }

            $product_detail['product_delivery'] = isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->deliveryMessages[0]->text) ? $response->RESPONSE->data->product_seller_detail_1->data[0]->value->deliveryMessages[0]->text : '';

            $keyfeatures = array();
            if (isset($response->RESPONSE->data->product_key_features_1->data)) {
                foreach ($response->RESPONSE->data->product_key_features_1->data as $val) {
                    $keyfeatures[] = $val->value->text;
                }
            }
            $product_detail['productFeatures'] = serialize($keyfeatures);

            $product_images = array();
            if (isset($response->RESPONSE->data->product_multimedia_1->data)) {
                $i = 1;
                foreach ($response->RESPONSE->data->product_multimedia_1->data as $val) {
                    if ($i <= 5) {
                        $product_images[] = str_replace(array('{@width}', '{@height}', '{@quality}'), array('1500', '1500', '100'), $val->value->url);
                    }
                    $i++;
                }
            }

            $arr_product_features = array();
            if (isset($response->RESPONSE->data->product_specification_1->data)) {
                $a = 0;
                foreach ($response->RESPONSE->data->product_specification_1->data as $features) {

                    $arr_product_features[$a]['group'] = isset($features->value->key) ? $features->value->key : '';
                    $b = 0;
                    if (!empty($arr_product_features[$a]['group']) && isset($features->value->attributes)) {
                        $c = 0;
                        foreach ($features->value->attributes as $feature_data) {
                            $arr_product_features[$a]['feature'][$b][$c]['feature'] = $feature_data->name;
                            $arr_product_features[$a]['feature'][$b][$c]['feature_value'] = isset($feature_data->values[0]) ? $feature_data->values[0] : '';
                            $c++;
                        }
                    }
                    $b++;
                    $a++;
                }
            }

            $featureHighlight= array();
            if (isset($response->RESPONSE->data->product_key_features_1->data)) {
                $a = count($arr_product_features);
                $b = 0;
                $c = 0;
                foreach ($response->RESPONSE->data->product_key_features_1->data as $features) {

                    if (isset($features->value->text)) {

                        $featureArrayGroup = explode("|", $features->value->text);
                        foreach ($featureArrayGroup as $featureValueArray) {
                            $featureArray = explode(":", trim($featureValueArray));
                            if (count($featureArray) >= 2) {
                                $arr_product_features[$a]['group'] = 'Extra Feature';
                                $arr_product_features[$a]['feature'][$b][$c]['feature'] = ucwords(trim($featureArray[0]));
                                $arr_product_features[$a]['feature'][$b][$c]['feature_value'] = ucwords(trim($featureArray[1]));
                                $c++;
                            } else {
                                $featureHighlight[] = ucwords(trim($features->value->text));
                            }
                        }

                    }

                }
                $b++;
                $a++;
            }

            if (!empty($featureHighlight)) {
                $a = count($arr_product_features);
                $arr_product_features[$a]['group'] = 'Feature Hightlight';
                $arr_product_features[$a]['feature'][0][0]['feature'] = 'highlight';
                $arr_product_features[$a]['feature'][0][0]['feature_value'] = implode(",", $featureHighlight);
            }

            if (isset($response->RESPONSE->data->product_seller_detail_1->data[0]->value->offerInfo->offers)) {
                foreach ($response->RESPONSE->data->product_seller_detail_1->data[0]->value->offerInfo->offers as $offer) {
                    if (isset($offer->title)) {
                        $offers[] = $offer->title;
                    }
                }
            }

            if (isset($response->RESPONSE->data->product_state_default->data[0]->value->state)) {
                $productState = $response->RESPONSE->data->product_state_default->data[0]->value->state;
                if ($productState == "OUT_OF_STOCK" || $productState == "COMING_SOON" || $productState == "NO_AVAILABLE_LISTING" || $productState == "TEMP_DISCONTINUED") {
                    $instock = false;
                }
            }

            $returnarray['name'] = $productNamehtml;
            $returnarray['imgpath'] = $productImgSrchtml;
            $returnarray['merchant_arr'] = $arr_merchant;
            $returnarray['product_detail'] = $product_detail;
            $returnarray['product_img_arr'] = $product_images;
            $returnarray['product_price'] = $productPrice;
            $returnarray['product_features'] = $arr_product_features;
            $returnarray['product_colors'] = implode("##", $colors);
            $returnarray['product_sizes'] = implode("##", $sizes);
            $returnarray['product_offers'] = implode("##", $offers);
            $returnarray['instock'] = $instock;
            return $returnarray;
        }
    }


}
