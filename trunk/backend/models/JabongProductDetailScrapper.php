<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class JabongProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName="")
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
		
        $retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url);
        $this->arr = $retrnarr;
        $html->clear();
		
        unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {
        $objHttpService = new \common\components\HttpService();
       // $response = $objHttpService->getResponse($strurl);
	   
	   $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url)
    {

		$scraperLog = new Logs();
		//echo $url;
        $siteName = 'http://www.jabong.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
		
		$product_unique_id = '';
		$productDeliverd = '';
		$productReturnPolicy = '';
		$productFeatures = '';
		$productCOD = '';


        $arr_merchant = '';
		// product title
        if ($html->find('.content', 0)) {
        	 if ($html->find('.product-title', 0)) {
        	 	$productNamehtml = $html->find('.product-title', 0)->plaintext;
				$productNamehtml = trim($productNamehtml);
        	 }else{
					$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.product-title class not found','Not Found',__LINE__);
			}
        }else{
			$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.content class not found','Not Found',__LINE__);
		}
		
		
		// product price
        if ($html->find('.content', 0)) {
            if ($html->find('.price', 0)) {
				if($html->find('.actual-price', 0)){
					$productPrice = $html->find('.actual-price', 0)->plaintext;
					$productPrice = $this->convet_price_string($productPrice);		
				}else{
					$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.actual-price class not found','Not Found',__LINE__);
				}
            }else{
				$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.price class not found','Not Found',__LINE__);
			}
        }else{
			$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.content class not found','Not Found',__LINE__);
		}
		
		// product main image
      if ($html->find('.slick-track', 0)) {
            $productImgSrchtml = $html->find('.slick-track img', 0)->getAttribute('src');
        }
		
		$main_img_anchors = $html->find('#bx-pager-left-image-panel a');
		$i_num = 0;
		foreach( $main_img_anchors as $i => $anchor ) {
			
				$img = explode('"',$anchor->innertext);
				
					if(isset($img[5]) && !empty($img[5]) && $i_num == 0){
						$productImgSrchtml = $img[5];
					}
	
			$i_num++;
		}
		
		
		//http://static.jabong.com/p/Xclusive-Chhabra-White-Embroidered-Dress-Material-1227-9864722-1-catalog_xxs_lr.jpg
		//static4.jassets.com/p/Xclusive-Chhabra-White-Embroidered-Dress-Material-1227-9864722-1-pdp_slider_l.jpg
		//$main_img = $html->find('//*[@id="product-details-wrapper"]', 0);
		
		if(empty($productImgSrchtml)){
			if ($html->find('/html/head/meta[10]', 0)) {
				$productImgSrchtml = $html->find('/html/head/meta[10]', 0)->getAttribute('content');
				$productImgSrchtml = str_replace(array('http://static.jabong.com','catalog_xxs_lr'),array('http://static4.jassets.com','pdp_slider_l'),$productImgSrchtml);
			}
		} 
		
		
		
		
		// product unique id
		if ($html->find('#product-groups-action', 0)) {
            if ($html->find('#product-groups-action', 0)) {
                $product_unique_id = $html->find('#product-groups-action', 0)->getAttribute('data-deffered-options');
            }
        }else{
			$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','#product-groups-action class not found','Not Found',__LINE__);
		}
		
		$arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
       // $arr_merchant[] = array();

       /* if ($html->find('.proDescript', 0)) {
             $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
        }*/
		
		
		// product details 
		$product_detail = array();
		 
		 // product category
		 $product_detail['Category'] = $categoryName;
		 $product_detail['cash_on_delivery'] = 'Cash On Delivery';
		 $product_detail['shipping'] = 'Free Shipping';
			
		
		// product original price
		$product_detail['original_price'] = $productPrice;
		if ($html->find('.content', 0)) {
            if ($html->find('.price', 0)) {
				if($html->find('.standard-price', 0)){
					$productOriginalPrice = $html->find('.standard-price', 0)->plaintext;
					$product_detail['original_price'] = $this->convet_price_string($productOriginalPrice);		
				}
                
            }
        }
		
		// product count users for product rating
		$product_detail['rating_user_count'] = '';
		
		 /**$product_detail['return_policy'] = '';		 
			 if ($html->find('#pdp-policy', 0)) {
				 if ($html->find('.delivery-info',0)) {
					$product_return_policyHtml = $html->find('.delivery-info',1)->plaintext;
					$product_detail['return_policy'] = trim($product_return_policyHtml);
				 }
				
	        } **/
			
		$product_detail['return_policy'] = '15 days free returnable/exchangeable';
		
		 $pro_title = explode(' ',$productNamehtml);
		 $product_detail['model_number'] = $pro_title[0];
		 
		 if ($html->find('.content', 0)) {
        	 if ($html->find('.brand', 0)) {
        	 	$product_detail['brand'] = trim($html->find('.brand', 0)->plaintext);
        	 }
            
        }
		
		
		
		/*******New filds*******/
		 
		 // product rating
		$product_detail['rating'] = '';
		 if ($html->find('.rating-container', 0)) {
			 if ($html->find('.current-rating', 0)) {
				$productRatingHtml = $html->find('.current-rating', 0)->plaintext;
				$product_detail['rating'] = trim($productRatingHtml);
			 }else{
				$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.current-rating class not found','Not Found',__LINE__);
			}
			
        }
		
		 // product reviews
		$product_detail['reviews'] = '';
		 if ($html->find('.productWriteReview', 0)) {
			 if ($html->find('.productWriteReview  a', 0)) {
				$productReviews = trim($html->find('.productWriteReview  a', 0)->plaintext);
				$product_detail['reviews'] = (int)$productReviews;
			 }else{
				$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.productWriteReview  a class not found','Not Found',__LINE__);
			}
			
        }
		
		
		// product discount
		 $product_detail['discount'] = '';
		 if ($html->find('.content', 0)) {
            if ($html->find('.price ', 0)) {
				if($html->find('.productDiscount', 0)){
					$productDiscount = $html->find('.productDiscount', 0)->plaintext;
					preg_match_all('!\d+!', $productDiscount, $matches);
					$product_detail['discount'] = $matches[0][0];
				}
			}
        }
		
		
		$product_detail['product_delivery'] = '';
		if ($html->find('.popover-options', 0)) {
			 if ($html->find('.delivery-info',0)) {
				$productRatingHtml = $html->find('.delivery-info',0)->plaintext;
				$product_detail['product_delivery'] = trim($productRatingHtml);
			 }else{
					$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.delivery-info  a class not found','Not Found',__LINE__);
			}
		
    	}


		
		$product_detail['emi'] = '';
		if ($html->find('.elecPriceTile', 0)) {
            if ($html->find('.pdp-emi', 0)) {
                $product_detail['emi']  = trim($html->find('.pdp-emi', 0)->plaintext);
			}else{
					$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.pdp-emi  a class not found','Not Found',__LINE__);
			}
        }
		
		// product description
/*		$description = '';
		if ($html->find('.pdp-e-i-keyfeatures ul', 0)) {
			
			$ulData = $html->find('.pdp-e-i-keyfeatures ul', 0);
			$li = 1;
            foreach ($ulData->find("li") as $skey => $sval) {
				if($li <= 5){
					$description .= $sval->plaintext.',';
				}
          	 $li++;
		    }

        }
		$product_detail['descritpion'] = $description;*/

		$product_detail['descritpion'] = '';
		if ($html->find('#productInfo', 0)) {
            if ($html->find('.prod-info', 0)) {
            	//if($html->find('p', 0)) {
            		$product_detail['descritpion']  = trim($html->find('.prod-info', 0)->outertext);
            	//}
            }else{
					$scraperLog->saveScraperLog('jabong scraper','JabongProductDetailScrapper','.prod-info  a class not found','Not Found',__LINE__);
			}
        }
		
		
		
		
		// product images		
	
		/**$arr_product_images = array();
        if ($html->find('.imgWrapper', 0)) {
			$im = 1;
            foreach ($html->find("img") as $skey => $sval) {
				
					if($sval->getAttribute('data-src')!="")
					{
						//echo $im.'<br>';
						$arr_product_images[] = $sval->getAttribute('data-src');
						
					}
				//}
          	 $im++;
		    }
        } **/
		
		$arr_product_images = array();
		
		$slider_div = $html->find('.product-carousel', 0);
        if ($slider_div) {
			$im = 1;
            foreach ($slider_div->find(".slide") as $skey => $sval) {
					//echo 'pass'; die;
					
					//	$arr_product_images[] = $sval->find('img', 0)->getAttribute('src');
					
				//}
          	 $im++;
		    }
        }
		
		if(empty($arr_product_images)){
			if ($html->find('/html/head/meta[10]', 0)) {
				$other_img = $html->find('/html/head/meta[10]', 0)->getAttribute('content');
				$arr_product_images[] = str_replace(array('http://static.jabong.com','catalog_xxs_lr'),array('http://static4.jassets.com','pdp_slider_l'),$other_img);
			}
		} 
	//echo '<pre>arr_product_images'; print_r($arr_product_images); die;
	
		// product feature details		
		$arr_product_features = array();
		 if ($html->find('.detailssubbox', 0)) {
		//	$g = 0;
		//	if($html->find(".product-spec",$g)){
           for($g =0; $g<=20;$g++) {
				
				$table = $html->find(".product-spec",$g); // table record

				// initialize empty array to store the data array from each row
				$theData = array();
				
				// loop over rows
				$n = 0;
				if(!empty($html->find('th', $g)->plaintext)){
				
						foreach($table->find('tr') as $row) { // loop for tr for get product group head or features
						
							$arr_product_features[$g]['group'] = $html->find('th', $g)->plaintext;
							// initialize array to store the cell data from each row
							$f = 0;
							
							foreach($row->find('td') as $cell) {
								// loop for get product feature name
									$attr =  str_replace('%','',$cell->getAttribute('width'));
									
									$feature_name_and_value = trim($cell->innertext);
									if(!empty($attr)){
										
										$arr_product_features[$g]['feature'][0][$n]['feature'] = $feature_name_and_value;
									}else{
										
										$arr_product_features[$g]['feature'][0][$n]['feature_value'] = $feature_name_and_value;
									}
									
							$f++;
							}
							
						$n++;	
						} 
					}
			// $g++;	
				}
			//}
           
			}
			
		
		$returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $arr_product_images;
        $returnarray['product_price'] = $productPrice;
        $returnarray['product_features'] = $arr_product_features;

        //echo '<pre>'; print_r($returnarray); die;
        return $returnarray;
    }

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
}
