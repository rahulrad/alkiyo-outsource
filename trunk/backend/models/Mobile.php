<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for collection "Mobile".
 *
 * @property \MongoId|string $_id
 * @property mixed $store_name
 * @property mixed $brand_name
 * @property mixed $product_name
 * @property mixed $product_description
 * @property mixed $model_number
 * @property mixed $cod
 * @property mixed $return_policy
 * @property mixed $delivery
 * @property mixed $price
 * @property mixed $url
 * @property mixed $image
 */
class Mobile extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['nayashoppy', 'Mobile'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'store_name',
            'brand_name',
            'product_name',
            'product_description',
            'model_number',
            'cod',
            'return_policy',
            'delivery',
            'price',
            'url',
            'image',
			'other_images',
			'unique_id',
			'emi',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_name', 'brand_name', 'product_name', 'product_description', 'model_number', 'cod', 'return_policy', 'delivery', 'price', 'url', 'image','other_images','emi','unique_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'store_name' => 'Store Name',
            'brand_name' => 'Brand Name',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'model_number' => 'Model Number',
            'cod' => 'Cod',
            'return_policy' => 'Return Policy',
            'delivery' => 'Delivery',
            'price' => 'Price',
            'url' => 'Url',
            'image' => 'Image',
			'other_images' => 'Other Images',
			'unique_id' => 'Unique Id',
			'emi'=> 'emi'
        ];
    }
	
	public function test(){
			echo 'my test mobile model'; die;
	}
}
