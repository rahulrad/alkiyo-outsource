<?php

namespace backend\models;

use Yii;
use common\models\Features;
use common\models\FeatureValues;
use common\models\FeatureGroups;
use common\models\ProductFeature;

/**
 * This is the model class for collection "MongoFeatureGroups".
 *
 * @property \MongoId|string $_id
 * @property mixed $id
 * @property mixed $category_id
 * @property mixed $name
 * @property mixed $sort_order
 * @property mixed $created
 */
class MongoFeatureGroups extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['nayashoppy', 'MongoFeatureGroups'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'category_id',
            'name',
            'sort_order',
            'created',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'name', 'sort_order', 'created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'id' => 'Id',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'created' => 'Created',
        ];
    }
	
	
	
	
	// save product features to prodcuts data
	public function saveProductFeatureGroup($product_id, $category_id, $productFeatures){
				
				
				foreach($productFeatures as $productFeature){
					
					$getFeatureDetail = MongoFeatures::findOne(['id'=>  (int)$productFeature['feature_id']]);
					
					if(!empty($getFeatureDetail)){
						
						$getFeatureGroupDetail = MongoFeatureGroups::findOne(['id'=>  (int)$getFeatureDetail->feature_group_id]);
						
						$feature_group = FeatureGroups::findOne(['name'=>  $getFeatureGroupDetail->name]);
						
						$max_sort_order = FeatureGroups::find()->orderBy('sort_order DESC')->one();
						
						if(!empty($max_sort_order)){
						
							$sort_order = $max_sort_order->sort_order + 1;
						
						}else{
						
							$sort_order = 1;
						}
						
						//echo '<pre>'; print_r($max_sort_order); 
						
						if ($feature_group == null) {
						  
						 //Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
									$feature_group_model = new FeatureGroups();
									$feature_group_model->category_id = $category_id;
									$feature_group_model->name = $getFeatureGroupDetail->name;
									$feature_group_model->sort_order = $sort_order;
									$feature_group_model->created = date('Y-m-d h:i:s');
									$feature_group_model->modified = date('Y-m-d h:i:s');
									$feature_group_model->save();
									
									$feature_group = FeatureGroups::findOne(['name'=>  $getFeatureGroupDetail->name]);
										
						}  
						
						if(isset($feature_group->id) && !empty($feature_group->id)){
							$mongoFeaturesModel = new MongoFeatures();
							$mongoFeaturesModel->saveProductFeatureData($product_id, $category_id,$feature_group->id, $productFeatures);
						
						}
					}
					
				} 
				
				return $feature_group;
	}
	
	
}
