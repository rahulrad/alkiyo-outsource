<?php

namespace backend\models;

use Yii;
use common\models\Features;
use common\models\FeatureValues;
use common\models\FeatureGroups;
use common\models\ProductFeature;

/**
 * This is the model class for collection "MongoFeatureValues".
 *
 * @property \MongoId|string $_id
 * @property mixed $id
 * @property mixed $feature_id
 * @property mixed $value
 * @property mixed $created
 */
class MongoFeatureValues extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['nayashoppy', 'MongoFeatureValues'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'feature_id',
            'value',
			'display_name',
            'created',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'feature_id', 'value','display_name', 'created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'id' => 'Id',
            'feature_id' => 'Feature ID',
            'value' => 'Value',
			'display_name' => 'Display Name',
            'created' => 'Created',
        ];
    }
	
	
	public function saveProductFeatureValue($product_id, $feature_id, $product_feature_values){
			$feature_value_data = '';
			if(!empty($product_feature_values)){
						
						foreach($product_feature_values as $feature_value_value){
									
									if(!empty($feature_value_value)){
											
											$getFeatureValueDetail = MongoFeatureValues::findOne(['id'=>  (int)$feature_value_value]);
											
											$feature_value_data = FeatureValues::findOne(['value'=>  $getFeatureValueDetail->value, 'feature_id' => $feature_id]);
											
											if ($feature_value_data == null) {
											  
											  
												
											//   Tools::alertMail("Create a new brand named $this->brand", "Create Brand", 'vikashkumar@girnarsoft.com');   
														$feature_value_model = new FeatureValues();
														$feature_value_model->feature_id = $feature_id;
														$feature_value_model->value = $getFeatureValueDetail->value;
														$feature_value_model->display_name = $getFeatureValueDetail->value;
														$feature_value_model->created = date('Y-m-d h:i:s');
														$feature_value_model->save();
														
														$feature_value_data = FeatureValues::findOne(['value'=>  $getFeatureValueDetail->value, 'feature_id' => $feature_id]);
															
											
											if(isset($feature_value_data->id) && !empty($feature_value_data->id)){
													$mongoProductFeaturesModel = new MongoProductFeatures();
													$mongoProductFeaturesModel->saveProductFeatureFromMongoDB($product_id, $feature_id,$feature_value_data->id);
									
										}
									
									}  
								
							}
						}
					}
		
	}
	
	
	
	
	
}
