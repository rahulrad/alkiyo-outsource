<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MongoFeatureValues;

/**
 * MongoFeatureValuesSearch represents the model behind the search form about `backend\models\MongoFeatureValues`.
 */
class MongoFeatureValuesSearch extends MongoFeatureValues
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'id', 'feature_id', 'value', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MongoFeatureValues::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'feature_id', $this->feature_id])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'created', $this->created]);

        return $dataProvider;
    }
}
