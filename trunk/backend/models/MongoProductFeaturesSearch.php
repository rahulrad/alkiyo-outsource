<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MongoProductFeatures;

/**
 * MongoProductFeaturesSearch represents the model behind the search form about `backend\models\MongoProductFeatures`.
 */
class MongoProductFeaturesSearch extends MongoProductFeatures
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'id_product_feature', 'id_product', 'id_feature', 'id_feature_value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MongoProductFeatures::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'id_product_feature', $this->id_product_feature])
            ->andFilterWhere(['like', 'id_product', $this->id_product])
            ->andFilterWhere(['like', 'id_feature', $this->id_feature])
            ->andFilterWhere(['like', 'id_feature_value', $this->id_feature_value]);

        return $dataProvider;
    }
}
