<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for collection "MongoProducts".
 *
 * @property \MongoId|string $_id
 * @property mixed $store_name
 * @property mixed $brand_name
 * @property mixed $product_name
 * @property mixed $product_description
 * @property mixed $model_number
 * @property mixed $cod
 * @property mixed $return_policy
 * @property mixed $delivery
 * @property mixed $price
 * @property mixed $url
 * @property mixed $image
 * @property mixed $other_images
 * @property mixed $emi
 * @property mixed $unique_id
 * @property mixed $status
 * @property mixed $colors
 * @property mixed $offers
 * @property mixed $product_mapping
 */
class MongoProducts extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['nayashoppy', 'MongoProducts'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
			'product_id',
            'store_name',
			'store_id',
            'brand_name',
			'category_name',
            'product_name',
            'product_description',
            'model_number',
            'cod',
            'return_policy',
            'delivery',
            'price',
            'url',
            'image',
            'other_images',
            'emi',
            'unique_id',
            'status',
			'discount',
			'rating',
			'reviews',
			'shipping',
			'original_price',
			'rating_user_count',
			'created',
			'updated',
            'colors',
            'sizes',
            'offers',
            'product_mapping',
            'instock'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id','store_name','store_id', 'brand_name','category_name', 'product_name', 'product_description', 'model_number', 'cod', 'return_policy', 'delivery', 'price', 'url', 'image', 'other_images', 'emi', 'unique_id', 'status','category_name','discount','rating','reviews','shipping','original_price','rating_user_count','created','colors','sizes','product_mapping','instock'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'store_name' => 'Store Name',
            'brand_name' => 'Brand Name',
			'category_name'=>'Category Name',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'model_number' => 'Model Number',
            'cod' => 'Cod',
            'return_policy' => 'Return Policy',
            'delivery' => 'Delivery',
            'price' => 'Price',
            'url' => 'Url',
            'image' => 'Image',
            'other_images' => 'Other Images',
            'emi' => 'Emi',
            'unique_id' => 'Unique ID',
            'status' => 'Status',
			'store_id' =>'Store Id',
			'discount'=>'Discount',
			'rating'=>'Rating',
			'reviews'=>'Reviews',
			'shipping' => 'Shipping',
			'original_price' => 'Original Price',
			'created' => 'created',
        ];
    }
	
	 
	 /**
	 /	@ getMongoProductFeatures
	 / this function for get mongo product feature by mongo product id
	 */
	 
	 public function getMongoProductFeatures($mongo_product_id){
			
			if(!empty($mongo_product_id)){
				
				$mongoProductDetail = MongoProducts::find()->where(['_id' => $mongo_product_id])->one();
				//echo '<pre>'; print_r($mongoProductDetail->product_id); die;
				$mongProductFeatureModel = new MongoProductFeatures();
				
				$mongoProductFeatures = $mongProductFeatureModel->getMongoProductFeatures($mongoProductDetail->product_id);
				//echo '<pre>mongoProductFeatures'; print_r($mongoProductFeatures); die;
				return $mongoProductFeatures;
				//$selectedFeatureValues = $productFeatureModel->getSelectedProductFeatures($product_id);
			}
				
	 }
	 
}
