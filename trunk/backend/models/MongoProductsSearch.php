<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MongoProducts;

/**
 * MongoProductsSearch represents the model behind the search form about `backend\models\MongoProducts`.
 */
class MongoProductsSearch extends MongoProducts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'store_name','product_id', 'store_id', 'brand_name', 'product_name', 'product_description', 'model_number', 'cod', 'return_policy', 'delivery', 'price', 'url', 'image', 'other_images', 'emi', 'unique_id', 'status','category_name','rating','discount','created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MongoProducts::find();
        $query->Where(['product_mapping' => null]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 100,
			],
			'sort'=> ['defaultOrder' => ['_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		/**$query->andFilterWhere([
            '_id' => (int)$this->_id,
            'store_id' => (int)$this->store_id,
            'product_id' => (int)$this->product_id,
        ]); **/

		//echo $this->created; die;

		$query->andFilterWhere([
            'status' => 1,
        ]);

		$query->andFilterWhere(['like', 'brand_name', $this->brand_name])
			->andFilterWhere(['==', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
			->andFilterWhere(['like','store_name', $this->store_name])
			->andFilterWhere(['==','store_id', (int)$this->store_id]);
		//	->andFilterWhere(['==','rating', $this->rating])
			//->andFilterWhere(['==','discount', $this->discount]);
		if(!empty($this->created)){
			$query->andFilterWhere(['like', 'created', date('Y-m-d', strtotime($this->created))]);
		}
		if(!empty($this->rating)){
			$query->andFilterWhere(['>=', 'rating', $this->rating]);
			//$query->andFilterWhere(['==', 'rating', $this->rating + 1]);
		}


		if(!empty($this->discount)){
			$discount = explode('-',$this->discount);
			$query->andFilterWhere(['>=', 'discount', $discount[0]]);
		}

		if(!empty($this->discount)){
			$discount = explode('-',$this->discount);
			$query->andFilterWhere(['<', 'discount', $discount[1]]);
		}





			//->andFilterWhere(['==', 'product_id', (int)$this->product_id]);
			//->andFilterWhere(['==', '_id', $this->_id])
			//;
			//->andFilterWhere(['==', 'product_id', (int)$this->product_id]);
		//echo '<pre>'; print_r($dataProvider); die;


        return $dataProvider;
    }
}
