<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class PayTmProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName="")
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
		
        $retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url);
        $this->arr = $retrnarr;
        $html->clear();
		
        unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url)
    {
        $siteName = 'http://www.paytm.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
		
		$product_unique_id = '';
		$productDeliverd = '';
		$productReturnPolicy = '';
		$productFeatures = '';
		$productCOD = '';

		
        $arr_merchant = '';
		// product title
        if ($html->find('.pdp-e-i-head', 0)) {
            $productNamehtml = $html->find('.pdp-e-i-head', 0)->plaintext;
			$productNamehtml = trim($productNamehtml);
        }
		
		
		// product price
        if ($html->find('.payBlkBig', 0)) {
            if ($html->find('.payBlkBig', 0)) {
                $productPrice = $html->find('.payBlkBig', 0)->plaintext;
                $productPrice = $this->convet_price_string($productPrice);
            }
        }
		
		// product main image
      if ($html->find('.imgWrapper', 0)) {
            $productImgSrchtml = $html->find('.imgWrapper img', 0)->getAttribute('data-src');
        }
		
		$main_img_anchors = $html->find('#bx-pager-left-image-panel a');
		$i_num = 0;
		foreach( $main_img_anchors as $i => $anchor ) {
			
				$img = explode('"',$anchor->innertext);
				
				
					if(isset($img[5]) && !empty($img[5]) && $i_num == 0){
						$productImgSrchtml = $img[5];
					}
	
			$i_num++;
		}
		
		
		// product unique id
		if ($html->find('#add-cart-button-id', 0)) {
            if ($html->find('#add-cart-button-id', 0)) {
                $product_unique_id = $html->find('#add-cart-button-id', 0)->getAttribute('catalog');
            }
        }
		
		$arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
       // $arr_merchant[] = array();

       /* if ($html->find('.proDescript', 0)) {
             $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
        }*/
		
		
		// product details 
		$product_detail = array();
		 
		 // product category
		 $product_detail['Category'] = $categoryName;
		 $product_detail['cash_on_delivery'] = 'Cash On Delivery';
		 $product_detail['return_policy'] = '100% Payment protection. 7 day easy return in case item is defective or damaged or different from what was described';
		
		 $pro_title = explode(' ',$productNamehtml);
		 $product_detail['brand'] = $pro_title[0];
		 
		 $product_detail['model_number'] = trim(str_replace($pro_title[0],'',$productNamehtml));
		 
		
		// product model number
		if ($html->find('.product-details', 0)) {
            $productModelHtml = $html->find('.title-wrap', 0)->getAttribute('data-evar2');
			$productModelHtml = str_replace($productBrandHtml,'',$productModelHtml);
			$product_detail['model_number'] = trim($productModelHtml);
        }
		
		// product description
		$product_detail['descritpion'] = '';
		if ($html->find('.spec-body', 0)) {
             $productDetailhtml = $html->find('.detailssubbox', 1)->plaintext;
			 $product_detail['descritpion'] = trim($productDetailhtml);
        }
		
		// product EMI details
		$product_detail['emi'] = '';
		if ($html->find('.pdp-emi', 0)) {
            if ($html->find('.pdp-emi', 0)) {
                $product_detail['emi']  = $html->find('.pincode-emi', 0)->plaintext;
				
            }
        }
		
		
		/*if ($html->find('.default-shipping-charge', 0)) {
            if ($html->find('.default-shipping-charge', 0)) {
                $product_detail['product_deliverd']  = $html->find('.default-shipping-charge span', 0)->plaintext;
                //$productDelivered = $this->convet_price_string($productPrice);
            }
        }*/
		
		//// product short features
		if ($html->find('.pdp-e-i-keyfeatures', 0)) {
            if ($html->find('.pdp-e-i-keyfeatures', 0)) {
                $product_detail['productFeatures']  = trim($html->find('.pdp-e-i-keyfeatures', 0)->plaintext);
               
            }
        }
		
		
		
		// product feature details		
	$arr_product_images = array();
		$anchors = $html->find('#bx-pager-left-image-panel a');
			// loop and print nodes content
		$img_num = 0;
		foreach( $anchors as $i => $anchor ) {
				if($img_num <= 5){
					$img = explode('"',$anchor->innertext);
					
					if(isset($img[5]) && !empty($img[5])){
						$arr_product_images[] = $img[5];
					}
				}
			$img_num++;
		}
		
	
	
		// product feature details		
		$arr_product_features = array();
		 if ($html->find('.specTable', 0)) {
			$g = 0;
            foreach ($html->find(".specTable",$g) as $skey => $sval) {
				
				$table = $html->find(".specTable",$g); // table record

				// initialize empty array to store the data array from each row
				$theData = array();
				
				// loop over rows
				$n = 0;
				if(!empty($html->find('.groupHead', $g)->plaintext)){
				
						foreach($table->find('tr') as $row) { // loop for tr for get product group head or features
						
							$arr_product_features[$g]['group'] = $html->find('.groupHead', $g)->plaintext;
							// initialize array to store the cell data from each row
							$f = 0;
							foreach($row->find('td.specsKey') as $cell) {  // loop for get product feature name
								 $feature_name = trim($cell->innertext);
								 $arr_product_features[$g]['feature'][$f][$n]['feature'] = $feature_name;
							
							$f++;
							}
							
							$v= 0;
							foreach($row->find('td.specsValue') as $cell) { // loop for get product feature value
						
								// push the cell's text to the array
								$feature_value = trim($cell->innertext);
								$arr_product_features[$g]['feature'][$v][$n]['feature_value'] = $feature_value;
							
							$v++;
							}
						
						$n++;	
						} 
					}
			 $g++;	
				}
           
			}
			
		
		
        $returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $arr_product_images;
        $returnarray['product_price'] = $productPrice;
		$returnarray['product_features'] = $arr_product_features;
 		echo '<pre>'; print_r($returnarray); die;
        return $returnarray;
    }

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
}
