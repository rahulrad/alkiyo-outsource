<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\simple_html_dom;
use common\components\HttpService;

//require_once 'simple_html_dom.php';
//require_once 'JungleeProductDetailScrapper.php';
//require_once('Net/URL2.php');

class PayTmProductScrapper {

    /**
     * get All Paging url from Junglee Site and save in DB All product pages Url.
     */
    public function getAllPagingUrl($url) {
        $strurl = str_replace('{pg}', 1, $url);

//        $response = $this->fetchContentUsingProxy($strurl);
//
//        if (!$response)
//            return false;
//
//        $html = $this->LoadHTMLContent($response);
//        if ($html->find('#resultCount')) {
//            $totalProductStr = $html->find('#resultCount span', 0)->plaintext;
//            $arr_records = explode('of', $totalProductStr);
//            $productPerPage = 24;
//            $totalProduct = $this->convert_total_pages_string($arr_records[1]);
//            $noOfPages = ceil($totalProduct / $productPerPage);
//        }
        $productPerPage = 20;
        $noOfPages = ceil(100 / $productPerPage);
        $arr_url = array();
        for ($i = 1; $i <= 8360; $i+=20) {
            $strurl = str_replace('{pg}', $i, $url);
            $arr_url[] = $strurl;
        }
        return $arr_url;
    }

    /**
     * Save All Product Pages Url in Data Base.
     */
    public function saveAllPagingUrl($arr_url, $categoryName, $categoryType) {
        $connection = Yii::$app->db;
        foreach ($arr_url as $key => $value) {
            $query = "SELECT site_url from category_url where site_url='" . $value . "'";
            $command = $connection->createCommand($query);
            $arr_result = $command->queryAll();
            $no_of_records = count($arr_result);

            if ($no_of_records == 0) {
                $insert_query = "INSERT INTO category_url (category_name,site_url, category_type) VALUES(:category_name, :site_url, :category_type)";
                $insert_command = $connection->createCommand($insert_query);
                $category_name = $categoryName;
                $insert_command->bindParam(':category_name', $category_name);
                $insert_command->bindParam(':site_url', $value);
                $insert_command->bindParam(':category_type', $categoryType);
                $insert_command->execute();
            }
        }
    }

    /**
     * Save Product In DB.
     */
    public function saveProduct($categoryName, $arr_department_type, $categoryType, $latest_product = 0,$store_id) {
		//echo $categoryName; die;
        $connection = Yii::$app->db;
        /* This condition indicate that if new product add on this category then first we set all the status as 1 and after that we updated status column as 0 of top 2 pages and save new product in our DB. */
        /* if ($latest_product == 1) {

          $update_sql= "update junglee_category_url set status=1 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url";
          $update_result = yii::app()->db->createCommand($update_sql);
          $update_result->execute();

          $update_query = "update junglee_category_url set status=0 where category_type='" . $categoryType . "' and category_name='" . $categoryName . "' order by id_category_url LIMIT 2";
          $update_query = yii::app()->db->createCommand($update_query);
          $update_query->execute();
          } */

        $objPayTmProduct = new \backend\models\PayTmProductScrapper();
        $query = "SELECT id_category_url,category_name,site_url from category_url where status=0 and category_type='" . $categoryType . "' and category_name='" . $categoryName . "' and store_id='" . $store_id . "' order by id_category_url limit 1";
        $command = $connection->createCommand($query);
        $arr_data = $command->queryAll();
		//echo '<pre>'; print_r($arr_data); die;
//        $arr_data[] = array('site_url'=>'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007');

		
	    //  echo '<pre>'; print_r($arr_data); die;
		
        if (is_array($arr_data)) {
            foreach ($arr_data as $key => $value) {
                //if ($key == 0) {
				//echo $value['site_url'].'=======>'.$categoryName; die;
              //  $data = $objPayTmProduct->getAllProductDetailsOnPage($value['site_url'], $categoryName);
			  
			  $ch = curl_init();
			  $timeout = 5;
			  curl_setopt($ch, CURLOPT_URL, $value['site_url']);
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			  $data = curl_exec($ch);
			  curl_close($ch);
			  
			 // return $data;
				
				$data = json_decode($data);
				
				
				
                try {
					if(isset($data->grid_layout)){
                    foreach ($data->grid_layout as $pkey => $pval) {
						
						
						 $ch = curl_init();
						  $timeout = 5;
						  curl_setopt($ch, CURLOPT_URL, $pval->url);
						  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
						  $product_data = curl_exec($ch);
						  curl_close($ch);
			  			
						$product_detail = json_decode($product_data);
						//echo '<pre>'; print_r($product_detail); die;
						if(isset($product_detail->name) && !empty($product_detail->name)){
								
								$pro_description = '';
								if(isset($product_detail->bullet_points) && isset($product_detail->bullet_points->salient_feature) && !empty($product_detail->bullet_points->salient_feature)){
									foreach($product_detail->bullet_points->salient_feature as $p_desc){
										$pro_description .= trim($p_desc).'<br>';
									}
								
								}
								
								// get product discount
								$product_discount = '';
								if(!empty($product_detail->discount)){
									$product_discount = str_replace('%','',$product_detail->discount); 
								}
								
								
								
								// get all images in array;
								$product_img_arr = array();
								if(isset($product_detail->other_images)){
									foreach($product_detail->other_images as $other_images){
										$product_img_arr[] = $other_images;
									}
								}
								
								// get product features
								$product_features = array();
								if(isset($product_detail->long_rich_desc)){
									foreach($product_detail->long_rich_desc as $pro_features){
										$product_features[] = $pro_features;
									}
								}
								
								// check emi or cash on delivery support
								$cash_on_delivery = '';
								$emi_policy = '';
								if(isset($product_detail->pay_type_supported)){
										foreach($product_detail->pay_type_supported as $key => $pay_type_supported){
											
											if($key == 'COD' && $pay_type_supported == 1){
												$cash_on_delivery = 'Cash on delivery';
											}elseif($key == 'EMI' && $pay_type_supported == 1){
												$emi_policy = 'EMI supported';
											}
										}
								
								}
								
								
								$name = $product_detail->name;
								$price = $product_detail->offer_price;
								//echo $price; die;
								$model = isset($product_detail->attributes->model_name) ? $product_detail->attributes->model_name : '';
								$brand = isset($product_detail->brand) ? $product_detail->brand : '';
								$description = $pro_description;
								$emi = $emi_policy;
								$cash_on_delivery = $cash_on_delivery;
								$return_policy = '';
								$imgpath = isset($product_detail->image_url) ? $product_detail->image_url : '';
								$murl = isset($product_detail->shareurl) ? $product_detail->shareurl : '';
								//echo $murl; die;
								$m_unique_id = isset($product_detail->merchant_sku) ? $product_detail->merchant_sku : '';
								$discount = $product_discount;
								$reviews = '';
								$rating = '';
								$product_delivery = '';
								
								// $model = \common\components\Tools::removeBrand($name, $brand);
		
								//$importer = new BaseProductImporter();
								//$importer->setName($name);
		
								$sandle_cat_name = 'Sandal';
								$slipper_cat_name = 'Slipper';
		
								if (stristr($name, $sandle_cat_name)) {
									//$importer->setCategory('Sandals');
									$importer = new BasePayTmProductImporter('Sandals', $name);
								} elseif (stristr($name, $slipper_cat_name)) {
									//$importer->setCategory('Slippers');
									$importer = new BasePayTmProductImporter('Slippers', $name);
								} else {
									//$importer->setCategory($value['category_name']);
									$importer = new BasePayTmProductImporter($value['category_name'], $name);
								}
								
								$importer->setLowestPrice($price);
								$importer->setLinkRewrite($name);
								$importer->setModel($model);
								$importer->setBrand($brand);
								$importer->setdescription($description);
								$importer->setProductEMI($emi);
								$importer->setCategory($value['category_name']);
								
								$importer->setCOD($cash_on_delivery);
								$importer->setReturnPolicy($return_policy);
								$importer->setMainImages($imgpath);
								$importer->setProducturl($murl);
								$importer->setProductUniqueId($m_unique_id);
								
								$importer->setMultipleImages($product_img_arr);
								$importer->setProductFeature($product_features);
								$importer->setProductDiscount($discount);
								$importer->setProductRating($rating);
								$importer->setProductDelivery($product_delivery);
								
								$importer->setProductReviews($reviews);
								
							   // $importer->setReferenceNumber($ref_no);
		
							   /* $front_image = array();
								$product_images = array();
								if (is_array($pval['product_img_arr']) && count($pval['product_img_arr'])) {
									foreach ($pval['product_img_arr'] as $ikey => $ival) {
										if ($ikey == 0)
											$front_image['name'] = "Front View";
										else
											$front_image['name'] = "";
										$front_image['url'] = $ival;
										$product_images[] = $front_image;
										$importer->setImages($product_images);
									}
								} else {
									$front_image['name'] = "Front View";
									$front_image['url'] = $pval['imgpath'];
									$product_images[] = $front_image;
									$importer->setImages($product_images);
								}*/
		
		
		//                        if (is_array($pval['product_detail'])) {
		//                            foreach ($pval['product_detail'] as $fkey => $fval) {
		//                                if ($fkey == 'Target Audience') {
		//                                    unset($pval['product_detail']['Category']);
		//                                    unset($pval['product_detail']['Name']);
		//                                    unset($pval['product_detail']['For']);
		//                                } else {
		//                                    unset($pval['product_detail']['Category']);
		//                                    unset($pval['product_detail']['Name']);
		//                                }
		//                            }
		//                        }
		//                        $product_features = array();
		//                        foreach ($pval['product_detail'] as $fkey => $fval) {
		//
		//                            if ($fkey == 'Target Audience')
		//                                $fkey = 'For';
		//
		//                            if ($fkey == 'Colour')
		//                                $fkey = 'Color';
		//
		//                            if ($fkey == 'Colour Name')
		//                                $fkey = 'Looks';
		//
		//                            if ($fkey == 'For') {
		//                                foreach ($arr_department_type as $akey => $aval) {
		//                                    $fval = $aval;
		//                                }
		//                            }
		//
		//                            if ($fval == 1)
		//                                $fval = str_replace("1", "One", $fval);
		//                            if ($fval == 0)
		//                                $fval = str_replace("0", "Zero", $fval);
		//                            $product_features[] = array('name' => $fkey, 'value' => $fval);
		//                        }
		//                        $importer->setFeatures($product_features);
		//                        $product_suppliers = array();
		//                        if (is_array($pval['merchant_arr'])) {
		//                            foreach ($pval['merchant_arr'] as $spkey => $suval) {
		//                                $mname = $this->getStoreName($suval['mname']);
		//                                $mprice = $suval['mprice'];
		//                                $murl = $this->storeNameURL($suval['murl'], $mname);
		//                                $product_suppliers[] = array('storename' => $mname, 'price' => $mprice, 'sellerurl' => $murl);
		//                            }
		//                        }
		
		//                        $importer->setSuppliers($product_suppliers);
		
								//echo '<pre>'; print_r($importer); die;
								$importer->save();
								
						}
                    }

                    $q = "update category_url SET status = '1' where id_category_url = '" . $value['id_category_url'] . "'";
                    echo $q;
                    $comm = $connection->createCommand($q);
                    $comm->bindParam(':id_category_url', $key);
                    $comm->execute();
				  }
                } catch (Exception $e) {
                    echo "wwwwwwwwwww".$e->getMessage().'::'.$e->getLine().'::'.$e->getTraceAsString();exit;
                    continue;
                }
            }
        }
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getProductUrlOnPage($url, $categoryName) {
		//echo $url.'====>'.$categoryName; die;
        $response = $this->fetchContentUsingProxy($url);
      // echo '<pre>'; print_r($response); die;
	    if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
		//echo '<pre>'; print_r($html); die;
		$arr_product = array();
        if ($html->find('.js-section', 0)) {
           // $siteName = 'http://www.snapdeal.com';
            foreach ($html->find(".product-tuple-listing") as $key => $pval) {
                if ($pval->find('.product-desc-rating a', 0)) {
                    $productUrl = $pval->find('.product-desc-rating a', 0)->getAttribute('href');
                    $arr_product[] =  $productUrl;
					//print_r($arr_product); echo '<br>';
                }
            }
        }
        $html->clear();
        unset($html);
//echo '<pre>'; print_r($arr_product); die;
        return $arr_product;
    }

    private function getAllProductDetailsOnPage($url, $categoryName) {
        $product_urls = $this->getProductUrlOnPage($url, $categoryName);
        $product_details = array();
        if (is_array($product_urls)) {
            foreach ($product_urls as $key => $value) {
                $product_details[] = $this->getProductDetail($value, $categoryName);
            }
        }

//       echo '<pre>';print_r($product_details);exit;
        return $product_details;
    }

    private function getProductDetail($url, $category) {
        $objPayTm_product_detail = new \backend\models\PayTmProductDetailScrapper($url, $category);
	
        return $objPayTm_product_detail->StoreDataInArray();
    }

    private function storeNameURL($url, $storename) {
        $oUrl = new Net_URL2($url);
        if ($storename == 'bata.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    if ($key != 'productID') {
                        $oUrl->unsetQueryVariable($key);
                    }
                }
            }
        }

        if ($storename == 'jabong.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'yebhi.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'fashos.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'trendstiger.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'shoppersbank.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'metroshoes.net') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    if ($key != 'productid') {
                        $oUrl->unsetQueryVariable($key);
                    }
                }
            }
        }

        if ($storename == 'shop4reebok.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    if ($key != 'pID') {
                        $oUrl->unsetQueryVariable($key);
                    }
                }
            }
        }

        if ($storename == 'brandooz.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'shopllers.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'playmore.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'yepme.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    if ($key != 'CampId') {
                        $oUrl->unsetQueryVariable($key);
                    }
                }
            }
        }

        if ($storename == 'playgroundonline.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    if ($key != 'id') {
                        $oUrl->unsetQueryVariable($key);
                    }
                }
            }
        }

        if ($storename == 'fabulloso.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'shoppersstop.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'indiaplaza.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'shop.inonit.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'freecultr.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'thebombaystore.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'oyhoy.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'pumashop.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'globalite.retailmart.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'casioindiaclub.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'buyatbrands.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'grabmore.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'rightshopping.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'amazon.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'watchkart.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'ezmaal.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'letsshop.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'indiarush.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'charma.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'gadgets.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'shop.fastrack.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'wopshop.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'maebag.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    if ($key != 'Code' || $key != 'Param') {
                        $oUrl->unsetQueryVariable($key);
                    }
                }
            }
        }

        if ($storename == 'mangostreet.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'inkfruit.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'crosscreek.in') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'fabindia.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'retailmart.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'brandfoyer.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'bestylish.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'redlily.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }

        if ($storename == 'globusstores.com') {
            $arr_query_string = $oUrl->getQueryVariables();

            if (is_array($arr_query_string)) {
                foreach ($arr_query_string as $key => $value) {
                    $oUrl->unsetQueryVariable($key);
                }
            }
        }
        /*
          if ($storename == 'kaiiros.com') {
          $arr_query_string = $oUrl->getQueryVariables();

          if (is_array($arr_query_string)) {
          foreach ($arr_query_string as $key => $value) {
          $oUrl->unsetQueryVariable($key);
          }
          }
          } */

        $return_url = $oUrl->getURL();
        if ($storename == 'amazon.com') {
            $return_url = str_replace('ref=junglee', '', $return_url);
        }

        return $return_url;
    }

    private function getStoreName($name) {
        if ($name === 'Yebhi')
            return 'yebhi.com';
        if ($name === 'Bata India Limited')
            return 'bata.in';
        if ($name === 'Jabong')
            return 'jabong.com';
        if ($name === 'FASHOS')
            return 'fashos.com';
        if ($name === 'Trendstiger')
            return 'trendstiger.com';
        if ($name === 'ShoppersBank')
            return 'shoppersbank.com';
        if ($name === 'Metro Shoes Ltd')
            return 'metroshoes.net';
        if ($name === 'REEBOK')
            return 'shop4reebok.com';
        if ($name === 'Brandooz')
            return 'brandooz.com';
        if ($name === 'Shopllers')
            return 'shopllers.com';
        if ($name === 'PlayMore.in')
            return 'playmore.in';
        if ($name === 'Yepme')
            return 'yepme.com';
        if ($name === 'Playgroundonline')
            return 'playgroundonline.com';
        if ($name === 'fabulloso!')
            return 'fabulloso.com';
        if ($name === 'Shoppers Stop')
            return 'shoppersstop.com';
        if ($name === 'indiaplaza' || $name == 'Indiaplaza')
            return 'indiaplaza.com';
        if ($name === 'Shop InOnIt')
            return 'shop.inonit.in';
        if ($name === 'FREECULTR')
            return 'freecultr.com';
        if ($name === 'The Bombay Store')
            return 'thebombaystore.com';
        if ($name === 'OyhOy - Curated Fashion Trends For You')
            return 'oyhoy.com';
        if ($name === 'Pumashop.in')
            return 'pumashop.in';
        if ($name === 'Globalite Retailmart')
            return 'globalite.retailmart.com';
        if ($name === 'Casio India Club')
            return 'casioindiaclub.com';
        if ($name === 'Buyatbrands India')
            return 'buyatbrands.com';
        if ($name === 'Grabmore')
            return 'grabmore.in';
        if ($name === 'RightShopping.in')
            return 'rightshopping.in';
        if ($name === 'Amazon.com')
            return 'amazon.com';
        if ($name === 'WatchKart')
            return 'watchkart.com';
        if ($name === 'eZmaal')
            return 'ezmaal.com';
        if ($name === 'LetsShop.in')
            return 'letsshop.in';
        if ($name === 'IndiaRush')
            return 'indiarush.com';
        if ($name === 'Charma Pvt. Ltd.')
            return 'charma.com';
        if ($name === 'Gadgets India')
            return 'gadgets.in';
        if ($name === 'shop.fastrack.in-OFFICIAL Fastrack Store')
            return 'shop.fastrack.in';
        if ($name === 'wopshop')
            return 'wopshop.com';
        if ($name === 'MaEbag')
            return 'maebag.com';
        if ($name === 'MangoStreet')
            return 'mangostreet.com';
        if ($name === 'Inkfruit')
            return 'inkfruit.com';
        if ($name === 'CROSSCREEK')
            return 'crosscreek.in';
        if ($name === 'Fabindia')
            return 'fabindia.com';
        if ($name === 'Retailmart')
            return 'retailmart.com';
        if ($name === 'brandfoyer')
            return 'brandfoyer.com';
        if ($name === 'beStylish')
            return 'bestylish.com';
        if ($name === 'Redlily')
            return 'redlily.com';
        if ($name === 'Globus Stores')
            return 'globusstores.com';
        if ($name === 'KAIIROS')
            return 'kaiiros.com';

        return $name;
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl) {
        $objHttpService = new \common\components\HttpService();
        $response = $objHttpService->getResponse($strurl);
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content) {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Convert No of Total Pages in a string.
     */
    private function convert_total_pages_string($userString) {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
	
	

}
