<?php

namespace backend\models;
use common\models\Suppliers;
use common\models\Categories;
use Yii;

/**
 * This is the model class for table "product_urls".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $cateogry_url_id
 * @property string $url
 * @property integer $status
 * @property integer $scrap_attempt
 * @property integer $scrap_error
 */
class ProductUrls extends \yii\db\ActiveRecord
{

	public $category_csv_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_urls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'cateogry_url_id', 'status', 'category_id', 'scrap_attempt'], 'integer'],
            [['url'], 'string'],
            [['url'], 'unique', 'message' => 'Url already saved', "on" => 'create'],
			[['other','scrap_error'], 'safe'],
			[['category_csv_file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store',
            'cateogry_url_id' => 'Cateogry Url',
			'category_id' => 'Cateogry',
            'url' => 'Url',
            'status' => 'Status',
            'scrap_attempt' => 'Scrap Attempt',
            'scrap_error' => 'Error Scrap'
        ];
    }

	public function getStoreName()
    {
	  return $this::hasOne(Suppliers::className(),['id'=>'store_id'] );
	 }


	 /**
     * @return yii\db\ActiveQuery
     */
    public function getCategoriesName() {
        return $this::hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    public static function getPendingProductsUrlToScrap()
    {
        return self::find()->where(['status' => 0])->andWhere(['<', 'scrap_attempt', 10])->orderBy('scrap_attempt asc')->limit(5)->all();
    }
}
