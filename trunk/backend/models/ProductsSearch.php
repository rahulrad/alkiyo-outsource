<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `backend\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['product_name', 'product_description', 'product_status', 'created', 'categories_category_id', 'brands_brand_id','store_id','rating','discount','is_delete','active','is_approved'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 100,
			],
			'sort'=> ['defaultOrder' => ['product_id'=>SORT_DESC]]
			
        ]);

		
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
	 
	 
		 $query->andFilterWhere([
            'product_id' => $this->product_id,
            'categories_category_id' => $this->categories_category_id,
            'brands_brand_id' => $this->brands_brand_id,
           'store_id' => $this->store_id,
		   'active' => 'active',
		   'is_approved' => 1,
             'is_delete'=>0
			//'rating' => $this->rating,
			//'discount' => $this->discount,
        ]);
		if(!empty($this->created)){
			$query->andFilterWhere(['like', 'created', date('Y-m-d', strtotime($this->created))]);
		}
		if(!empty($this->rating)){
			$query->andFilterWhere(['>=', 'rating', $this->rating])
				->andFilterWhere(['<', 'rating', $this->rating + 1]);
		}
		
		if(!empty($this->discount)){
			$discount = explode('-',$this->discount);
			$query->andFilterWhere(['>=', 'discount', (int)$discount[0]])
				->andFilterWhere(['<', 'discount', (int)$discount[1]]);
		}
		
		
		
        //$query->andFilterWhere(['like', 'created', date('Y-m-d', strtotime($this->created))]);
		

        return $dataProvider;
    }
}
