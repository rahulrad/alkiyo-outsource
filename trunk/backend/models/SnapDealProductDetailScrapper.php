<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

class SnapDealProductDetailScrapper
{
    public $arr;

    public function __construct($url, $categoryName="")
    {
        $response = $this->fetchContentUsingProxy($url);
        if (!$response)
            return false;

        $html = $this->LoadHTMLContent($response);
		
		// finding if product url is already exit then we will only get image
		$exitProduct = Products::find()->select(['product_id','url','image_path'])->where(['url' => $url,'image_path'=>''])->one();
		if(empty($exitProduct)){
			$retrnarr = $this->getHTMLUsingXPath($html, $categoryName, $url);
		}else{
			$retrnarr = $this->getMissingImage($html, $categoryName, $url);
		}
        
		
        $this->arr = $retrnarr;
        $html->clear();
		
        unset($html);
    }

    /**
     * Return site content from a specifice site url.
     */
    private function fetchContentUsingProxy($strurl)
    {
        $objHttpService = new \common\components\HttpService();
        //$response = $objHttpService->getResponse($strurl);
		
		$response = $objHttpService->getResponse($strurl);
		
        return $response;
    }

    /**
     * Load HTML
     */
    private function LoadHTMLContent($content)
    {
        $html = new \common\components\simple_html_dom();
        $html->load($content);
        unset($content);
        return $html;
    }

    /**
     * Sotre Data in Array Format.
     */
    public function StoreDataInArray()
    {
        return $this->arr;
    }

    /**
     * get HTML Data finding with XPath.
     */
    private function getHTMLUsingXPath($html, $categoryName, $url)
    {
		$scraperLog = new Logs();
		//echo $url;
        $siteName = 'http://www.snapdeal.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        $returnarray['product_detail'] = '';
        $returnarray['merchant_arr'] = '';

        $productNamehtml = '';
        $productPrice = '';
        $productImgSrchtml = '';
        $productDetailhtml = '';
        $merchantNamehtml = 'BabyOye';
        $colors = array();
        $sizes = array();
        $offers = array();
        $instock = true;
		
		$product_unique_id = '';
		$productDeliverd = '';
		$productReturnPolicy = '';
		$productFeatures = '';
		$productCOD = '';


        $arr_merchant = '';
		// product title
        if ($html->find('.pdp-e-i-head', 0)) {
            $productNamehtml = $html->find('.pdp-e-i-head', 0)->plaintext;
			$productNamehtml = trim($productNamehtml);
        }else{
				$scraperLog->saveScraperLog('snapdeal scraper','SnapDealProductDetailScrapper','.pdp-e-i-head class not found','Not Found',__LINE__);
		}
		
		
		// product price
        if ($html->find('.payBlkBig', 0)) {
            if ($html->find('.payBlkBig', 0)) {
                $productPrice = $html->find('.payBlkBig', 0)->plaintext;
                $productPrice = $this->convet_price_string($productPrice);
            }
        }else{
				$scraperLog->saveScraperLog('snapdeal scraper','SnapDealProductDetailScrapper','.payBlkBig class not found','Not Found',__LINE__);
		}
		
		// product main image
      
		
		$main_img_anchors = $html->find('#bx-pager-left-image-panel a');
		$i_num = 0;
		foreach( $main_img_anchors as $i => $anchor ) {
			
				$img = explode('"',$anchor->innertext);
				
					if(isset($img[5]) && !empty($img[5]) && $i_num == 0){
						$productImgSrchtml = $img[5];
					}
	
			$i_num++;
		}
		
		if(empty($productImgSrchtml)){
			if ($html->find('.cloudzoom', 0)) {
				$productImgSrchtml = $html->find('.cloudzoom', 0)->getAttribute('src');
			}
		}
		
		
		// product unique id
		if ($html->find('#add-cart-button-id', 0)) {
            if ($html->find('#add-cart-button-id', 0)) {
                $product_unique_id = $html->find('#add-cart-button-id', 0)->getAttribute('catalog');
            }
        }else{
				$scraperLog->saveScraperLog('snapdeal scraper','SnapDealProductDetailScrapper','#add-cart-button-id class not found','Not Found',__LINE__);
		}
		
		$arr_merchant = array('mname' => $merchantNamehtml, 'mprice' => $productPrice, 'murl' => $url, 'm_unique_id' => $product_unique_id);
       // $arr_merchant[] = array();

       /* if ($html->find('.proDescript', 0)) {
             $productDetailhtml = $html->find('.proDescript', 0)->plaintext;
        }*/
		
		
		// product details 
		$product_detail = array();
		 
		 // product category
		 $product_detail['Category'] = $categoryName;
		 $product_detail['cash_on_delivery'] = 'Cash On Delivery';
		 $product_detail['shipping'] = 'Free Shipping';
		 $product_detail['return_policy'] = '100% Payment protection. 7 day easy return in case item is defective or damaged or different from what was described';
		
		 $pro_title = explode(' ',$productNamehtml);
		 $product_detail['brand'] = $pro_title[0];
		 
		 
		 $product_detail['model_number'] = trim(str_replace($pro_title[0],'',$productNamehtml));
		 
		 
		 
		// product original price
		$product_detail['original_price'] = $productPrice;
		 if ($html->find('.pdp-e-i-PAY-r', 0)) {
			 if ($html->find('.pdpCutPrice', 0)) {
                $productOriginalPrice = $html->find('.pdpCutPrice', 0)->plaintext;
                $product_detail['original_price'] = $this->convet_price_string($productOriginalPrice);
            }
        }
		
		// product count users for product rating
		$product_detail['rating_user_count'] = '';
		 if ($html->find('.ratings-wrapper', 0)) {
			 if($html->find('.showRatingTooltip', 0)){
				 $productRatingUserHtml = $html->find('.showRatingTooltip', 0)->plaintext;
				$product_detail['rating_user_count'] = $this->convet_price_string($productRatingUserHtml);
			 }else{
				$scraperLog->saveScraperLog('snapdeal scraper','SnapDealProductDetailScrapper','.showRatingTooltip class not found','Not Found',__LINE__);
			}
            
        }
		
		
		
		// product model number
		if ($html->find('.product-details', 0)) {
			if($html->find('.title-wrap', 0)){
				$productModelHtml = $html->find('.title-wrap', 0)->getAttribute('data-evar2');
				$productModelHtml = str_replace($productBrandHtml,'',$productModelHtml);
				$product_detail['model_number'] = trim($productModelHtml);
			}
        }
		
		
		/*******New filds*******/
		 
		 // product rating
		$product_detail['rating'] = '';
		 if ($html->find('.pdp-e-i-ratereviewQA', 0)) {
			 if ($html->find('.pdp-e-i-ratings div', 0)) {
				$productRatingHtml = $html->find('.pdp-e-i-ratings div', 0)->getAttribute('ratings');
				$product_detail['rating'] = trim($productRatingHtml);
			 }else{
				$scraperLog->saveScraperLog('snapdeal scraper','SnapDealProductDetailScrapper','.title-wrap class not found','Not Found',__LINE__);
			}
			
        }
		
		 // product reviews
		$product_detail['reviews'] = '';
		 if ($html->find('.productWriteReview', 0)) {
			 if ($html->find('.productWriteReview  a', 0)) {
				$productReviews = trim($html->find('.productWriteReview  a', 0)->plaintext);
				$product_detail['reviews'] = (int)$productReviews;
			 }else{
				$scraperLog->saveScraperLog('snapdeal scraper','SnapDealProductDetailScrapper','.productWriteReview  a class not found','Not Found',__LINE__);
			}
			
        }
		
		
		
		 $product_detail['discount'] = '';
		 if ($html->find('.pdp-e-i-PAY-r', 0)) {
            if ($html->find('.pdpDiscount ', 0)) {
                $productDiscount = $html->find('.pdpDiscount span', 0)->plaintext;
				$product_detail['discount'] = $productDiscount;
            }
        }
		
		
		$product_detail['product_delivery'] = '';
		if ($html->find('.check-avail-pin-inner', 0)) {
            if ($html->find('.check-avail-pin-info', 0)) {
                $product_detail['product_delivery']  = trim($html->find('.check-avail-pin-info p', 0)->plaintext);
               
            }
        }
		
		$product_detail['emi'] = '';
		if ($html->find('.elecPriceTile', 0)) {
            if ($html->find('.pdp-emi', 0)) {
                $product_detail['emi']  = trim($html->find('.pdp-emi', 0)->plaintext);
				
            }
        }
		
		// product description
		/**$description = '';
		if ($html->find('.pdp-e-i-keyfeatures ul', 0)) {
			
			$ulData = $html->find('.pdp-e-i-keyfeatures ul', 0);
			$li = 1;
            foreach ($ulData->find("li") as $skey => $sval) {
				if($li <= 5){
					$description .= $sval->plaintext.',';
				}
          	 $li++;
		    }

        }
		$product_detail['descritpion'] = $description; **/
		
		$product_detail['descritpion'] = '';
		if ($html->find('.pdp-e-i-keyfeatures', 0)) {
             $productDetailhtml = $html->find('.pdp-e-i-keyfeatures', 0)->outertext;
			 $product_detail['descritpion'] = trim($productDetailhtml);
        }

        if ($html->find('.color-attr-value', 0)) {
            $productSpecs = $html->find('.color-attr-value', 0);
            foreach ($productSpecs->find('.pull-left') as $highlight) {
                if ($highlight->find('.clr-name', 0)) {
                    $colors[] = trim($highlight->find('.clr-name', 0)->plaintext);
                }
            }
        }

        if (empty($colors) && $html->find('#productSpecs', 0)) {
            $productSpecs = $html->find('#productSpecs', 0);
            if ($productSpecs->find('.highlightsTileContent', 0)) {
                $highlightsDiv = $productSpecs->find('.highlightsTileContent', 0);
                foreach ($highlightsDiv->find('.dtls-li') as $highlight) {
                    if ($highlight->find('.h-content', 0)) {
                        $highlightValue = explode(":",$highlight->find('.h-content', 0)->plaintext);
                        if(isset($highlightValue[0]) && $highlightValue[0]=="Color")
                        $colors[] = trim($highlightValue[1]);
                    }
                }
            }
        }

        if ($html->find('.clsSizeAttr', 0)) {
            $sizeDiv = $html->find('.clsSizeAttr', 0);
            $sizeOptions = $sizeDiv->find('.pull-left');
            foreach ($sizeOptions as $size) {
                if ($size->find('.attr-val', 0)) {
                    $sizes[] = trim($size->find('.attr-val', 0)->plaintext);
                }
            }
        }

        if ($html->find('#offersModal', 0)) {
            $offerDiv = $html->find('#offersModal', 0);
            foreach ($offerDiv->find('.offer-data') as $offer) {
                if ($offer->find('.offer-title', 0)) {
                    $offers[] = trim($offer->find('.offer-title', 0)->plaintext);
                }
            }
        }

        if (empty($offers)) {
            foreach ($html->find('.offerBlock') as $offer) {
                if ($offer->find('.genericOfferClass', 0)) {
                    $offers[] = trim(preg_replace('/\t+/', '', strip_tags($offer->find('.genericOfferClass', 0)->plaintext)));
                }
            }
        }

        if ($html->find('.sold-out-err', 0)) {
            if ($html->find('.sold-out-err', 0)->plaintext == "This product has been sold out") {
                $instock = false;
            }
        }

		
		
		// product feature details		
	$arr_product_images = array();
		$anchors = $html->find('#bx-pager-left-image-panel a');
			// loop and print nodes content
		$img_num = 0;
		foreach( $anchors as $i => $anchor ) {
				if($img_num <= 3){
					$img = explode('"',$anchor->innertext);
					
					if(isset($img[5]) && !empty($img[5])){
						$arr_product_images[] = $img[5];
					}
				}
			$img_num++;
		}
	if(empty($arr_product_images)){
			if ($html->find('.cloudzoom', 0)) {
				$arr_product_images[] = $html->find('.cloudzoom', 0)->getAttribute('src');
			}
		}	
	
	
		// product feature details		
		$arr_product_features = array();
		 if ($html->find('.detailssubbox', 0)) {
			$g = 0;
			//echo $g; 
			//if($html->find(".product-spec",$g)){
             for($g =0; $g<=20;$g++) {
				
				$table = $html->find(".product-spec",$g); // table record

				// initialize empty array to store the data array from each row
				$theData = array();
				
				// loop over rows
				$n = 0;
				if(!empty($html->find('th', $g)->plaintext)){
				
						foreach($table->find('tr') as $row) { // loop for tr for get product group head or features
						
							$arr_product_features[$g]['group'] = $html->find('th', $g)->plaintext;
							// initialize array to store the cell data from each row
							$f = 0;
							
							foreach($row->find('td') as $cell) {
								// loop for get product feature name
									$attr =  str_replace('%','',$cell->getAttribute('width'));
									
									$feature_name_and_value = trim($cell->innertext);
									if(!empty($attr)){
										
										$arr_product_features[$g]['feature'][0][$n]['feature'] = $feature_name_and_value;
									}else{
										
										$arr_product_features[$g]['feature'][0][$n]['feature_value'] = $feature_name_and_value;
									}
									
							$f++;
							}
							
						$n++;	
						} 
					}
			// $g++;	
				}
			//}
           
			}
			
		
        $returnarray['name'] = $productNamehtml;
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['merchant_arr'] = $arr_merchant;
        $returnarray['product_detail'] = $product_detail;
        $returnarray['product_img_arr'] = $arr_product_images;
        $returnarray['product_price'] = $productPrice;
        $returnarray['product_features'] = $arr_product_features;
        $returnarray['product_colors'] = implode("##", $colors);
        $returnarray['product_sizes'] = implode("##", $sizes);
        $returnarray['product_offers'] = implode("##", $offers);
        $returnarray['instock'] = $instock;
		//echo '<pre>dtaa'; print_r($returnarray); die;
        return $returnarray;
    }

    private function convet_price_string($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }

        return($result);
    }
	
	
	
	
	/**
     * get HTML Data finding with XPath.
     */
    private function getMissingImage($html, $categoryName, $url)
    {
		$scraperLog = new Logs();
		//echo $url;
        $siteName = 'http://www.snapdeal.com';
        $returnarray = array();
        $returnarray['name'] = '';
        $returnarray['imgpath'] = '';
        

       
		
		// product main image
      
		
		$main_img_anchors = $html->find('#bx-pager-left-image-panel a');
		$i_num = 0;
		foreach( $main_img_anchors as $i => $anchor ) {
			
				$img = explode('"',$anchor->innertext);
				
					if(isset($img[5]) && !empty($img[5]) && $i_num == 0){
						$productImgSrchtml = $img[5];
					}
	
			$i_num++;
		}
		
		if(empty($productImgSrchtml)){
			if ($html->find('.cloudzoom', 0)) {
				$productImgSrchtml = $html->find('.cloudzoom', 0)->getAttribute('src');
			}
		}
		
		
		
		
		// product feature details		
	$arr_product_images = array();
		$anchors = $html->find('#bx-pager-left-image-panel a');
			// loop and print nodes content
		$img_num = 0;
		foreach( $anchors as $i => $anchor ) {
				if($img_num <= 3){
					$img = explode('"',$anchor->innertext);
					
					if(isset($img[5]) && !empty($img[5])){
						$arr_product_images[] = $img[5];
					}
				}
			$img_num++;
		}
		
		if(empty($arr_product_images)){
			if ($html->find('.cloudzoom', 0)) {
				$arr_product_images[] = $html->find('.cloudzoom', 0)->getAttribute('src');
			}
		}	
	
		
        $returnarray['imgpath'] = $productImgSrchtml;
        $returnarray['product_img_arr'] = $arr_product_images;
		
		return $returnarray;
    }

	
	
}
