<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\Ads */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ads-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
	
	<?php echo  $form->field($model, 'start_date')->textInput(['id' => 'start_date']) ?>
	<?php echo  $form->field($model, 'end_date')->textInput(['id' => 'end_date']) ?>
	<?php echo $form->field($model, 'position')->dropDownList([ 'left' => 'Left', 'right' => 'Right', 'top' => 'Top', 'bottom' => 'Bottom' ],['prompt' => '--Select Position--']) ?>
	
    <?php //$form->field($model, 'description')->textarea(['rows' => 6]) ?>
        <?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

    <?php //echo $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
        <?php echo  $form->field($model, 'file')->fileInput(); ?>
    
    <?php if ($model->image): ?>
    
        <div class="form-group text-center">
		    <?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = '/';
			?>
            <?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Ads Image']); ?>
           
        </div>
        
    <?php endif; ?>

    <?php echo $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>

    <?php echo $form->field($model, 'meta_desc')->textarea(['rows' => 2]) ?>

    <?php echo $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive' ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
