<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $model backend\models\Brands */

$this->title = 'Update Brands: ' . ' ' . $model->brand_name;
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->brand_name, 'url' => ['view', 'id' => $model->brand_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
			<?php echo  $this->render('_form', [
				'model' => $model,
			]) ?>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>