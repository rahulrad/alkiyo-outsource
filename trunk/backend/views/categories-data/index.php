
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Categories;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategoriesDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories Data';
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
					<?php echo  Html::a('Download CSV Template', ['download_blank_csv_format'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
                <?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Categories Data', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
					
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$category_name = '';
			$store_id = '';
			if(isset($_GET['CategoriesDataSearch'])){
				$category_name = $_GET['CategoriesDataSearch']['category_name'];
				$store_id = $_GET['CategoriesDataSearch']['store_id'];
			}
	?>
	<div class="searchblock">
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		$model->store_id = $store_id;
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
<?php

		
		$model->category_name = $category_name;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryListsName();
		echo $form->field($model, 'category_name')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>
	

	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
                <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'category_name',
            'category_type',
            //'store_name',
			[
				'attribute'=>'store_id',
				'value'=>'storeName.name',
			   ],
            'count_products',
             'status',
             'created',
				[
			 	'label'=>'Scraper Url',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-eye" aria-hidden="true" title="View Store"></i>', $dataProvider->url, ['target'=>'_blank']);
                 },
             ],
           [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}',
			],
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>

