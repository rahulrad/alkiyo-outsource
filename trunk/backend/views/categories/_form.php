<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Categories;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo  $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>
	
	<?php echo  $form->field($model, 'slug')->textInput(); ?>
	<?php echo  $form->field($model, 'display_name')->textInput(); ?>
	
	<?php echo   $form->field($model, 'file')->fileInput(); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
			<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = '/';
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive   img-thumbnail small-thumbnail','alt'=>'Category Image']); ?>
           
        </div>
		
    <?php endif; ?>
	
	<?php /** ?>
	<?php echo   $form->field($model, 'api_icon_file')->fileInput(); ?>
	
	<?php if ($model->api_icon): ?>
	
        <div class="form-group">
			<?php $path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());?>
			<?php echo Html::img($path.$model->api_icon,['class'=>'img-responsive   img-thumbnail small-thumbnail','alt'=>'Api Icon']); ?>
           
        </div>
		
    <?php endif; ?>
	<?php **/ ?>
	<?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'parent_category_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Categories::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'category_id','category_name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	<?php /*echo  $form->field($model, 'parent_category_id')->dropDownList(ArrayHelper::map(Categories::find()->all(),'category_id','category_name'),
	 	['prompt' => '-Select Category-']
	 );*/ ?>
	
	<?php //echo  $form->field($model, 'category_description')->textarea(['rows' => 2]) ?>
	<?php echo $form->field($model, 'category_description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
	 <?php //echo  $form->field($model, 'meta_title')->textarea(['rows' => 1]) ?>
	 
	 <?php //echo  $form->field($model, 'meta_keyword')->textarea(['rows' => 2]) ?>
	 
	 <?php //echo  $form->field($model, 'meta_desc')->textarea(['rows' => 2]) ?>
	 
	 
   <?php //echo  $form->field($model, 'folder_slug')->textInput(['maxlength' => true]) ?>
    <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
    
	<?php echo  $form->field($model, 'sort_order')->textInput(); ?>
	
	<?php echo $form->field($model, 'show_home_page')->checkBox(['label' => 'show home page', 'uncheck' => null, 'checked' => 'checked']);  ?>
	
	<?php echo $form->field($model, 'compare')->checkBox(['label' => 'compare', 'uncheck' => null, 'checked' => 'checked']);  ?>


	<?php echo $form->field($model, 'is_index')->checkBox(['label' => 'Is Index', 'uncheck' => null, 'checked' => 'checked']);  ?>

    <?php echo $form->field($model, 'image_scrap_preset')->radioList([\common\models\Products::IMAGE_PRESET_HORIZONTAL => 'Horizontal', \common\models\Products::IMAGE_PRESET_VERTICAL => 'Vertical']); ?>

	<div class="form-group">
        <?php echo  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
