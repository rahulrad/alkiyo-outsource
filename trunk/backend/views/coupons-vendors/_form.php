<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use jlorente\remainingcharacters\RemainingCharacters;
/* @var $this yii\web\View */
/* @var $model backend\models\CouponsVendors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupons-vendors-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

 <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => 70]) ?>

 <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>


    <?php echo  $form->field($model, 'file')->fileInput(); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
			
                $path = '';
				 $module = Yii::$app->getModule('articles');
				 $path= $module->frontPageUrl;
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Coupons vendor Image']); ?>
           
        </div>
		
    <?php endif; ?>

 
<?php echo $form->field($model, 'description')->widget(CKEditor::className(), [


 'clientOptions' => [
'filebrowserUploadUrl' => \yii\helpers\Url::to(['/coupons/upload-file']),
'extraPlugins' => 'wordcount,notification',
                                                'toolbarGroups' => [
                                                    ['name' => 'undo'],
                                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                                                    ['name' => 'colors'],
                                                    ['name' => 'links', 'groups' => ['links', 'insert']],
                                                    ['name' => 'others', 'groups' => ['others', 'about']],
                                                    ['name' => 'styles'],
                                                    ['name' => 'document', 'groups' => ['mode', 'document']],
                                                    ['name' => 'videodetector'], // <--- OUR NEW PLUGIN YAY!
						['name' => 'wordcount'],
						['name' => 'notification']
                                                ]],
                                            'options' => ['rows' => 6],
                                            'preset' => 'custom'

        

 




   ]) ?>


    <?php echo $form->field($model, 'short_description')->widget(CKEditor::className(), [
    


 'clientOptions' => [
'filebrowserUploadUrl' => \yii\helpers\Url::to(['/coupons/upload-file']),
                                                'toolbarGroups' => [
                                                    ['name' => 'undo'],
                                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                                                    ['name' => 'colors'],
                                                    ['name' => 'links', 'groups' => ['links', 'insert']],
                                                    ['name' => 'others', 'groups' => ['others', 'about']],
                                                    ['name' => 'styles'],
                                                    ['name' => 'document', 'groups' => ['mode', 'document']],
                                                    ['name' => 'videodetector'] // <--- OUR NEW PLUGIN YAY!
                                                ]],
                                            'options' => ['rows' => 6],
                                            'preset' => 'custom'



]) ?>



    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'meta_keyword')->textInput(['maxlength' => true]) ?>

    <?= 

$form->field($model, 'meta_descrption')->widget(RemainingCharacters::classname(), [
    'type' => RemainingCharacters::INPUT_TEXTAREA,
    'text' => Yii::t('app', '{n} characters remaining'),
    'label' => [
        'tag' => 'p',
        'id' => 'my-counter',
        'class' => 'counter',
        'invalidClass' => 'error'
    ],
    'options' => [
        'rows' => '3',
        'class' => 'col-md-12',
        'maxlength' => 320,
        'placeholder' => Yii::t('app', 'Write something')
    ]
]);



?>

    <?php echo $form->field($model, 'google_index')->checkBox(['label' => 'Index at Google', 'uncheck' => 0, 'checked' => 'checked']);  ?>

 <?php echo $form->field($model, 'is_popular')->checkBox(['label' =>'Is Popular', 'uncheck' => 0, 'checked' => 'checked']);  ?>

  <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
    
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
