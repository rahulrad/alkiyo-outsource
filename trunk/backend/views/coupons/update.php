<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use backend\models\MenuTypes;
use common\models\CouponCategories;
use backend\models\CouponsVendors;
use backend\models\Brands;
use backend\models\Menus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model backend\models\Coupons */

$this->title = 'Update '.Yii::$app->controller->id.': ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->id];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>


<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
			

<div class="coupons-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?php //echo  $form->field($model, 'merchant')->textInput() ?>


<?php
		// Normal select with ActiveForm & model
		$couponsVendors = new CouponsVendors();
		$vendors=  $couponsVendors->getLists();
		echo $form->field($model, 'merchant')->widget(Select2::classname(), [
			'data' => $vendors,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Merchant'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>

	


<?php
		// Normal select with ActiveForm & model
		$categoriesModel = new CouponCategories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>

	 <?php echo  $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?php //echo  $form->field($model, 'offer')->textInput(['maxlength' => true]) ?>
	<?php echo  $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
	 
   <?php echo  $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
    <?php echo  $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

   	<?php echo  $form->field($model, 'link')->textInput() ?>
    <?php echo  $form->field($model, 'track_link')->textInput() ?>
<?php echo  $form->field($model, 'start_date')->textInput(['id' => 'start_date']) ?>
	<?php echo  $form->field($model, 'expiry_date')->textInput(['id' => 'end_date']) ?>
	
<?php echo  $form->field($model, 'discount')->textInput() ?>

 <?php echo $form->field($model, 'featured')->checkBox(['label' => 'Featured', 'uncheck' => 0, 'checked' => 'checked']);  ?>
 <?php echo $form->field($model, 'exclusive')->checkBox(['label' => 'Exclusive', 'uncheck' => 0, 'checked' => 'checked']);  ?>


<?php echo $form->field($model, 'description')->widget(CKEditor::className(), [


 'clientOptions' => [
 'filebrowserUploadUrl' => \yii\helpers\Url::to(['/coupons/upload-file']),

                                                'toolbarGroups' => [
                                                    ['name' => 'undo'],
                                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                                                    ['name' => 'colors'],
                                                    ['name' => 'links', 'groups' => ['links', 'insert']],
                                                    ['name' => 'others', 'groups' => ['others', 'about']],
                                                    ['name' => 'styles'],
                                                    ['name' => 'document', 'groups' => ['mode', 'document']],
                                                    ['name' => 'videodetector'] // <--- OUR NEW PLUGIN YAY!
                                                ]],
                                            'options' => ['rows' => 6],
                                            'preset' => 'custom'

 


   ]) ?>

   <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>
<?php  echo  $form->field($model, 'code')->textInput();  ?>

 <?php echo  $form->field($model, 'type')->dropDownList([ '0' => 'Coupon', '1' => 'CashBack','2' => 'Deals' ]) ?>

<?php echo  $form->field($model, 'verified_on')->textInput(['id' => 'verified_on']) ?>
	 	<?php echo  $form->field($model, 'call_to_action_button')->textInput() ?>
	<?php // echo  $form->field($model, 'offer_id')->textInput() ?>
	<?php //echo  $form->field($model, 'promo_id')->textInput() ?>
	<?php // echo  $form->field($model, 'offer_type')->textInput() ?>
	
	<?php // echo  $form->field($model, 'merchant')->textInput() ?>
	
		<?php // echo  $form->field($model, 'merchant_logo_url')->textInput() ?>
	




	<?php //echo  $form->field($model, 'offer_url')->textInput() ?>
	
	

	<?php //echo  $form->field($model, 'ref_id')->textInput() ?>

	<?php //echo  $form->field($model, 'store_link')->textInput() ?>
	
   

   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>

