<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $model backend\models\Deals */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deals-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

    <?= $form->field($model, 'brand')->textInput() ?>
	
	<?= $form->field($model, 'link')->textInput() ?>
	
	<?php echo  $form->field($model, 'main_image')->fileInput(['accept' => 'image/*']); ?>
	
	<?php if ($model->image): ?>
	
        <div class="form-group">
		<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = '/';
			?>
			<?php echo Html::img($path.'uploads/deals/'.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Deal Image']); ?>
           
        </div>
		
    <?php endif; ?>
	
	<?= $form->field($model, 'price')->textInput() ?>
	
	<?= $form->field($model, 'offer_price')->textInput() ?>
	
	<?= $form->field($model, 'discount')->textInput() ?>
	
	<?php echo  $form->field($model, 'start_date')->textInput(['id' => 'start_date']) ?>
	
	<?php echo  $form->field($model, 'end_date')->textInput(['id' => 'end_date']) ?>
	
	<?= $form->field($model, 'availability')->textInput() ?>
	
	<?= $form->field($model, 'sizes')->textInput() ?>
	
	<?php echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>


   <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
