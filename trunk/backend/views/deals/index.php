<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Suppliers;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\DealsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deals-index">

    <section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
	
	<section class="content">
	<div class="row">
	<div class="col-xs-12">
	<div class="box">
<div class="box-header">
    
					<!--<?php echo  Html::a('Download Csv Format', ['download_deals_csv_format'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
					
					<?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
					<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?> --->
				 <?php echo  Html::a('Run Scrapper', ['scraper'], ['class' => 'btn btn-success','style' => 'float:right','target'=>'_blank']) ?>
                <p> <?php echo  Html::a('Create Deals', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
    </p>
	
	</div>
	
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			
			$store_id = '';
			if(isset($_GET['DealsSearch'])){
				$store_id = $_GET['DealsSearch']['store_id'];
			}
	?>
	<div class="searchblock">
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		$model->store_id = $store_id;
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
<div class="box-body">
<?=Html::beginForm(['deals/move'],'post');?>
<?=Html::submitButton('Move Deals', ['class' => 'btn btn-primary pull-right',]);?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
           
			[
				'attribute'=>'title',
			    'content'=> function($data){
				 return substr($data->title, 0, 50);
				}
		     ],
            'price',
            'offer_price',
            'store_id',
            // 'end_date',
             'status',
             'created',
			[
			 	//'label'=>'sort down',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', $dataProvider->link, ['target'=>'_blank']);
                 },
             ],
            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
			],
			[
			'class' => 'yii\grid\CheckboxColumn',
				'checkboxOptions' => function ($dataProvider) {
					
						return ['value'=>$dataProvider->id];
					
					
				} 
			]
        ],
    ]); ?>
	<?= Html::endForm();?> 
</div>
</div>
</div>
</div>
</div>
</section>