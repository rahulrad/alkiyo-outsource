<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\FeatureGroups;
use common\models\Features;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeaturesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Features';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Features', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$category_id = '';
			$feature_group_id = '';
			$feature_id = '';
			$auto_feature_name = '';
			if(isset($_GET['FeaturesSearch'])){
				$category_id = $_GET['FeaturesSearch']['category_id'];
				$feature_group_id = $_GET['FeaturesSearch']['feature_group_id'];
				
				$feature_id = !empty($_GET['auto_feature_name']) ? $_GET['FeaturesSearch']['id'] : '';
				$auto_feature_name = $_GET['auto_feature_name'];
			}
	?>
	<div class="searchblock">
	
	
	 <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
<?php

		
		$model->category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>

	
	
	<div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
<?php
		// Normal select with ActiveForm & model
		
		
		$featureGroupsData  = FeatureGroups::find()->all();
		$featureGroups = array();
			if(!empty($featureGroupsData)){
				foreach($featureGroupsData as $feature_group){
					//echo '<pre>'; print_r($category); die;
					$categoryName = Categories::find()->select(['category_name'])->where(['category_id'=>$feature_group->category_id])->one();
					if(!empty($categoryName)){
						$featureGroups[$feature_group->id] = $feature_group->name.' ('.$categoryName->category_name.')';
					}else{
						$featureGroups[$feature_group->id] = $feature_group->name;
					}
					
				}
			}
			
		$model->feature_group_id = $feature_group_id;
		echo $form->field($model, 'feature_group_id')->widget(Select2::classname(), [
			'data' => $featureGroups,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Product'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	<div class="col-sm-8 col-lg-8 col-md-8 col-xs-12">
	<label> Feature </label>
	<input type="text" name="auto_feature_name" value="<?php echo $auto_feature_name; ?>" class="form-control searchAutoComplete" model_name="Features" field_name="name" id_value="featuressearch-id">
	<?php echo $form->field($model, 'id')->hiddenInput(['value'=>$feature_id])->label(false) ?>
	
<?php
		// Normal select with ActiveForm & model
		/** if(isset($_GET['FeaturesSearch']['category_id']) && !empty($_GET['FeaturesSearch']['category_id'])){
			$FeaturesData = Features::find()->select(['id','name','feature_group_id','category_id'])->where(['category_id'=>$_GET['FeaturesSearch']['category_id']])->all();
		}else{
			$FeaturesData = Features::find()->select(['id','name','feature_group_id','category_id'])->all();
		}
		
		$Features = array();
			if(!empty($FeaturesData)){
				foreach($FeaturesData as $feature){
					
					$featureGroup = FeatureGroups::find()->select(['name'])->where(['id' => $feature->feature_group_id])->one();
					
					$category = Categories::find()->select(['category_name'])->where(['category_id' => $feature->category_id])->one();
					
					if(!empty($category)){
						$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' ~~ '. $category->category_name .' )';
					}else{
						$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' )';
					}
					
				}
			}
			
			
		$model->id = $feature_id;
		echo $form->field($model, 'id')->widget(Select2::classname(), [
			'data' => $Features,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Feature'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); **/
	?>
	</div>
	
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
	
                <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
           // 'name',
			[
			 	'attribute'=>'name',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					$name = substr($dataProvider->name, 0, 35);
                      return Html::tag('div', $name, ['title'=>$dataProvider->name,'style'=>'cursor:pointer;']);
                 },
             ],
			 [
			 	'attribute'=>'display_name',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					$display_name = substr($dataProvider->display_name, 0, 35);
                      return Html::tag('div', $display_name, ['title'=>$dataProvider->display_name,'style'=>'cursor:pointer;']);
                 },
             ],
			//'display_name',
             [
				'attribute'=>'category_id',
				'value'=>'categoriesCategory.category_name',
			   ],
            //'feature_group_id',
			 [
				'attribute'=>'feature_group_id',
				'value'=>'featureGroup.name',
			   ],
			
            

//ctionChange_keyfeature_statusi


[
                                'label'=>'Show Feature',
                                'format' => 'raw',
                                'attribute'=>'is_filter',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->show_feature == 1){
                                                return Html::a('<i class="fa fa-check" aria-hidden="true" title="Is Filter"></i>', Yii::$app->homeUrl.'features/change_keyfeature_status?type=0&feature_id=' . $dataProvider->id);
                                         }else{
                                                 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Is Filter"></i>', Yii::$app->homeUrl.'features/change_keyfeature_status?type=1&feature_id=' . $dataProvider->id);
                                         }
                 },
             ],

			[
			 	'label'=>'Is Filter',
			 	'format' => 'raw',
				'attribute'=>'is_filter',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->is_filter == 1){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Is Filter"></i>', Yii::$app->homeUrl.'features/change_is_filter_status?type=0&feature_id=' . $dataProvider->id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Is Filter"></i>', Yii::$app->homeUrl.'features/change_is_filter_status?type=1&feature_id=' . $dataProvider->id); 
					 }
                 },
             ],
            // 'is_required',
            // 'slug',
            // 'type',
            // 'unit',
            // 'display_text:ntext',
            // 'sort_order',
            // 'status',
            // 'created',
            // 'modified',

           [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}',
			],
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>


