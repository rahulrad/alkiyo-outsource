<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\FeatureGroups;
use common\models\Features;
use common\models\FeatureValues;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeatureValuesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feature Values';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				   <?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
					<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Feature Values', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
					
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$feature_id = '';
			$feature_value = '';
			$auto_feature_value = '';
			$auto_feature_name = '';
			if(isset($_GET['FeatureValuesSearch'])){
				$feature_id = !empty($_GET['auto_feature_name']) ? $_GET['FeatureValuesSearch']['feature_id'] : '';
				$auto_feature_name = $_GET['auto_feature_name'];
				
				$feature_value = !empty($_GET['auto_feature_value']) ? $_GET['FeatureValuesSearch']['id'] : '';
				$auto_feature_value = $_GET['auto_feature_value'];
				
			}
			
			
	?>
	<div class="searchblock">
	
	

	
	
	<div class="col-sm-5 col-lg-5 col-md-5 col-xs-12">
	<label> Feature </label>
	<input type="text" name="auto_feature_name" value="<?php echo $auto_feature_name; ?>" class="form-control searchAutoComplete" model_name="Features" field_name="name" id_value="featurevaluessearch-feature_id">
	<?php echo $form->field($model, 'feature_id')->hiddenInput(['value'=>$feature_id])->label(false) ?>
	
	<?php //echo  $form->field($model, 'feature_id')->textInput(['class' =>'form-control searchAutoComplete','model_name'=>'Features','field_name' =>'name']); ?>
<?php
		// Normal select with ActiveForm & model
		/**$FeaturesData = Features::find()->select(['id','name','feature_group_id','category_id'])->all();
		
		$Features = array();
			if(!empty($FeaturesData)){
				foreach($FeaturesData as $feature){
					
					$featureGroup = FeatureGroups::find()->select(['name'])->where(['id' => $feature->feature_group_id])->one();
					
					$category = Categories::find()->select(['category_name'])->where(['category_id' => $feature->category_id])->one();
					
					if(!empty($category->category_name)){
						$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' ~~ '. $category->category_name .' )';
					}else{
						$Features[$feature->id] = $feature->name.' ('.$featureGroup->name.' )';
					}
					
				}
			}
			
			
		$model->feature_id = $feature_id;
		echo $form->field($model, 'feature_id')->widget(Select2::classname(), [
			'data' => $Features,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Feature'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);  **/
	?>
	</div>
	
	
	<div class="col-sm-5 col-lg-5 col-md-5 col-xs-12">
	<label> Value </label>
	<input type="text" name="auto_feature_value" value="<?php echo $auto_feature_value; ?>"  class="form-control searchAutoComplete" model_name="FeatureValues" field_name="value" id_value="featurevaluessearch-id">
	<?php echo $form->field($model, 'id')->hiddenInput(['value'=>$feature_value])->label(false) ?>
<?php
		// Normal select with ActiveForm & model
		
		/**if(isset($_GET['FeatureValuesSearch']['feature_id']) && !empty($_GET['FeatureValuesSearch']['feature_id'])){
			$FeaturesValueData = FeatureValues::find()->select(['id','value','feature_id'])->where(['feature_id' => $_GET['FeatureValuesSearch']['feature_id']])->all();
		}else{
			$FeaturesValueData = FeatureValues::find()->select(['id','value','feature_id'])->all();
		}
		
		$Feature_values = array();
			if(!empty($FeaturesValueData)){
				foreach($FeaturesValueData as $feature_val){
					
					$featureDetail = Features::find()->select(['name'])->where(['id' => $feature_val->feature_id])->one();
					
					if(!empty($featureDetail->name)){
						$Feature_values[$feature_val->id] = $feature_val->value.' ('.$featureDetail->name .' )';
					}else{
						$Feature_values[$feature_val->id] = $feature_val->value;
					}
					
				}
			}
			
			
		$model->id = $feature_value;
		echo $form->field($model, 'id')->widget(Select2::classname(), [
			'data' => $Feature_values,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Feature Value'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);  **/
	?>
	</div>
	
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?php  echo Html::submitButton('Search', ['class' => 'btn btn-primary btn-block saveSearchButton']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
	
                <div class="box-body">
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            //'feature_id',
			
			
			 [
				'attribute'=>'feature_id',
				'value'=>'feature.name',
			   ],
			  [
			 	'attribute'=>'value',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					$value = substr($dataProvider->value, 0, 35);
                      return Html::tag('div', $value, ['title'=>$dataProvider->value,'style'=>'cursor:pointer;']);
                 },
             ],
			 [
			 	'attribute'=>'display_name',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					$display_name = substr($dataProvider->display_name, 0, 35);
                      return Html::tag('div', $display_name, ['title'=>$dataProvider->display_name,'style'=>'cursor:pointer;']);
                 },
             ],
           // 'value',
            'created',
           // 'modified',

            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}',
			],
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>
