<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\FilterGroups;
use backend\models\FilterGroupItemsSearch;
use common\models\Features;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FilterGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Filter Group Items';
$this->params['breadcrumbs'][] = $this->title;
?>



<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">
    <?php 
		$group_id = '';
		if(isset($_GET['FilterGroupItemsSearch']['group_id']) && !empty($_GET['FilterGroupItemsSearch']['group_id'])){
			$group_id = $_GET['FilterGroupItemsSearch']['group_id'];
		}
	?>
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				  <p><?php echo  Html::a('Create Filter Group Items', ['create?group_id='.$group_id], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$group_id = '';
			if(isset($_GET['FilterGroupItemsSearch'])){
				$group_id = $_GET['FilterGroupItemsSearch']['group_id'];
			}
	?>
	<div class="searchblock">
	
	
	 <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
<?php
		// Normal select with ActiveForm & model
		$model->group_id = $group_id;
		echo $form->field($model, 'group_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(FilterGroups::find()->where(['status'=>1])->andWhere(['!=', 'is_delete' ,1])->all(),'id','title'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Brand'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>

	
	
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
	
                <div class="box-body">
	<?=Html::beginForm(['filter-group-items/save_sort_order'],'post');?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			'title',
             [
				'attribute'=>'group_id',
				'value'=>'filterGroupName.title',
			   ],
            
            [
			 	'label'=>'Status',
			 	'format' => 'raw',
				'attribute'=>'status',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->status == 1){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Filture Group Item Status"></i>', Yii::$app->homeUrl.'filter-group-items/change_status?type=0&filter_group_item_id=' . $dataProvider->id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Filture Group Item Status"></i>', Yii::$app->homeUrl.'filter-group-items/change_status?type=1&filter_group_item_id=' . $dataProvider->id); 
					 }
                 },
             ],
			 
			 [
				'attribute' => 'sort_order',
				'value' => function($model){
					return Html::textInput('sort_order['.$model->id.']', $model->sort_order, ['class' => 'form-control sortOrderTextBox']);
				},
				'format' => 'raw'
			],
            //'is_delete',
             'created',
			 
			
            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}  {delete}',
			],
        ],
    ]); ?>
	
	<?=Html::submitButton('Save Sort Order', ['class' => 'btn btn-primary pull-right',]);?>
	</div>
	</div>
	</div>

</div>
</section>
