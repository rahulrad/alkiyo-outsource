<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\FilterGroupValues */

$this->title = 'Create Filter Group Values';
$this->params['breadcrumbs'][] = ['label' => 'Filter Group Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-group-values-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
