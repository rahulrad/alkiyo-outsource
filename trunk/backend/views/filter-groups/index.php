<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\Features;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FilterGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Filter Groups';
$this->params['breadcrumbs'][] = $this->title;
?>



<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				  <p><?php echo  Html::a('Create Filter Group', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$category_id = '';
			if(isset($_GET['FilterGroupsSearch'])){
				$category_id = $_GET['FilterGroupsSearch']['category_id'];
			}
	?>
	<div class="searchblock">
	
	
	 <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
<?php

		
		$model->category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			
		
	?>
	</div>

	
	
	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	<?=Html::beginForm(['filter-groups/save_sort_order'],'post');?>

	
	
                <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			'title',
			[
				'attribute' => 'sort_order',
				'value' => function($model){
					return Html::textInput('sort_order['.$model->id.']', $model->sort_order, ['class' => 'form-control sortOrderTextBox']);
				},
				'format' => 'raw'
			],
             [
				'attribute'=>'category_id',
				'value'=>'categoriesName.category_name',
			   ],
			 
            
            [
			 	'label'=>'Status',
			 	'format' => 'raw',
				'attribute'=>'status',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     if($dataProvider->status == 1){
						return Html::a('<i class="fa fa-check" aria-hidden="true" title="Filter Group Status"></i>', Yii::$app->homeUrl.'filter-groups/change_status?type=0&filter_group_id=' . $dataProvider->id);
					 }else{
						 return Html::a('<i class="fa fa-close" aria-hidden="true" title="Filter Group Status"></i>', Yii::$app->homeUrl.'filter-groups/change_status?type=1&filter_group_id=' . $dataProvider->id); 
					 }
                 },
             ],
            //'is_delete',
             'created',
			 
			 [
			 	'label'=>'View Group Items',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					   return Html::a('<i class="fa fa-eye" aria-hidden="true" title="View Group Items"></i>', Yii::$app->homeUrl.'filter-group-items/index?FilterGroupItemsSearch[group_id]=' .$dataProvider->id, ['target'=>'_blank']);
                 },
             ],
			
            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}  {delete}',
			],
			
			
			
        ],
    ]); ?>
	
	<?=Html::submitButton('Save Sort Order', ['class' => 'btn btn-primary pull-right',]);?>
	</div>
	</div>
	</div>

</div>
</section>
