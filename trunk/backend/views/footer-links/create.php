<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FooterLinks */

$this->title = 'Create Footer Links';
$this->params['breadcrumbs'][] = ['label' => 'Footer Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
<div class="footer-links-create ">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

</section>
