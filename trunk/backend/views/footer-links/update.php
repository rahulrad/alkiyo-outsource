<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FooterLinks */

$this->title = 'Update Footer Links: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Footer Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content">
<div class="footer-links-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</section>
