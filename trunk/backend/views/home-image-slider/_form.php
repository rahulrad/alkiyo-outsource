<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomeImageSlider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="home-image-slider-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'url')->textInput() ?>

    <div class="form-group">
        <img alt="Slider Image" width="100%" src="<?= $model->imageUrl; ?>"
             class="img-responsive  img-thumbnail small-thumbnail">
    </div>

    <?= $form->field($model, 'uploadFile')->widget(\kartik\file\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'gif', 'png']
        ]]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
