<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Image Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <p><?= Html::a('Create Home Image Slider', ['create'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'url:ntext',
                            [
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return Html::img($data->imageUrl, [
                                        'height' => '50px',
                                        'width' => '100px'
                                    ]);
                                }
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>
</section>