<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoProducts */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="mongo-products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'store_name') ?>

    <?php echo $form->field($model, 'brand_name') ?>

    <?php echo $form->field($model, 'product_name') ?>

    <?php echo $form->field($model, 'product_description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

    <?php echo $form->field($model, 'model_number') ?>

    <?php echo $form->field($model, 'cod') ?>

    <?php echo $form->field($model, 'colors') ?>

    <?php echo $form->field($model, 'sizes') ?>

    <?php echo $form->field($model, 'offers') ?>

    <?php echo $form->field($model, 'return_policy') ?>

    <?php echo $form->field($model, 'delivery') ?>

    <?php echo $form->field($model, 'price') ?>

    <?php echo $form->field($model, 'url') ?>

    <?php echo $form->field($model, 'image') ?>
	<div class="form-group text-center">
		<?php if(!empty($model->image)){ 
				 echo Html::img($model->image,['class'=>'img-responsive img-thumbnail small-thumbnail','alt'=>'User Image']); 
			 }
		?>
	</div>

    <?php echo $form->field($model, 'other_images') ?>
	<div class="form-group text-center">
		<?php 
			$otherImages = unserialize($model->other_images);
			
			if(!empty($otherImages)){
				
				foreach($otherImages as $other_img){
					
					if(!empty($other_img)){
						
						echo Html::img($other_img,['class'=>'img-responsive img-thumbnail small-thumbnail','alt'=>'User Image']); 
					}
				}	
			}
		?>
	</div>

    <?php echo $form->field($model, 'emi') ?>

    <?php echo $form->field($model, 'unique_id') ?>

	
   <?php //echo $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive', ], ['prompt' => '-Select Status-']) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
