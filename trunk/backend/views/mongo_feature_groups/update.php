<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MongoFeatureGroups */

$this->title = 'Update Mongo Feature Groups: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mongo Feature Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mongo-feature-groups-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
