<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MongoProductFeatures */

$this->title = 'Create Mongo Product Features';
$this->params['breadcrumbs'][] = ['label' => 'Mongo Product Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-product-features-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
