<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MongoProductFeaturesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mongo Product Features';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongo-product-features-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mongo Product Features', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'id_product_feature',
            'id_product',
            'id_feature',
            'id_feature_value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
