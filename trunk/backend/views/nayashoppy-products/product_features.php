<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use backend\models\MenuTypes;
use backend\models\Categories;
use backend\models\Brands;
use backend\models\Menus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model backend\models\Menus */

$this->title = 'Product Features';
$this->params['breadcrumbs'][] = ['label' => 'Product Features', 'url' => ['custom_menu']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?> Update: <?php echo $productDetail->product_name; ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
				<?php $form = ActiveForm::begin(); ?>
   
		
		<input type="hidden" value="<?php echo $productDetail->product_id; ?>" name="product_id">
		<?php if(!empty($productFeatures)){  ?>
			<?php 
				$i = 0;
				foreach($productFeatures as $productFeature){ 
			?>
			  
			  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
				<label for="menus-status" class="control-label"><?php echo $productFeature['features']['name']; ?></label>
				
				<input type="hidden" name="ProductFeature[<?php echo $i; ?>][feature_id]" value="<?php echo $productFeature['features']['id']; ?>" />
				<select name="ProductFeature[<?php echo $i; ?>][feature_value_id][]" class="form-control" id="" multiple="multiple">
				  <option value="">-Select Feature Value-</option>
				  <?php foreach($productFeature['features']['feature_value'] as $feature_value){  ?>
				  <option value="<?php echo $feature_value['id']; ?>" <?php if(in_array($feature_value['id'],$selectedFeatureValues)){ echo 'selected=selected';} ?>><?php echo $feature_value['value']; ?></option>
				  <?php } ?>
				</select>
				<div class="help-block"></div>
			  </div>
			<?php $i++; } ?>
		<?php }else{  echo '<div class="col-lg-12"><h6>No Any Features for this category</h6></div>'; } ?>
<div class="clearfix"></div>
	

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
	<!--<div class="add_more_filter_list row"></div>-->
	
	<input type="hidden" id="menu_id" value="<?php echo $model->id ?>" />
	<input type="hidden" id="model_name" value="menus" />

    <?php ActiveForm::end(); ?>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>