<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductFeatureMappings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-feature-mappings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_category')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Categories::find()->all(), 'category_id', 'category_name'),
        'language' => 'eg',
        'options' => ['placeholder' => 'Select Category'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'id_feature')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Features::find()->all(), 'id', 'name'),
        'language' => 'eg',
        'options' => ['placeholder' => 'Select Feature'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'store_feature_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
