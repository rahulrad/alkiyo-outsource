<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductFeatureMappings */

$this->title = 'Create Product Feature Mappings';
$this->params['breadcrumbs'][] = ['label' => 'Product Feature Mappings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-feature-mappings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
