<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Categories;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductUrlsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Feature Mappings';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    &nbsp;&nbsp;
                    <?php echo Html::a('Sample CSV', ['download-sample-csv'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                    &nbsp;&nbsp;
                    <?php echo Html::a('Import', ['import-data'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                    &nbsp;&nbsp;
                    <?php echo Html::a('Export All', ['export'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                    &nbsp;&nbsp;
                </div><!-- /.box-header -->

                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                ]); ?>

                <div class="searchblock">
                    <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
                        <?php
                        $categoriesModel = new Categories();
                        echo $form->field($model, 'id_category')->widget(Select2::classname(), [
                            'data' => $categoriesModel->getCategoryLists(),
                            'language' => 'eg',
                            'options' => ['placeholder' => 'Select Category'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-8 col-lg-8 col-md-8 col-xs-12">
                        <?= $form->field($model, 'featureName') ?>
                    </div>

                    <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
                        <div class="form-group" style="margin-top:24px;">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'attribute' => 'id_category',
                                'value' => 'category.category_name',
                            ],
                            [
                                'attribute' => 'Group',
                                'value' => 'feature.featureGroup.name',
                            ],
                            [
                                'attribute' => 'id_feature',
                                'value' => 'feature.display_name',
                            ],
                            'store_feature_name',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}'
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>
</section>



