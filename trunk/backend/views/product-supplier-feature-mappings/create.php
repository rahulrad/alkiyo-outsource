<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductSupplierFeatureMappings */

$this->title = 'Create Product Supplier Feature Mappings';
$this->params['breadcrumbs'][] = ['label' => 'Product Supplier Feature Mappings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-supplier-feature-mappings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
