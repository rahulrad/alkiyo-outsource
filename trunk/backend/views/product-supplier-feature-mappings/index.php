<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Categories;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductUrlsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Supplier Feature Mappings';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <?php echo Html::a('Download CSV Template', ['download-sample-csv'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                    <?php echo Html::a('Import', ['import-data'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                    <?php echo Html::a('Export All', ['export'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                </div><!-- /.box-header -->


                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'category_id',
                                'value' => 'category.category_name',
                            ],
                            'store_name',
                            'emi:ntext',
                            'cod:ntext',
                            'delivery:ntext',
                            'return_policy:ntext',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>
</section>


