<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Suppliers;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Categories;
/* @var $this yii\web\View */
/* @var $model backend\models\ProductUrls */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-urls-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
		// Normal select with ActiveForm & model
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->where(['status'=>'active'])->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>

    <?php //echo  $form->field($model, 'cateogry_url_id')->textInput() ?>
	<?php


		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);


	?>
	</div>

    <?php echo  $form->field($model, 'url')->textInput() ?>

    <?php if ($model->isNewRecord) {
        echo $form->field($model, 'status')->dropDownList(['0' => 'No', '1' => 'Yes'])->label('Auto Scrap');
    } else {
        echo $form->field($model, 'status')->dropDownList(['0' => 'Inactive', '1' => 'Active',]);
    } ?>

    <div class="form-group">
       <?php echo   Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
