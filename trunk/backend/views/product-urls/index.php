<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Categories;
use common\models\Suppliers;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductUrlsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Urls';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				<?php echo  Html::a('Download CSV Template', ['download_blank_csv_format'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
                <?php echo  Html::a('Export', ['export'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				<?php echo  Html::a('Import', ['importdata'], ['class' => 'btn btn-success','style' => 'float:right']) ?>
				  <p><?php echo  Html::a('Create Product Url', ['create'], ['class' => 'btn btn-success','style' => 'float:right']) ?></p>
                </div><!-- /.box-header -->
				
				
				
				<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	
	<?php 
			$store_id = '';
			$category_id = '';
			if(isset($_GET['ProductUrlsSearch'])){
				$category_id = $_GET['ProductUrlsSearch']['category_id'];
				$store_id = $_GET['ProductUrlsSearch']['store_id'];
			}
	?>
	<div class="searchblock">
	<div class="col-sm-2 col-lg-2 col-md-2  col-xs-12">
	<?php
		// Normal select with ActiveForm & model
		$model->store_id = $store_id;
		echo $form->field($model, 'store_id')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete' ,1])->all(),'id','name'),
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Store','id' => 'store_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
	?>
	</div>
	
	 <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
<?php

		
		$model->category_id = $category_id;
		// Normal select with ActiveForm & model
		$categoriesModel = new Categories();
		$categories =  $categoriesModel->getCategoryLists();
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => $categories,
			'language' => 'eg',
			'options' => ['placeholder' => 'Select Category','id' => 'category_id'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); 
			
		
	?>
	</div>
	

	 <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12">
<div class="form-group" style="margin-top:24px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
    </div></div>
	</div>
	<?php ActiveForm::end() ?>
    <div class="clearfix"></div>
	
	
	
                <div class="box-body">
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            [
				'attribute'=>'store_id',
				'value'=>'storeName.name',
			   ],
			   
			 [
				'attribute'=>'category_id',
				'value'=>'categoriesName.category_name',
			   ],
//            'cateogry_url_id:url',
            [
                'attribute' => 'scrap_attempt',
                'value'=>function($data){return $data->scrap_attempt;}

            ],
            [
                'attribute' => 'scrap_error',
                'format' => 'raw',
//                'contentOptions' => ['style' => 'max-width:w00px;'],
                'value' => function ($data) {
                    if(!empty($data->status)){
                        return Html::tag('div', "Product Data Saved", ['class'=>'text-green']);
                    }else{
                        $url = substr($data->scrap_error, 0, 20) . '....';
                        return Html::tag('div', $url, ['title' => $data->scrap_error, 'style' => 'cursor:pointer;','class'=>'text-red']);
                    }
                }

            ],
            [
			 	'attribute'=>'url',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($data) {
					$url = substr($data->url, 0,40).'....';
                      return Html::tag('div', $url, ['title'=>$data->url,'style'=>'cursor:pointer;']);
                 },
             ],
			
            'status',
			 [
			 	'label'=>'View',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
                     return Html::a('<i class="fa fa-eye" aria-hidden="true" title="View Store"></i>', $dataProvider->url, ['target'=>'_blank']);
                 },
             ],
			 [
			 	'label'=>'Scrape',
			 	'format' => 'raw',
                // here comes the problem - instead of parent_region I need to have parent
                'value' => function ($dataProvider) {
					if($dataProvider->status == 0){
						  return Html::a('<i class="fa fa-unlink" aria-hidden="true" title="Product Scrape Button"></i>', Yii::$app->homeUrl.'product-urls/product-scraper?id='.$dataProvider->id, ['target'=>'_blank']);
					}
                },
             ],
             [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}',
			],
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>

