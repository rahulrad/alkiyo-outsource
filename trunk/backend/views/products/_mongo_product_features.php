<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Breadcrumbs;

?>

<div class="" role="document">
	<?php if(!empty($postData)){ ?>
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Mongo Product: <?php echo $mongoProductDetail->product_name; ?></h3>
      </div>
	  <?php //echo Html::beginForm(['products/save_ajax_mongo_product'],'post');?>
      <div class="modal-body">
	  
        <!--
          <div class="form-group">
            <label for="recipient-name" class="control-label">Product Name:</label>
            <input type="text" class="form-control" name="MongoProducts[product_name]" id="recipient-name" value="<?php echo htmlentities($mongoProductDetail->product_name, ENT_QUOTES, 'UTF-8'); ?>">
			<input type="hidden" class="form-control" name="MongoProducts[_id]" value="<?php echo $postData['mongo_product_id']; ?>">
          </div>  -->
		  
		  <?php if(!empty($productFeatures)){  ?>
			<?php 
				$i = 0;
				foreach($productFeatures as $productFeature){ 
			?>
			  
			  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
				<!--<input type="text" class="form-control" name="ProductFeature[<?php echo $i; ?>][feature_name]" value="<?php echo $productFeature['features']['name']; ?>" /> -->
				<?php echo $productFeature['features']['name']; ?>
				
				<input type="hidden" name="ProductFeature[<?php echo $i; ?>][feature_id]" value="<?php echo $productFeature['features']['id']; ?>" />
				<select name="ProductFeature[<?php echo $i; ?>][feature_value_id][]" class="form-control" id="" multiple="multiple">
				  <option value="">-Select Feature Value-</option>
				  <?php foreach($productFeature['features']['feature_value'] as $feature_value){  ?>
				  <option value="<?php echo $feature_value['id']; ?>" <?php if(in_array($feature_value['id'],$selectedFeatureValues)){ echo 'selected=selected';} ?>><?php echo $feature_value['value']; ?></option>
				  <?php } ?>
				</select>
				<div class="help-block"></div>
			  </div>
			<?php $i++; } ?>
		<?php }else{  echo '<div class="col-lg-12"><h6>No Any Features for this category</h6></div>'; } ?>
      </div>
      <div class="modal-footer">
         
      </div>
	  <?php //echo Html::endForm();?> 
    </div>
	<?php } ?>
  </div>