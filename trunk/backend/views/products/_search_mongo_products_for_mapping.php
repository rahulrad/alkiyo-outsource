<?php
use yii\helpers\Html;


$output = "";
if (!empty($similer_products)) {
    $noOfProducts = count($similer_products);
    $s_no = 1;

    foreach ($similer_products as $product) {

        $product_data = array(
            'product_id' => $product->_id,
            'category_id' => $getFormData['Products']['categories_category_id'],
            'store_name' => $product->store_name,
            'product_name' => $product->product_name,
            'brand_name' => $product->brand_name,
            'price' => $product->price
        );

        $productMappingLinks = array();
        if (!empty($product->product_mapping)) {
            foreach (explode(",", $product->product_mapping) as $productId) {
                array_push($productMappingLinks, Html::a("Supplier", ["/products/view_suppilers", 'product_id' => $productId], ['target' => '_blank']));
            }
        }

        if ($getFormData['Products']['showUnMapped']) {
            if (empty($productMappingLinks)) {
                $product_json_data = json_encode($product_data);
                $output .= '<p class="MongoProductLists mongo_products list_product_for_mongo_mapping" id="' . $product->_id . '" data-product="' . htmlentities($product_json_data, ENT_QUOTES, 'UTF-8') . '">' . $s_no . '. ';
                $output .= '<input type="radio" class="radio_' . $product->_id . '" name=Products[map_product_id] value="' . $product->_id . '">';
                $output .= '<span class="mongoProductName">' . $product->product_name . "(<code>" . $product->store_name . " $product->url".")</code>" . '</span>';
                $output .= ' <b>Brand:-</b> ' . $product->brand_name . ' <b>Price:-</b> ' . $product->price . ' <a href="' . $product->url . '" target="_blank">Read More</a><br>';
                if (!empty($productMappingLinks))
                    $output .= '<b>Current Product Mapping:-</b> ' . implode("&nbsp;,&nbsp;", $productMappingLinks);
                $output .= '</p>';
                $s_no++;
            } else {
                $noOfProducts--;
            }

        } else {

            $product_json_data = json_encode($product_data);
            $output .= '<p class="MongoProductLists mongo_products list_product_for_mongo_mapping" id="' . $product->_id . '" data-product="' . htmlentities($product_json_data, ENT_QUOTES, 'UTF-8') . '">' . $s_no . '. ';
            $output .= '<input type="radio" class="radio_' . $product->_id . '" name=Products[map_product_id] value="' . $product->_id . '">';
            $output .= '<span class="mongoProductName">' . $product->product_name . "(<code>" . $product->store_name . " $product->url".")</code>" . '</span>';
            $output .= ' <b>Brand:-</b> ' . $product->brand_name . ' <b>Price:-</b> ' . $product->price . ' <a href="' . $product->url . '" target="_blank">Read More</a><br>';
            if (!empty($productMappingLinks))
                $output .= '<b>Current Product Mapping:-</b> ' . implode("&nbsp;,&nbsp;", $productMappingLinks);
            $output .= '</p>';
            $s_no++;
        }


    }
    $output = '<span>' . $noOfProducts . ' products.</span>' . $output;
} else {
    $output .= '<h4>No Any Similer Product</h4>';
}

echo $output;

?>

<?php /**if(!empty($similer_products)){ ?>
 * <div class="form-group">
 * <?php echo Html::submitButton($model->isNewRecord ? 'Mapp' : 'Mapp', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
 * </div>
 * <?php } **/ ?>
