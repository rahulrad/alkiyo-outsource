<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Breadcrumbs;
use common\models\Brands;
use common\models\Products;
use common\models\Categories;
use common\models\Suppliers;

$this->title = 'Mapping Products';
$this->params['breadcrumbs'][] = ['label' => 'Map Products', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->product_name, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Update';

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>
<?php //echo Yii::$app->request->baseUrl. '/supermarkets/sample' ?>
<?php //$this->registerJsFile(Yii::$app->request->baseUrl.'/js/admin_development.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<section class="content-header">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</section>

<?php
$category_id = '';
$brand_id = '';
$store_id = '';
$auto_brand_name = '';
if (isset($_GET['Products'])) {
    $category_id = $_GET['Products']['categories_category_id'];
    $brand_id = $_GET['Products']['brands_brand_id'];
    $auto_brand_name = isset($_GET['auto_brand_name']) ? $_GET['auto_brand_name'] : '';
    $store_id = $_GET['Products']['store_id'];
}
?>
<section class="content">
    <div class="panel panel-primary">
        <div class="panel-heading">NayaShopi Products<span
                    class="pull-right">Mongo Products</span></div>


        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                    <div class="searchblock">
                        <!----------- Code for Mongo Product Searching -------------->

                        <?php $form = ActiveForm::begin(['method' => 'post',
                            'id' => 'nayashoppyProductForm'
                        ]); ?>


                        <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                            <?php
                            $productModel->ns_category_id = $category_id;
                            $categoriesModel = new Categories();
                            $categories_data = $categoriesModel->getCategoryLists();
                            echo $form->field($productModel, 'ns_category_id')->widget(Select2::classname(), [
                                'data' => $categories_data,
                                'language' => 'eg',
                                'options' => ['placeholder' => 'Select Category'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);


                            ?>
                        </div>
                        <div class="col-sm-6 col-lg-5 col-md-6 col-xs-12">
                            <label> NS Brand </label>
                            <input type="text" class="form-control searchAutoComplete"
                                   value="<?php echo $auto_brand_name; ?>" model_name="Brands" field_name="brand_name"
                                   id_value="products-ns_brand_id">
                            <?php echo $form->field($model, 'ns_brand_id')->hiddenInput(['value' => $brand_id])->label(false) ?>
                        </div>

                        <div class="col-sm-6 col-lg-4 col-md-6 col-xs-12">
                            <?php
                            echo $form->field($productModel, 'ns_created')->textInput(['id' => 'start_date']);
                            ?>
                        </div>

                        <div class="col-sm-6 col-lg-4 col-md-6 col-xs-12">
                            <div class="form-group" style="margin-top:24px;">
                                <?php
                                echo $form->field($productModel, 'showUnMapped')->checkbox(['id' => 'showUnMapped']);
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-3 col-md-6 col-xs-12">
                            <div class="form-group" style="margin-top:24px;">
                                <input type="button" value="Search Ns Products"
                                       class="btn btn-primary searchButtonForNayashoppyProduct">
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                        <!--------- End code for mongoDb Product Searching ------------>
                    </div>

                </div>
                <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                    <div class="searchblock">
                        <!----------- Code for Mongo Product Searching -------------->

                        <?php $form = ActiveForm::begin([
//                            'action' => ['products-mapping'],
                            'id' => "mongoProductsSearchForm",
                            'method' => 'get',
                        ]); ?>

                        <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                            <?php
                            // Normal select with ActiveForm & model
                            $model->store_id = $store_id;
                            echo $form->field($model, 'store_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Suppliers::find()->andWhere(['!=', 'is_delete', 1])->all(), 'id', 'name'),
                                'language' => 'eg',
                                'options' => ['placeholder' => 'Select Store', 'id' => 'store_id'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                            <?php
                            $model->categories_category_id = $category_id;
                            // Normal select with ActiveForm & model
                            $categoriesModel = new Categories();
                            $categories = $categoriesModel->getCategoryLists();
                            echo $form->field($model, 'categories_category_id')->widget(Select2::classname(), [
                                'data' => $categories,
                                'language' => 'eg',
                                'options' => ['placeholder' => 'Select Category', 'id' => 'category_id'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-6 col-lg-5 col-md-6 col-xs-12">
                            <label> Brand </label>
                            <input type="text" name="auto_brand_name" value="<?php echo $auto_brand_name; ?>"
                                   class="form-control searchAutoComplete" model_name="Brands" field_name="brand_name"
                                   id_value="products-brands_brand_id">
                            <?php echo $form->field($model, 'brands_brand_id')->hiddenInput(['value' => $brand_id])->label(false) ?>
                        </div>

                        <div class="col-sm-6 col-lg-4 col-md-6 col-xs-12">
                            <?php
                            echo $form->field($model, 'created')->textInput(['id' => 'start_date_pick']);
                            ?>
                        </div>

                        <div class="col-sm-6 col-lg-4 col-md-6 col-xs-12">
                            <div class="form-group" style="margin-top:24px;">
                                <?php
                                echo $form->field($model, 'showUnMapped')->checkbox(['id' => 'showUnMapped']);
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-3 col-md-6 col-xs-12">
                            <div class="form-group" style="margin-top:24px;">
                                <?= Html::button('Search Mongo Products', ['class' => 'btn btn-primary searchButtonForMongoProduct', "onclick" => "searchMongoProducts()"]) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                        <!--------- End code for mongoDb Product Searching ------------>
                    </div>

                </div>


            </div>
            <div>
                <?php $form = ActiveForm::begin(['id' => "mongoToNsProductsMapForm", 'method' => 'post', 'action' => ['products/product_map']]); ?>
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="box box-info">
                            <div class="box-body">
                                <input type="text" value="" id="searchingOurProduct" class="form-control"
                                       placeholder="search products"> <br>

                                <div class="products-form productLists left-scrollblock">
                                    <div class="products-form productLists" id="search_nayashoppy_products_for_mapping">


                                        <h4>No Product selected</h4>


                                    </div>
                                </div>

                            </div>
                            <!--<div class="box-footer">&nbsp;</div>-->
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                        <div class="box box-info">
                            <div class="box-body ">


                                <input type="text" value="" id="searchingMongoProduct" class="form-control"
                                       placeholder="Search Products">

                                <div class="products-form productLists left-scrollblock">


                                    <div class="products-form productLists" id="search_mongo_products_for_mapping">


                                        <h4>No Product selected</h4>


                                    </div>


                                </div>
                            </div>


                            <!--<div class="box-footer">&nbsp;</div>--->
                        </div>

                    </div>


                </div>
                <div class="form-group">
                    <?php echo Html::button($model->isNewRecord ? 'Map' : 'Map', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'onclick' => "mongoToNsProductMap()"]) ?>
                </div>
                <?php ActiveForm::end(); ?>


            </div>
        </div>

        <div class="modal fade" id="AddMongoproduct" tabindex="-1" role="dialog" aria-labelledby="AddMongoproduct">
            <div id="mongo_product_popup_form"></div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                Mapping Product Supplier....
            </div>
        </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    //    $(window).load(function () {
    //        $(".searchButtonForNayashoppyProduct").trigger("click");
    //        searchMongoProducts();
    //    });
    function searchMongoProducts() {
        var formData = $('#mongoProductsSearchForm').serialize();
        $.ajax({
            url: '/products/search-mongo-products-for-mapping',
            type: 'post',
            dataType: 'html',
            data: {
                formData: formData,
            },
            success: function (data) {
                $('#search_mongo_products_for_mapping').html(data);
            }
        });
    }

    function mongoToNsProductMap() {
        $('#myModal').find('.modal-body').html("Mapping Product Supplier....");
        $('#myModal').modal('show');
        var formData = $("#mongoToNsProductsMapForm").serialize();
        $.ajax({
            url: '/products/product-ajax-map',
            type: 'post',
            dataType: 'json',
            data: formData,
            success: function (data) {
                if (data.status) {
                    $(".searchButtonForNayashoppyProduct").trigger("click");
                    $(".searchButtonForMongoProduct").trigger("click");
                    $('#myModal').find('.modal-body').html(data.data + ".Refreshing Data...");
                } else {
                    $('#myModal').find('.modal-body').html(data.data);
                }
                console.log(data);
                setTimeout(function () {
                    $('#myModal').modal('hide');
                }, 1000);
            }
        });
    }
</script>

<script>

    $('#searchingOurProduct').on('paste', function (e) {
        setTimeout(function () {
            var query = $('#searchingOurProduct').val();
            $('.OurProductLists .ourProductName').each(function (index) {

                var $this = $(this);
                if ($this.text().toLowerCase().indexOf(query.toLowerCase()) === -1) {
                    $this.closest('p.our_products').fadeOut();
                } else {
                    $this.closest('p.our_products').fadeIn();
                }
            });
        }, 100);

    });

    // this function for search similler our products in listing
    $('#searchingOurProduct').keyup(function(){
        var query = $(this).val();

        $('.OurProductLists .ourProductName').each(function(){

            var $this = $(this);
            if($this.text().toLowerCase().indexOf(query.toLowerCase()) === -1)
                $this.closest('p.our_products').fadeOut();
            else $this.closest('p.our_products').fadeIn();
        });
    });

    $('#searchingMongoProduct').on('paste', function (e) {
        setTimeout(function () {
            var query = $('#searchingMongoProduct').val();

            $('.MongoProductLists .mongoProductName').each(function () {

                var $this = $(this);
                if ($this.text().toLowerCase().indexOf(query.toLowerCase()) === -1)
                    $this.closest('p.mongo_products').fadeOut();
                else $this.closest('p.mongo_products').fadeIn();
            });
        }, 100);
    });

    $(function () {
        $("#start_date_pick").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
