<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Redirect */

$this->title = 'Create Redirect';
$this->params['breadcrumbs'][] = ['label' => 'Redirects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>




<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
			<?php echo  $this->render('_form', [
				'model' => $model,
			]) ?>
			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>