<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redirects';
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	
</section>
<section class="content">
<div class="redirect-index">

  

    <p>
        <?= Html::a('Create Redirect', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'old_url:url',
            'new_url:url',
            'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</section>