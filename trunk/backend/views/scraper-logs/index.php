<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ScraperLogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Scraper Logs';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>
<section class="content">
    
	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				  
                </div><!-- /.box-header -->
                <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'=>'created',
			    'value'=> function($data){
					$date = '';
					if(!empty($data->created)){ $date = date('d-m-Y', strtotime($data->created));	}
					return $date;
				}
		     ],
            //'id',
            'file_name',
            //'created',

            [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{view} {delete}',
			],
        ],
    ]); ?>
	</div>
	</div>
	</div>

</div>
</section>

