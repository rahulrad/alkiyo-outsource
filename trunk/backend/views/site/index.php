<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

$this->title = 'Dashoboard | Nayashopi';
?>
<!-- Content Header (Page header) -->

<section class="content-header">
  <h1> Dashboard <small>Control panel</small> </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::$app->homeUrl; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- Main content -->

<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><?php if(isset($data['products'])){ echo $data['products']; } ?></h3>
          <p>Products</p>
        </div>
        <div class="icon"> <i class="ion ion-bag"></i> </div>
        <a href="<?php echo Yii::$app->homeUrl.'products'; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php if(isset($data['suppliers'])){ echo $data['suppliers']; } ?></h3>
          <p>Suppliers</p>
        </div>
        <div class="icon"> <i class="ion ion-stats-bars"></i> </div>
        <a href="<?php echo Yii::$app->homeUrl.'suppliers'; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php if(isset($data['users'])){ echo $data['users']; } ?></h3>
          <p>Admin User</p>
        </div>
        <div class="icon"> <i class="ion ion-person-add"></i> </div>
        <a href="<?php echo Yii::$app->homeUrl.'users'; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>65</h3>
          <p>Unique Visitors</p>
        </div>
        <div class="icon"> <i class="ion ion-pie-graph"></i> </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-7 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <!-- /.nav-tabs-custom -->
      <!-- Chat box -->
      <!-- /.box (chat box) -->
      <!-- TO DO List -->
      <div class="box box-primary">
        <div class="box-header"> <i class="ion ion-clipboard"></i>
          <h3 class="box-title">To Do List</h3>
          
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <ul class="todo-list">
            <li>
              <!-- drag handle -->
              <span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
              <!-- checkbox -->
              <!-- todo text -->
              <span class="text">Design a nice theme</span>
              <!-- Emphasis label -->
              <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
              <!-- General tools such as edit or delete-->
              <!--<div class="tools"> <i class="fa fa-edit"></i> <i class="fa fa-trash-o"></i> </div>-->
            </li>
            <li> <span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
              <span class="text">Make the theme responsive</span> <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
              <!--<div class="tools"> <i class="fa fa-edit"></i> <i class="fa fa-trash-o"></i> </div>-->
            </li>
            <li> <span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
              <span class="text">Let theme shine like a star</span> <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
              <!--<div class="tools"> <i class="fa fa-edit"></i> <i class="fa fa-trash-o"></i> </div>-->
            </li>
            <li> <span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
              <span class="text">Let theme shine like a star</span> <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
              <!--<div class="tools"> <i class="fa fa-edit"></i> <i class="fa fa-trash-o"></i> </div>-->
            </li>
            <li> <span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
              <span class="text">Check your messages and notifications</span> <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
              <!--<div class="tools"> <i class="fa fa-edit"></i> <i class="fa fa-trash-o"></i> </div>-->
            </li>
            <li> <span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
              <span class="text">Let theme shine like a star</span> <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
              <!--<div class="tools"> <i class="fa fa-edit"></i> <i class="fa fa-trash-o"></i> </div>-->
            </li>
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!-- quick email widget -->
    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">
      <div class="box box-solid bg-light-blue-gradient">
        <div class="box-header">
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
            <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
          </div>
          <!-- /. tools -->
          <i class="fa fa-map-marker"></i>
          <h3 class="box-title"> Visitors </h3>
        </div>
        <div class="box-body">
          <div id="world-map" style="height: 250px; width: 100%;"></div>
        </div>
        <!-- /.box-body-->
        <div class="box-footer no-border">
          <div class="row">
            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
              <div id="sparkline-1"></div>
              <div class="knob-label">Visitors</div>
            </div>
            <!-- ./col -->
            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
              <div id="sparkline-2"></div>
              <div class="knob-label">Online</div>
            </div>
            <!-- ./col -->
            <div class="col-xs-4 text-center">
              <div id="sparkline-3"></div>
              <div class="knob-label">Exists</div>
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- Map box -->
      <!-- /.box -->
      <!-- solid sales graph -->
      <!-- /.box -->
      <!-- Calendar -->
      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
<!-- /.content -->
