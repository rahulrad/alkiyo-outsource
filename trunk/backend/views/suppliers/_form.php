<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Suppliers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="suppliers-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	
	 <?php echo  $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

   <?php echo $form->field($model, 'site_url')->textInput(['maxlength' => true]) ?>

   <?php echo $form->field($model, 'data_url')->textInput(['maxlength' => true]) ?>

	 <?php echo  $form->field($model, 'file')->fileInput(); ?>
   <?php if ($model->image): ?>
	
        <div class="form-group">
		<?php 
				//$path = str_replace('admin','frontend/web/',Yii::$app->getUrlManager()->getBaseUrl());
				$path = 'https://s3.ap-south-1.amazonaws.com/nayashoppy/';
			?>
			<?php echo Html::img($path.$model->image,['class'=>'img-responsive  img-thumbnail small-thumbnail','alt'=>'Suppiler Image']); ?>
           
        </div>
		
    <?php endif; ?>

     <?php echo  $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ]) ?>

    <div class="form-group">
       <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
