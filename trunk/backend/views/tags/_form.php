<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Tags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tags-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'tag')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'type')->dropDownList([ 'products' => 'Products', 'blogs' => 'Blogs' ]) ?>

    <?php echo $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive' ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
