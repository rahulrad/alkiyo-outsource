<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Adminusers */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = ['label' => 'Adminusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
	   <h1><?php echo  Html::encode($this->title) ?></h1>
	  <?php echo Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
</section>

<section class="content">	
	
	  <div class="box box-info">
			 <div class="box-body">	
			<div class="adminusers-form">
	<?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'password_hash')->textInput(['maxlength' => true,'required' => true]) ?>

    <?php echo $form->field($model, 'confirm_password')->textInput(['maxlength' => true,'required' => true]) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Change Password' : 'Change Password', ['class' => $model->isNewRecord ? 'btn btn-success changePasswordButton' : 'btn btn-primary changePasswordButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

			</div>
			 <div class="box-footer">&nbsp;</div>
	</div>

</section>
