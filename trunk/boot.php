<?php
/**
 * Use this file to detect environment and set specific settings
 */
$scriptName = __FILE__;
if (strpos($scriptName, '/var/www/html/nayashoppy/') !== false) {
    define('YII_ENV', 'test');
    define('YII_DEBUG', true);
} elseif (strpos($scriptName, '/var/www/nayashoppy.com/') !== false) {
    define('YII_ENV', 'prod');
    define('YII_DEBUG', true);
}else{
    define('YII_ENV', 'dev');
    define('YII_DEBUG', true);
}
