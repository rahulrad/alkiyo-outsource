<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Image
 *
 * @author root
 */
namespace common\components;

use Yii;
use yii\helpers\Console;
use common\components\AmazonECS;
class Aws
{
    
	public function sendMessage($data,$type=null){
		
		$sendUrlData = array();
		$sendUrlData['id'] = $data['id'];
		$sendUrlData['url'] = $data['url'];
		$sendUrlData['store_id'] = $data['store_id'];
		$sendUrlData['type'] = $type;
		
		$aws = \Yii::$app->awssdk->getAwsSdk();
		$sqs = $aws->createSqs();
		$queueUrl = \Yii::$app->awssdk->extra['queueUrl'];

		 try {
				$response = $sqs->sendMessage([
							'QueueUrl' => $queueUrl, 
							'MessageBody' => json_encode($sendUrlData)
						]);
		//echo '<pre>response=>'; print_r($response); die;
		} catch (\Exception $e) {
			echo "Sqs error on product scraper sendMessage: ".$e->getMessage();
		}
		
	}
	
	
	
	public function priceScraperSendMessage($data,$type=null){
		
		$sendUrlData = array();
		$sendUrlData['id'] = $data['id'];
		$sendUrlData['type'] = $type;
		
		$aws = \Yii::$app->awssdk->getAwsSdk();
		$sqs = $aws->createSqs();
		$queueUrl = \Yii::$app->awssdk->extra['queuePriceUrl'];

		 try {
				$response = $sqs->sendMessage([
							'QueueUrl' => $queueUrl, 
							'MessageBody' => json_encode($sendUrlData)
						]);
		//echo '<pre>$queueUrl'; print_r($queueUrl); die;	
		} catch (\Exception $e) {
			echo "Sqs error on price scrper sendMessage: ".$e->getMessage();
		}
		
	}
	
	
	
public function deleteQueueMessage($messageData, $type=null){
		
		$aws = \Yii::$app->awssdk->getAwsSdk();
        $sqs = $aws->createSqs();
		
		if($type == 'price_scraper'){
			$queueUrl = \Yii::$app->awssdk->extra['queuePriceUrl'];
		}elseif($type == 'product_scraper'){
			$queueUrl = \Yii::$app->awssdk->extra['queueUrl'];
		}
		
		//echo '<pre>'; print_r($messageData); echo $queueUrl;// die;
		
		try{
			$deleteResult = $sqs->deleteMessageBatch(array(
					// QueueUrl is required
					'QueueUrl' => $queueUrl,
					// Entries is required
					'Entries' => array(
						array(
							// Id is required
							'Id' => $messageData['id'],
							// ReceiptHandle is required
							'ReceiptHandle' => $messageData['ReceiptHandle'],
						),
						// ... repeated
					),
				)); 
			
			//echo '<pre>'; print_r($deleteResult); die;
		} catch (\Exception $e) {
            echo "SQS Error for delete message: ".$e->getMessage();
        }
		
	}
	

}
