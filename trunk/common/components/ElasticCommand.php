<?php

namespace common\components;

use yii\helpers\Json;

class ElasticCommand extends \yii\elasticsearch\Command {

    function bulk($index, $type, $data, $options = []) {
        $body = '';
        foreach ($data as $row):
            $id = isset($row['id']) ? "_id:" . $row['id'] : "";
            if(isset($row['is_delete']) && $row['is_delete'] ){
                $body.='{"delete":{' . $id . '}}' . PHP_EOL;
            }else{
            $body.='{"index":{' . $id . '}}' . PHP_EOL;
            $body.= Json::encode($row) . PHP_EOL;
            }
        endforeach;
        unset($data);
        $response = $this->db->post([$index, $type, '_bulk'], $options, $body);
        return $response;
    }
    /**
     * Expects _index and _type in data
     * @param type $data
     * @param type $options
     * @return type
     */
    function bulkAll($data, $options = []) {
        $body = '';
        foreach ($data as $row):
            $id = isset($row['id']) ? "_id:" . $row['id'] : "";
            $indexMeta = ', _index:"' . $row['_index'].'"';
            $typeMeta = ', _type:"' . $row['_type'].'"';
            unset($row['_index']);
            unset($row['_type']);
            $body.='{"index":{' . $id .$indexMeta.$typeMeta.'}}' . PHP_EOL;
            $body.= Json::encode($row) . PHP_EOL;
        endforeach;
        unset($data);
//        echo $body;die; 
        return $this->db->post(['_bulk'], $options, $body);
    }

}
