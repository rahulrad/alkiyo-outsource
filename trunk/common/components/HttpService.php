<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HttpService
 *
 * @author root
 */

namespace common\components;

use Yii;

class HttpService {

    private $_use_proxy = false;
    private $_curl = null;
    private $_http_status = null;
    private $proxy_index = 0;
    private static $_user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13';
    private static $_proxies = array(
        '79.140.105.122:80',
        '217.23.140.24:80',
        '72.240.34.19:80',
        '221.179.35.71:80',
        '216.164.122.171:8080',
        '209.62.91.216:3128',
        '216.157.222.83:80',
        '209.62.91.210:3128',
        '66.135.33.17:3128',
        '97.81.243.127:8080',
        '216.118.70.30:80',
        '108.52.61.19:80',
        '122.194.11.208:808',
        '118.174.131.94:3128',
        '182.71.214.229:3128',
        '182.72.139.50:3128',
        '182.71.75.30:3128',
        '221.179.35.71:80',
        '220.225.219.165:3128',
        '210.212.20.173:3128',
        '124.124.93.154:3128',
        '59.93.201.10:1080'
    );

    public function __construct($use_proxy=false, $username="", $password="") {
        $this->_use_proxy = $use_proxy;

        //init curl
        $this->_curl = curl_init();
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->_curl, CURLOPT_ENCODING, "gzip");
        curl_setopt($this->_curl, CURLOPT_MAXREDIRS, 4);
        curl_setopt($this->_curl, CURLOPT_USERAGENT, HttpService::$_user_agent);
        curl_setopt($this->_curl, CURLOPT_COOKIEJAR, "/tmp/cookie.txt");
        curl_setopt($this->_curl, CURLOPT_COOKIEFILE, "/tmp/cookie.txt");
        curl_setopt($this->_curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($this->_curl, CURLOPT_TIMEOUT, 10);

        if ($username)
            curl_setopt($this->_curl, CURLOPT_USERPWD, $username . ':' . $password);
    }

    public function getLastHttpStatus() {
        return $this->_http_status;
    }

    private function getProxyUrl() {
        return isset(HttpService::$_proxies[$this->proxy_index]) ? HttpService::$_proxies[$this->proxy_index++] : '';
    }

    private function getContent() {
        $content = curl_exec($this->_curl);
		//echo '<pre>content'; print_r($content);
        $this->_http_status = curl_getinfo($this->_curl, CURLINFO_HTTP_CODE);
        if ($this->_http_status < 200 || $this->_http_status >= 300)
            $content = false;
        return $content;
    }

  /**  public function getResponse($url) {
		
        curl_setopt($this->_curl, CURLOPT_URL, $url);
        if ($this->_use_proxy) {
            do {
                $proxy_url = $this->getProxyUrl();
				//echo 'proxy_url'; print_r($proxy_url); 
                curl_setopt($this->_curl, CURLOPT_PROXY, $proxy_url);
                $content = $this->getContent();
				//echo 'content_1'; print_r($content); 
            } while (!$content AND !empty($proxy_url));
        } else
            $content = $this->getContent();
		
		//echo 'content_2'; print_r($content); die;
		
        return $content;
    } **/
	
	

    /**
     * Scrap content using phantomJS
     * if content is rendering dynamic
     */
    public static function getResponseViaPhatomJs($webUrl) {
        $scraper_js_path = Yii::getPathOfAlias('application.vendors.phantomjs');        
        $execute_command = $scraper_js_path.'/scraper.js';
        $phantomjsCommand = "phantomjs ".$execute_command.' '.$webUrl;
        echo $phantomjsCommand;
        $result = shell_exec($phantomjsCommand);
        return $result;
        /* $file = fopen("/tmp/new.txt", "a+");
          fwrite($file, $result);
          fclose($file); */
    }
    
    public static function getHttpResponse($url, $use_proxy = false, $user_name = "", $password = ""){
        $http_service_object = new HttpService($use_proxy, $user_name, $password);
        return $http_service_object->getResponse($url);        
    }
    
    public static function getLastEffectiveUrl($url) {
        // initialize cURL
        $curl = curl_init($url);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
        ));
        // execute the request
        $result = curl_exec($curl);
        // fail if the request was not successful
        if ($result === false) {
            curl_close($curl);
            return null;
        }
        // extract the target url
        $redirect_url = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        curl_close($curl);
        return $redirect_url;
    }
    
    /**
     * Post data using curl
     * @return $http_status
     */
    public static function postData($url, array $data) {        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $http_status;
    }
	
	
	
	
	public function getProductUrl($url){
		
			
			//$url = 'http://staging.nayashoppy.com/test.php';
			//$url = 'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy,4io&filterNone=true&start=141&ajax=true&_=1460746573007';
			//$url = 'http://www.flipkart.com/lenovo-vibe-k5-note-grey-32-gb/p/itmejj6kmhh2khk9?pid=MOBEJJ6KYARZGWJC&al=4ao0%2BU1o2oN1Yme6AJrPUsldugMWZuE7Qdj0IGOOVqtvDk3MWSGsxgsnp%2FRLvRX0f0th3CfnuI0%3D&ref=L%3A8726666236569012690&srno=b_1';

			//$proxy = '81.21.77.204:8083';
			$proxyauth = '';
			//$proxy = '202.43.147.226:1080';
			$proxy = '127.0.0.1:9050';
			//$proxyauth = 'xteamweb.com:xteam';

			$headers = array();

			$headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=utf-8';

			$headers[] = 'Referer: http://www.flipkart.com/'; //Your referrer address

			$headers[] = 'Connection: keep-alive';
			$headers[] = 'Cache-Control: max-age=0';
			$headers[] = 'Upgrade-Insecure-Requests: 1';
			$headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36';
			$headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
			$headers[] = 'Accept-Encoding: gzip, deflate, sdch';
			$headers[] = 'Accept-Language: en-GB,en-US;q=0.8,en;q=0.6';

			//$headers[] = 'Cookie: AMCV_17EB401053DAF4840A490D4C%40AdobeOrg=-227196251%7CMCIDTS%7C17030%7CMCMID%7C90817622659473430161421530176947204375%7CMCAAMLH-1471935620%7C3%7CMCAAMB-1471935620%7CNRX38WO0n5BH8Th-nqAG_A%7CMCOPTOUT-1471338021s%7CNONE%7CMCAID%7CNONE; T=TI145474214400000035148356600610872171368178180620557441363223266063; __utmt=1; s_ch_list=%5B%5B%27Direct%2528No%2520referrer%2529%27%2C%271471331067438%27%5D%2C%5B%27Direct%2528No%2520referrer%2529%27%2C%271471445124894%27%5D%5D; ft-ls=1; s_cc=true; __utma=19769839.302976562.1454742114.1471331065.1471445122.3; __utmb=19769839.3.9.1471445135595; __utmc=19769839; __utmz=19769839.1471331065.2.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); FK-CMP-DATA=; s_ppv=6; Tkt=83d58d93; SN=2.VI5ED906F058274205B58DC8FE5234A280.SI6D3E3CDD6CA4478CA7B530C36087350E.VS147144512717490018772.1471445138; VID=2.VI5ED906F058274205B58DC8FE5234A280.1471445138.VS147144512717490018772; NSID=2.SI6D3E3CDD6CA4478CA7B530C36087350E.1471445138.VI5ED906F058274205B58DC8FE5234A280; atl=atlo_v1; gpv_pn=Mobile%3AMoto%20X%20Style%20%28Black%2C%2016%20GB%29; gpv_pn_t=Product%20Page; s_visit=1; Direct=1; s_sq=%5B%5BB%5D%5D; _vz=viz_56b59a47bc8e8';




			$ch = curl_init();
			curl_setopt($ch,CURLOPT_ENCODING , "gzip");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_URL, $url);         // URL for CURL call
			curl_setopt($ch, CURLOPT_PROXY, $proxy);     // PROXY details with port
			if($proxyauth != ''){
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);   // Use if proxy have username and password
			}
			curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5); // If expected to call with specific PROXY type
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  // If url has redirects then go to the final redirected URL.
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // Do not outputting it out directly on screen.
			curl_setopt($ch, CURLOPT_HEADER, 1);   // If you want Header information of response else make 0
			curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			$curl_scraped_page = curl_exec($ch);

			print_r(curl_getinfo($ch));
			print_r(curl_error($ch));

			curl_close($ch);
			
			echo '<pre>content=>'; print_r($curl_scraped_page); die;
			return  $curl_scraped_page;
		
	}

	

// code for get flipkart detail	
    public function getFlipkartProductDetail($url)
    {
        //echo $url;
        if (!empty($url)) {
            $product_url = parse_url($url);
            if (isset($product_url['query'])) {
                parse_str($product_url['query'], $query);
                //echo '<pre>'; print_r($query);
                if (isset($query['pid']) && !empty($query['pid'])) {
                    $product_id = trim($query['pid']);
                    echo $product_id;
                    $proxy = $this->getProxyIps();

                    //echo $product_id;
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://www.flipkart.com/api/3/page/dynamic/product",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => false,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_PROXY => $proxy['proxy_url'],
                        CURLOPT_PROXYUSERPWD => $proxy['proxy_user'] . ':' . $proxy['proxy_pwd'],
                        CURLOPT_POSTFIELDS => "{\"requestContext\":{\"productId\":\"$product_id\",\"sessionContext\":{\"pids\":[]}}}",
                        CURLOPT_HTTPHEADER => array(
                            "accept: */*",
                            "accept-language: en-GB,en-US;q=0.8,en;q=0.6",
                            "cache-control: no-cache",
                            "content-type: application/json",
                            "postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
                            "referer: https://www.flipkart.com/lenovo-vibe-k5-note-grey-32-gb/p/itmejj6kmhh2khk9?pid=$product_id&al=4ao0%2BU1o2oN1Yme6AJrPUsldugMWZuE7Qdj0IGOOVqtvDk3MWSGsxgsnp%2FRLvRX0f0th3CfnuI0%3D&ref=L%3A8726666236569012690&srno=b_1",
                            "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
                            "x-user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36 FKUA/website/41/website/Desktop"
                        ),
                    ));


                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    //echo '<pre>curl_getinfo==>';print_r(curl_getinfo($curl));
                    //echo '<pre>curl_error==>';print_r(curl_error($curl));

                    curl_close($curl);


                    //echo '<pre>proxyDetail=>'.$product_id; print_r($proxy);
                    //echo '<pre>Server=>'; print_r($_SERVER);
                    //echo '<pre>$response'; print_r($response);
                    //echo '<pre>json_decode($response);'; print_r(json_decode($response)); die;

                    if ($err) {
                        //echo '<pre>'; print_r($err); die;
                        return 'error';
                    } else {
                        //echo '<pre>json_decode($response);'; print_r(json_decode($response)); die;
                        $returnData = json_decode($response);
                        return $returnData;
                    }
                } else {
                    throw new ProductExistsInMongoException("flipkart Product Id not found in url");
                }
            } else {
                throw new ProductExistsInMongoException("flipkart Product Id not found in url");
            }

        } else {
            throw new ProductExistsInMongoException("flipkart url not found!");
        }


    }
	

	

	
// get snapdeal product detail by curl  

 public function getAmazonProductDetail($product_url){
											//https://www.amazon.in/Samsung-inches-32J4003-Ready-Television/dp/B00ZG29MPG%3Fpsc%3D1%26SubscriptionId%3DAKIAI32XY7KT5TQRGNEQ%26tag%3Dnayashoppy-21%26linkCode%3Dsp1%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00ZG29MPG
							//$product_url = 'http://www.amazon.in/Samsung-inches-32J4003-Ready-Television/dp/B00ZG29MPG?ie=UTF8&SubscriptionId=AKIAI32XY7KT5TQRGNEQ&camp=2025&creative=165953&creativeASIN=B00ZG29MPG&linkCode=sp1&psc=1&tag=nayashoppy-21';
							
							$product_url = str_replace('https','http',$product_url);
							
							$proxy = $this->getProxyIps();
							
							$curl = curl_init();
							curl_setopt_array($curl, array(
							CURLOPT_URL => $product_url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 30,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => false,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_PROXY => $proxy['proxy_url'],
							CURLOPT_PROXYUSERPWD =>  $proxy['proxy_user'].':'.$proxy['proxy_pwd'],
							CURLOPT_HTTPHEADER => array(
								"accept: */*",
								"accept-language: en-US,en;q=0.8",
								"Accept-Encoding: gzip, deflate, sdch, br",
								"cache-control: no-cache",
								"content-type: application/json",
								"postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
								"referer: $product_url",
								"user-agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
							  ),
							));

							
							
							$response = curl_exec($curl);
							$err = curl_error($curl);
							//echo '<pre>curl_getinfo==>';print_r(curl_getinfo($curl));
							//echo '<pre>curl_error==>';print_r(curl_error($curl));
							
							curl_close($curl);
							
							
							//echo '<pre>proxyDetail=>'.$product_url; print_r($proxy);
							//echo '<pre>Server=>'; print_r($_SERVER); 
							//echo '<pre>response=>'; print_r($response); die;
							
							if ($err) {
							  return 'error';
							} else {
								
								return $response;
								
							}
	 
 }
 
	
// get All other scraper product detail by curl  

 public function getResponse($product_url){
							//$product_url = 'http://www.jabong.com/Nike-As-Academy-Ss-Red-Round-Neck-T-Shirt-3549728.html?pos=1';
							$proxy = $this->getProxyIps();
							
							$curl = curl_init();
							curl_setopt_array($curl, array(
							CURLOPT_URL => $product_url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_ENCODING => "",
							//CURLOPT_SETDELAY => 1000,
							CURLOPT_MAXREDIRS => 10,
							//CURLOPT_TIMEOUT => 30,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => false,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_PROXY => $proxy['proxy_url'],
							CURLOPT_PROXYUSERPWD =>  $proxy['proxy_user'].':'.$proxy['proxy_pwd'],
							CURLOPT_HTTPHEADER => array(
								"accept: */*",
								"accept-language: en-US,en;q=0.8",
								"Accept-Encoding: gzip, deflate, sdch, br",
								"cache-control: no-cache",
								"content-type: application/json",
								"postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
								"referer: $product_url",
								"user-agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
							  ),
							));

							
							$response = curl_exec($curl);
							$err = curl_error($curl);
							//echo '<pre>defaultcurl_getinfo==>';print_r(curl_getinfo($curl));
							//echo '<pre>curl_error==>';print_r(curl_error($curl));
							
							curl_close($curl);
							
							
							//echo '<pre>proxyDetail=>'.$product_url; print_r($proxy);
							//echo '<pre>Server=>'; print_r($_SERVER); 
							//echo '<pre>response=>'; print_r($response); die;
							
							if ($err) {
							  return 'error';
							} else {
								return $response;
							}
	 
 }
 



 // code for getting proxy ips for scraper
 public function getProxyIps(){
		$proxies = array(
            '23.27.236.236:29842:skonde:KjfGYJ4N',
            '23.80.138.202:29842:skonde:KjfGYJ4N',
            '23.80.138.210:29842:skonde:KjfGYJ4N',
            '23.82.85.122:29842:skonde:KjfGYJ4N',
            '23.82.85.74:29842:skonde:KjfGYJ4N',
			);
			
			$rand_proxy = array_rand($proxies);
			
			$proxy = $proxies[$rand_proxy];

			$proxy = explode(':',$proxy);
			
			$proxyArr = array();
			$proxyArr['proxy_ip'] = isset($proxy[0]) ? $proxy[0] : '23.27.236.236';
			$proxyArr['proxy_port']  = isset($proxy[1]) ? $proxy[1] : '29842';
			$proxyArr['proxy_url'] = $proxyArr['proxy_ip'].':'.$proxyArr['proxy_port']; 
			$proxyArr['proxy_user'] = isset($proxy[2]) ? $proxy[2] : 'skonde';
			$proxyArr['proxy_pwd'] = isset($proxy[3]) ? $proxy[3] : 'KjfGYJ4N';
			
			
			return $proxyArr;
			
 }
 
 
	
}
