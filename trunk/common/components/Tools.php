<?php
/**
 * Tools.php
 *
 * PHP version 5.4 
 * 
 * @category  PHP
 * @package   PriceDekho
 * @author    Vikash Kumar <vikashkumar@girnarsoft.com>
 * @copyright 2014 GirnarSoft Pvt. Ltd.
 * @license   2014 GirnarSoft Pvt. Ltd.  
 * @link      http://www.girnarsoft.com
 */

/**
 * Description of Tools.php
 *
 * PHP version 5.4 
 * 
 * @category  PHP
 * @package   PriceDekho
 * @author    Vikash Kumar <vikashkumar@girnarsoft.com>
 * @copyright 2014 GirnarSoft Pvt. Ltd.
 * @license   2014 GirnarSoft Pvt. Ltd.
 * @link      http://www.girnarsoft.com
 * 
 * 
 */

namespace common\components;

use Yii;

class Tools
{

    /**
     * To get value of a key from $_GET or $_POST.
     * You can also set default value for that key.
     * 
     * @param mixed $key          Key.
     * @param mixed $defaultValue Default value.
     * 
     * @return mixed $value Value.
     */
    public static function getValue($key, $defaultValue = false)
    {
        if (!isset($key) || empty($key) || !is_string($key)) {
            return false;
        }
        $ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $defaultValue));

        if (is_string($ret) === true) {
            $ret = urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret)));
        }
        return !is_string($ret) ? $ret : stripslashes($ret);
    }
    
    /**
     * To check is key is set in $_POST or $_GET.
     * 
     * @param mixed $key Key.
     * 
     * @return boolean true|false boolean 
     */
    public static function getIsset($key)
    {
        if (!isset($key) || empty($key) || !is_string($key)) {
            return false;
        }
        return isset($_POST[$key]) ? true : (isset($_GET[$key]) ? true : false);
    }
    
    /**
     * To create ad script for an ad key.
     * 
     * @param string $ad_key ad_ket
     * 
     * @return string $ad_script ad script. 
     */
    public static function getAdsScript($ad_key)
    {
        $ad_script = "<script type='text/javascript'>                
            GA_googleAddSlot('ca-pub-1470596075054486','{$ad_key}');
            GA_googleFetchAds();
            GA_googleFillSlot('{$ad_key}');
        </script>";
        return $ad_script;
    }

    /**
     * Check if submit has been posted
     *
     * @param string $submit submit name
     * 
     * @return boolean $result true|false
     */
    public static function isSubmit($submit)
    {
        return (
            isset($_POST[$submit]) || isset($_POST[$submit . '_x']) || isset($_POST[$submit . '_y']) || isset($_GET[$submit]) || isset($_GET[$submit . '_x']) || isset($_GET[$submit . '_y'])
        );
    }
    
    /**
     * To clean string.
     * 
     * @param string $str str.
     * 
     * @return string $str cleaned string. 
     */
    public static function cleanString($str)
    {
        $str = strip_tags($str);
        $str = preg_replace(array('/&[^\s]*;/'), '', $str);
        $str = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str);
        return (string) trim(preg_replace("/[\t\n ]+/", " ", trim($str)));
    }

    /**
     * To clean ckeditor text.
     * 
     * @param string $text text
     * 
     * @return string $text clean text. 
     */
    public static function cleanupCKEditorText($text)
    {
        $text = preg_replace('/<p[^>]*>(&nbsp;|\s)*<\/p>/ui', '', $text);
        $text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);

        //remove all tag except div, h1 ... h5, div, span, b, strong, br, i, img, object, embed, table, tr, td, tbody, th, a, p, ul, li, ol
        return strip_tags($text, "<div><h1><h2><h3><strong><h4><h5><span><b><i><br><img><object><embed><table><tr><td><tbody><th><a><p><ul><li><ol>");
    }
    
    // @codingStandardsIgnoreStart
    
    /**
     * To get clean url string.
     * 
     * @param string  $str         string.
     * @param boolean $utf8_decode is utf8 decode
     * 
     * @return string $clean_url_string clean url string. 
     * @deprecated Using Tools::getLinkRewrite
     */
    public static function link_rewrite($str, $utf8_decode = false)
    {
        $purified = '';
        $str = trim($str);
        $length = self::strlen($str);
        if ($utf8_decode) {
            $str = utf8_decode($str);
        }
        for ($i = 0; $i < $length; $i++) {
            $char = self::substr($str, $i, 1);
            if (self::strlen(htmlentities($char)) > 1) {
                $entity = htmlentities($char, ENT_COMPAT, 'UTF-8');
                $purified .= $entity{1};
            } elseif (preg_match('|[[:alpha:]]{1}|u', $char)) {
                $purified .= $char;
            } elseif (preg_match('<[[:digit:]]|-{1}>', $char)) {
                $purified .= $char;
            } elseif ($char == ' ') {
                $purified .= '-';
            } elseif ($char == '\'') {
                $purified .= '-';
            }
        }
        $purified = preg_replace('/\-+/', '-', $purified);
        return trim(self::strtolower($purified));
    }
    
    /**
     * To get price
     * 
     * @param string $userString userString
     *  
     * @return integer $price price 
     * @deprecated
     */
    public static function numpass_filter_cronupdatenodes($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }
        return($result);
    }
    
    // @codingStandardsIgnoreEnd
    
    /**
     * To get clean url string.
     * 
     * @param string  $str         string.
     * @param boolean $utf8_decode is utf8 decode
     * 
     * @return string $clean_url_string clean url string. 
     */
    public static function getLinkRewrite($str, $utf8_decode = false)
    {
        $purified = '';
        $str = trim($str);
        $length = self::strlen($str);
        if ($utf8_decode) {
            $str = utf8_decode($str);
        }
        for ($i = 0; $i < $length; $i++) {
            $char = self::substr($str, $i, 1);
            if (self::strlen(htmlentities($char)) > 1) {
                $entity = htmlentities($char, ENT_COMPAT, 'UTF-8');
                $purified .= $entity{1};
            } elseif (preg_match('|[[:alpha:]]{1}|u', $char)) {
                $purified .= $char;
            } elseif (preg_match('<[[:digit:]]|-{1}>', $char)) {
                $purified .= $char;
            } elseif ($char == ' ') {
                $purified .= '-';
            } elseif ($char == '\'') {
                $purified .= '-';
            }
        }
        $purified = preg_replace('/\-+/', '-', $purified);
        return trim(self::strtolower($purified));
    }
    
    /**
     * To get hyperlink.
     * 
     * @param string $link       Link.
     * @param string $text       Text.
     * @param string $title      Link title.
     * @param array  $htmlOption Key value pair of html options
     * 
     * @return string $hyperlink hyperlink 
     */
    public static function getLink($link, $text, $title = null, $htmlOption = array())
    {
        if (!$title) {
            $title = $text;
        }
        $htmlOption['title'] = $title;
        return CHtml::link($text, $link, $htmlOption);
    }
    
    /**
     * To get display price.
     * 
     * @param mixed $num num
     * 
     * @return string $display_price Display price.
     */
    public static function displayPrice($num)
    {
        $num = (int) $num;  //remove any fractional part
        if ($num >= 100000) {
            if ($num <= 9999999) {
                $pricedec = $num / 100000;
                $inlakh = number_format($pricedec, 2);
                $stringtoreturn = $inlakh . ' <span class="bigprice">lakh</span>';
            } else {
                $pricedec = $num / 10000000;
                $inlakh = number_format($pricedec, 2);
                $stringtoreturn = $inlakh . ' <span class="bigprice">crore</span>';
            }
            return $stringtoreturn;
        }

        if (strlen($num) > 3 & strlen($num) <= 12) {
            $last3digits = substr($num, -3);
            $numexceptlastdigits = substr($num, 0, -3);
            $formatted = self::makeComma($numexceptlastdigits);
            $stringtoreturn = $formatted . "," . $last3digits;
        } elseif (strlen($num) <= 3) {
            $stringtoreturn = $num;
        } elseif (strlen($num) > 12) {
            $stringtoreturn = number_format($num, 2);
        }

        if (substr($stringtoreturn, 0, 2) == "-,") {
            $stringtoreturn = "-" . substr($stringtoreturn, 2);
        }

        return $stringtoreturn;
    }
    
    /**
     * To append comma after 2 character recurcively.
     * 
     * @param string $input text
     * 
     * @return string $comma_separated_string comma separated string.
     */
    private static function makeComma($input)
    {
        if (strlen($input) <= 2) {
            return $input;
        }
        $length = substr($input, 0, strlen($input) - 2);
        $formatted_input = self::makeComma($length) . "," . substr($input, -2);
        return $formatted_input;
    }
    
    /**
     * To convert string in lowercase.
     * 
     * @param string $str str.
     * 
     * @return string $str lowercase string. 
     */
    public static function strtolower($str)
    {
        if (is_array($str)) {
            return false;
        }
        if (function_exists('mb_strtolower')) {
            return mb_strtolower($str, 'utf-8');
        }
        return strtolower($str);
    }

    /**
     * To get lenght of a string.
     * 
     * @param string $str str
     * 
     * @return integer $length length of a string.
     */
    public static function strlen($str)
    {
        if (is_array($str)) {
            return false;
        }
        $str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        if (function_exists('mb_strlen')) {
            return mb_strlen($str, 'utf-8');
        }
        return strlen($str);
    }
    
    /**
     * To convert in uppercase.
     * 
     * @param string $str string
     * 
     * @return string $string Uppercase string.
     */
    public static function strtoupper($str)
    {
        if (is_array($str)) {
            return false;
        }
        if (function_exists('mb_strtoupper')) {
            return mb_strtoupper($str, 'utf-8');
        }
        return strtoupper($str);
    }
    
    /**
     * To get substring.
     * 
     * @param string  $str      string.
     * @param integer $start    start index. 
     * @param integer $length   nos of character.
     * @param string  $encoding characer encoding.
     * 
     * @return string $sub_string Sub string. 
     */
    public static function substr($str, $start, $length = false, $encoding = 'utf-8')
    {
        if (is_array($str)) {
            return false;
        }
        if (function_exists('mb_substr')) {
            return mb_substr($str, (int) ($start), ($length === false ? self::strlen($str) : (int) ($length)), $encoding);
        }
        return substr($str, $start, ($length === false ? self::strlen($str) : (int) ($length)));
    }
    
    /**
     * To convert first character of a string in uppercase.
     * 
     * @param string $str string.
     * 
     * @return string $string ucfirst string. 
     */
    public static function ucfirst($str)
    {
        return self::strtoupper(self::substr($str, 0, 1)) . self::substr($str, 1);
    }

    /**
     * To get encrypted string.
     * 
     * @param mixed $value string
     * 
     * @return string $encypted_string encypted_string. 
     */
    public static function getEncryptedData($value)
    {
        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, 'secure_key', $text, MCRYPT_MODE_ECB, $iv);
        return trim(base64_encode($crypttext)); //encode for cookie
    }

    /**
     * To get decrypted string.
     * 
     * @param mixed $value encrypted string
     * 
     * @return string $string decrypted string. 
     */
    public static function getDecrypedData($value)
    {
        if (!$value) {
            return false;
        }
        $crypttext = base64_decode($value); //decode cookie
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, 'secure_key', $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
    
    /**
     * To check for crawling bot from user agent.
     * 
     * @param string $agent user agent.
     * 
     * @return boolean $is_bot is bot. 
     */
    public static function isBot($agent = false)
    {

        $botlist = array('Teoma', 'alexa', 'froogle', 'Gigabot', 'inktomi',
            'looksmart', 'URL_Spider_SQL', 'Firefly', 'NationalDirectory',
            'Ask Jeeves', 'TECNOSEEK', 'InfoSeek', 'WebFindBot', 'girafabot',
            'crawler', 'www.galaxy.com', 'Googlebot', 'Scooter', 'Slurp',
            'msnbot', 'appie', 'FAST', 'WebBug', 'Spade', 'ZyBorg', 'rabaz',
            'Baiduspider', 'Feedfetcher-Google', 'TechnoratiSnoop', 'Rankivabot',
            'Mediapartners-Google', 'Sogou web spider', 'WebAlta Crawler',
            'bingbot', 'msn', 'abacho', 'abcdatos', 'abcsearch', 'acoon', 'adsarobot', 'aesop', 'ah-ha',
            'alkalinebot', 'almaden', 'altavista', 'antibot', 'anzwerscrawl', 'aol', 'search', 'arachnoidea',
            'araneo', 'architext', 'ariadne', 'arianna', 'ask', 'jeeves', 'aspseek', 'asterias', 'astraspider', 'atomz',
            'augurfind', 'backrub', 'baiduspider', 'bannana_bot', 'bbot', 'bdcindexer', 'blindekuh', 'boitho', 'boito',
            'borg-bot', 'bsdseek', 'christcrawler', 'computer_and_automation_research_institute_crawler', 'coolbot',
            'cosmos', 'crawler', 'crawler@fast', 'crawlerboy', 'cruiser', 'cusco', 'cyveillance', 'deepindex', 'denmex',
            'dittospyder', 'docomo', 'dogpile', 'dtsearch', 'elfinbot', 'entire', 'esismartspider', 'exalead',
            'excite', 'ezresult', 'fast', 'fast-webcrawler', 'fdse', 'felix', 'fido', 'findwhat', 'finnish', 'firefly',
            'firstgov', 'fluffy', 'freecrawl', 'frooglebot', 'galaxy', 'gaisbot', 'geckobot', 'gencrawler', 'geobot',
            'gigabot', 'girafa', 'goclick', 'goliat', 'googlebot', 'griffon', 'gromit', 'grub-client', 'gulliver',
            'gulper', 'henrythemiragorobot', 'hometown', 'hotbot', 'htdig', 'hubater', 'ia_archiver', 'ibm_planetwide',
            'iitrovatore-setaccio', 'incywincy', 'incrawler', 'indy', 'infonavirobot', 'infoseek', 'ingrid', 'inspectorwww',
            'intelliseek', 'internetseer', 'ip3000.com-crawler', 'iron33', 'jcrawler', 'jeeves', 'jubii', 'kanoodle',
            'kapito', 'kit_fireball', 'kit-fireball', 'ko_yappo_robot', 'kototoi', 'lachesis', 'larbin', 'legs',
            'linkwalker', 'lnspiderguy', 'look.com', 'lycos', 'mantraagent', 'markwatch', 'maxbot', 'mercator', 'merzscope',
            'meshexplorer', 'metacrawler', 'mirago', 'mnogosearch', 'moget', 'motor', 'muscatferret', 'nameprotect',
            'nationaldirectory', 'naverrobot', 'nazilla', 'ncsa', 'beta', 'netnose', 'netresearchserver', 'ng/1.0',
            'northerlights', 'npbot', 'nttdirectory_robot', 'nutchorg', 'nzexplorer', 'odp', 'openbot', 'openfind',
            'osis-project', 'overture', 'perlcrawler', 'phpdig', 'pjspide', 'polybot', 'pompos', 'poppi', 'portalb',
            'psbot', 'quepasacreep', 'rabot', 'raven', 'rhcs', 'robi', 'robocrawl', 'robozilla', 'roverbot', 'scooter',
            'scrubby', 'search.ch', 'search.com.ua', 'searchfeed', 'searchspider', 'searchuk', 'seventwentyfour',
            'sidewinder', 'sightquestbot', 'skymob', 'sleek', 'slider_search', 'slurp', 'solbot', 'speedfind', 'speedy',
            'spida', 'spider_monkey', 'spiderku', 'stackrambler', 'steeler', 'suchbot', 'suchknecht.at-robot', 'suntek',
            'szukacz', 'surferf3', 'surfnomore', 'surveybot', 'suzuran', 'synobot', 'tarantula', 'teomaagent', 'teradex',
            't-h-u-n-d-e-r-s-t-o-n-e', 'tigersuche', 'topiclink', 'toutatis', 'tracerlock', 'turnitinbot', 'tutorgig',
            'uaportal', 'uasearch.kiev.ua', 'uksearcher', 'ultraseek', 'unitek', 'vagabondo', 'verygoodsearch', 'vivisimo',
            'voilabot', 'voyager', 'vscooter', 'w3index', 'w3c_validator', 'wapspider', 'wdg_validator', 'webcrawler',
            'webmasterresourcesdirectory', 'webmoose', 'websearchbench', 'webspinne', 'whatuseek', 'whizbanglab', 'winona',
            'wire', 'wotbox', 'wscbot', 'www.webwombat.com.au', 'xenu', 'link', 'sleuth', 'xyro', 'yahoobot',
            'slurp', 'yandex', 'yellopet-spider', 'zao/0', 'zealbot', 'zippy', 'zyborg'
        );

        if ($agent) {
            $bottest = $agent;
        } else {
            $bottest = strtolower(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "");
        }

        $thebot = false;
        foreach ($botlist as $bot) {
            if (stripos($bottest, $bot) !== false) {
                $thebot = $bot;
                break;
            }
        }
        return ( $thebot ? $thebot : false );
    }
    
    /**
     * To validate url.
     * 
     * @param string $url Url to validate.
     * 
     * @return boolean $is_valid true|false 
     */
    public static function isValidateSupplierUrl($url)
    {
        $urlarray = parse_url($url);

        if (is_null($url)) {
            return false;
        }
        if (isset($urlarray['path']) && strcmp(trim($urlarray['path']), '/')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * To convert the string containing '_' into the IN part of where query.
     * 
     * @param string $text text.
     * 
     * @return string $text comma separated text.
     */
    public static function buildWhereIn($text)
    {
        $temp = explode("_", $text);
        $temp = implode("','", $temp);
        return $temp;
    }
    
    /**
     * To perform regular expression match.
     * 
     * @param string $s     string. 
     * @param string $delim delim
     * 
     * @return string $s string 
     */
    public static function pRegexp($s, $delim)
    {
        $s = str_replace($delim, '\\' . $delim, $s);
        foreach (array('?', '[', ']', '(', ')', '{', '}', '-', '.', '+', '*', '^', '$') as $char) {
            $s = str_replace($char, '\\' . $char, $s);
        }
        return $s;
    }
    
    /**
     * To get media sever URI.
     * 
     * @param string $filename name of file.
     * 
     * @return string $media_server_uri media server uri.
     */
    public static function getMediaServer($filename = null)
    {
        if ($filename) {
            $filename = mb_convert_encoding(md5(strtoupper($filename)), "UCS-4BE", 'UTF-8');
            $intvalue = 0;
            for ($i = 0; $i < mb_strlen($filename, "UCS-4BE"); $i++) {
                $s2 = mb_substr($filename, $i, 1, "UCS-4BE");
                $val = unpack("N", $s2);
                $intvalue += ( $val[1] + $i) * 2;
            }
            $nb_server = ($intvalue % 3) + 1;
            if (($value = constant('_MEDIA_SERVER_' . $nb_server . '_')) && $value != '') {
                return "http://" . $value;
            }
            return self::getBaseUrl();
        } else {
            return "http://" . constant('_MEDIA_SERVER_1_');
        }
    }

    /**
     * To get encrypted id.
     * 
     * @param integer $dec dec 
     * 
     * @return boolean|string $id encrypted string.
     */
    public static function getEncryptedId($dec)
    {
        if (!is_numeric($dec)) {
            return false;
        }
        $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $id = "";
        $base = 62;
        //multiply by 131 & then change base
        $dec*=131;
        while ($dec >= $base) {
            $mod = $dec % $base;
            $id = $an[$mod] . $id;
            $dec = $dec / $base;
        }
        $id = $an[(int) $dec] . $id;
        return $id;
    }

    /**
     * To get id from encrypted string.
     * 
     * @param string $alpha encrypted string.
     * 
     * @return boolean|string $id Id. 
     */
    public static function getDecryptedId($alpha)
    {
        if (!$alpha) {
            return false;
        }
        $d = array("0" => 0, "1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7, "8" => 8, "9" => 9, "a" => 10, "b" => 11, "c" => 12, "d" => 13, "e" => 14, "f" => 15, "g" => 16, "h" => 17, "i" => 18, "j" => 19, "k" => 20, "l" => 21, "m" => 22, "n" => 23, "o" => 24, "p" => 25, "q" => 26, "r" => 27, "s" => 28, "t" => 29, "u" => 30, "v" => 31, "w" => 32, "x" => 33, "y" => 34, "z" => 35, "A" => 36, "B" => 37, "C" => 38, "D" => 39, "E" => 40, "F" => 41, "G" => 42, "H" => 43, "I" => 44, "J" => 45, "K" => 46, "L" => 47, "M" => 48, "N" => 49, "O" => 50, "P" => 51, "Q" => 52, "R" => 53, "S" => 54, "T" => 55, "U" => 56, "V" => 57, "W" => 58, "X" => 59, "Y" => 60, "Z" => 61);
        $len = strlen($alpha);
        $dec = 0;
        $i = 1;
        while ($i <= $len) {
            $c = $alpha[$i - 1];
            $dec = $dec + (int) $d[$c] * pow(62, $len - $i);
            $i++;
        }
        return ($dec / 131);
    }

    /**
     * To get truncated string.
     * 
     * @param string  $text   text.
     * @param integer $length length
     * 
     * @return boolean|string $text truncated string.
     */
    public static function truncate($text, $length)
    {
        if (is_array($text)) {
            return false;
        }
        if (self::strlen($text) > ($length + 2)) {
            $text = self::substr($text, 0, $length);
            $text .='...';
        }
        return $text;
    }

    /**
     * To sort filter tags on basis of their group_priority.
     * 
     * @param array $a filter tag
     * @param array $b filter tag
     * 
     * @return int $a -1|1
     */
    public static function filterTagSort($a, $b)
    {
        if ($a['group_priority'] <= $b['group_priority']) {
            return -1;
        }
        return 1;
    }

    /**
     * Is this number.
     *  
     * @param type $num number
     * 
     * @return boolean $is_number true|false 
     */
    public static function isNumber($num)
    {
        // the regular expression for matching numbers.
        $numberPattern = '/^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$/';
        return preg_match($numberPattern, $num);
    }
    
    /**
     * To send mail from contact@pricedekho.com.
     * 
     * @param string $message      Mail message.
     * @param string $subject      Mail subject.
     * @param array  $mail_address Mail addresses.
     * 
     * @return boolean $numsent numsent.
     */
    public static function contactMail($message, $subject, $mail_address)
    {
        $mail_message = new YiiMailMessage();
        $mail_message->setTo($mail_address);
        $mail_message->setFrom(array('vinayverma158@gmail.com' => 'Contact Alert'));
        $mail_message->setSubject($subject);
        $mail_message->setBody($message, 'text/html', 'utf-8');

        return $numsent = Yii::app()->mail->send($mail_message);
    }
    
    /**
     * To send alert mail.
     * 
     * @param string   $message        Mail message.
     * @param string   $subject        Mail subject.
     * @param array    $mail_address   Mail addresses.
     * @param string   $name           Name.
     * @param boolean  $is_attachement Is attachment.
     * @param resource $filename       filename.
     * 
     * @return boolean $numsent numsent.
     */
    public static function alertMail($message, $subject, $mail_address = null, $name = null, $is_attachement = false, $filename = null)
    {
        $mail_message = Yii::$app->mailer->compose();
		
		/*$toAddresses = array(
            'vinayverma158@gmail.com' => 'Vinay'
        ); */
		

        if (is_array($mail_address)) {
            foreach ($mail_address as $k => $madd) {
                $toAddresses[$madd] = $madd;
            }
        } elseif ($mail_address) {
            $toAddresses[$mail_address] = $name;
        }
		
        $mail_message->setTo($toAddresses);
		
        $mail_message->setFrom(array('sameervamra6@gmail.com' => 'Alert'));
        $mail_message->setSubject($subject);
        if ($is_attachement === true) {
            $mail_message->attach(Swift_Attachment::fromPath($filename));
        }
		
        $mail_message->setHtmlBody($message, 'text/html', 'utf-8');
		
        return $mail_message->send();
    }
    
    /**
     * To send newsletter.
     * 
     * @param string   $message        Mail message.
     * @param string   $subject        Mail subject.
     * @param array    $mail_address   Mail addresses.
     * @param string   $name           Name.
     * @param boolean  $is_attachement Is attachment.
     * @param resource $filename       filename.
     * 
     * @return boolean $numsent numsent.
     */
    public static function alertMailForNewsletter($message, $subject, $mail_address = null, $name = null, $is_attachement = false, $filename = null)
    {
        $mail_message = new YiiMailMessage();
        if (is_array($mail_address)) {
            foreach ($mail_address as $k => $madd) {
                $toAddresses[$madd] = $madd;
            }
        } elseif ($mail_address) {
            $toAddresses[$mail_address] = $name;
        }
        $mail_message->setTo($toAddresses);
        $mail_message->setFrom(array('vinayverma158@gmail.com' => 'nayashoppy'));
        $mail_message->setSubject($subject);
        if ($is_attachement === true) {
            $mail_message->attach(Swift_Attachment::fromPath($filename));
        }
        $mail_message->setBody($message, 'text/html', 'utf-8');
        return $numsent = Yii::app()->newslettermail->send($mail_message);
    }
    
    /**
     * To execute process via shell.
     * 
     * @param string $process process name.
     * 
     * @return boolean $result true|false 
     */
    public static function startProcess($process)
    {
        shell_exec($process);
        if (isset($out[0])) {
            return $out[0];
        }
        return false;
    }
    
    /**
     * To get current year.
     * 
     * @return integer $year Current year. 
     */
    public static function getYear()
    {
        return date('Y');
    }
    
    /**
     * To get date in {'M d, Y'} format.
     * 
     * @param string $date date.
     * 
     * @return string $date {'M d, Y'} format.
     */
    public static function getDate($date)
    {
        return date('M d, Y', strtotime($date));
    }
    
    /**
     * To get date in {'m/d/Y'} format.
     * 
     * @param string $date date.
     * 
     * @return string $date {'m/d/Y'} format.
     */
    public static function changeDateFormat($date)
    {
        return date('m/d/Y', strtotime($date));
    }
    
    /**
     * To get date in {'jS F Y'} format.
     * 
     * @param string $date date.
     * 
     * @return string $date {'jS F Y'} format.
     */
    public static function getDateFormat($date)
    {
        return date('jS F Y', strtotime($date));
    }
    
    /**
     * To get date in {'j F Y'} format.
     * 
     * @param string $date date.
     * 
     * @return string $date {'j F Y'} format.
     */
    public static function getUpdateDateFormat($date)
    {
        return date("j F Y", strtotime($date));
    }
    
    /**
     * To get date in {'D M j G:i:s T Y'} format.
     * 
     * @param string $date date.
     * 
     * @return string $date {'D M j G:i:s T Y'} format.
     */
    public static function getDateMetaFormat($date)
    {
        return date("D M j G:i:s T Y", strtotime($date));
    }
    
    /**
     * Is blank text.
     * 
     * @param string $text text.
     * 
     * @return boolean $is_blank true|false 
     */
    public static function checkBlank($text)
    {
        return (is_null($text) || (trim($text) == ''));
    }
    
    /**
     * To resize video.
     * 
     * @param string  $html   Video html.
     * @param integer $height height.
     * @param integer $width  width.
     * 
     * @return string $resized_video Resized video.  
     */
    public static function resizeVideo($html, $height, $width)
    {
        if ((!is_numeric($height)) || (!is_numeric($width))) {
            return;
        }
        if (strstr($html, 'style="')) {
            $vvar = explode('style="', $html);
            $ttt = explode('"', $vvar[1]);
            $ttt[0] = 'height:' . $height . 'px;width:' . $width . 'px;';
            $vvar[1] = implode('"', $ttt);
            if (isset($vvar[2])) {
                $ttt = explode('"', $vvar[2]);
                $ttt[0] = 'height:' . $height . ';width:' . $width . ';';
                $vvar[2] = implode('"', $ttt);
            }
            $html = implode('style="', $vvar);
        }
        if (strstr($html, 'height="')) {
            $var = explode('height="', $html);
            $height = explode('"', $var[1]);
            if ($height[0] != $height) {
                $height[0] = $height;
                $var[1] = implode('"', $height);
                if (isset($var[2])) {
                    $height1 = explode('"', $var[2]);
                    $height1[0] = $height;
                    $var[2] = implode('"', $height1);
                }
                $html = implode('height="', $var);
            }
            $var = explode('width="', $html);
            $width = explode('"', $var[1]);
            if ($width[0] != $width) {
                $width[0] = $width;
                $var[1] = implode('"', $width);
                if (isset($var[2])) {
                    $width1 = explode('"', $var[2]);
                    $width1[0] = $width;
                    $var[2] = implode('"', $width1);
                }
                $html = implode('width="', $var);
            }
        }
        return $html;
    }
    
    /**
     * To get facebook like button html.
     * 
     * @param string $url    url
     * @param string $title  title
     * @param string $layout layout
     * 
     * @return string $facebook_like_button Facebook like button html. 
     */
    public static function getFacebookLikeButton($url = null, $title = null, $layout = "standard")
    {
        return '<div class="addthis_toolbox addthis_default_style" addthis:url="' . ($url) . '">
                    <a class="addthis_button_facebook_like" fb:like:layout="' . $layout . '"></a>
                </div>';
    }
    
    /**
     * To get google one button html.
     * 
     * @param string $url   url
     * @param string $title title
     * @param string $style style
     * 
     * @return string $google_one_button Google one button html. 
     */
    public static function getGoogleOneButton($url = null, $title = null, $style = "standard")
    {
        $title = ($title == 'base') ? self::getBaseTitle() : $title;
        return '<div class="addthis_toolbox addthis_default_style" addthis:url="' . ($url) . '" addthis:title="' . ($title) . '">
                    <a class="addthis_button_google_plusone" g:plusone:size="' . $style . '"></a>
                </div>';
    }
    
    /**
     * To get hyphen separated image name.
     * 
     * @param string $image_name Name of image.
     * 
     * @return string $image_name Hyphen separated image name.
     */
    public static function getImagename($image_name)
    {
        return strtolower(str_replace(' ', '-', $image_name));
    }

    /**
     * To get base title.
     * 
     * @return string $base_title Base Title.
     */
    public static function getBaseTitle()
    {
        return 'Smart Shopping is Here - Compare Prices Before You Buy';
    }
    
    /**
     * To get base url.
     * 
     * @return string $base_url Base url of pricedekho.
     */
    public static function getBaseUrl()
    {
        return _PD_HOME_URL_;
    }

    /**
     * To get facebook share button html.
     * 
     * @param string $url        Url.
     * @param string $page_title Page title.
     * @param string $title      Title.
     * 
     * @return string $button Facebook share button html. 
     */
    public static function facebookShareButton($url, $page_title, $title)
    {
        $page_title = ($page_title == 'base') ? self::getBaseTitle() : $page_title;
        $url = '<script type="text/javascript">var fbShare = {
                url: "' . $url . '",
                size : "small",
                google_analytics: "true",
                title : "' . $page_title . '"
                }</script>
            <script type="text/javascript" src="http://widgets.fbshare.me/files/fbshare.js"></script>';
        return $url;
    }
    
    /**
     * To get twitter and email button.
     * 
     * @return string $buttons_html Twitter and Email button. 
     */
    public static function twitterEmailButton()
    {
        return '<div class="addthis_toolbox addthis_default_style" addthis:url="' . Yii::app()->request->getHostInfo() . '"
                addthis:title="Smart Shopping is Here - Compare Prices Before You Buy"
                addthis:description="Compare Prices in India before you Buy. Check price in India of Mobiles, Cameras, Laptops, Books, and more... Pehle Price Dekho Fir Khareedo !">
                    <a class="addthis_button_tweet"></a>
                    <span class="stButton_gradient">
                        <a class="addthis_button_email"></a><span class="chicklets email">Email</span>
                    </span>
                </div>';
    }
    
    /**
     * To get twitter button.
     * 
     * @param string  $url        Url
     * @param string  $page_title Page title
     * @param integer $count      Count
     * 
     * @return string $button Twitter share button. 
     */
    public static function twitterButton($url, $page_title, $count = null)
    {
        $page_title = ($page_title == 'base') ? self::getBaseTitle() : $page_title;
        return '<div class="addthis_toolbox addthis_default_style" addthis:url="' . ($url) . '" addthis:title="' . ($page_title) . '">
                    <a class="addthis_button_tweet" tw:count="' . ($count == null ? 'none' : $count) . '"></a>
                </div>';
    }
    
    /**
     * To get email button.
     * 
     * @param string $url        Url.
     * @param string $page_title Page title.
     * 
     * @return string $button Email button. 
     */
    public static function emailButton($url, $page_title)
    {
        $page_title = ($page_title == 'base') ? self::getBaseTitle() : $page_title;
        return '<div class="addthis_toolbox addthis_default_style" addthis:url="' . ($url) . '" addthis:title="' . ($page_title) . '">
                    <span class="stButton_gradient">
                        <a class="addthis_button_email"></a><span class="chicklets email">Email</span>
                    </span>
                </div>';
    }
    
    /**
     * To get bookmark button.
     * 
     * @return string $button Bookmark button. 
     */
    public static function bookmarkButton()
    {
        return '<a href="http://www.addthis.com/bookmark.php"
                               class="addthis_button" addthis:url="' . (self::getBaseUrl()) . '"
                              addthis:title="' . (self::getBaseTitle()) . '"></a>';
    }
    
    /**
     * To get filter title text.
     * 
     * @param array  $filter   Product Filter.
     * @param string $category Category name.
     * 
     * @return string $title Filter title. 
     */
    public static function getFilterTitle($filter, $category)
    {
        $title = '';
        switch (strtolower($filter['groupname'])) {
            case 'brand' :
                $title = $filter['name'] . ' Price';
                break;
            case 'price range' :
                $title = 'Where ' . $category . 'Price Range is From ' . $filter['name'];
                break;
            default :
                $title = $filter['name'];
        }
        return ucwords($title);
    }
    
    /**
     * To get youtube video image url.
     * 
     * @param string $data data
     * 
     * @return string $video_url Video image url.
     */
    public static function getVideoImageUrl($data)
    {
        $code = Tools::getVideoCode($data);
        return 'http://i2.ytimg.com/vi/' . $code . '/default.jpg';
    }
    
    /**
     * To get video code.
     * 
     * @param string $data data
     * 
     * @return string $code Video Id. 
     */
    public static function getVideoCode($data)
    {
        $matches = array();
        $code = '';
        preg_match('#(\.be/|/embed/|/v/|/watch\?v=)([A-Za-z0-9_-]{5,11})#', $data, $matches);
        if (isset($matches[2]) && $matches[2] != '') {
            $code = $matches[2];
        }
        return $code;
    }
    
    /**
     * To get image code from image.
     * 
     * @param string $img Imgage url.
     * 
     * @return string $image_code Image code.
     */
    public static function getImageCodeFromImagePath($img)
    {
        preg_match('/\/([0-9]+).jpg/', $img, $imgArray);
        return isset($imgArray[1]) ? (int) $imgArray[1] : 0;
    }
    
    /**
     * To get facebook comments.
     * 
     * @param string $url Url
     * 
     * @return string $html facebook comments html. 
     */
    public static function getFBComments($url)
    {
        return '<div class="fb-comments" data-href="' . $url . '" data-num-posts="3" data-width="640"></div>';
    }
    
    /**
     * To get clean product name.
     * 
     * @param string $name Product name.
     * 
     * @return string $name Clean product name. 
     */
    public static function cleanProductName($name)
    {
        $namearray = explode('(', $name);
        $name = trim($namearray[0]);
        $namearray = explode('Point & Shoot', $name);
        $name = trim($namearray[0]);
        $namearray = explode('with', $name);
        $name = trim($namearray[0]);
        $name = str_replace('+', '', str_replace('- ', '', str_replace('& ', '', $name)));
        return trim($name);
    }
    
    /**
     * To get share widget html.
     * 
     * @param string  $url    Url
     * @param integer $rating Rating
     * 
     * @return string $share_widget_html Share widget html 
     */
    public static function getPostShareWidget($url, $rating = null)
    {
        $data = '<div class="post-share-widget">';
        if ($rating !== null) {
            $data.='<div class="platform comments">
                         <div class="comment-count-bubble">
                            <div class="fbcomment-count ">' . $rating . ' </div>
                        </div>
            <a class="btn-comment">Rating</a>

                     </div>';
        }
        $data.='<div class="platform facebook">
            ' . Tools::getFacebookLikeButton(_FACEBOOK_PAGE_, 'base', 'box_count') . '
        </div>
        <div class="platform twitter">
            ' . Tools::Twitter_Button(Tools::getBaseUrl(), Tools::getBaseTitle(), 'vertical') . '
        </div>
        <div class="platform gplus">
            ' . Tools::getGoogleOneButton(Tools::getBaseUrl(), Tools::getBaseTitle(), 'tall') . '
        </div>
        <div class="bottom"><b></b></div>
    </div>';
        return $data;
    }
    
    /**
     * To get filter price.
     * 
     * @param string $userString userString
     * 
     * @return string $result result 
     */
    public static function filterPrice($userString)
    {
        $userString = str_ireplace('rs.', '', strtolower($userString));
        $money = explode(".", $userString);

        $dollars = preg_replace("/[^0-9]/", null, $money[0]);

        $cents = '';
        if (isset($money[1])) {
            $cents = preg_replace("/[^0-9]/", null, $money[1]);
            $result = $dollars . "." . $cents;
        } else {
            $result = $dollars;
        }
        return($result);
    }

    /** 
     * To get title for lisiting page.
     * 
     * @param array  $selected_filters Selected Filters.
     * @param object $category         Category object.
     * 
     * @return string $listing_pae_title Product listing page title.
     */
    public static function getTitle($selected_filters, $category)
    {
        if (count($selected_filters) == 0) {
            return $category->name . ' price list';
        }
        $categoryname = $category->name;
        $mergedarray = array();
        foreach ($selected_filters as $index => $selected_filter) {
            $groupname = $selected_filter['groupname'];
            $mergedarray[$groupname] = $selected_filter['name'];
            if ($selected_filter['name'] == "Apple" && $categoryname == "Mp3 players Ipod") {
                $categoryname = "Ipod";
            }
        }
        $mergedarray['Category'] = $categoryname;
        try {
            $displaytextrules = new DisplayTextRules($category->id_category);
            return $displaytextrules->getString($mergedarray);
        } catch (Exception $e) {
        }
    }
    
    /**
     * To set cache header for an ajax request.
     * 
     * @param integer $seconds Seconds.
     * 
     * @return void
     */
    public static function setAjaxCacheHeader($seconds = 3600)
    {
        header("Cache-Control: public, max-age=$seconds");
        header("Expires: " . gmdate('r', time() + $seconds));
        header("Pragma: cache");
    }
    
    /**
     * To remove brand name.
     * 
     * @param string $haystack Name.
     * @param string $needle   Brand Name
     * 
     * @return string $name name
     */
    public static function removeBrand($haystack, $needle)
    {
        $haystack = trim($haystack);
        $needle = trim($needle);
        $needleLength = strlen($needle);
        $dupl = substr($haystack, 0, $needleLength);
        if (!strcasecmp($needle, $dupl)) {
            return substr($haystack, $needleLength);
        } else {
            return $haystack;
        }
    }

    /**
     * To get rating positions.
     * 
     * @param int    $rating star position.
     * @param string $size   display size.
     * 
     * @return string $image_position Image position of star.
     */
    public static function getStarRatingPosition($rating, $size = 'small')
    {
        if ($size == 'small') {
            $positions = array(
                0 => '0px -45px',
                1 => '0px -36px',
                2 => '0px -27px',
                3 => '0px -18px',
                4 => '0px -9px',
                5 => '0px 0px',
            );
        } else {
            $positions = array(
                0 => '0px -71px',
                1 => '0px -57px',
                2 => '0px -43px',
                3 => '0px -29px',
                4 => '0px -15px',
                5 => '0px -1px',
            );
        }
        return $positions[(int) $rating];
    }

    /**
     * To get fancy box html alert
     * 
     * @param string $title       title
     * @param string $description description
     * 
     * @return string $fancy_box_html Fancy box html 
     */
    public static function displayAlert($title, $description)
    {
        return '<div style="display: none;">
                    <a href="#pricealertconfirm" id="pricealertmsg">&nbsp;</a>
                    <!--[if !IE]>Main Popup Content<![endif]-->
                    <div class="PopupArea" id="pricealertconfirm">

                        <div class="AlertPopup">
                            <div class="ImgHolder"><img src="' . Tools::getBaseUrl() . '/css/images/icon_alert.png" width="84" height="77" /></div>
                            <div class="Content">
                                <div class="Heading MarginT10">' . $title . '</div>
                                <p>' . $description . '</p>
                            </div>
                            <div class="Close"><a href="javascript:;" onclick="$.fancybox.close();">Close</a></div>
                            <div class="Clear"></div>
                        </div>
                    </div>
                    <!--[if !IE]>Main Popup Content<![endif]-->
                </div>
                <style>
                    #fancybox-outer{background: none}#fancybox-content{border:none}
                </style>
                <script>
                    $("#pricealertmsg").fancybox({
                        "titlePosition"		: "inside",
                        "transitionIn"		: "none",
                        "transitionOut"		: "none",
                        "scrolling"         : "no",
                        "showCloseButton"	: false
                    });
                    $("#pricealertmsg").trigger("click");
                </script>';
    }

    /**
     * Whether the supplier is featured or not.
     * 
     * @param string $supplier Supplier name.
     * 
     * @return boolean $is_featured true|false 
     */
    public static function isFeaturedSupplier($supplier)
    {
        return Affiliate::isFeatured($supplier);
    }

    /**
     * Whether the supplier is sponsored or not.
     * 
     * @param string $supplier Supplier name.
     * 
     * @return boolean $is_sponsored true|false 
     */
    public static function isSponseredSupplier($supplier)
    {
        return Affiliate::isSponsered($supplier);
    }
    
    /**
     * To get facebook comment count of a url.
     * 
     * @param string $url Url
     * 
     * @return integer $count Facebook comment count. 
     */
    public static function fbCommentCount($url)
    {
        $filecontent = file_get_contents('https://graph.facebook.com/?ids=' . $url);
        $json = json_decode($filecontent);
        $count = (isset($json->$url->comments)) ? $json->$url->comments : 0;
        if ($count == 0 || !isset($count)) {
            $count = 0;
        }
        return $count;
    }
    
    /**
     * To get http response of a url.
     * 
     * @param string $url        Url
     * @param array  $postfields postfields
     * @param string $username   username
     * @param string $password   password
     * 
     * @return string|boolean $output http response 
     */
    public static function getHttpResponse($url, $postfields = "", $username = "", $password = "")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        if ($postfields) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        if ($username) {
            curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
        }
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpcode >= 200 && $httpcode < 300) {
            
        } else {
            $output = false;
        }
        $ch = null;
        return $output;
    }
    
    /**
     * To get percentage.
     * 
     * @param integer|float $highest_value Highest Value
     * @param integer|float $lowest_value  Lowest Value
     * 
     * @return integer|float $percentage percentage 
     */
    public static function calculatePercentage($highest_value, $lowest_value)
    {
        return ($highest_value - $lowest_value) / $highest_value * 100;
    }
    
    /**
     * Top get current version of 
     * 
     * @return string 
     */
    public static function getVersionNumber()
    {
        $version = "151442014";
        return $version;
    }
    
    /**
     * To get facebook script
     * 
     * @return String $fb_script facebook script 
     */
    public static function getFbScript()
    {
        return '<script>
          window.fbAsyncInit = function() {
                FB.init({
                    appId : "' . _FBAppID_ . '",
                    status : true,
                    cookie : true,
                    xfbml : true,
                    channelUrl : "' . Tools::getBaseUrl() . '/channel.html",
                    oauth: true
                });
            };

            (function(d){
                var js, id = \'facebook-jssdk\'; if (d.getElementById(id)) {return;}
                js = d.createElement(\'script\'); js.id = id; js.async = true;
                js.src = "https://connect.facebook.net/en_US/all.js";
                d.getElementsByTagName(\'head\')[0].appendChild(js);
            }(document));
         </script>';
    }
    
    /**
     * To get old facebook comments.
     * 
     * @param String $url Url
     * 
     * @return object $fb_comments facebook comments 
     */
    public static function getOldFbComments($url)
    {
        $url = str_replace('local', '', $url);
        if ($url) {
            $fb_comments = Tools::getHttpResponse("https://graph.facebook.com/comments/?ids=$url");
            $fb_comments = json_decode($fb_comments);
            $fb_comments = $fb_comments->$url->data;
        }
        return $fb_comments;
    }
    
    /**
     * To flush memcache
     * 
     * @return void
     */
    public static function flushMemcache()
    {
        self::sendMemcacheCommand("localhost", "11211", 'flush_all');
    }
    
    /**
     * To send memcache command on server
     * 
     * @param string  $server  Ip address of a server
     * @param integer $port    Port number
     * @param string  $command Command
     * 
     * @return array $result result 
     */
    private static function sendMemcacheCommand($server, $port, $command)
    {
        $s = @fsockopen($server, $port);
        if ($s) {
            fwrite($s, $command . "\r\n");

            $buf = '';
            while ((!feof($s))) {
                $buf .= fgets($s, 256);
                if (strpos($buf, "END\r\n") !== false) { // stat says end
                    break;
                }
                if (strpos($buf, "DELETED\r\n") !== false || strpos($buf, "NOT_FOUND\r\n") !== false) { // delete says these
                    break;
                }
                if (strpos($buf, "OK\r\n") !== false) { // flush_all says ok
                    break;
                }
            }
            fclose($s);
            return self::parseMemcacheResults($buf);
        }
    }
    
    /**
     * Parse memcache results
     * 
     * @param string $str str
     * 
     * @return array $result result 
     */
    private static function parseMemcacheResults($str)
    {
        $res = array();
        $lines = explode("\r\n", $str);
        $cnt = count($lines);
        for ($i = 0; $i < $cnt; $i++) {
            $line = $lines[$i];
            $l = explode(' ', $line, 3);
            if (count($l) == 3) {
                $res[$l[0]][$l[1]] = $l[2];
                if ($l[0] == 'VALUE') { // next line is the value
                    $res[$l[0]][$l[1]] = array();
                    list ($flag, $size) = explode(' ', $l[2]);
                    $res[$l[0]][$l[1]]['stat'] = array('flag' => $flag, 'size' => $size);
                    $res[$l[0]][$l[1]]['value'] = $lines[++$i];
                }
            } elseif ($line == 'DELETED' || $line == 'NOT_FOUND' || $line == 'OK') {
                return $line;
            }
        }
        return $res;
    }

    /**
     * Find other process running given command and kill them all
     * 
     * @param string $process process name
     * 
     * @return void
     */
    public static function killOtherProcess($process)
    {
        exec("ps aux | grep '" . $process . "' | grep -v grep | awk '{ print $2 }'", $pids);
        $mypid = getmypid();
        $pid_key = array_search($mypid, $pids);
        unset($pids[$pid_key]);
        foreach ($pids as $key => $value) {
            exec("kill -9 '" . $value . "'");
        }
    }
    
    /**
     * To get suppliers of book.
     * 
     * @return array $suppliers Suppliers 
     */
    public static function getBooksSuppliers()
    {
        return array(
            //"flipkart.com", //done
            "infibeam", // done
            "crossword", // done  search doesnot return response on every isbn
            "uread", // done
            "maansu", // not done .searches with isbn 13
            "bookadda", // done
            "landmarkonthenet" // not done . searches with isbn-13
        );
    }
    
    /** 
     * To get network sites xml
     * 
     * @return string|boolean $string xml string of network sites
     */
    public static function getNetWorkSiteData()
    {
        $filePath = Yii::getPathOfAlias("application.runtime") . '/sitelink.xml';
        try {
            if (is_file($filePath)) {
                //$intervel = self::daysDifferences(date("Y-m-d", strtotime('now')), date("Y-m-d", filemtime($filePath)));
                $current_time = strtotime('now');
                $file_time = filemtime($filePath);
                $intervel = ($current_time - $file_time) / 3600;
                if ($intervel > 1) {
                    self::makeSiteLinkFiles($filePath);
                }
            } else {
                self::makeSiteLinkFiles($filePath);
            }
            $xml = file_get_contents($filePath);
            return $xml = @simplexml_load_string($xml);
        } catch (Exception $e) {
            
        }
        return null;
    }
    
    /**
     * To make site link files.
     * 
     * @param resource $filePath file path 
     * 
     * @return void
     */
    public static function makeSiteLinkFiles($filePath)
    {
        $xml = Tools::getHttpResponse("http://process.girnarsoft.com/sitelinksynchronization/pricedekho/xml");
        if ($xml) {
            $fhandle = fopen($filePath, "w+");
            fwrite($fhandle, $xml);
            fclose($fhandle);
        }
    }
    
    /**
     * To get date difference of two date.
     * 
     * @param string $endDate   End date
     * @param string $beginDate Begin date
     * 
     * @return integer $date_difference Date difference 
     */
    public static function daysDifferences($endDate, $beginDate)
    {
        $date_parts1 = explode("-", $beginDate);
        $date_parts2 = explode("-", $endDate);
        $start_date = gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
        $end_date = gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
        return $end_date - $start_date;
    }
    
    /**
     * To send mail from cache.
     * 
     * @param resource $cachedFileName Cached file name.
     * @param string   $to             Mail address.
     * @param string   $title          Mail subject.
     * @param string   $body           Mail body.
     * @param integer  $expire         Cache expire time in seconds.
     * 
     * @return void
     */
    public static function cachedMailAlert($cachedFileName, $to, $title, $body, $expire)
    {
        $cachedata = Yii::app()->cache->get($cachedFileName);
        if (!Yii::app()->cache->get($cachedFileName)) {
            self::alertMail($body, $title, $to);
            Yii::app()->cache->set($cachedFileName, 'yes', $expire);
        }
    }
    
    /**
     * To get first index of an array.
     * 
     * @param array $a array
     * 
     * @return mixed $key First index of an array 
     */
    public static function getFirstIndex($a)
    {
        foreach ($a as $k => $v) {
            return $k;
        }
    }
    
    /**
     * To get coupon text based on count.
     * 
     * @param integer $coupon_count Coupon count
     * 
     * @return string $coupon_string
     */
    public static function getCouponText($coupon_count)
    {
        if ($coupon_count == 1) {
            echo $coupon_count . " coupon";
        } else {
            echo $coupon_count . " coupons";
        }
    }
    
    /**
     * To remove hash from url
     * 
     * @param string $url Url
     * 
     * @return string $url Url after removing hash 
     */
    public static function removeHashFromUrl($url)
    {
        $url = explode('#', $url);
        return $url[0];
    }

    /**
     * Is surfing device is iPad.
     * 
     * @return boolean $iPad Is Ipad device
     */
    public static function getSurfingDevice()
    {
        $iPad = false;
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        }
        return $iPad;
    }
    
    /**
     * To generate random password of 10 character.
     * 
     * @return string $password Random password 
     */
    public static function getRandomPassword()
    {
        $length = 10;
        $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        shuffle($chars);
        return $new_password = implode(array_slice($chars, 0, $length));
    }

    /**
     * Convert associative array in xml string.
     * 
     * @param array  $array   array
     * @param string $root    root 
     * @param string $element element
     * 
     * @return string xml_string 
     */
    public static function arrayToXML(array $array, $root, $element = 'root')
    {
        $xml = "<{$root}>\n";
        foreach ($array as $key => $value) {
            if ($key == $element) {
                $element = substr($element, 0, (strlen($element) - 1));
            }
            if (is_array($value)) {
                $xml .= self::arrayToXML($value, $element, $key);
            } else {
                if (is_numeric($key)) {
                    $xml .= "<{$root}><![CDATA[{$value}]]></{$root}>\n";
                } else {
                    $xml .= "<{$key}><![CDATA[{$value}]]></{$key}>\n";
                }
            }
        }
        $xml .= "</{$root}>\n";
        $xml = str_replace("<root>", "", $xml);
        $xml = str_replace("</root>", "", $xml);
        return trim($xml);
    }
    
    /**
     * To check cookie by name.
     * 
     * @param string $name name
     * 
     * @return boolen $isTrue true|false
     */
    public static function hasCookie($name)
    {
        return !empty(Yii::app()->request->cookies[$name]->value);
    }
    
    /**
     * To get cookie value by name.
     * 
     * @param string $name name
     * 
     * @return mixed $value value 
     */
    public static function getCookie($name)
    {
        return Yii::app()->request->cookies[$name]->value;
    }
    
    /**
     * To set cookie
     * 
     * @param string  $name                 name
     * @param mixed   $value                value
     * @param integer $time                 time in seconds
     * @param boolean $disableClientCookies disableClientCookies true|false
     * 
     * @return void
     */
    public static function setCookie($name, $value, $time = 0, $disableClientCookies = false)
    {
        $cookie = new CHttpCookie($name, $value);
        if ($time) {
            $cookie->expire = time() + $time;
        }
        $cookie->httpOnly = $disableClientCookies;
        Yii::app()->request->cookies[$name] = $cookie;
    }
    
    /**
     * To unset cookie
     * 
     * @param string $name cookie key
     * 
     * @return void
     */
    public static function removeCookie($name)
    {
        unset(Yii::app()->request->cookies[$name]);
    }
    
    /**
     * To get ad mappings.
     * 
     * @param string $slot ad slot name
     * 
     * @return string $mapping ad mapping string 
     */
    public static function getAdSizeMapping($slot)
    {

        $mapping = '';
        switch ($slot) {
            case 'TopBannerLeftSideAds':
                $mapping = 'googletag.sizeMapping().                        
                        addSize([750, 0], [728, 90]).
                        addSize([320, 0], [234, 60]).
                        build()';
                break;
            case 'TopBannerRightSideAds':
                $mapping = 'googletag.sizeMapping().                                                
                        addSize([1000, 0], [234, 90]).
                        build()';
                break;
            case 'RightColumnTopAds':
                $mapping = 'googletag.sizeMapping().                        
                        addSize([1000, 0], [300, 250]).
                        addSize([750, 0], [320, 50]).
                        addSize([470, 0], [468, 60]).
                        addSize([340, 0], [320, 50]).
                        addSize([300, 0], [234, 60]).
                        build()';
                break;
            case 'RightColumnBottomAds':
                $mapping = 'googletag.sizeMapping().                        
                        addSize([1000, 0], [300, 250]).
                        addSize([750, 0], [320, 50]).
                        addSize([470, 0], [468, 60]).
                        addSize([340, 0], [320, 50]).
                        addSize([300, 0], [234, 60]).
                        build()';
                break;
            case 'LeftCenterBannerAds':
                $mapping = 'googletag.sizeMapping().                        
                        addSize([500, 0], [468, 60]).
                        addSize([400, 0], [320, 50]).
                        addSize([320, 0], [234, 60]).
                        build()';
                break;
            case 'LeftBottomBannerAds':
                $mapping = 'googletag.sizeMapping().                        
                        addSize([1100, 0], [728, 90]).
                        addSize([500, 0], [468, 60]).
                        addSize([400, 0], [320, 50]).
                        addSize([320, 0], [234, 60]).
                        build()';
                break;
            case 'LeftColumnPagingAds':
                $mapping = 'googletag.sizeMapping().                                                
                        addSize([500, 0], [468, 60]).
                        addSize([400, 0], [320, 50]).
                        addSize([320, 0], [234, 60]).
                        build()';
                break;
            case 'LeftColumnLargePagingAds':
                $mapping = 'googletag.sizeMapping().                        
                        addSize([500, 0], [468, 60]).
                        addSize([400, 0], [320, 50]).
                        addSize([320, 0], [234, 60]).
                        build()';
                break;
        }
        return $mapping;
    }

    /**
     * To export data as csv.
     * 
     * @param array    $data      data
     * @param resource $file_name file name
     * 
     * @return void
     */
    public static function exportCsv(array $data, $file_name)
    {
        if (count($data)) {
            $fp = fopen('php://temp', 'w');
            foreach ($data as $result) {
                fputcsv($fp, $result);
            }
            rewind($fp);
            Yii::app()->user->setState('export', stream_get_contents($fp));
            fclose($fp);
            Yii::app()->request->sendFile($file_name, Yii::app()->user->getState('export'));
            Yii::app()->user->clearState('export');
        }
    }

    /**
     * To copy directory.
     * 
     * @param resource $source      source
     * @param resource $destination destination
     * 
     * @return void
     */
    public static function copyDirectory($source, $destination)
    {
        if (is_dir($source)) {
            @mkdir($destination, 0777, true);
            try {
                chmod($destination, 0777);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            $directory = dir($source);
            while (false !== ( $readdirectory = $directory->read() )) {
                if ($readdirectory == '.' || $readdirectory == '..') {
                    continue;
                }
                $PathDir = $source . '/' . $readdirectory;
                if (is_dir($PathDir)) {
                    //copy_directory( $PathDir, $destination . '/' . $readdirectory );
                    //continue;
                } else {
                    copy($PathDir, $destination . '/' . $readdirectory);
                }
            }

            $directory->close();
        } else {
            copy($source, $destination);
        }
    }

    /**
     * To remove directory.
     * 
     * @param resource $dirname dirname
     * 
     * @return boolean result true|false
     */
    public static function deleteDirectory($dirname)
    {
        if (is_dir($dirname)) {
            $dir_handle = opendir($dirname);
        }
        if (!$dir_handle) {
            return false;
        }
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file)) {
                    unlink($dirname . "/" . $file);
                } else {
                    self::deleteDirectory($dirname . '/' . $file);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

    /**
     * To get host from url.
     *
     * @param string $url Url
     *
     * @return string|boolean host|false
     */
    public static function getHostFromUrl($url)
    {
        if (is_null($url)) {
            return false;
        }
        $url_array = parse_url($url);
        return (isset($url_array['host']) && $url_array['host']) ? $url_array['host'] : false;
    }

    /**
     * To get path from url
     *
     * @param string $url Url
     *
     * @return string|boolean path|false
     */
    public static function getPathFromUrl($url)
    {
        if (is_null($url)) {
            return false;
        }
        $url_array = parse_url($url);
        return (isset($url_array['path']) && $url_array['path']) ? $url_array['path'] : false;
    }
}
