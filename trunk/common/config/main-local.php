<?php
if (YII_ENV_PROD):
    $db = ['class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=205.147.100.232;dbname=nayashoppy_prod_db',
        'username' => 'usr-nayashoppy-p',
        'password' => 'EfeZveb6z2EAPwJll66I',
        'charset' => 'utf8',];

    $mongodb = [
        'class' => '\yii\mongodb\Connection',
        //'dsn' => 'mongodb://vijay:admin@localhost:27017/nayashoppy',
        'dsn' => 'mongodb://localhost/database?connectTimeoutMS=300000',
        'defaultDatabaseName' => 'nayashoppy', // Avoid autodetection of default database name
    ];
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ];
elseif (YII_ENV_TEST):
    $db = ['class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=nayashoppy',
        'username' => 'root',
        'password' => 'redhat123?',
        'charset' => 'utf8',];
    $mongodb = [
        'class' => '\yii\mongodb\Connection',
        //'dsn' => 'mongodb://vijay:admin@localhost:27017/nayashoppy',
        'dsn' => 'mongodb://localhost/database?connectTimeoutMS=300000',
        'defaultDatabaseName' => 'nayashoppy', // Avoid autodetection of default database name
    ];
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ];
elseif (YII_ENV_DEV):
    $db = ['class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=db_nayashoppy',
        'username' => 'tally',
        'password' => 'Yourpassword1!',
        'charset' => 'utf8',];
    $mongodb = [
        'class' => '\yii\mongodb\Connection',
        //'dsn' => 'mongodb://vijay:admin@localhost:27017/nayashoppy',
        'dsn' => 'mongodb://localhost/database?connectTimeoutMS=300000',
        'defaultDatabaseName' => 'nayashoppy', // Avoid autodetection of default database name
    ];
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ];
endif;




return [
    'components' => [
        'db' => $db,
//        'mongodb' => $mongodb,
        'mailer' => $mailer,
    ],
];
