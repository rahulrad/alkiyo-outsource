<?php

//if (YII_ENV_PROD):
    $apiUrl = 'http://api.pricekya.com/';
    $backendUrl = 'http://backend.pricekya.com/';
    $frontendUrl='https://www.pricekya.com/';
//elseif (YII_ENV_TEST):
  //  $apiUrl = 'http://api.naaya.com/';
    //$backendUrl = 'http://backend.naaya.com/';
    //$frontendUrl='https://local.naaya.com/';
//elseif (YII_ENV_DEV):
  //  $apiUrl = 'http://api.naaya.com/';
  //  $backendUrl = 'http://backend.naaya.com/';
   // $frontendUrl='https://local.naaya.com/';
//endif;


return [
    'api' => $apiUrl,
    'frontend' => $frontendUrl,
    'backend' => $backendUrl,
];
