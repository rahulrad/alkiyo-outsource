<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "activity_tracker".
 *
 * @property integer $id
 * @property string $url
 * @property string $session_id
 * @property string $type
 * @property string $created_at
 */
class ActivityTracker extends \yii\db\ActiveRecord
{
    const Activity_PRODUCT = "product";
    const Activity_COUPON = "coupon";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity_tracker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'type', 'store_name'], 'required'],
            [['url', 'type'], 'string'],
            [['created_at'], 'safe'],
            [['session_id', 'store_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'session_id' => 'Session ID',
            'type' => 'Type',
            'store_name' => 'Store',
            'created_at' => 'Date',
        ];
    }

    public static function saveActivity($url, $sessionId, $activityType, $storeName)
    {
        $model = new ActivityTracker();
        $model->setAttributes([
            'url' => $url,
            'session_id' => $sessionId,
            'type' => $activityType,
            'store_name' => $storeName,
        ]);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
}
