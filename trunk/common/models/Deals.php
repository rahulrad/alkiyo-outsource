<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deals".
 *
 * @property integer $id
 * @property string $title
 * @property integer $product_id
 * @property integer $discount
 * @property string $start_date
 * @property string $end_date
 * @property string $status
 * @property string $created
 * @property integer $moved
 */
class Deals extends \yii\db\ActiveRecord
{
	public $file;
	public $main_image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'title'], 'required'],
            [['id', 'store_id'], 'integer'],
            [['created','description','moved'], 'safe'],
            [['title','brand','link','image','availability','sizes','image_path'], 'string', 'max' => 225],
            [['price', 'offer_price'], 'string', 'max' => 20],
			[['discount', 'start_date','end_date'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 10],
			[['file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store',
            'title' => 'Title',
            'description' => 'Description',
            'brand' => 'Brand',
            'link' => 'Link',
			'image' => 'Image',
			'price'=>'Price',
			'offer_price' => 'Offer Price',
			'availability' => 'Availability',
			'sizes' => 'Sizes',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	
	public function getFlipkartDealUrls($affiliateID, $token){
			
	
			//$url = 'https://affiliate-api.flipkart.net/affiliate/offers/v1/dotd/json';
			$url = 'https://affiliate-api.flipkart.net/affiliate/offers/v1/dotd/json';
			$headers = array(
	            'Cache-Control: no-cache',
	            'Fk-Affiliate-Id: '.$affiliateID,
	            'Fk-Affiliate-Token: '.$token
	            );
               

			$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			//echo '<pre>'; print_r($result); die;
			if(!empty($result)){
				
				$resultData =  json_decode($result, true);
				
				if(!empty($resultData)){
					
					if(isset($resultData['dotdList'])){
						
						// save flipkart deal urls
						foreach($resultData['dotdList'] as $data){
							
							$img_url = $data['imageUrls'][0]['url'];
							if(isset($data['imageUrls'][2]['url']) && !empty($data['imageUrls'][2]['url'])){
								$img_url = $data['imageUrls'][2]['url'];
							}
							$dataResult = array(
											'store_id' => 1,
											'title' => $data['title'],
											'description' => $data['description'],
											'brand' => '',
											'link' => $data['url'],
											'image' => $img_url,
											'price' => '',
											'offer_price' => '',
											'availability' => $data['availability'],
											'sizes' => '',
											'discount' => '',
											'start_date' => '',
											'end_date' => '',
										 );
							
							$this->save_deal_urls($dataResult);
							
						}
					
					}
					
					
				}
				
			}
			
	}
	
	
	public function getSnapdealDealUrls($affiliateID, $token){
			
			$url = 'http://affiliate-feeds.snapdeal.com/feed/api/dod/offer';
			
			$headers = array(
	            'Accept:application/json',
	            'Snapdeal-Affiliate-Id: '.$affiliateID,
	            'Snapdeal-Token-Id: '.$token
	            );
               

			$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
			if(!empty($result)){
				
				$resultData =  json_decode($result, true);
				//echo '<pre>'; print_r($resultData); die;
				if(!empty($resultData)){
					//echo '<pre>'; print_r($resultData); die;
					// save flipkart deal urls
					if(isset($resultData['products'])){
							foreach($resultData['products'] as $data){
							//echo '<pre>'; print_r($data); die;
							$dataResult = array(
											'store_id' => 3,
											'title' => $data['title'],
											'description' => $data['description'],
											'brand' => $data['brand'],
											'link' => $data['link'],
											'image' => $data['imageLink'],
											'price' => $data['mrp'],
											'offer_price' => $data['offerPrice'],
											'availability' => $data['availability'],
											'sizes' => $data['sizes'],
											'discount' => '',
											'start_date' => '',
											'end_date' => '',
										 );
										 
							$this->save_deal_urls($dataResult);
							
						}
					}
					
					
				}
				
			}
			
	}
	
	
	public function getEbayDealUrls(){
		
		$json_url = "http://api.epn.ebay.com/deals/v1/country/us/feed/json?feedType=json";

		$json = file_get_contents($json_url);
		$foo = utf8_encode($json);
		$resultData = json_decode($foo, true);
		
		if(!empty($resultData)){
					// save flipkart deal urls
					if(isset($resultData['entry'])){
						foreach($resultData['entry'] as $data){
							//echo '<pre>'; print_r($data); 
							//echo $data['Location'].'==>'.$data['title'].'<br>';
							if(isset($data['Location']) && !empty($data['Location']) && $data['Location'] == ', IN'){
								//echo $data['StartTime'].'<br>';
								$start_date = date('Y-m-d: H:i:s', strtotime($data['StartTime']));
								$end_date =  date('Y-m-d: H:i:s', strtotime($data['EndTime']));
								//echo $start_date; die;
								$dataResult = array(
												'store_id' => 7,
												'title' => $data['title'],
												'description' => $data['ItemSummary'],
												'brand' => '',
												'link' => $data['DealURL'],
												'image' => $data['ImageURL'],
												'price' => $data['ListPrice'],
												'offer_price' => $data['SalePrice'],
												'availability' => '',
												'sizes' => '',
												'discount' => $data['SavingsRate'],
												'start_date' => $start_date,
												'end_date' => $end_date,
											 );
											 
								$this->save_deal_urls($dataResult);
							}
							
							
						} //die;
					}
					
					
				}
				
		
	}
	
	public function getShopcluesDealUrls(){
			
			$json_url = "http://api.shopclues.com/api/v9/campaigns?gid=1332&key=d32121c70dda5edfgd1df6633fdb36c0";
			
			$json = file_get_contents($json_url);
			$foo = utf8_encode($json);
			$resultData = json_decode($foo, true);
			
			if(!empty($resultData['text']) && isset($resultData['text'])){
				
				$foo = utf8_encode($resultData['text']);
				$productData = json_decode($foo, true);
				
				if(!empty($productData)){
					
					foreach($productData as $data){
						
						$dataResult = array(
										'store_id' => 4,
										'title' => $data['product_name'],
										'description' => $data['banner_title'],
										'brand' => '',
										'link' => 'http://www.shopclues.com/'.$data['seo_name'].'html',
										'image' => $data['banner_url']['main_image'],
										'price' => $data['product_list_price'],
										'offer_price' => $data['product_third_price'],
										'availability' => '',
										'sizes' => '',
										'discount' => $data['percentage'],
										'start_date' => '',
										'end_date' => '',
									 );
									 
						$this->save_deal_urls($dataResult);
						
					}
					
				}
				
			}
			
	}
	
	
	public function getAmazonDealUrls(){
		
		
	}
	
	// save deal urls in deal_urls table
	// $deal_url is deal url
	// $store_id is a store id 
	public function save_deal_urls($data){
		
			//echo '<pre>vinay'; print_r($data);
			$model = new Deals();
			$exitDealUrl = $model::find()->where(['link' => $data['link'],'store_id' => $data['store_id'],'status' => 'active'])->one();
			
			if(empty($exitDealUrl)){
					$dealsModel = new Deals();
					$dealsModel->store_id = $data['store_id'];
					$dealsModel->title = $data['title'];
					$dealsModel->description = $data['description'];
					$dealsModel->brand = $data['brand'];
					$dealsModel->link = $data['link'];
					$dealsModel->image_path = $data['image'];
					$dealsModel->price = $data['price'];
					$dealsModel->offer_price = $data['offer_price'];
					$dealsModel->availability = $data['availability'];
					$dealsModel->sizes = $data['sizes'];
					$dealsModel->discount = $data['discount'];
					$dealsModel->start_date = !empty($data['start_date']) ? $data['start_date'] : date('Y-m-d: H:i:s');
					$dealsModel->end_date = !empty($data['end_date']) ? $data['end_date'] :  date('Y-m-d: H:i:s', strtotime(' +1 day'));
					$dealsModel->status = 'active';
					$dealsModel->created = date('Y-m-d h:i:s');
					
					/**$image_name = time().uniqid().'.jpg';
					if(!empty($data['image'])){
						copy($data['image'], 'uploads/deals/'.$image_name);
						$dealsModel->image = $image_name;
					} **/
					
					
					$path = Yii::getAlias('@frontend') .'/web/';
					$imageName = time().uniqid().'.jpg';
					
					if(isset($data['image']) && !empty($data['image'])){
						//if(file_exists($data['image'])){
							copy($data['image'], $path.'uploads/deals/'.$imageName);
							$dealsModel->image = $imageName;
						//}
						
						
					}
					
					
					$dealsModel->save(false);
					
					//echo '<pre>vinay'; print_r($dealsModel); die;
			}
			
			
	}
	
	public static function getPickOfTheDayDeal(){
		
		$subQuery = Suppliers::getActiveSupplierQueryObject()->select('id');
		
		$sql = 'SELECT d.* FROM `'. self::tableName().'` as d WHERE
				NOW() between d.start_date
				AND d.end_date AND d.`store_id` IN ('. $subQuery->createCommand()->rawSql.') 
				LIMIT 10';
		$query = self::findBySql($sql);
		return $query->all();
	}
	
	public static function getDealsBySupplierQuery($supplierId, $offset, $limit){
	
		$sql = 'SELECT d.* FROM `'. self::tableName().'` as d WHERE
				NOW() between d.start_date
				AND d.end_date AND d.store_id = :storeId LIMIT '. (int) $limit. ' OFFSET '. (int) $offset;
		$query = self::findBySql($sql, [':storeId' => $supplierId]);
		return $query;
	}
	
	public static function getFieldsArray(){
		return [
			'id',
			'store_id',
			'title',
			'description',
			'brand',
			'link',
			'image',
			'image_path',
			'price',
			'offer_price',
			'discount',
			'start_date',
			'end_date',
			'availability',
			'sizes',
			'status',
			'created',
		];
	}
	
}
