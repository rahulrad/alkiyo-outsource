<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "feature_groups".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property integer $sort_order
 * @property string $created
 * @property string $modified
 * @property string $uploadFile
 */
class FeatureGroups extends \yii\db\ActiveRecord {

    public $uploadFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'feature_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['category_id', 'sort_order'], 'required'],
            [['category_id', 'sort_order'], 'integer'],
            [['created', 'modified','is_delete'], 'safe'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['sort_order']);
        unset($fields['created']);
        unset($fields['is_delete']);
        unset($fields['modified']);
        
        return $fields;
       
    }
    
    public static function getFeatureAttributeByCategory($categoryId){
    	return self::find()->where(['category_id' => $categoryId, 'is_delete' => 0])
    						->orderBy(['sort_order'=>SORT_ASC])->all();
    }

    public static function getFeatureGroupandItemsByCategory($categoryId, $productid){

    $sql="select fg.name, f.id,f.show_feature, f.display_name, group_concat(fv.display_name) as featurevalue from features f 
        inner join feature_groups fg on fg.id=f.feature_group_id left join feature_values fv
         on fv.feature_id=f.id left join product_feature pf on pf.id_feature=f.id and fv.id=pf.id_feature_value where 
         f.category_id=:id_category and f.is_delete=0 and fg.is_delete=0 and fv.is_delete=0 and pf.id_product=:id_product 
         group by display_name  order by fg.sort_order asc";
       
        $result  = Yii::$app->getDb()->createCommand($sql,[':id_category' => $categoryId,'id_product'=>$productid])->queryAll();
        return $result;

    }

    /**'
     * @return yii\db\ActiveQuery
     */
    public function getCategoriesCategory() {
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    public function getFeatures() {
        return $this->hasMany(Features::className(), ['feature_group_id' => 'id'])
        	->Where(['is_delete' => 0, 'category_id' => $this->category_id]);
    }

}
