<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_image".
 *
 * @property integer $id_image
 * @property integer $id_product
 * @property integer $default
 * @property string $name
 * @property string $date_add
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'default'], 'required'],
            [['id_product', 'default'], 'integer'],
            [['date_add'], 'safe'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_image' => Yii::t('app', 'Id Image'),
            'id_product' => Yii::t('app', 'Id Product'),
            'default' => Yii::t('app', 'Default'),
            'name' => Yii::t('app', 'Name'),
            'date_add' => Yii::t('app', 'Date Add'),
        ];
    }
}
