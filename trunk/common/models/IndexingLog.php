<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "indexing_log".
 *
 * @property string $log_id
 * @property string $index_name
 * @property string $on_date
 * @property string $max_id
 * @property string $memory
 * @property string $requests
 * @property integer $success
 * @property string $type
 * @property string $documents
 * @property string $took_time
 * @property double $total_time
 * @property string $error_message
 */
class IndexingLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indexing_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index_name', 'on_date', 'max_id', 'memory', 'requests', 'type', 'documents', 'took_time', 'total_time'], 'required'],
            [['on_date'], 'safe'],
            [['max_id', 'memory', 'requests', 'success', 'documents', 'took_time'], 'integer'],
            [['type'], 'string'],
            [['total_time'], 'number'],
            [['index_name'], 'string', 'max' => 255],
            [['error_message'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'index_name' => 'Index Name',
            'on_date' => 'On Date',
            'max_id' => 'Max ID',
            'memory' => 'Memory',
            'requests' => 'Requests',
            'success' => 'Success',
            'type' => 'Type',
            'documents' => 'Documents',
            'took_time' => 'Took Time',
            'total_time' => 'Total Time',
            'error_message' => 'Error Message',
        ];
    }
}
