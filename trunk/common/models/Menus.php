<?php
namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use backend\models\MenuTypes;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property integer $category_id
 * @property integer $parent_id
 * @property integer $type
 * @property string $others
 * @property integer $status
 * @property string $created
 * @property string $short_description
 */
class Menus extends \yii\db\ActiveRecord {

    public $file;
	public $api_icon_file;
	public $banner_image_file;

    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;

    public function behaviors() {
        return [

            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            // 'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'menus';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['category_id', 'brand_id', 'parent_id', 'type', 'sort_order','field_index','show_on_mega_menu','is_landing_page','is_popular','price_min','price_max'], 'integer'],
            [['title'], 'required'],
            [['others'], 'string'],
            [['created', 'short_description','description','description', 'meta_title', 'meta_keyword', 'meta_desc', 'menu_type', 'status','show_on_app','is_delete','banner_image','is_landing_page','products','is_popular'], 'safe'],
            [['file'], 'file'],
			[['api_icon_file'], 'file'],
            [['title', 'slug','api_icon','image_name'], 'string', 'max' => 255],
            [['banner_image_file'], 'file', 'extensions' => 'jpeg, jpg, png', 'maxSize' => 1024 * 1024 * 5],
            ['price_max', 'compare', 'compareAttribute' => 'price_min', 'operator' => '>=', 'type' => 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'category_id' => 'Category Name',
            'brand_id' => 'Brand Name',
            'parent_id' => 'Parent Name',
            'type' => 'Type',
            'others' => 'Others',
            'status' => 'Status',
            'created' => 'Created',
            'file' => 'Image',
			'image_name'=> 'Image Name',
			'banner_image_file'=> 'Banner Image',
            'products' => 'Products',
            'price_min' => 'Price Min',
            'price_max' => 'Price Max'
        ];
    }

    public function getParent() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getParentName() {
        $model = $this->parent;

        return $model ? $model->title : '';
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getCategoriesCategory() {
        return $this::hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getBrandsBrand() {
        return $this::hasOne(Brands::className(), ['brand_id' => 'brand_id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getMenuTypeTitle() {
        return $this::hasOne(MenuTypes::className(), ['id' => 'type']);
    }

    public function getSortorder($parent_id, $menu_type) {

        $last_sort_order = Menus::find()->select(['sort_order'])->where(['parent_id' => $parent_id, 'menu_type' => $menu_type])->orderBy('sort_order desc')->one();

        //echo '<pre>'; print_r($last_sort_order); 
        if (!empty($last_sort_order)) {
            $next_sort_order = $last_sort_order->sort_order + 1;
        } else {
            $next_sort_order = 1;
        }

        return $next_sort_order;
    }

    /*
     *  Gets sub menu of a menu
     */

    public function getChildMenus($mType, $parent_menu_id, $for = 'frontend') {
        $query = self::find()->where(' menu_type = :mType AND parent_id= :pId AND is_delete = :isDelete', [":mType" => $mType, ":pId" => $parent_menu_id, ':isDelete' => 0] );
        
        if($for === 'mobile')
        	$query->andWhere('show_on_app = :app', [':app' => 1]);
			$query->andWhere('show_on_mega_menu = :show_on_mega_menu', [':show_on_mega_menu' => 1]);
        return $query->orderBy('sort_order')->all();
    }
    
    public function getMenuCategoryTypeList($menuSlug, $categoryId, $menuType){
        $menu = Menus::find()->where(['slug' => $menuSlug])->one();
        $menuList = []; 
        foreach ($menu as $men){
            $menuList = Menus::find()->where(['parent_id'=> $menu->id,'category_id'=>$categoryId,'type'=>$menuType,'status'=>'active'])->all();
        }
        return $menuList;
    }
    
    public static function getTopMenu($for = 'frontend'){
    	$subQuery = \backend\models\MenuTypes::find()->select('id')->where(' title = :Title', [':Title' => 'Top Menu']);
    	$query = self::find()->where([' in', 'type', $subQuery])
    	->andWhere('parent_id = :Parent', [':Parent' => 0])
    	->andWhere(['is_delete' => 0]);
    	
    	if(strtolower($for) === 'mobile')
    		$query->andWhere('show_on_app = :app', [':app' => 1]);
    	
    	$query->orderBy('sort_order');
    	
    	return $query->all();
    }
    
    public static function getTopeMenuArrayForAPI($for = 'frontend'){
    	$menus = self::getTopMenu($for);
    	$parent = $child = $grandChild = [];
    	$i = $j = $k = 0;
    	
    	foreach ($menus as $menuItem){
    		$child = [];
    		$secondLevel = $menuItem->getChildMenus( 'sub_menu', $menuItem->id, $for );
    		foreach($secondLevel as $second){
    			// help url: http://stackoverflow.com/questions/31125334/yii2-activerecord-to-array
    			$secondArray = \yii\helpers\ArrayHelper::toArray($second,[
    				'common\models\Menus' => self::getFieldsArray(),
    			]);
    			
    			$secondArray = self::convertNullFieldsToEmptyString($secondArray);
    			$child[$j] = (array) $secondArray;
    			if(count($second) > 0){
    				$grandChild = []; $k = 0;
    				$thirdLevel = $second->getChildMenus('custom_menu', $second->id, $for);
    				if(count($thirdLevel)){
    					foreach($thirdLevel as $third){
    						$grandChild = \yii\helpers\ArrayHelper::toArray($third,[
    								'common\models\Menus' => self::getFieldsArray(),
    						]);
    						$grandChild = self::convertNullFieldsToEmptyString($grandChild);
    						$child[$j]['children'][] = (array) $grandChild;
    						$k++;
    					}
    				}else{
    					$child[$j]['children'] = [];
    				}
    				
    			}else{
    				$child[$j]['children'] = [];
    			}
    			$j++;
    		}
    		
    		$parent[$i] = \yii\helpers\ArrayHelper::toArray($menuItem,[
    			'common\models\Menus' => self::getFieldsArray(),
    		]);
    		$parent[$i] = self::convertNullFieldsToEmptyString($parent[$i]);
    		$parent[$i]['children'] = $child;
    		$i++; $j=0;
    	}
    	return Products::createFormatedArrayForAPI( $parent );
    }
    
    public static function getFieldsArray(){
    	return [
	    	'id',
	    	'title',
	    	'category_id',
	    	'slug',
	    	'brand_id',
	    	'parent_id',
	    	'type',
	    	'image',
    		'api_icon',
	    	'others',
	    	'description',
	    	'meta_title',
	    	'meta_keyword',
	    	'meta_desc',
	    	'menu_type',
	    	'sort_order',
    		'image_name',
    		'show_on_app',
	    	'status',
	    	'created',
    	];
    }
    
    public static function getOneLevelMenuItemsByType($menuTypeString){
    	$subQuery = \backend\models\MenuTypes::find()->select('id')
    	->where(' title = :Title', [':Title' => $menuTypeString])
    	->andWhere(['is_delete' => 0])
    	->andWhere(['status' => 'active']);
    	$query = self::find()->where([' in', 'type', $subQuery])
    	->andWhere('parent_id = :Parent', [':Parent' => 0]);
    	
    	$parent = $query->one();
    	$data = [];
    	if(@count($parent)>0){
    		$data = self::findAll(['parent_id' => $parent->id,'is_delete' => 0, 'status' => 'active']);
    	}
    	
    	return $data;
    }
    
    public static function convertNullFieldsToEmptyString(array $array){
    	$config = \HTMLPurifier_Config::createDefault();
    	$config->set('HTML.AllowedElements', []);
    	$domain = \yii::$app->params['parentDomain'];
    	$purifyHtml = new \HTMLPurifier($config);
    	$slug = str_replace('-','_',$array['slug']);
    	$imageName =  $slug. '.png';
    	$apiImageName = 'api_'.$slug. '.png';
    	$mergingarray = [];
    	$mergingarray['images']['ios'] = [
    		'2x' => $domain.\yii::$app->params['2xIconsPath'].$imageName,
    		'3x' => $domain.\yii::$app->params['3xIconsPath'].$imageName,
    	];
    	$mergingarray['images']['android'] = [
    		'hdpi' => $domain.\yii::$app->params['hdpiIconsPath'].$imageName,
    		'mdpi' => $domain.\yii::$app->params['mdpiIconsPath'].$imageName,
    		'xxdpi' => $domain.\yii::$app->params['xxdpiIconsPath'].$imageName,
    	];
    	$mergingarray['api_icon']['ios'] = [
    		'2x' => $domain.\yii::$app->params['2xIconsPath'].$apiImageName,
    		'3x' => $domain.\yii::$app->params['3xIconsPath'].$apiImageName,
    	];
    	$mergingarray['api_icon']['android'] = [
    		'hdpi' => $domain.\yii::$app->params['hdpiIconsPath'].$apiImageName,
    		'mdpi' => $domain.\yii::$app->params['mdpiIconsPath'].$apiImageName,
    		'xxdpi' => $domain.\yii::$app->params['xxdpiIconsPath'].$apiImageName,
    	];
    	$array['others'] = empty($array['others']) ? [] : unserialize($array['others']);
    	$array['image'] = empty($array['image']) ? '' : $array['image'];
    	$array['image_name'] = empty($array['image_name']) ? '' : $array['image_name'];
    	$array['description'] = empty($array['description']) ? '' : $purifyHtml->purify( $array['description'] );
    	$array['show_on_app'] = empty($array['show_on_app']) ? 0 : $array['show_on_app'];
    	unset ($config, $purifyHtml);
    	$array = array_merge($array, $mergingarray);
    	return $array;
    }

    public function getPreviewImage()
    {
        $path = Yii::getAlias('@frontend') . '/web/';
        $uploadPath = $this->banner_image;
        if (is_file($path . $uploadPath)) {
            return Yii::$app->getModule('articles')->frontPageUrl . $uploadPath;
        }
        return null;
    }

	public static function searchData($term){

     $term=trim(strtolower($term));
	 $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
           select  m.title, m.slug from menus m    where m.is_delete=0 and m.parent_id>12 and LOWER(m.title) like :term ;
                ', [':term' => '%'.$term.'%']) ;


        $list = $command->queryAll();
        $listArr = [];
         foreach($list as $menusdata){
            $listArr[]= array(
		'url' =>  \yii\helpers\Url::to(['prettyurl/index','slug'=>$menusdata['slug']]),
                 'icon' 	=>'',
                'label' => ucwords(str_replace('-',' ',$menusdata['slug'])),
                'value' =>  $menusdata['title'],
                'slug' 	=>  $menusdata['slug'],
        		'supplierName' => '',
        		'supplierImage' => '',
        		'lowest_price' => '',
        		'original_price' => '',
                "isMenu"=>true,
                "isProduct"=>false,

            ) ;
        }
        return $listArr;

    }

}
