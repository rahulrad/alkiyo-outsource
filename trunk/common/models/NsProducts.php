<?php

namespace common\models;

use Yii;

class NsProducts extends \yii\elasticsearch\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ns_products';
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return ['id','product_id', 'product_name', 'slug','image'];
    }
    
    public static function index(){
        return self::tableName();
    }
    
    public static function type(){
        return "product";
    }
    
    
}

