<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_feature".
 *
 * @property integer $id_product_feature
 * @property integer $id_product
 * @property integer $id_feature
 * @property integer $id_feature_value
 * @property string $value
 */
class ProductFeature extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_feature';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_product', 'id_feature'], 'required'],
            [['id_product', 'id_feature', 'id_feature_value'], 'integer'],
            [['value'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_product_feature' => 'Id Product Feature',
            'id_product' => 'Id Product',
            'id_feature' => 'Id Feature',
            'id_feature_value' => 'Id Feature Value',
            'value' => 'Value',
        ];
    }

    public function getProductFeatures($product_id) {


        $productFeaturesWithValues = array();

        $productFeatureModel = new ProductFeature();

        $productFeatures = $productFeatureModel::find()->where(['id_product' => (int) $product_id])->groupBy(['id_feature'])->all();

        if (!empty($productFeatures)) {



            $i = 1;
            foreach ($productFeatures as $productFeature) {

                $FeaturesModel = new Features();
                $FeatureValuesModel = new FeatureValues();

                //$productFeaturesWithValues[$i]['Product']['features'] = $FeaturesModel::find()->where(['id' => (int)$productFeature->id_feature])->one();
                //$productFeaturesWithValues[$i]['Product']['feature_values'] = $FeaturesModel::find()->where(['id' => (int)$productFeature->id_feature_value])->one();
                // get feature to product id_feature
                $features = $FeaturesModel::find()->where(['id' => (int)$productFeature->id_feature])->one();


                if (!is_null($features)) {


                    $features = array('id' => $features->id, 'name' => $features->name);
                    $productFeaturesWithValues[$i]['features'] = $features;

                    // get product feature values
                    $featurevalues = $FeaturesModel->findFeatureValuesToFeatureId($features['id']);
                    $productFeaturesWithValues[$i]['features']['feature_value'] = $featurevalues;

                    // get feature value to product id_feature_value
                    //$featureValues = $FeatureValuesModel::find()->where(['id' => (int)$productFeature->id_feature_value])->one();
                    //$feature_values = array('id' => $featureValues->id,'value'=>$featureValues->value);
                    //$productFeaturesWithValues[$i]['Product']['feature_values']= $feature_values;


                    $i++;
                }
            }

            return $productFeaturesWithValues;
        }
        return $productFeaturesWithValues;
    }

    public function getSelectedProductFeatures($product_id) {

        $ProductFeaturesModel = new ProductFeature();

        $SelectedFeatureValues = $ProductFeaturesModel::find()->where(['id_product' => (int) $product_id])->all();

        $featureValueArr = array();

        foreach ($SelectedFeatureValues as $feature_val) {
            $featureValueArr[] = $feature_val->id_feature_value;
        }

        return $featureValueArr;
    }

    public function updateProductFeatures($featureData) {
        $ProductFeaturesModel = new ProductFeature();

        // delete data from product_features table to product_id
        $ProductFeaturesModel::deleteAll('id_product = ' . $featureData['product_id']);


        foreach ($featureData['ProductFeature'] as $data) {
            foreach ($data['feature_value_id'] as $feature_val) {

                //echo '<pre>'; print_r($feature_val); 
                $model = new ProductFeature();
                $model->id_product = $featureData['product_id'];
                $model->id_feature = $data['feature_id'];
                $model->id_feature_value = $feature_val;

                $model->save();
                //echo '<pre>'; print_r($ProductFeaturesModel); die;
            }
        } // die;
    }

    public function getProducts() {
        return $this->hasMany(Product::className(), ['id_product' => 'product_id']);
    }

    public function getProduct() {
        return $this->hasOne(Products::className(), ['product_id' => 'id_product']);
    }

    public function getFeature() {
        return $this->hasOne(Features::className(), ['id' => 'id_feature']);
    }

    public function getFeatureValue() {
        return $this->hasOne(FeatureValues::className(), ['id' => 'id_feature_value'])->where(['is_delete' => 0]);
    }

    public static function getProductFeatureValues($product_id) {
        $results = ProductFeature::find()
                ->where(['id_product' => (int) $product_id])
                ->all();
        $features = [];
        foreach ($results as $result) {
            $featureValues = [];
            foreach($result->featureValues as $featureValue) {
                $featureValues[] = $featureValue->value;
            }
	if( array_key_exists($result->id_feature,$features)){
            $features[$result->id_feature] = $features[$result->id_feature].", ".implode(', ', $featureValues);
	}else{
		 $features[$result->id_feature]=implode(', ', $featureValues);

	}


        }
        return $features;
    }

    public static function getProductFeatureValuesForApi($product_id) {
        $results = ProductFeature::find()
                ->where(['id_product' => (int) $product_id])
                ->all();
        $features = [];
        foreach ($results as $result) {
                    $featureValues = [];
                    foreach($result->featureValues as $featureValue) {
                        $featureValues[] = $featureValue->value;
                    }
                if( array_key_exists($result->id_feature,$features)){
                        $features[$result->id_feature] = array('id'=>$result->id_feature,'value'=>$features[$result->id_feature]['value'].", ".implode(', ', $featureValues));
                }else{
                    $features[$result->id_feature]= array('id'=>$result->id_feature,'value'=>implode(', ', $featureValues));
                        

                }   


        }
        return $features;
    }



    public function getFeatureValues() {
        return $this->hasMany(FeatureValues::className(), ['id' => 'id_feature_value']);
    }
    /**
     * 
     * @param array $productIds
     */
//    public function getFeaturesForFilter(array $productIds){
//        
//        /*
//SELECT `v`.`id` AS `value_id`, `v`.`value`, `f`.`id` AS `feature_id`, `f`.`name`, `g`.`name` AS `group_name`, `g`.`id` AS `group_id` FROM `product_feature` `p` INNER JOIN `feature_values` `v` ON p.id_feature_value = v.id INNER JOIN `features` `f` ON v.feature_id = f.id AND f.is_filter = 1 INNER JOIN `feature_groups` `g` ON f.feature_group_id = g.id WHERE `p`.`id_product` IN (1, 2)
//
//         *          */
//        
//        $query = new \yii\db\Query();
//        //'v.value','feature_name'=>'f.name','group_name'=>'g.name','group_id'=>'g.id'
//        $query->select(['p.id_product', 'value_id'=>'v.id', 'feature_id'=>'f.id'])
//                ->from(['p'=>\common\models\ProductFeature::tableName()])
//                ->innerJoin(['v'=>\common\models\FeatureValues::tableName()] , 'p.id_feature_value = v.id')
//                ->innerJoin(['f'=>  \common\models\Features::tableName()] , 'v.feature_id = f.id AND f.is_filter = 1')
//                ->innerJoin(['g'=> \common\models\FeatureGroups::tableName()] , 'f.feature_group_id = g.id')
//                ->where(['p.id_product'=>$productIds]);
//        
//        $command = $query->createCommand();
//        $records = $command->queryAll();
////        \common\components\Utils::d($command->getRawSql(),1);
////        \common\components\Utils::d($records,1);
//        $return = [];
//        foreach($records as $row){
////            $item = $row;
//            $id = $row['id_product'];
//            unset($row['id_product']);
////            $row['value_analyzed'] = $row['value'];
//            
//            // product_id, value_id
//            $return[$id][] = $row;
//        }
//        
////        \common\components\Utils::d($return,1);
//        return $return;
//        
//        
//    }
    public function getFeaturesFilter(array $productIds){

        $query = new \yii\db\Query();
        // 'group_name'=>'fg2.title','name'=>'fg1.title',
//        $query->select(['p.id_product', 'group_id'=>'fg1.group_id', 'item_id'=>'fg1.id'])
//                ->from(['p'=>\common\models\ProductFeature::tableName()])
//                ->innerJoin(['v'=>\common\models\FeatureValues::tableName()] , 'p.id_feature_value = v.id')
//                ->innerJoin(['fg'=> \backend\models\FilterGroupValues::tableName()],'fg.value_id = v.id')
//                ->innerJoin(['fg1'=> \backend\models\FilterGroupItems::tableName()],'fg1.id = fg.group_item_id')
////                ->innerJoin(['fg2'=> \backend\models\FilterGroups::tableName()],'fg2.id = fg1.group_id')
//                ->where(['p.id_product'=>$productIds]);
        
        $query->select(['product_id'=>'pf.id_product', 'group_id'=>'fg.id', 'item_id'=>'group_item_id'])
                ->from(['pf'=>\common\models\ProductFeature::tableName()])
                ->innerJoin(['fv'=> \common\models\FeatureValues::tableName()] , 'fv.id = pf.id_feature_value AND fv.is_delete=0')
                ->innerJoin(['fgv'=> \backend\models\FilterGroupValues::tableName()],'fgv.value_id=fv.id')
                ->innerJoin(['fgi'=> \backend\models\FilterGroupItems::tableName()],'fgi.id=fgv.group_item_id AND fgi.is_delete=0')
                ->innerJoin(['fg'=> \backend\models\FilterGroups::tableName()],'fg.id=fgi.group_id')
                ->where(['pf.id_product'=>$productIds])
                ->groupBy('fgi.id');

                

        // $qql="select fg.id, fv.id, pf.id_product, pf.id_feature, fv.display_name, fv.value, group_item_id from 
        // product_feature pf inner join feature_values fv on pf.id_feature_value=fv.id inner join
        //  filter_group_values fgv on fgv.value_id=fv.id inner join 
        //  filter_group_items fgi on fgi.id=fgv.group_item_id  inner join
        //   filter_groups fg on fg.id=fgi.group_id  where id_product=".productIds." group by fgi.id;";
        
        $command = $query->createCommand();

        $records = $command->queryAll();

      
//        \common\components\Utils::d($command->getRawSql(),1);
//        \common\components\Utils::d($records,1);
        $return = [];
        $group = [];
        foreach($records as $row){
//            $item = $row;
            $id = $row['product_id'];
            
            if(!isset($group[$row['group_id']])):
                $group[$row['group_id']] = [
                    'group_id'=> (int) $row['group_id'],
//                    'group_name'=> $row['group_name'],
                    'items' => [],
                ];
            endif;
            
            $group[$row['group_id']]['items'][] = [
                'item_id'=> (int) $row['item_id'],
//                'item_name'=> $row['name'],
             ];
                    
            unset($row['product_id']);
//            $row['value_analyzed'] = $row['value'];
            
            // product_id, value_id
            // reset indexes
            $return = $group;
//            $return = array_values($group);
        }
        
        
        
//        \common\components\Utils::d($return,1);
        return $return;
        
        
    }

}
