<?php

namespace common\models;

use Yii;
use common\components\CImage;

/**
 * This is the model class for table "product_image".
 *
 * @property integer $id
 * @property integer $id_product
 * @property string $image
 * @property string $created
 */
class ProductImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product'], 'required'],
            [['id_product'], 'integer'],
            [['created','scraper_image_status'], 'safe'],
            [['image','image_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'image' => 'Image',
            'created' => 'Created',
        ];
    }
    
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'id_product']);
    }
	
	public function saveProductMultiImages($other_images,$product_id){
		
		foreach($other_images as $other_img){
			
				$ProductImageModel = new ProductImage();
				
				/**$imageName = $other_img->baseName;
				
				$path = Yii::getAlias('@frontend') .'/web/';
				
				$other_img->saveAs($path.'uploads/products/'.$imageName.'.'.$other_img->extension);
				
				
				$ProductImageModel->image = $imageName.'.'.$other_img->extension;
                 **/
				$productDetail = Products::findOne(['product_id'=>  $product_id]);
				$path = Yii::getAlias('@frontend') .'/web/';

				$categoryDetail = Categories::findOne(['category_id'=>  $productDetail->categories_category_id]);
				$brandDetail = Brands::findOne(['brand_id'=>  $productDetail->brands_brand_id]);
				/**
				$folder_path = $path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug;
				if (!file_exists($folder_path)) {
					mkdir($folder_path, 0755, true);
				}
				
				$imageName = time().$other_img->name;
				$other_img->saveAs($folder_path.'/'.$imageName);
				
				$ProductImageModel->image = $imageName;
				
				
				$thumImageSizes = array('150x200','300x400');
				foreach($thumImageSizes as $size){
					$sizeArr = explode('x',$size);
					
						if (!file_exists($folder_path.'/'.$size)) {
							mkdir($folder_path.'/'.$size, 0755, true);
						}
					$destFile = $folder_path.'/'.$size.'/'.$imageName;
					$mainImage = $folder_path.'/'.$imageName;
					$objImageResize = new \common\components\CImage();
					$response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
				}
				
				**/
                $ProductImageModel->image = Products::saveProductImage($categoryDetail, $brandDetail, $other_img, "main", true);
				$ProductImageModel->id_product = $product_id;
				
				$ProductImageModel->save();
				
		}
	}
	
	public static function getFieldsArray(){
		return [
			'id',
			'id_product',
			'image',
			'image_path',
			'created',
		];
	}

    public function getPreviewImage($category, $brand)
    {
        $path = Yii::getAlias('@frontend') . '/web/';
        $imgFolder = explode(".", $this->image);
        $uploadPath = 'uploads/products/' . $category->folder_slug . '/' . $brand->folder_slug . "/" . $imgFolder[0] . "/" . $this->image;
        if (is_file($path . $uploadPath)) {
            return Yii::$app->getModule('articles')->frontPageUrl . $uploadPath;
        }
        return $this->image_path;
    }

}
