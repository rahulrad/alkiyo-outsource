<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_reviews".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $email
 * @property integer $product_id
 * @property string $review
 * @property string $created
 * @property string $updated
 */
class ProductReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['description'], 'string'],
            [['created'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['title'], 'required'],
        	[['rating'], 'integer'],
        	[['ip'], 'string'],
        	[['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'product_id' => 'Product ID',
            'description' => 'Description',
			'status' => 'Status',
            'created' => 'Created',
        	'rating' => 'Rating',
        ];
    }
    
    public function getReviews($userId, $userIP, $productId){
        $ProductReviews = self::find()->where(['user_id' => (int) $userId, 'ip' =>  $userIP, 'product_id' =>  $productId ])->all();
        
        return $ProductReviews;
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getReviewsRank() {
        return $this->hasMany(ProductReviewsRank::className(), ['review_id' => 'id'])->where(['rank' => 1])->all();
    }
    
    public static function convertProductReviewObjectIntoArrayForAPI(\common\models\ProductReviews $reviewObj){
    	$review = \yii\helpers\ArrayHelper::toArray($reviewObj,[
    		'common\models\ProductReviews' => self::getFieldsArray(),
    	]);
    	
    	return self::cleanArrayForAPI($review);
    }
    
    public static function getFieldsArray(){
    	return [
   			'id',
   			'title',
   			'product_id',
   			'description',
   			'status',
   			'created',
   			'rating',
    	];
    }
    
    public static function cleanArrayForAPI(array $array){
    	return $array;
    }

}
