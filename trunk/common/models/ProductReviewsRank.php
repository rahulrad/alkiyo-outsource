<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_reviews".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $email
 * @property integer $product_id
 * @property string $review
 * @property string $created
 * @property string $updated
 */
class ProductReviewsRank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_reviews_rank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['user_id'], 'integer'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'rank' => 'Rank',
            'created' => 'Created',
        ];
    }

}
