<?php
namespace common\models;

use backend\models\MongoFeatureGroups;
use backend\models\MongoFeatures;
use backend\models\MongoFeatureValues;
use backend\models\MongoProductFeatures;
use backend\models\MongoProducts;
use common\models\Brands;
use common\models\Categories;
use Yii;
use yii\behaviors\SluggableBehavior;
use common\components\CImage;
use backend\models\Logs;
use yii\db\Query;
use yii\helpers\Url;
use frontend\helpers\NoImage;
use yii\web\Linkable;

/**
 * This is the model class for table "products".
 *
 * @property integer $product_id
 * @property integer $categories_category_id
 * @property integer $brands_brand_id
 * @property string $product_name
 * @property string $product_description
 * @property string $product_status
 * @property string $created
 */
class Products extends \yii\db\ActiveRecord implements  Linkable
{

    public $file;
    public $main_image;
    public $other_images;
    public $product_tag;

    public $ns_category_id;
    public $ns_brand_id;
    public $ns_created;
    public $brandname;
    public $categoryname;

    public $showUnMapped;

    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;
    const PRODUCT_VIEW = 'viewedByUser';

    const IMAGE_PRESET_VERTICAL = "vertical";
    const IMAGE_PRESET_HORIZONTAL = "horizontal";

    public function init()
    {

        $this->on(self::PRODUCT_VIEW, [$this, 'saveProductViewedToDatabase']);

        // first parameter is the name of the event and second is the handler.
        // For handlers I use methods sendMail and notification
        // from $this class.
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'product_name',
                // 'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    public function fields()
    {
        $fields = parent::fields();
        $extraFields= [
            'brandname' => function ($model) {
                return $model->brand->folder_slug;
            },
            'categoryname' => function ($model) {
                return $model->category->folder_slug;
            },

            'image' => function ($model) {

                return  \frontend\helpers\NoImage::getProductImageFullPathFromObject($model);
            },

            'productimage' => function ($model) {
                
                                return  \frontend\helpers\NoImage::getProductImageFullPathFromObject($model,'','product');
                            },
           
                            'imageList' =>function($model){
                                $array=[];
                                $brandSlug = $model->brand->folder_slug;
                                $categorySlug = $model->category->folder_slug;
                                $imagePreset = $model->category->image_scrap_preset;
                                foreach( $model->productImages  as $imagesListObj){
                                    $array[]=\Yii::$app->params['frontend'].NoImage::getProductImageFullPathFromBrandCategorySlug($brandSlug,$categorySlug, $imagesListObj, 'full',$imagePreset,'product');
                                }
                                return $array;
                            }
       
        ];
        unset($fields['categories_category_id']);
        unset($fields['brands_brand_id']);
        unset($fields['image_path']);
        unset($fields['scraper_image_status']);
        unset($fields['product_status']);
        unset($fields['meta_title']);
        unset($fields['meta_keyword']);
        unset($fields['meta_desc']);
        unset($fields['show_home_page']);
        unset($fields['is_delete']);
        unset($fields['is_approved']);
        unset($fields['image_scrapped']);
        unset($fields['feature_scrapped']);
        return array_merge( $fields,$extraFields);
    }
    
    public function extraFields()
    {
        // $fields = parent::fields();
        // $fields[]='suppliersProducts';
       
        // return $fields;
      return ['suppliersProducts'];
    }


    public function getLinks()
    {
     return [];  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['categories_category_id', 'brands_brand_id'], 'required'],
            [['categories_category_id', 'brands_brand_id', 'is_approved', 'instock', 'image_scrapped', 'feature_scrapped'], 'integer'],
            [['product_description', 'active', 'product_status', 'meta_title', 'meta_keyword', 'meta_desc', 'image', 'model_number', 'cod', 'return_policy', 'delivery', 'price', 'unique_id', 'url'], 'string'],
            [['created', 'show_home_page', 'store_name', 'store_id', 'lowest_price', 'is_delete', 'updated', 'image_path', 'scraper_image_status', 'store_product_name','date_launch'], 'safe'],
            [['product_name'], 'string', 'max' => 255],
            [['discount', 'rating', 'emi', 'colors', 'sizes', 'offers','upcoming'], 'safe'],
            //[['file'],'required'],
            [['file'], 'file', 'extensions' => 'csv', 'maxSize' => 1024 * 1024 * 5],
            [['main_image'], 'file'],
            [['other_images'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'maxFiles' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product',
            'categories_category_id' => ' Category',
            'brands_brand_id' => ' Brand',
            'product_name' => ' Name',
            'product_description' => 'Description',
            'product_status' => 'Status',
            'created' => 'Created',
            'show_home_page' => 'Show Home Page',
            'store_id' => 'store id',
            'is_approved' => 'Approved',
            'showUnMapped' => 'Exclude Mapped',
            'image_scrapped' => "Image Scrapped",
	    'upcoming' => 'Upcoming Product',
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getCategoriesCategory()
    {
        return $this::hasOne(Categories::className(), ['category_id' => 'categories_category_id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getBrandsBrand()
    {
        return $this::hasOne(Brands::className(), ['brand_id' => 'brands_brand_id']);
    }

    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['brand_id' => 'brands_brand_id']);
    }

    public function categoryNameToCategoryId($categoryId)
    {

        $categoryModel = new Categories();

        $categoryResult = $categoryModel::find()->select(['category_name'])->where(['category_id' => (int)$categoryId])->one();

        if (!empty($categoryResult)) {

            return $categoryResult->category_name;
        }
        return false;
    }

    public function brandNameToBrandId($brandId)
    {

        $brandModel = new Brands();

        $brandResult = $brandModel::find()->select(['brand_name'])->where(['brand_id' => (int)$brandId])->one();

        if (!empty($brandResult)) {

            return $brandResult->brand_name;
        }
        return false;
    }

    public function storeNameToStoreId($storeId)
    {

        $suppliersModel = new Suppliers();

        $supplierResult = $suppliersModel::find()->select(['name'])->where(['id' => (int)$storeId])->one();

        if (!empty($supplierResult)) {

            return $supplierResult->name;
        }
        return false;
    }

    public function Check_similler_products($productData)
    {
        if (!empty($productData)) {

            $brand_model = new Brands();
            $brandDetail = $brand_model::find()->where(['brand_name' => $productData['productData']['brand_name']])->one();
            $model = new Products();

            $products = $model::find()->joinWith('brand')
                ->select(['product_id', 'product_name', 'lowest_price', 'url', 'brands_brand_id'])
                ->andWhere([
                        'brands_brand_id' => $brandDetail->brand_id,
                        'categories_category_id' => $productData['productData']['category_id'],
                        'is_approved' => 1,
                        'is_delete' => 0,
                    ]
                )
                // ->andWhere(['LIKE','product_name', $productData['productData']['product_name']])
                ->all();

            return $products;
        }
    }


    public function Check_similler_products_for_nayashoppy($productData)
    {
        if (!empty($productData)) {
            $model = new Products();

            $searchData = array();
            if (!empty($productData['Products']['ns_brand_id'])) {
                $searchData['brands_brand_id'] = $productData['Products']['ns_brand_id'];
            }

            if (!empty($productData['Products']['ns_category_id'])) {
                $searchData['categories_category_id'] = $productData['Products']['ns_category_id'];
            }

            //if (!empty($productData['Products']['ns_created'])) {
              //  $searchData['products.created'] = $productData['Products']['ns_created'];
           // }



  
            $searchData['is_approved'] = 1;
            $searchData['products.is_delete'] = 0;
            $products = $model::find()->joinWith('brand')
                ->select(['product_id', 'product_name', 'url','lowest_price', 'url', 'brands_brand_id', 'categories_category_id', 'products.slug'])
                ->where($searchData);
              
              if (!empty($productData['Products']['ns_created'])) {
                $products->andWhere(['LIKE','products.created', $productData['Products']['ns_created']]);
              }
              
            
 
              
         return   $products ->all();


//            $searchData['is_approved'] = 1;
  //          $searchData['products.is_delete'] = 0;
    //        $products = $model::find()->joinWith('brand')
      //          ->select(['product_id', 'product_name', 'url','lowest_price', 'url', 'brands_brand_id', 'categories_category_id', 'products.slug'])
        //        ->where($searchData)
                // ->andWhere(['LIKE','product_name', $productData['productData']['product_name']])
          //      ->all();


            //return $products;
        }
    }

    public function productFindByName($name)
    {

        if (!empty($name)) {
            $product = Products::find()->where(['product_name' => $name, id_delete => 0, is_approved => 1])->one();
            return $product;
        }
    }

    public function changeProductFlagAccordingPrice($productData, $saveProductSuppilerData)
    {
        //echo '<pre>'; print_r($saveProductSuppilerData); die;
        if (!empty($productData) && !empty($saveProductSuppilerData)) {

            $product_price = $productData->lowest_price;

            $suppiler_product_price = $saveProductSuppilerData->price;
            //echo $suppiler_product_price.'====>'.$product_price; die;
            if ($suppiler_product_price < $product_price) {

                if (!empty($saveProductSuppilerData)) {
                    $model = new Products();
                    $model->productSave($saveProductSuppilerData);
                }
            }
        }
    }

    public function changeProductSupplierAccoringToLowestPrice($productData, $saveProductSuppilerData)
    {
        if (!empty($productData) && !empty($saveProductSuppilerData)) {

            $productData->productSave($saveProductSuppilerData);
        }
    }

    public function productSave($saveProductSuppilerData)
    {
        //echo '<pre>dssds'; print_r($saveProductSuppilerData); die;
        $product_model = Products::findOne($saveProductSuppilerData->product_id);

        $product_model->store_id = $saveProductSuppilerData->store_id;

        $product_model->store_name = $saveProductSuppilerData->store_name;

        $product_model->lowest_price = $saveProductSuppilerData->price;

        $product_model->original_price = $saveProductSuppilerData->original_price;

        $product_model->cod = $saveProductSuppilerData->cod;

        $product_model->emi = $saveProductSuppilerData->emi;

        $product_model->colors = $saveProductSuppilerData->colors;

        $product_model->sizes = $saveProductSuppilerData->sizes;

        $product_model->offers = $saveProductSuppilerData->offers;

        $product_model->return_policy = $saveProductSuppilerData->return_policy;

        $product_model->delivery = $saveProductSuppilerData->delivery;

        $product_model->unique_id = $saveProductSuppilerData->unique_id;

        $product_model->discount = $saveProductSuppilerData->discount;

        $product_model->rating = $saveProductSuppilerData->rating;

        $product_model->url = $saveProductSuppilerData->url;

        $product_model->reviews = $saveProductSuppilerData->reviews;

        $product_model->shipping = $saveProductSuppilerData->shipping;

        $product_model->active = 'active';

        $product_model->is_approved = 1;

        $product_model->updated = date('Y-m-d H:i:s');

        $product_model->created = $saveProductSuppilerData->created;

        if ($product_model->save(false)) {

            return $product_model;
        }
    }

    public function saveFilterMongoProducts($mongoProduct)
    {
        //echo '<pre>'; print_r($mongoProduct); die;
        $product_model = Products::find()->where([
            'product_name' => $mongoProduct->product_name,
            'url' => $mongoProduct->url,
            'store_id' => $mongoProduct->store_id
        ])->one();

        if (empty($product_model)) {
            $product_model = new Products();
        }

        $product_model->is_delete = 0;

        $product_model->store_name = $mongoProduct->store_name;

        $product_model->store_id = $mongoProduct->store_id;

        $product_model->product_name = $mongoProduct->product_name;

        $product_model->store_product_name = $mongoProduct->product_name;

        $logsModel = new Logs();
        $product_model->slug = $logsModel->getProductSlugFromName($mongoProduct->product_name);


        $product_model->product_description = $mongoProduct->product_description;

        $product_model->model_number = $mongoProduct->model_number;

        $product_model->lowest_price = $mongoProduct->price;

        $product_model->emi = $mongoProduct->emi;

        $product_model->cod = $mongoProduct->cod;

        $product_model->colors = $mongoProduct->colors;

        $product_model->sizes = $mongoProduct->sizes;

        $product_model->offers = $mongoProduct->offers;

        $product_model->instock = $mongoProduct->instock;

        $product_model->return_policy = $mongoProduct->return_policy;

        $product_model->url = $mongoProduct->url;

        $product_model->unique_id = $mongoProduct->unique_id;

        $product_model->discount = $mongoProduct->discount;

        $product_model->rating = $mongoProduct->rating;

        $product_model->reviews = $mongoProduct->reviews;

        $product_model->image = $mongoProduct->image;

        $product_model->shipping = $mongoProduct->shipping;

        $product_model->active = 'active';

        $product_model->is_approved = 1;

        $product_model->created = $mongoProduct->created;

        $product_model->updated = date('Y-m-d H:i:s');

        $brandDetail = Brands::find()->where(['brand_name' => $mongoProduct->brand_name])->one();

        if (!empty($brandDetail)) {
            $product_model->brands_brand_id = $brandDetail->brand_id;
        }

        $categoryDetail = Categories::find()->where(['category_name' => $mongoProduct->category_name])->one();
        if (!empty($categoryDetail)) {
            $product_model->categories_category_id = $categoryDetail->category_id;
        }


        if (!empty($mongoProduct->image)) {

            $product_model->image = self::saveProductImage($categoryDetail, $brandDetail, $mongoProduct->image);
            $product_model->image_scrapped = 1;
            $product_model->image_path = $mongoProduct->image;
            /**$path = Yii::getAlias('@frontend') .'/web/';
             * copy($mongoProduct->image, $path.'uploads/products/'.$image_name);
             * $product_model->image = $image_name;
             *
             * $thumImageSizes = array('150x200','300x400');
             * foreach($thumImageSizes as $size){
             * $sizeArr = explode('x',$size);
             *
             * $destFile = $path.'uploads/products/'.$size.'/'.$image_name;
             * $objImageResize = new \common\components\CImage();
             * $response = $objImageResize->imageResize($mongoProduct->image,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
             * } **/

            /**$logsModel = new Logs();
             * $category_slug = $logsModel->getSlugFromName($categoryDetail->category_name);
             * $brand_slug =  $logsModel->getSlugFromName($brandDetail->brand_name); **/

            /** $path = Yii::getAlias('@frontend') .'/web/';
             * $folder_path = $path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug;
             * if (!file_exists($folder_path)) {
             * mkdir($folder_path, 0755, true);
             * }
             *
             * copy($mongoProduct->image, $folder_path.'/'.$image_name);
             * $product_model->image = $image_name;
             *
             *
             * $thumImageSizes = array('150x200','300x400');
             * foreach($thumImageSizes as $size){
             * $sizeArr = explode('x',$size);
             *
             * if (!file_exists($folder_path.'/'.$size)) {
             * mkdir($folder_path.'/'.$size, 0755, true);
             * }
             * $destFile = $folder_path.'/'.$size.'/'.$image_name;
             * $mainImage = $folder_path.'/'.$image_name;
             * $objImageResize = new \common\components\CImage();
             * $response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
             * } ***/

        }


        $product_model->save(false);
        $other_images = unserialize($mongoProduct->other_images);
        if (!empty($other_images)) {
            $this->saveMultipleImages($other_images, $product_model->product_id, $categoryDetail, $brandDetail);
        }

        $exitproduct = Products::find()->where(['product_name' => $mongoProduct->product_name])->one();

        // save products in product suppiler table//
        $productsSuppliersModel = new ProductsSuppliers();
        $exitProductSuppiler = $productsSuppliersModel::find()->where(['store_id' => $exitproduct->store_id, 'product_id' => $exitproduct->product_id])->one();
        if (empty($exitProductSuppiler)) {
            $productsSuppliersModel->saveSuppilerDataFromMongoDb($mongoProduct, $exitproduct);
        }

        $this->saveProductFeatureData($product_model, $mongoProduct);

        $MongoProductModel = $mongoProduct;
        $MongoProductModel->status = 0;
        $MongoProductModel->save();

        return $exitproduct;
    }

    public static function saveProductImage($category, $brand, $imageUrl, $type = "main", $isUploaded = false)
    {
        $imageFolderName = time() . uniqid();
        $image_name = $imageFolderName . '.jpg';
        $presets = array(
            self::IMAGE_PRESET_VERTICAL => array(
                "main" => array(
                    "thumb" => "60x125",
                    "product" => "150x300",
                    "zoom" => "450x900"
                ),
                "other" => array(
                    "product" => "150x300",
                    "zoom" => "450x900"
                )
            ),
            self::IMAGE_PRESET_HORIZONTAL => array(
                "main" => array(
                    "thumb" => "180x125",
                    "product" => "250x170",
                    "zoom" => "750x540"
                ),
                "other" => array(
                    "product" => "250x170",
                    "zoom" => "750x540"
                )
            ),
        );

        $imagePreset = $category->image_scrap_preset;
        print_r("\n" . $imagePreset . "\n");
        error_log("\n" . $imagePreset . "\n");
        print_r($type . "\n");
        error_log($type . "\n");
        $path = Yii::getAlias('@frontend') . '/web/';

        $folder_path = $path . 'uploads/products/' . $category->folder_slug . '/' . $brand->folder_slug . "/" . $imageFolderName;
        print_r($folder_path . "\n");
        error_log($folder_path . "\n");
        if (!file_exists($folder_path)) {
            mkdir($folder_path, 0755, true);
        }
        try {
            if ($isUploaded) {
                $imageUrl->saveAs($folder_path . '/' . $image_name);
            } else {
                copy($imageUrl, $folder_path . '/' . $image_name);
            }
            if (isset($presets[$imagePreset])) {
                $thumImageSizes = $presets[$imagePreset][$type];
                foreach ($thumImageSizes as $size) {
                    $sizeArr = explode('x', $size);
                    if (!file_exists($folder_path . '/' . $size)) {
                        mkdir($folder_path . '/' . $size, 0755, true);
                    }
                    $destFile = $folder_path . '/' . $size . '/' . $image_name;
                    print_r($destFile . "\n");
                    error_log($destFile . "\n");
                    $mainImage = $folder_path . '/' . $image_name;
                    $objImageResize = new \common\components\CImage();
                    $response = $objImageResize->imageResize($mainImage, $destFile, $sizeArr[1], $sizeArr[0], 'jpg');
                }
            }
        } catch (\Exception $e) {
            print_r("Error while scrapping image \n");
            print_r($e->getMessage() . "\n");
        }
        print_r($image_name . "\n");
        error_log($image_name . "\n");
        return $image_name;
    }

    private function saveProductFeatureData(Products $product, MongoProducts $mongoProduct)
    {
        $mongoProductFeatures = MongoProductFeatures::find()->where(['id_product' => $mongoProduct->product_id])->all();
        foreach ($mongoProductFeatures as $productFeature) {
            $feature = MongoFeatures::find()->where(['id' => $productFeature->id_feature])->one();
//            $featureGroup = MongoFeatureGroups::find()->where(['id' => $feature->feature_group_id])->one();
            $featureValue = MongoFeatureValues::find()->where(['id' => $productFeature->id_feature_value])->one();
//            $featureGroupDB = $this->saveProductFeatureGroupInDB($product, $featureGroup);
            $featureDB = $this->saveProductFeatureInDB($product, $feature);
            if (!is_null($featureDB)) {
                $featureValueDB = $this->saveProductFeatureValueInDB($featureDB, $featureValue);
                if (!is_null($featureValueDB)) {
                    $this->saveProductFeatureDB($product->product_id, $featureDB->id, $featureValueDB->id);
                }
            }
        }
    }

    private function saveProductFeatureGroupInDB(Products $product, MongoFeatureGroups $featureGroup)
    {
        $feature_group = FeatureGroups::findOne(['name' => $featureGroup->name]);
        if ($feature_group == null) {
            $max_sort_order = FeatureGroups::find()->orderBy('sort_order DESC')->one();
            if (!empty($max_sort_order)) {
                $sort_order = $max_sort_order->sort_order + 1;
            } else {
                $sort_order = 1;
            }
            $feature_group_model = new FeatureGroups();
            $feature_group_model->category_id = $product->categories_category_id;
            $feature_group_model->name = $featureGroup->name;
            $feature_group_model->sort_order = $sort_order;
            $feature_group_model->created = date('Y-m-d h:i:s');
            $feature_group_model->modified = date('Y-m-d h:i:s');
            if ($feature_group_model->save()) {
                return $feature_group_model;
            }
        }
        return $feature_group;
    }

    private function saveProductFeatureInDB(Products $product, MongoFeatures $feature)
    {
        $model = ProductFeatureMappings::find()->where([
            'id_category' => $product->categories_category_id,
            'store_feature_name' => trim($feature->name)
        ])->one();

        if (!is_null($model)) {
            return $model->feature;
        }
        return null;
    }

    private function _saveProductFeatureInDB(Products $product, FeatureGroups $featureGroup, MongoFeatures $feature)
    {
        $feature_data = Features::findOne(['name' => $feature->name, 'category_id' => $product->categories_category_id]);

        if (empty($feature_data)) {
            $max_sort_order = Features::find()->orderBy('sort_order DESC')->one();
            if (!empty($max_sort_order)) {
                $sort_order = $max_sort_order->sort_order + 1;
            } else {
                $sort_order = 1;
            }
            $feature_model = new Features();
            $feature_model->category_id = $product->categories_category_id;
            $feature_model->feature_group_id = $featureGroup->id;
            $feature_model->name = $feature->name;
            $feature_model->display_name = $feature->name;
            $feature_model->sort_order = $sort_order;
            $feature_model->status = 1;
            $feature_model->created = date('Y-m-d h:i:s');
            $feature_model->modified = date('Y-m-d h:i:s');
            if ($feature_model->save()) {
                return $feature_model;
            }
        }
        return $feature_data;
    }

    public function saveProductFeatureValueInDB(Features $featureDB, MongoFeatureValues $featureValue)
    {
        $feature_value_data = FeatureValues::findOne(['value' => $featureValue->value, 'feature_id' => $featureDB->id]);
        if (empty($feature_value_data)) {
            $feature_value_model = new FeatureValues();
            $feature_value_model->feature_id = $featureDB->id;
            $feature_value_model->value = $featureValue->value;
            $feature_value_model->display_name = $featureValue->display_name;
            $feature_value_model->created = date('Y-m-d h:i:s');
            $feature_value_model->modified = $feature_value_model->created;
            if ($feature_value_model->save()) {
                return $feature_value_model;
            } else {
                error_log("--error while saving feature value--");
                error_log(var_export($feature_value_model->errors, true));
                return null;
            }
        }
        return $feature_value_data;
    }

    public function saveProductFeatureDB($productId, $featureId, $featureValueId)
    {
        $feature_value_data = ProductFeature::findOne(['id_product' => $productId, 'id_feature' => $featureId, 'id_feature_value' => $featureValueId]);
        if (is_null($feature_value_data)) {
            $product_feature_model = new ProductFeature();
            $product_feature_model->id_product = $productId;
            $product_feature_model->id_feature = $featureId;
            $product_feature_model->id_feature_value = $featureValueId;
            if ($product_feature_model->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    // by vinay 19 july
    public function saveMultipleImages($otherImages, $product_id, $categoryDetail = null, $brandDetail = null)
    {

        $i = 1;
        foreach ($otherImages as $image) {
            if (!empty($image)) {
                $product_image_model = new ProductImage;

                $image_name = time() . uniqid() . $i . '.jpg';

                $product_image_model->id_product = $product_id;
                $product_image_model->image_path = $image;
                $product_image_model->image = self::saveProductImage($categoryDetail, $brandDetail, $image, "other");

                $product_image_model->created = date('Y-m-d h:i:s');
                $product_image_model->save();

                /**$path = Yii::getAlias('@frontend') .'/web/';
                 *
                 * copy($image, $path.'uploads/products/'.$category_slug.'/'.$brand_slug.'/'.$image_name);
                 *
                 * $thumImageSizes = array('150x200','300x400');
                 * foreach($thumImageSizes as $size){
                 * $sizeArr = explode('x',$size);
                 *
                 * $destFile = $path.'uploads/products/'.$category_slug.'/'.$brand_slug.'/'.$size.'/'.$image_name;
                 * $objImageResize = new \common\components\CImage();
                 * $response = $objImageResize->imageResize($image,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
                 * } **/
            }
            $i++;
        }
    }

    public function unlink_product_from_suppilers($product_id)
    {

        if (!empty($product_id)) {
            $productModel = Products::findOne($product_id);
            $productModel->store_name = '';
            $productModel->store_id = '';
            $productModel->save(false);

            return $productModel;
        }
    }

    public function saveProductData($data)
    {


        if (!empty($data)) {
            foreach ($data as $product) {

                $exit_brand = Brands::find()->where(['brand_name' => $product['product_detail']['brand']])->one();
                if (empty($exit_brand)) {
                    $brad_model = new Brands();
                    $brad_model->brand_name = $product['product_detail']['brand'];
                    $brad_model->brand_description = $product['product_detail']['brand'];
                    $model->created = date('Y-m-d h:i:s');
                    $brad_model->save();
                }

                $exit_category = Categories::find()->where(['category_id' => $product['product_detail']['Category']])->one();
                if (empty($exit_category)) {
                    $category_model = new Categories();
                    $category_model->category_name = $product['product_detail']['Category'];
                    $category_model->category_description = $product['product_detail']['Category'];
                    $category_model->category_status = 'active';
                    $category_model->created = date('Y-m-d h:i:s');
                    $category_model->save();
                }

                $exit_product = Products::find()->where(['unique_id' => $product['merchant_arr']['m_unique_id']])->one();
                if (isset($product['name']) && empty($exit_product)) {
                    $model = new Products();
                    //echo $product['product_detail']['descritpion']; die;

                    $model->categories_category_id = $exit_category->category_id;
                    $model->brands_brand_id = $$exit_brand->brand_id;


                    $model->product_name = trim($product['name']);


                    if (isset($product['imgpath'])) {
                        $model->image = $product['imgpath'];
                    }

                    if (isset($product['merchant_arr']['mprice'])) {
                        $model->price = $product['merchant_arr']['mprice'];
                    }

                    if (isset($product['merchant_arr']['m_unique_id'])) {
                        $model->unique_id = trim($product['merchant_arr']['m_unique_id']);
                    }

                    if (isset($product['merchant_arr']['murl'])) {
                        $model->url = trim($product['merchant_arr']['murl']);
                    }
                    $model->product_status = 'active';

                    if (isset($product['product_detail']['cash_on_delivery'])) {
                        $model->cod = trim($product['product_detail']['cash_on_delivery']);
                    }

                    if (isset($product['product_detail']['return_policy'])) {
                        $model->return_policy = trim($product['product_detail']['return_policy']);
                    }

                    if (isset($product['product_detail']['product_deliverd'])) {
                        $model->delivery = trim($product['product_detail']['product_deliverd']);
                    }

                    if (isset($product['product_detail']['descritpion'])) {
                        $model->product_description = trim($product['product_detail']['descritpion']);
                    }

                    if (isset($product['product_detail']['model_number'])) {
                        $model->model_number = trim($product['product_detail']['model_number']);
                    }
                    //$model->save();
                    $model->created = date('Y-m-d h:i:s');

                    if (!$model->save()) {
                        //echo '<pre>fail'; print_r($product); 
                        echo 'Error to save user model<br />';
                        var_dump($model->getErrors());
                    } else {
                        //echo '<pre>pass'; print_r($product); 
                        $model->save();
                    }
                }
            }
        }
    }

    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['id_product' => 'product_id']);
    }

    public function getSupplier()
    {
        return $this->hasOne(Suppliers::className(), ['id' => 'store_id']);
    }

      /**
     * @return yii\db\ActiveQuery
     */
    public function getSuppliersProducts()
    {
        return $this->hasMany(ProductsSuppliers::className(), ['product_id' => 'product_id'])->andWhere(['product_status' => 'active'])->orderBy(['instock' => SORT_DESC, 'abs(price)' => SORT_ASC]);
    }

    public function getSuppliersProductWithLowestPrice()
    {
        return $this->hasOne(ProductsSuppliers::className(), ['product_id' => 'product_id'])->andWhere(['product_status' => 'active'])->orderBy(['instock' => SORT_DESC, 'abs(price)' => SORT_ASC])->one();
    }

        public function getBaseProductSupplier()
    {
        return $this->hasOne(ProductsSuppliers::className(), ['product_id' => 'product_id'])->andWhere(['product_status' => 'active'])->orderBy(['id' => SORT_ASC])->one();
    }

    public function getSuppliersProductsCount($productId)
    {
        return ProductsSuppliers::find()->where(['product_id' => $productId])->count();
    }

    public function getInstockSuppliersProductsCount()
    {
        return ProductsSuppliers::find()->where(['product_id' => $this->product_id, 'instock' => 1])->count();
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_id' => 'categories_category_id']);
    }

    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['id_product' => 'product_id']);
    }

    public static function compareProducts(array $productSlugs)
    {
        $comparasionProduct = Products::find()->where(['in', 'slug', $productSlugs, 'product_status' => 'active'])->all();

        if (empty($comparasionProduct)) {
            return [];
        }
        //var_dump($productSlugs, $comparasionProduct[0]->categories_category_id);die;
        $featureGroups = FeatureGroups::getFeatureAttributeByCategory($comparasionProduct[0]->categories_category_id);
        //     	var_dump($featureGroups);die;
        $productFeatures = [];
        foreach ($comparasionProduct as $currProd) {
            $productFeatures[] = ProductFeature::getProductFeatureValues($currProd->product_id);
        }

        return ['comparasionProducts' => $comparasionProduct, 'productFeatureGroup' => $featureGroups, 'productsFeatures' => $productFeatures];
    }

    public static function getProductsInTwoPriceRanges($lowestRange, $highestRange, $categoryId)
    {
        $query1 = (new \yii\db\Query())
            ->select('*')
            ->from(['range1' => self::tableName()])
            ->where(['>', 'lowest_price', $lowestRange])
            ->andWhere(['categories_category_id' => $categoryId])
            ->andWhere(['is_delete' => 0])
            ->orderBy(['lowest_price' => SORT_ASC])
            ->limit(5);

        $query2 = (new \yii\db\Query())
            ->select('*')
            ->from(['range2' => self::tableName()])
            ->where(['<', 'lowest_price', (int)$highestRange])
            ->andWhere(['categories_category_id' => $categoryId])
            ->andWhere(['is_delete' => 0])
            ->orderBy(['lowest_price' => SORT_DESC])
            ->limit(5);

        return self::findBySql($query1->union($query2)->createCommand(self::getDb())->getRawSql())->all();
    }

    public function getProductReviews()
    {
        return $this->hasMany(ProductReviews::className(), ['product_id' => 'product_id'])->andWhere(['status' => 'active'])->orderBy(['rating' => SORT_DESC]);
    }

    public static function getSimilarProductsForAPI($price, $categoryId)
    {
        $similarProducts = self::getProductsInTwoPriceRanges($price, $price, $categoryId);
        $similarProductsArray = [];
        $index = 0;
        foreach ($similarProducts as $product) {
            $similarProductsArray[$index] = self::convertProductObjectIntoArrayForAPI($product);
            $similarProductsArray[$index]['images'] = self::getProductImagesForAPI($product->productImages);
            $index++;
        }
        return $similarProductsArray;
    }

    public static function convertProductObjectIntoArrayForAPI(\common\models\Products $product)
    {
        $product = \yii\helpers\ArrayHelper::toArray($product, [
            'common\models\Products' => self::getFieldsArray(),
        ]);

        return self::cleanArrayForAPI($product);
    }

    public static function getProductDetailsForAPI($productSlug)
    {
        $productDetail = Products::findOne(['slug' => $productSlug], ['product_status' => 'active']);
        $ProductArray = $productSuppliers = [];
        $index = 0;
        if (empty($productDetail)) {
            return [];
        }
        $ProductArray[$index] = self::convertProductObjectIntoArrayForAPI($productDetail);

        // Fetching Product Images
        $ProductArray[$index]['images'] = self::getProductImagesForAPI($productDetail->productImages);

        //fetching Product Supplier
        $ProductArray[$index]['suppliers'] = self::getProductSupplierForAPI($productDetail->suppliersProducts);

        $ProductArray[$index]['featuresList'] = self::getProductFeaturesListForAPI($productDetail->product_id, (int)$productDetail->categories_category_id);
        return $ProductArray;
    }

    public static function getProductImagesForAPI(array $productImages)
    {
        $images = [];
        foreach ($productImages as $image) {
            $images[] = \yii\helpers\ArrayHelper::toArray($image, [
                'common\models\ProductImage' => ProductImage::getFieldsArray(),
            ]);
        }
        return $images;
    }

    public static function getProductFeaturesListForAPI($product_id, $categoryId)
    {
        //get product category feature groups
        if (!is_int($categoryId)) {
            return [];
        }
        $featureGroups = FeatureGroups::getFeatureAttributeByCategory($categoryId);
        //product features array
        $productFeatures = ProductFeature::getProductFeatureValues($product_id);
        $groups = [];
        $groupIndex = 0;
        $featureIndex = 0;
        foreach ($featureGroups as $group) {
            $featureIndex = 0;
            $groups[$groupIndex] = ['featureGroupName' => $group->name];
            $featureValues = [];
            foreach ($group->features as $feature) {
                $featureValues[$featureIndex]['featureName'] = $feature->name;
                $featureValues[$featureIndex]['featureValue'] = (isset($productFeatures[$feature->id]) ? $productFeatures[$feature->id] : 'Not Available');
                $featureValues[$featureIndex]['isFilter'] = ($feature->is_filter == 1) ? true : false;
                $featureIndex++;
            }
            $groups[$groupIndex]['featureValues'] = $featureValues;
            $groupIndex++;
        }
        return $groups;
    }

    public function getProductSupplierForAPI(array $suppliersProducts)
    {
        $productSuppliers = [];

        foreach ($suppliersProducts as $supplier) {
            $productSuppliers[] = self::cleanArrayForAPI(\yii\helpers\ArrayHelper::toArray($supplier, [
                'common\models\ProductsSuppliers' => ProductsSuppliers::getFieldsArray(),
            ]), 'suppliersProducts');

        }
        return $productSuppliers;
    }

    public static function getProductsByCategoryForAPI($categoryId, $brandId, array $sortArray)
    {
        $whereArray = ['product_status' => 'active'];
        $query = Products::find()
            ->where($whereArray);

        if (empty($categoryId)) {
            return ['data' => []];
        } else
            $query->andWhere(['categories_category_id' => $categoryId]);

        if (!empty($brandId) && is_int($brandId))
            $query->andWhere(['brands_brand_id' => $brandId]);

        $countQuery = clone $query;

        if (isset($sortArray[0])) {
            $query->orderBy($sortArray[0]);
        } else
            $query->orderBy('product_id DESC');

        $totalRecords = $countQuery->count();
        $pages = new \yii\data\Pagination(['totalCount' => $totalRecords, 'defaultPageSize' => \Yii::$app->params['recordsPerPage']]);
        if (\Yii::$app->request->get($pages->pageParam) > $pages->pageCount) {
            return ['data' => []];
        }
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $ProductsArray = [];
        $index = 0;
        foreach ($models as $product) {
            $ProductsArray[$index] = self::convertProductObjectIntoArrayForAPI($product);
            $ProductsArray[$index]['images'] = self::getProductImagesForAPI($product->productImages);
            $ProductsArray[$index]['suppliers'] = self::getProductSupplierForAPI($product->suppliersProducts);
            $index++;
        }

        return ['data' => $ProductsArray, 'pagination' => (!empty($models)) ? $models[0]->getPaginationForAPI($pages, $totalRecords) : []];
    }

    public static function getPopularProductsForAPI($categoryId = 0, $brandId = 0, array $sortArray = [])
    {
        $whereArray = ['product_status' => 'active', 'is_delete' => 0];

        $query = Products::find()
            ->where($whereArray);

        if (empty($categoryId)) {
            return ['data' => []];
        } else
            $query->andWhere(['categories_category_id' => $categoryId]);

        if (!empty($brandId) && is_int($brandId))
            $query->andWhere(['brands_brand_id' => $brandId]);

        $query->orderBy('rating DESC');
        $countQuery = clone $query;
        $totalRecords = $countQuery->count();
        $pages = new \yii\data\Pagination(['totalCount' => $totalRecords, 'defaultPageSize' => \Yii::$app->params['recordsPerPage']]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $ProductsArray = [];
        $index = 0;
        foreach ($models as $product) {
            $ProductsArray[$index] = self::convertProductObjectIntoArrayForAPI($product);
            $ProductsArray[$index]['images'] = self::getProductImagesForAPI($product->productImages);
            $ProductsArray[$index]['suppliers'] = self::getProductSupplierForAPI($product->suppliersProducts);
            $ProductsArray[$index]['featuresList'] = self::getProductFeaturesListForAPI($product->product_id, (int)$product->categories_category_id);
            $index++;
        }

        return ['data' => $ProductsArray, 'pagination' => (!empty($models)) ? $models[0]->getPaginationForAPI($pages, $totalRecords) : []];
    }

    public static function getNewProductsForAPI($limit)
    {

        $models = Products::find()
            ->where(['product_status' => 'active', 'is_delete' => 0])
            ->orderBy('product_id DESC')
            ->limit($limit)
            ->all();
        $ProductsArray = [];
        $index = 0;
        foreach ($models as $product) {
            $ProductsArray[$index] = self::convertProductObjectIntoArrayForAPI($product);
            $ProductsArray[$index] = self::cleanArrayForAPI($ProductsArray[$index]);
            $ProductsArray[$index]['images'] = self::getProductImagesForAPI($product->productImages);
            $ProductsArray[$index]['suppliers'] = self::getProductSupplierForAPI($product->suppliersProducts);
            $index++;
        }
        return $ProductsArray;
    }

    public static function getProductReviewsAndItsInfoForAPI($product_id)
    {
        $product = Products::find()->where(['product_id' => $product_id])->one();
        $productReviews = $product->productReviews;
        $productReviewsArray = [];
        if (count($productReviews) > 0) {
            $index = 0;
            foreach ($productReviews as $review) {
                $productReviewsArray[$index] = ProductReviews::convertProductReviewObjectIntoArrayForAPI($review);
                $user = $review->user;
                if (isset($user)) {
                    $productReviewsArray[$index]['user'] = [User::convertUserObjectIntoArrayForAPI($review->user)];
                } else {
                    $productReviewsArray[$index]['user'] = [];
                }
                $index++;
            }
        }

        return self::createFormatedArrayForAPI($productReviewsArray);
    }

    public static function cleanArrayForAPI(array $arr, $modelString = 'Products')
    {
        if ($modelString === 'Products') {
            $arr['shipping'] = empty($arr['shipping']) ? '' : $arr['shipping'];
            $arr['cod'] = empty($arr['cod']) ? '' : (string)$arr['cod'];
            $arr['rating'] = empty($arr['rating']) ? '' : (string)$arr['rating'];
            $arr['return_policy'] = empty($arr['return_policy']) ? '' : (string)$arr['return_policy'];
            $arr['price'] = empty($arr['price']) ? '' : (string)$arr['price'];
            $arr['rating_user_count'] = empty($arr['rating_user_count']) ? '' : (string)$arr['rating_user_count'];
            $arr['reviews'] = empty($arr['reviews']) ? '' : $arr['reviews'];
            $arr['original_price'] = empty($arr['original_price']) ? '' : (string)$arr['original_price'];
            $arr['lowest_price'] = empty($arr['lowest_price']) ? '' : (string)$arr['lowest_price'];
            $arr['highest_price'] = empty($arr['highest_price']) ? '' : (string)$arr['highest_price'];
            $arr['discount'] = empty($arr['discount']) ? '' : (string)$arr['discount'];
            $arr['meta_keyword'] = empty($arr['meta_keyword']) ? '' : $arr['meta_keyword'];
            $arr['meta_desc'] = empty($arr['meta_desc']) ? '' : $arr['meta_desc'];
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('HTML.AllowedElements', []);
            $purifyHtml = new \HTMLPurifier($config);
            $arr['product_description'] = trim($purifyHtml->purify($arr['product_description']));
        } elseif ($modelString === 'suppliersProducts') {
            $arr['cod'] = empty($arr['cod']) ? '' : $arr['cod'];
            $arr['delivery'] = empty($arr['delivery']) ? '' : $arr['delivery'];
            $arr['shipping'] = empty($arr['shipping']) ? '' : $arr['shipping'];
            $arr['original_price'] = empty($arr['original_price']) ? '' : (string)$arr['original_price'];
            $arr['unique_id'] = empty($arr['unique_id']) ? '' : (string)$arr['unique_id'];
            $arr['discount'] = empty($arr['discount']) ? '' : (string)$arr['discount'];
            $arr['rating_user_count'] = empty($arr['rating_user_count']) ? '' : (string)$arr['rating_user_count'];
            $arr['reviews'] = empty($arr['reviews']) ? '' : $arr['reviews'];
            $arr['return_policy'] = empty($arr['return_policy']) ? '' : $arr['return_policy'];
            $arr['rating'] = empty($arr['rating']) ? '' : (string)$arr['rating'];
            $arr['active'] = empty($arr['active']) ? '' : $arr['active'];
            $arr['meta_keyword'] = empty($arr['meta_keyword']) ? '' : $arr['meta_keyword'];
            $arr['meta_desc'] = empty($arr['meta_desc']) ? '' : $arr['meta_desc'];
        }/* elseif($modelString === 'productImages'){
    		
    	} */
        return $arr;
    }

    public static function getProductIndividualStarRating($productId)
    {
        $connection = self::getDb();
        $sql = 'SELECT 
			(SELECT COUNT(product_reviews.product_id) FROM product_reviews WHERE product_reviews.rating=5 and product_reviews.product_id=:ProductId ) as fiveStars,
			(SELECT COUNT(product_reviews.product_id) FROM product_reviews WHERE product_reviews.rating=4 and product_reviews.product_id=:ProductId ) as fourStars,
			(SELECT COUNT(product_reviews.product_id) FROM product_reviews WHERE product_reviews.rating=3 and product_reviews.product_id=:ProductId ) as threeStars,
			(SELECT COUNT(product_reviews.product_id) FROM product_reviews WHERE product_reviews.rating=2 and product_reviews.product_id=:ProductId ) as twoStars,
			(SELECT COUNT(product_reviews.product_id) FROM product_reviews WHERE product_reviews.rating=1 and product_reviews.product_id=:ProductId ) as oneStars
			
			FROM product_reviews where product_reviews.product_id = :ProductId LIMIT 1;';
        $command = $connection->createCommand($sql, [':ProductId' => $productId]);
        $result = $command->queryAll();
        return $result;
    }

  
    public function getPaginationForAPI(\yii\data\Pagination $pages, $totalRecords)
    {
        $paginationArray = [];
        if (\Yii::$app->request->get($pages->pageParam) >= $pages->pageCount) {
            $paginationArray['currentPage'] = (int)$pages->pageCount;
            $paginationArray['nextPage'] = (int)$pages->pageCount;
            $paginationArray['prevPage'] = ((int)$pages->pageCount - 1);
        } else {
            $paginationArray['currentPage'] = (int)\Yii::$app->request->get($pages->pageParam);
            $paginationArray['nextPage'] = ((int)\Yii::$app->request->get($pages->pageParam) + 1);
            $paginationArray['prevPage'] = ((int)\Yii::$app->request->get($pages->pageParam) - 1);;
        }
        $paginationArray['totalPages'] = (int)$pages->pageCount;
        $paginationArray['totalRecords'] = (int)$totalRecords;
        $paginationArray['recordsPerPage'] = (int)$pages->defaultPageSize;

        return $paginationArray;
    }

    public function saveProductViewedToDatabase()
    {
        $recentlyViewed = new RecentlyViewedProducts();
        $query = RecentlyViewedProducts::find()
            ->Where(['product_id' => $this->product_id]);
        if (\Yii::$app->user->isGuest) {
            $query->andWhere(['user_ip' => \Yii::$app->request->userIP]);
        } else {
            $query->andWhere(['user_id' => \Yii::$app->user->id]);
        }
        $exsits = $query->all();
        if (count($exsits) == 0) {
            $recentlyViewed->product_id = $this->product_id;
            $recentlyViewed->user_id = (\Yii::$app->user->isGuest ? 0 : \Yii::$app->user->id);
            $recentlyViewed->user_ip = \Yii::$app->request->userIP;
            $recentlyViewed->created_at = date('Y-m-d H:i:s');
            $recentlyViewed->updated_at = date('Y-m-d H:i:s');
            $recentlyViewed->save();
        }
    }

    public static function getAllProductsCount()
    {
        $sql = 'SELECT COUNT(*) FROM ' . self::tableName() . ' WHERE is_delete=0 AND active=\'active\'';
        return $numClients = \Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public static function getFieldsArray()
    {
        return [
            'product_id',
            'categories_category_id',
            'brands_brand_id',
            'product_name',
            'slug',
            'image',
            'product_description',
            'model_number',
            'cod',
            'emi',
            'return_policy',
            'delivery',
            'shipping',
            'price',
            'unique_id',
            'discount',
            'rating',
            'rating_user_count',
            'reviews',
            'url',
            'product_status',
            'date_add',
            'date_update',
            'date_launch',
            'lowest_price',
            'original_price',
            'highest_price',
            'link_rewrite',
            'supplier_count',
            'store_name',
            'store_id',
            'meta_title',
            'meta_keyword',
            'meta_desc',
            'show_home_page',
            'active',
            'created',
            
        ];
    }

    public static function createFormatedArrayForAPI(array $data)
    {
        if (empty($data)) {
            $data = ['meta' => ['status' => 404, 'message' => 'No data available at the moment.'], 'data' => []];
        } else {
            $data = ['meta' => ['status' => 200, 'message' => 'Data available to show...'], 'data' => $data];
        }

        return $data;
    }

    public static function metaTags($product)
    {

        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'NayaShopi.in - Compare ' . $product->product_name . ' prices and buy on leading online stores in India. Grab the best price, offers or discounts @ NayaShopi.in.',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'keyword',
            'content' => 'Buy ' . $product->product_name . ' at Lowest Price, Compare ' . $product->product_name . ' Prices Online, Highest Discounts on ' . $product->product_name . ', ' . $product->product_name . ' Offers Online',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => $product->product_name . ' Best Price Online and Discounts',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => NoImage::getProductImageFullPathFromObject($product,'full','zoom')
        ]);


 $category = Categories::findOne(['category_id' => $product->categories_category_id]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:url',
            'content' => \Yii::$app->getUrlManager()->getBaseUrl()  . '/'.$category->slug.'/' . $product->slug,
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:site_name',
            'content' => 'NayaShopi.in',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => 'NayaShopi.in - The lowest price for ' . $product->product_name . ' is ' . $product->lowest_price . ' in India as on ' . date('d-m-y') . '. View specifications, reviews; compare prices and buy ' . $product->product_name . ' at NayaShopi.in',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'fb:app_id',
            'content' => 'App_Id Created At Facebook',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'author',
            'content' => '',
        ]);
        \Yii::$app->view->registerMetaTag([
            'http-equiv' => 'content-language',
            'content' => 'en-in',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'language',
            'content' => 'en',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'twitter:cart',
            'content' => 'summary_large_image',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'twitter:site',
            'content' => '@nayashopi',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'twitter:creator',
            'content' => '@nayashopi',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'twitter:title',
            'content' => $product->product_name . ' at the lowest price in India',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'twitter:description',
            'content' => 'NayaShopi.in - Compare ' . $product->product_name . ' prices and buy on leading online stores in India. Grab the best price, offers or discounts @ NayaShopi.in.',
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'twitter:image',
          'content' => NoImage::getProductImageFullPathFromObject($product,'full','zoom'),
        ]);




        \Yii::$app->view->registerLinkTag([
            'rel' => 'alternate',
            'hreflang' => 'x-default',
            'href' => \Yii::$app->getUrlManager()->getBaseUrl() . '/'.$category->slug.'/' . $product->slug,
        ]);
        \Yii::$app->view->registerLinkTag([
            'rel' => 'canonical',
            'href' => \Yii::$app->getUrlManager()->getBaseUrl() . '/' . $category->slug.'/'.$product->slug,
        ]);
    }

    /**
     * Moving to mongo on product delete
     */
    function moveProductDataToMongo()
    {
        $searchData = array();
        $brand = Brands::find()->where(['brand_id' => $this->brands_brand_id])->one();
        $category = Categories::find()->where(['category_id' => $this->categories_category_id])->one();
        $productSupplier = ProductsSuppliers::find()->where(['product_id' => $this->product_id, 'store_id' => $this->store_id])->one();

        $searchData['product_name'] = $this->product_name;
        $searchData['store_id'] = $this->store_id;
        $cat_name = $category->category_name;
        $brand_name = $brand->brand_name;
        if (!empty($cat_name)) {
            $searchData['category_name'] = $cat_name;
        }
        if (!empty($brand_name)) {
            $searchData['brand_name'] = $brand_name;
        }

        $mongoProduct = MongoProducts::find()->where($searchData)->one();
        $current_id = 1;
        if (is_null($mongoProduct)) {
            $mongoProduct = new MongoProducts();
            $last_insert_id = MongoProducts::find()->orderBy(['product_id' => SORT_DESC])->one();
            if (!empty($last_insert_id)) {
                $current_id = (int)$last_insert_id->product_id + 1;
            }
        } else {
            $current_id = $mongoProduct->product_id;
        }

        $mongoProduct->product_id = $current_id;
        $mongoProduct->store_name = !is_null($productSupplier) ? $productSupplier->store_name : $this->store_name;
        $mongoProduct->store_id = !is_null($productSupplier) ? $productSupplier->store_id : $this->store_id;
        $mongoProduct->brand_name = $brand->brand_name;
        $mongoProduct->category_name = $category->category_name;
        $mongoProduct->product_name = $this->product_name;
        $mongoProduct->product_description = $this->product_description;
        $mongoProduct->model_number = $this->model_number;
        $mongoProduct->price = $this->lowest_price;
        $mongoProduct->emi = !is_null($productSupplier) ? $productSupplier->emi : $this->emi;
        $mongoProduct->colors = $this->colors;
        $mongoProduct->sizes = $this->sizes;
        $mongoProduct->offers = $this->offers;
        $mongoProduct->cod = !is_null($productSupplier) ? $productSupplier->cod : $this->cod;
        $mongoProduct->return_policy = $this->return_policy;
        $mongoProduct->url = $this->url;
        $mongoProduct->unique_id = $this->unique_id;
        $mongoProduct->image = $this->image;
        $mongoProduct->status = 1;
        $mongoProduct->discount = $this->discount;
        $mongoProduct->rating = !is_null($productSupplier) ? $productSupplier->rating : $this->rating;
        $mongoProduct->reviews = !is_null($productSupplier) ? $productSupplier->reviews : $this->reviews;
        $mongoProduct->delivery = !is_null($productSupplier) ? $productSupplier->delivery : $this->delivery;
        $mongoProduct->shipping = $this->shipping;
        $mongoProduct->rating_user_count = !is_null($productSupplier) ? $productSupplier->rating_user_count : $this->rating_user_count;
        $mongoProduct->created = date('Y-m-d H:i:s');
        $mongoProduct->updated = date('Y-m-d H:i:s');
        $mongoProduct->image = $this->image_path;
        $mongoProduct->save();
        $this->saveProductInMongo($mongoProduct);
    }

    public function saveProductInMongo(MongoProducts $mongoProduct)
    {
        MongoProductFeatures::deleteAll(['id_product' => $mongoProduct->product_id]);
        $productFeatures = ProductFeature::find()->where(['id_product' => $this->product_id])->all();
        foreach ($productFeatures as $productFeature) {
            $featureInDB = Features::find()->where(['id' => $productFeature->id_feature])->one();
            $featureGroupInDB = FeatureGroups::find()->where(['id' => $featureInDB->feature_group_id])->one();
            $featureValuesInDB = FeatureValues::find()->where(['id' => $productFeature->id_feature_value])->one();

            $featureGroupInMongo = $this->saveFeatureGroupInMongo($featureGroupInDB);
            $featureInMongo = $this->saveFeatureInMongo($featureInDB, $featureGroupInMongo);
            $featureValueInMongo = $this->saveFeatureValueInMongo($featureValuesInDB, $featureInMongo);
            $this->saveProductFeatureInMongo($mongoProduct, $featureInMongo, $featureValueInMongo);
        }
    }

    private function saveFeatureGroupInMongo(FeatureGroups $featureGroupInDB)
    {
        $mongoFeatureGroup = MongoFeatureGroups::find()->where(['name' => $featureGroupInDB->name])->one();
        if ($mongoFeatureGroup == null) {
            $max_sort_order = MongoFeatureGroups::find()->orderBy('sort_order DESC')->one();
            if (!empty($max_sort_order)) {
                $sort_order = $max_sort_order->sort_order + 1;
            } else {
                $sort_order = 1;
            }
            $last_insert_id = MongoFeatureGroups::find()->orderBy(['id' => SORT_DESC])->one();
            $current_id = 1;
            if (!empty($last_insert_id)) {
                $current_id = (int)$last_insert_id->id + 1;
            }
            $feature_group_model = new MongoFeatureGroups();
            $feature_group_model->id = $current_id;
            $feature_group_model->category_id = $featureGroupInDB->category_id;
            $feature_group_model->name = $featureGroupInDB->name;
            $feature_group_model->sort_order = $sort_order;
            $feature_group_model->created = date('Y-m-d h:i:s');
            $feature_group_model->save();
            return $feature_group_model;
        }
        return $mongoFeatureGroup;
    }

    private function saveFeatureInMongo(Features $featureInDB, MongoFeatureGroups $featureGroupInMongo)
    {
        $mongoFeature = MongoFeatures::findOne(['name' => $featureInDB->name, 'category_id' => $featureInDB->category_id, 'feature_group_id' => $featureGroupInMongo->id]);
        if ($mongoFeature == null) {
            $max_sort_order = MongoFeatures::find()->orderBy('sort_order DESC')->one();
            if (!empty($max_sort_order)) {
                $sort_order = $max_sort_order->sort_order + 1;
            } else {
                $sort_order = 1;
            }
            $last_insert_id = MongoFeatures::find()->orderBy(['id' => SORT_DESC])->one();
            $current_id = 1;
            if (!empty($last_insert_id)) {
                $current_id = (int)$last_insert_id->id + 1;
            }
            $feature_model = new MongoFeatures();
            $feature_model->id = $current_id;
            $feature_model->category_id = $featureInDB->category_id;
            $feature_model->feature_group_id = $featureGroupInMongo->id;
            $feature_model->name = $featureInDB->name;
            $feature_model->display_name = $featureInDB->name;
            $feature_model->sort_order = $sort_order;
            $feature_model->status = 1;
            $feature_model->created = date('Y-m-d h:i:s');
            $feature_model->save();
            return $feature_model;
        }
        return $mongoFeature;
    }

    private function saveFeatureValueInMongo(FeatureValues $featureValuesInDB, MongoFeatures $featureInMongo)
    {
        $feature_value_data = MongoFeatureValues::findOne(['value' => $featureValuesInDB->value, 'feature_id' => $featureInMongo->id]);
        if ($feature_value_data == null) {
            $last_insert_id = MongoFeatureValues::find()->orderBy(['id' => SORT_DESC])->one();
            $current_id = 1;
            if (!empty($last_insert_id)) {
                $current_id = (int)$last_insert_id->id + 1;
            }
            $feature_value_model = new MongoFeatureValues();
            $feature_value_model->id = $current_id;
            $feature_value_model->feature_id = $featureInMongo->id;
            $feature_value_model->value = $featureValuesInDB->value;
            $feature_value_model->display_name = $featureValuesInDB->value;
            $feature_value_model->created = date('Y-m-d h:i:s');
            $feature_value_model->save();
            return $feature_value_model;
        }
        return $feature_value_data;
    }

    private function saveProductFeatureInMongo(MongoProducts $mongoProduct, MongoFeatures $featureInMongo, MongoFeatureValues $featureValueInMongo)
    {
        $last_insert_id = MongoProductFeatures::find()->orderBy(['id_product_feature' => SORT_DESC])->one();
        $current_id = 1;
        if (!empty($last_insert_id)) {
            $current_id = (int)$last_insert_id->id_product_feature + 1;
        }
        $product_feature_model = new MongoProductFeatures();
        $product_feature_model->id_product_feature = $current_id;
        $product_feature_model->id_product = $mongoProduct->product_id;
        $product_feature_model->id_feature = $featureInMongo->id;
        $product_feature_model->id_feature_value = $featureValueInMongo->id;
        $product_feature_model->save();
    }

    public function queueProductSuppliers()
    {
        $scrapAfterHours = 4;
        $amazonStoreId = 15;

        //$productSupplier->store_id != $amazonStoreId
        $productSuppliers = $this->getSuppliersProducts()->all();
        foreach ($productSuppliers as $productSupplier) {
            if ($this->isSupplierScrappedLongBefore($productSupplier->updated) > $scrapAfterHours) {
                error_log("adding supplier in queue = " . $productSupplier->id);
                \Yii::$app->rabbit->send($productSupplier->id);
            }
        }
    }

    private function isSupplierScrappedLongBefore($lastUpdateDate)
    {
        $date1 = new \DateTime();
        $date2 = new \DateTime(date("Y-m-d H:i:s", strtotime($lastUpdateDate)));
        $diff = $date2->diff($date1);
        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);
        return $hours;
    }

    public function updateLowestProductSupplier()
    {
        $lowestPriceProductsSuppliers = $this->getSuppliersProductWithLowestPrice();
        if (!is_null($lowestPriceProductsSuppliers)) {
            $this->changeProductSupplierAccoringToLowestPrice($this, $lowestPriceProductsSuppliers);
        }
    }

    public function getEmiDisplayString()
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $this->categories_category_id,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            return ucwords($productSupplierFeatureMapping->emi);
        } else {
            return ucwords($this->emi);
        }
    }

    public function getCodDisplayString()
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $this->categories_category_id,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            return ucwords($productSupplierFeatureMapping->cod);
        } else {
            return ucwords($this->cod);
        }
    }

    public function getDeliveryDisplayString()
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $this->categories_category_id,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            return ucwords($productSupplierFeatureMapping->delivery);
        } else {
            return ucwords($this->delivery);
        }
    }

    public function getReturnPolicyDisplayString()
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $this->categories_category_id,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            error_log(var_export($productSupplierFeatureMapping->attributes, true));
            return ucwords($productSupplierFeatureMapping->return_policy);
        } else {
            return ucwords($this->return_policy);
        }
    }

    public function getPreviewImage($category, $brand)
    {
        if (!is_null($category) && !is_null($brand)) {

            $path = Yii::getAlias('@frontend') . '/web/';
            $imgFolder = explode(".", $this->image);
            $uploadPath = 'uploads/products/' . $category->folder_slug . '/' . $brand->folder_slug . "/" . $imgFolder[0] . "/" . $this->image;
            if (is_file($path . $uploadPath)) {
                return Yii::$app->getModule('articles')->frontPageUrl . $uploadPath;
            }
        }
        return $this->image_path;
    }

    public static function deleteDir($dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    public function deleteAllProductFeatureValues()
    {
        ProductFeature::deleteAll(['id_product' => $this->product_id]);
    }

    public static function getMenuProducts($ids)
    {
        $data = [];
        $query = new Query();
        $query->select('product_id as id, product_name as text')
            ->from('products')
            ->where(['product_id' => $ids]);
        $queryData = $query->createCommand()->queryAll();
        foreach ($queryData as $qdata) {
            $data[$qdata['id']] = $qdata['text'];
        }
        return $data;
    }
}
