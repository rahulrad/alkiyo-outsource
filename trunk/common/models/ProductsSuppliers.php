<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "products_suppliers".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $cod
 * @property string $emi
 * @property string $return_policy
 * @property string $delivery
 * @property string $price
 * @property string $unique_id
 * @property string $url
 * @property string $product_status
 * @property double $lowest_price
 * @property double $highest_price
 * @property string $store_name
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_desc
 * @property string $active
 * @property string $track_id
 * @property string $created
 * @property string $updated
 * @property string $instock
 */
class ProductsSuppliers extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products_suppliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['product_id'], 'required'],
            [['product_id','instock'], 'integer'],
            [['url', 'product_status', 'store_name', 'meta_keyword', 'meta_desc'], 'string'],
            [['created', 'price', 'store_id', 'discount', 'rating', 'rating_user_count', 'reviews', 'updated', 'colors', 'sizes', 'offers','track_id'], 'safe'],
            [['cod', 'emi', 'return_policy', 'delivery', 'unique_id', 'meta_title'], 'string', 'max' => 255],
            [['active'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'cod' => 'Cod',
            'emi' => 'Emi',
            'return_policy' => 'Return Policy',
            'delivery' => 'Delivery',
            'price' => 'Price',
            'unique_id' => 'Unique ID',
            'url' => 'Url', 'store_name' => 'Store Name',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_desc' => 'Meta Desc',
            'active' => 'Active',
            'created' => 'Created',
            'store_id' => 'Store',
            'updated'=>'updated'
        ];
    }
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['return_policy']);
        unset($fields['meta_title']);
        unset($fields['meta_desc']);
        unset($fields['meta_keyword']);
        unset($fields['return_policy']);
        unset($fields['active']);
        unset($fields['cod']);
        unset($fields['url']);
        unset($fields['product_id']);
        unset($fields['emi']);
        unset($fields['offers']);
        unset($fields['delivery']);

        $extraFields= [
            'image' => function ($model) {
                return $model->supplier->image;
            },
            'redirectlink' => function ($model) {
                return $model->storeLink;
            },
          
        ];
        
      
        
        return array_merge($fields,$extraFields);
       
    }

    public function saveSuppilerDataFromMongoDb($mongodbProductData, $productData)
    {

        //echo '<pre>'; print_r($mongodbProductData); die;

        $ProductsSuppliersModel = new ProductsSuppliers();

        if (!empty($productData) && !empty($mongodbProductData)) {

            $exitMongoProduct = $ProductsSuppliersModel::find()->where(['product_id' => $productData->product_id, 'store_id' => $mongodbProductData->store_id])->one();

            if (empty($exitMongoProduct)) {
                //echo '<pre>'; print_r($mongodbProductData); die;
                $ProSuppliersModel = new ProductsSuppliers();
                $ProSuppliersModel->product_id = $productData->product_id;
                $ProSuppliersModel->store_name = $mongodbProductData->store_name;
                $ProSuppliersModel->store_id = (int)$mongodbProductData->store_id;
                $ProSuppliersModel->price = (int)$mongodbProductData->price;
                $ProSuppliersModel->emi = strlen($mongodbProductData->emi) > 255 ? substr($mongodbProductData->emi, 0, 255) : $mongodbProductData->emi;
                $ProSuppliersModel->cod = strip_tags($mongodbProductData->cod);
                $ProSuppliersModel->colors = $mongodbProductData->colors;
                $ProSuppliersModel->sizes = $mongodbProductData->sizes;
                $ProSuppliersModel->offers = $mongodbProductData->offers;
                $ProSuppliersModel->instock = $mongodbProductData->instock;
                $ProSuppliersModel->return_policy = strip_tags($mongodbProductData->return_policy);
                $ProSuppliersModel->unique_id = $mongodbProductData->unique_id;
                $ProSuppliersModel->discount = $mongodbProductData->discount;
                $ProSuppliersModel->rating = $mongodbProductData->rating;
                $ProSuppliersModel->delivery = strip_tags($mongodbProductData->delivery);
                $ProSuppliersModel->reviews = $mongodbProductData->reviews;
                $ProSuppliersModel->url = $mongodbProductData->url;
                $ProSuppliersModel->shipping = $mongodbProductData->shipping;
                $ProSuppliersModel->active = 'active';
                $ProductsSuppliersModel->meta_title = "";
                //$ProSuppliersModel->discount = $productData->discount;
                //$ProSuppliersModel->rating = $productData->rating;
                $ProSuppliersModel->created = date('Y-m-d h:i:s');
                if ($ProSuppliersModel->save()) {
                    $MongoProductModel = $mongodbProductData;
                    $currentProductMap = [];
                    if(!empty($MongoProductModel->product_mapping)){
                        $currentProductMap = explode(",", $MongoProductModel->product_mapping);
                    }
                    array_push($currentProductMap, $ProSuppliersModel->product_id);
                    $MongoProductModel->product_mapping = implode(",", $currentProductMap);
                    $MongoProductModel->save(false);
                    return $ProSuppliersModel;
                } else {
                    error_log(var_export($ProSuppliersModel->attributes, true));
                    error_log(var_export($ProSuppliersModel->errors, true));
                    return null;
                }
            }

            //echo '<pre>'; print_r($MongoProductModel); die;
            return $exitMongoProduct;
        }
    }

    public function getSupplier()
    {
        return $this->hasOne(Suppliers::className(), ['id' => 'store_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['product_id' => 'product_id']);
    }

    public static function getFieldsArray()
    {
        return [
            'id',
            'product_id',
            'cod',
            'emi',
            'return_policy',
            'delivery',
            'shipping',
            'price',
            'original_price',
            'unique_id',
            'discount',
            'rating',
            'rating_user_count',
            'reviews',
            'url',
            'product_status',
            'store_name',
            'store_id',
            'meta_title',
            'meta_keyword',
            'meta_desc',
            'price_status',
            'active',
            'created',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $product = Products::findOne($this->product_id);
        if ($this->instock) {
            if (!is_null($product)) {
                $product->instock = $this->instock;
                $product->save();
                $product->updateLowestProductSupplier();
            }
        } else {
            if (!is_null($product)) {
                $productInStockSuppliersCount = $product->getInstockSuppliersProductsCount();
                if ($productInStockSuppliersCount > 0) {
                    $product->instock = 1;
                } else {
                    $product->instock = $this->instock;
                }
                $product->save();
            }
        }
    }

    public function getOffersCount()
    {
        if (!empty($this->offers)) {
            $offers = explode("##", $this->offers);
            return count($offers);
        } else {
            return 0;
        }
    }

    public function getEmiDisplayString($categorId)
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $categorId,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            return ucwords($productSupplierFeatureMapping->emi);
        } else {
            return ucwords($this->emi);
        }
    }

    public function getCodDisplayString($categorId)
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $categorId,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            return ucwords($productSupplierFeatureMapping->cod);
        } else {
            return ucwords($this->cod);
        }
    }

    public function getDeliveryDisplayString($categorId)
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $categorId,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            return ucwords($productSupplierFeatureMapping->delivery);
        } else {
            return ucwords($this->delivery);
        }
    }

    public function getReturnPolicyDisplayString($categorId)
    {
        $productSupplierFeatureMapping = ProductSupplierFeatureMappings::find()->where([
            'category_id' => $categorId,
            'store_id' => $this->store_id
        ])->one();
        if (!is_null($productSupplierFeatureMapping)) {
            error_log(var_export($productSupplierFeatureMapping->attributes,true));
            return ucwords($productSupplierFeatureMapping->return_policy);
        } else {
            return ucwords($this->return_policy);
        }
    }

    public function getStoreLink()
    {
        if (empty($this->track_id)) {
            $this->track_id = md5($this->id);
            $this->save(false);
        }
        return \Yii::$app->params['frontend'].Url::toRoute(['/product/store-out', 'id' => $this->track_id]);
    }
}
