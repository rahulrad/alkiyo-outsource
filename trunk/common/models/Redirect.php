<?php

namespace common\models;


use Yii;
use yii\base\Component;
use yii\db\Query;

/**
 * This is the model class for table "redirect".
 *
 * @property integer $id
 * @property string $old_url
 * @property string $new_url
 * @property string $date
 */
class Redirect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'redirect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_url', 'new_url'], 'required'],
            [['date'], 'safe'],
            [['old_url', 'new_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'old_url' => 'Old Url',
            'new_url' => 'New Url',
            'date' => 'Date',
        ];
    }

    public static function getNewUrl($oldUrl){
        return (new Query())->select(['new_url'])
        ->from('redirect')
        ->where(['old_url' => $oldUrl])
        ->scalar();
    }
}
