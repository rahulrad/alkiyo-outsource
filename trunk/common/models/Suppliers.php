<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "suppliers".
 *
 * @property integer $id
 * @property string $site_url
 * @property string $data_url
 * @property string $image
 * @property string $status
 */
class Suppliers extends \yii\db\ActiveRecord {

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'suppliers';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_url', 'data_url', 'name'], 'required'],
			[['is_delete'], 'safe'],
            [['site_url', 'data_url', 'image', 'name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'site_url' => 'Site Url',
            'data_url' => 'Affiliate Url',
            'image' => 'Image',
            'status' => 'Status',
            'name' => 'Suppliers Name'
        ];
    }

    public function getProducts() {
        return $this->hasMany(Product::className(), ['id' => 'store_id']);
    }

	public static function topDiscountedSuppliers(){
       	$topSuppliersIDs = (new \yii\db\Query())
       	->select('store_id as supplier_id')
       	->from('deals')
      	->limit(7)
       	->distinct()->all();
       	$arrSupplier = [];
       	foreach($topSuppliersIDs as $topSupplier){
			$arrSupplier[]= $topSupplier['supplier_id'] ;
       	}

       	$topSuppliers=  self::find()->where(['id' => $arrSupplier])->andWhere(['is_delete' => 0])->all();
       	return $topSuppliers;
	}
	
	public static function getActiveSupplierQueryObject(){
		return self::find()->where(['is_delete' => 0, 'status' => 'active']);
	}
    
	public function getDeals() {
		return $this->hasMany(Deals::className(), ['store_id' => 'id'])->where(['>', 'end_date', date('Y-m-d H:i:s')]);
    }
    
    public static function getDealOfTheDayForAPI(){
    	$rsDealsOfTheDaySuppliers = self::topDiscountedSuppliers();
    	$supplierArray = $dealArray = [];
    	$supplierIndex = $dealIndex = 0;
    	
    	foreach($rsDealsOfTheDaySuppliers as $topSupplier){
    		$supplier =  self::find()->where(['id' => $topSupplier['id']])->one();
    		
    		if(count($supplier) > 0){
    			$supplierArray[$supplierIndex] = \yii\helpers\ArrayHelper::toArray($supplier,[
    					'common\models\Suppliers' => self::getFieldsArray(),
    			]);
    			$supplierArray[$supplierIndex]['image'] = empty($supplierArray[$supplierIndex]['image']) ? '' : $supplierArray[$supplierIndex]['image'];
    			$deals = $supplier->getDeals()->Where([ 'store_id' => $topSupplier['id'] ])->orderBy('discount DESC')->limit(6)->all();
    			$dealIndex = 0;
    			foreach($deals as $deal){
    				$dealArray = \yii\helpers\ArrayHelper::toArray($deal,[
    						'common\models\Deals' => Deals::getFieldsArray(),
    				]);
    				$supplierArray[$supplierIndex]['children'][$dealIndex] = $dealArray;
    				$dealIndex++;
    			}
    			
    		}
    		
    		$supplierIndex++;
    	}
    	
    	return Products::createFormatedArrayForAPI($supplierArray);
    }
    
    public static function getFieldsArray(){
    	return [
    		'id',
    		'name',
    		'site_url',
    		'data_url',
    		'image',
    		'status',
    	];
    }
    
    
    public static function loadAll(){
        $query = new \yii\db\Query();
        $query->select('*')
                ->from(self::tableName(). ' s')
                ->indexBy('id');
        
        return $query->all();
    }

}
