<?php

/**
 * @copyright Copyright &copy; Gogodigital Srls
 * @company Gogodigital Srls - Wide ICT Solutions
 * @website http://www.gogodigital.it
 * @github https://github.com/cinghie/yii2-articles
 * @license GNU GENERAL PUBLIC LICENSE VERSION 3
 * @package yii2-articles
 * @version 0.6.3
 */
namespace common\modules\articles\assets;

use yii\web\AssetBundle;

class CmsAsset extends AssetBundle {
	public $sourcePath = __DIR__;
	public $css = array (
			'css/reset.css',
			'css/font-awesome.min.css',
			'css/dat-menu.css',
			'css/main-stylesheet.css',
			'css/lightbox.css',
			'css/shortcodes.css',
			'css/custom-fonts.css',
			'css/custom-colors.css',
			'css/animate.css',
			'css/owl.carousel.css',
			'css/responsive.css' 
	);
	public $js = [ 
			'jscript/jquery-latest.min.js',
			'jscript/modernizr.custom.50878.js',
			'jscript/iscroll.js',
			'jscript/dat-menu.js',
			'jscript/theme-scripts.js',
			'jscript/lightbox.js',
			'jscript/owl.carousel.min.js' 
	];
}
