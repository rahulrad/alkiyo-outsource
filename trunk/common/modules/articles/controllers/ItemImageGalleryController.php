<?php

namespace common\modules\articles\controllers;

use Yii;
use common\modules\articles\models\ItemImageGallery;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * ItemImageGalleryController implements the CRUD actions for ItemImageGallery model.
 */
class ItemImageGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ItemImageGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$model = new ItemImageGallery();
    	$dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        	'model' => $model
        ]);
    }

    /**
     * Displays a single ItemImageGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ItemImageGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ItemImageGallery();

        if ($model->load(Yii::$app->request->post()) ) {
        	
        	// Upload Image and Thumb if is not Null
        	$imagePath = Yii::getAlias(Yii::$app->controller->module->itemImagePath);
        	$thumbPath = Yii::getAlias(Yii::$app->controller->module->itemThumbPath);
        	$imgNameType = Yii::$app->controller->module->imageNameType;
        	$imgOptions = Yii::$app->controller->module->thumbOptions;
        	$imgName = time();
        	$fileField = "image";
        	
        	// Create UploadFile Instance
        	$image = $model->uploadFile($imgName, $imgNameType, $imagePath, $fileField);
        	
        	if($model->image == false && $image === false) {
        		unset($model->image);
        	}
        	
        	if ($model->save()) {
        	
        		// upload only if valid uploaded file instance found
        		if ($image !== false) {
        			// save thumbs to thumbPaths
        			$thumb = $model->createThumbImages($image, $imagePath, $imgOptions, $thumbPath);
        		}
        	
        		// Set Success Message
        		Yii::$app->session->setFlash('success', Yii::t('articles', 'Article Image has been added!'));
        	
        		return $this->redirect(['index']);
        	
        	} else {
        		// Set Error Message
        		Yii::$app->session->setFlash('error', Yii::t('articles', 'Article Image could not be saved!'));
        	
        		return $this->render('create', [
        				'model' => $model,
        		]);
        	}
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ItemImageGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
        	
        	// Upload Image and Thumb if is not Null
        	$imagePath = Yii::getAlias(Yii::$app->controller->module->itemImagePath);
        	$thumbPath = Yii::getAlias(Yii::$app->controller->module->itemThumbPath);
        	$imgNameType = Yii::$app->controller->module->imageNameType;
        	$imgOptions = Yii::$app->controller->module->thumbOptions;
        	$imgName = time();
        	$fileField = "image";
        	 
        	// Create UploadFile Instance
        	$image = $model->uploadFile($imgName, $imgNameType, $imagePath, $fileField);
        	 
        	if($model->image == false && $image === false) {
        		unset($model->image);
        	}
        	if ($model->save()) {
        		// upload only if valid uploaded file instance found
        		if ($image !== false) {
        			// save thumbs to thumbPaths
        			$thumb = $model->createThumbImages($image, $imagePath, $imgOptions, $thumbPath);
        		}
        		// Set Success Message
        		Yii::$app->session->setFlash('success', Yii::t('articles', 'Article Image has been updated!'));
        		return $this->redirect(['index']);
        		 
        	} else {
        		// Set Error Message
        		Yii::$app->session->setFlash('error', Yii::t('articles', 'Article Image could not be saved!'));
        		return $this->render('update', [
        				'model' => $model,
        		]);
        	}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ItemImageGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ItemImageGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ItemImageGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ItemImageGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCategoryArticles(){
    	$postData = Yii::$app->request->post();
    	$out = [];
    	if (isset($postData['depdrop_parents'])) {
    		$parents = $postData['depdrop_parents'];
    		$selected =$postData['depdrop_params'][0];
    		if ($parents != null) {
    			$categories = \common\modules\articles\models\Items::find()->where(['type' => $parents[0] ])->all();
    			foreach($categories as $category){
    				$out[] = [
    						'id' => $category->id,
    						'name'=>ucwords($category->title)
    				];
    			}
    			echo Json::encode(['output'=>$out, 'selected'=>$selected>0?$selected:'']);
    			return;
    		}
    	}
    	echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}
