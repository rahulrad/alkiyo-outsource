<?php

namespace common\modules\articles\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "article_item_video_gallery".
 *
 * @property integer $id
 * @property string $video
 * @property string $type
 * @property string $caption
 * @property integer $item_id
 */
class ItemVideoGallery extends Articles
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_item_video_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video', 'type', 'item_id','category_id'], 'required'],
            [['video'], 'string'],
            [['item_id','category_id'], 'integer'],
            [['type'], 'string', 'max' => 20],
            [['caption'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video' => 'Video',
            'type' => 'Type',
            'caption' => 'Caption',
            'item_id' => 'Article',
        	'category_id' => 'Category'
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
    	return $this->hasOne(Items::className(), ['id' => 'item_id'])->from(Items::tableName() . ' AS item');
    }
    
    /*
     * Return array for Video Type
     * @return array
     */
    public function getVideoTypeSelect2()
    {
    	$videotype = [ "youtube" => "YouTube", "vimeo" => "Vimeo", "dailymotion" => "Dailymotion" ];
    
    	return $videotype;
    }
    
    public function search($params)
    {
    	$query = self::find();
    	$query->joinWith('item');
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' => [
    					'defaultOrder' => [
    							'id' => SORT_DESC
    					],
    			],
    	]);
    	$this->load($params);
    
    	//     	if (!$this->validate()) {
    	//     		return $dataProvider;
    	//     	}
    
    	$query->andFilterWhere(['like', 'item.title', $this->item_id]);
    	return $dataProvider;
	}
}
