<?php
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = $category->name ;
 if(isset($parentCategory) && !is_null($parentCategory->name)){
                    $this->title.= " $parentCategory->name ";
                }

?>

<div class="main-content-wrapper with-double-sidebar">


    <!-- BEGIN .main-content-spacy -->
    <div class="main-content">

        <!-- BEGIN .def-panel -->
        <div class="def-panel">
            <div class="panel-title">
                <a href="<?= Url::to(['/articles']) ?>" class="right">back to homepage</a>
                <h1>Recent <?= $category->name ?>

			<?php 
                if(isset($parentCategory) && !is_null($parentCategory->name)){
                    echo " $parentCategory->name ";
                }
                ?> 
	
		</h2>
            </div>
            <div class="panel-content main-article-list">
                <!-- BEGIN .item -->
                <?= ListView::widget(['dataProvider' => $dataProvider, 'options' => ['tag' => 'div', 'class' => 'list-wrapper', 'id' => 'list-wrapper'], 'layout' => "{items}\n{pager}", 'itemView' => '_list_item','pager' => [
                        'nextPageLabel' => 'Next Page',
                        'nextPageCssClass' => 'page-numbers',
                        'prevPageLabel' => 'Previous',
                        'prevPageCssClass' => 'pre page-numbers',
                        'maxButtonCount' => 3,
                        'pageCssClass' => 'page-numbers',
                        'activePageCssClass'=>'current'
                    ],]); ?>


                <!-- 				<div class="pagination">
                                    <a class="prev page-numbers" href="#"><i class="fa fa-caret-left"></i>Previous</a>
                                    <a class="page-numbers" href="#">1</a> <span
                                        class="page-numbers current">2</span> <a class="page-numbers"
                                        href="#">3</a> <a class="page-numbers" href="#">4</a> <a
                                        class="page-numbers" href="#">5</a> <a class="next page-numbers"
                                        href="#">Next Page<i class="fa fa-caret-right"></i></a>
                                </div>
                                -->

            </div>
            <!-- END .def-panel -->
        </div>

        <!-- END .main-content-spacy -->
    </div>


    <aside id="sidebar">

        <!-- BEGIN .widget -->
        <div class="widget">
            <div class="banner">
            
</div>
            <!-- END .widget -->
        </div>


        <!-- BEGIN .widget -->
        <div class="widget">
            <h3>Popular Content</h3>
            <div class="small-article-list">
                <?php foreach ($topCateogryItem as $model) : ?>
                    <div class="item">
                        <div class="item-header">
                            <a
                                    href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"><?= Html::img($model->getImageThumbUrl("57x37")) ?></a>
                        </div>
                        <div class="item-content">
                            <h4>
                                <a
                                        href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"><?= ucwords($model->title) ?></a></h4>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- END .widget -->
        </div>

    </aside>

</div>
