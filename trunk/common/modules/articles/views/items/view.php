<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
?>

<div class="main-content-wrapper with-double-sidebar">
    <!-- BEGIN .main-content-spacy -->
    <div class="main-content">

        <!-- BEGIN .def-panel -->
        <div class="def-panel">
            <!-- <div class="panel-title">
<a href="index.html" class="right">3 Comments</a>
<h2>Most Recent News</h2>
</div> -->
            <div class="panel-content shortocde-content">

                <div class="article-header">
                    <h1><?= ucwords($model->title) ?></h1>
                    <div class="article-header-photo">
                        <?php if (count($articleGallery) > 1) { ?>
                            <!-- BEGIN .ot-slider -->
                            <div class="owl-carousel">
                                <?php foreach ($articleGallery as $img) : ?>
                                    <div class="item">
                                        <p class="featured-caption"><?= $model->image_caption ?></p>
                                        <img src="<?= $img ?>" alt=""/>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php } else { ?>
                            <p class="featured-caption"><?= $model->image_caption ?></p>
                            <img src="<?= $model->getImageThumbUrl("700x400") ?>" alt=""/>
                        <?php } ?>
                    </div>
                    <div class="article-header-info">
                        <div class="right social-headers">
                            <a href="#" class="soc-facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="soc-twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="soc-pinterest"><i class="fa fa-pinterest"></i></a>
                            <a href="#" class="soc-google-plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="soc-linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <span class="article-header-meta"> <span
                                    class="article-header-meta-date"><?= date("M d", strtotime($model->created)) ?></span> <span
                                    class="article-header-meta-time"> <span
                                        class="head-time"><?= date("H s", strtotime($model->created)) ?></span>
								<span class="head-year"><?= date("Y", strtotime($model->created)) ?></span>
						</span> <span class="article-header-meta-links"> <a
                                        href="blog.html"><i
                                            class="fa fa-user"></i><strong>by <?= isset($model->modifiedby->username) ? $model->modifiedby->username : "" ?></strong></a> <a
                                        onclick="window.print();"><i class="fa fa-print"></i><span>Print
										This Article</span></a>
						</span>
						</span>
                    </div>
                </div>

                <?= $model->fulltext ?>

                <div class="panel-tags-cats">
                    <span><i class="fa fa-tag"></i>&nbsp;&nbsp;Article "tagged" as:</span>
                    <div class="tagcloud">
                        <?php foreach ($articleTags as $tag) : ?>
                            <a href="#"><?= $tag ?></a>
                        <?php endforeach; ?>
                    </div>
                    <div class="article-splitter"></div>
                </div>
                <div class="fb-comments"
                     data-href="http://www.nayashoppy.com/<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"
                     data-numposts="5"></div>
            </div>
            <!-- END .def-panel -->
        </div>

    </div>


    <aside id="sidebar">

        <!-- BEGIN .widget -->
        <div class="widget">
            <div class="banner">
            
	</div>
            <!-- END .widget -->
        </div>


        <!-- BEGIN .widget -->
        <div class="widget">
            <h3>Popular Articles</h3>
            <div class="small-article-list">
                <?php foreach ($topCateogryItem as $model) : ?>
                    <div class="item">
                        <div class="item-header">
                            <a href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"><?= Html::img($model->getImageThumbUrl("57x37")) ?></a>
                        </div>
                        <div class="item-content">
                            <h4>
                                <a href="<?= Url::to(['/articles/items/view', 'type' => $model->articleType->alias, 'alias' => $model->alias]) ?>"><?= ucwords($model->title) ?></a></h4>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- END .widget -->
        </div>


    </aside>

</div>
