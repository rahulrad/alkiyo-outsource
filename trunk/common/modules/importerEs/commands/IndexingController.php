<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\modules\importerEs\commands;

use yii\console\Controller;
use yii\helpers\Json;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexingController extends Controller {

    public function actionDoIndex($index, $type = "full") {

        ini_set('memory_limit','1024M');
        
        $start = \Yii::getLogger()->getElapsedTime();
        $stats = [];
        $logData = [
            'index_name' => $index,
            'type' => $type,
            'on_date' => date('y-m-d h:i:s'),
            'error_message' => '',
        ];


            $indexData = new \common\modules\importerEs\components\ElasticIndexDataProducts();
            $indexData->indexData(['index' => $index, 'type' => $type]);
            $stats = $indexData->stats;
            \common\components\Utils::d($indexData->stats);
//                    break;
//            }
            $logData['success'] = 1;


        $logData['documents'] = isset($stats['total_documents']) ? $stats['total_documents'] : 0;
        $logData['took_time'] = isset($stats['index_time']) ? $stats['index_time'] : 0;
        $logData['requests'] = isset($stats['index_requests']) ? $stats['index_requests'] : 0;
        $logData['on_date'] = isset($stats['on_date']) ? $stats['on_date'] : $logData['on_date'];
        $logData['total_time'] = \Yii::getLogger()->getElapsedTime() - $start;
        $logData['memory'] = memory_get_peak_usage();
        $logData['max_id'] = isset($stats['max_id']) ? $stats['max_id'] : "0";

//        \common\components\Utils::d($logData);
        $log = new \common\models\IndexingLog();
        $log->attributes = $logData;
        \common\components\Utils::d($logData);
        if ($log->insert()):
            echo "Log saved" . PHP_EOL;
        else:
            echo print_r($log->getErrors());
            echo "Log not saved" . PHP_EOL;
        endif;

    }


    public function actionDoIndexLatest($index, $type = "full") {

        ini_set('memory_limit','1024M');
        
        $start = \Yii::getLogger()->getElapsedTime();
        $stats = [];
        $logData = [
            'index_name' => $index,
            'type' => $type,
            'on_date' => date('y-m-d h:i:s'),
            'error_message' => '',
        ];


            $indexData = new \common\modules\importerEs\components\ElasticIndexDataProducts();
            $indexData->indexDataRecentlyUpdated(['index' => $index, 'type' => $type]);
            $stats = $indexData->stats;
            \common\components\Utils::d($indexData->stats);
//                    break;
//            }
            $logData['success'] = 1;


        $logData['documents'] = isset($stats['total_documents']) ? $stats['total_documents'] : 0;
        $logData['took_time'] = isset($stats['index_time']) ? $stats['index_time'] : 0;
        $logData['requests'] = isset($stats['index_requests']) ? $stats['index_requests'] : 0;
        $logData['on_date'] = isset($stats['on_date']) ? $stats['on_date'] : $logData['on_date'];
        $logData['total_time'] = \Yii::getLogger()->getElapsedTime() - $start;
        $logData['memory'] = memory_get_peak_usage();
        $logData['max_id'] = isset($stats['max_id']) ? $stats['max_id'] : "0";

//        \common\components\Utils::d($logData);
        $log = new \common\models\IndexingLog();
        $log->attributes = $logData;
        \common\components\Utils::d($logData);
        if ($log->insert()):
            echo "Log saved" . PHP_EOL;
        else:
            echo print_r($log->getErrors());
            echo "Log not saved" . PHP_EOL;
        endif;

    }
    
    
    public function actionDefinition() {

        
        $definer = new \common\modules\importerEs\components\ElasticIndex();

        $index = $definer::INDEX_PRODUCTS ;
        $settings = $definer->getSettings($index);

//        $settings['number_of_replicas'] = 0;
//        $settings['number_of_shards'] = 5;
//        $settings['translog'] = ['sync_interval' => '15s', 'durability' => 'async'];
//        $settings['analysis'] = [
//            "analyzer" => [
//                "autocomplete_analyzer" => [
//                    "type" => "custom",
//                    "tokenizer" => "standard",
//                    "filter" => ["lowercase", "autocomplete_filter"]
//                ]
//            ],
//            'filter' => [
//                'autocomplete_filter' => [
//                    "type" => 'edge_ngram',
//                    "min_gram" => 1,
//                    "max_gram" => 20
//                ]
//            ],
//        ];

        $mappings = $definer->getMappings($index);

        $template = [
            'settings' => $settings,
            'mappings' => $mappings,
        ];
        echo Json::encode($template);
    }
    
    public function actionRecreate($index){
        $definer = new \common\modules\importerEs\components\ElasticIndex();

        $settings = $definer->getSettings($index);
        $mappings = $definer->getMappings($index);

        $indexDefinition = [
            'settings' => $settings,
            'mappings' => $mappings,
        ];
        
        $command = new \common\components\ElasticCommand(['db' => \Yii::$app->elasticsearch]);
        
        $command->deleteIndex($index);
        
        $response = $command->createIndex($index, $indexDefinition);
        
        print_r($response);
    }

}
