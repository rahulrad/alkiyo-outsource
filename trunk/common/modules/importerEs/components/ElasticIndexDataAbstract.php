<?php

namespace common\modules\importerEs\components;

use yii\base\Component;
use common\components\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ElasticIndexDataAbstract extends Component {

    const INDEX_TYPE_FULL = 'full';
    const INDEX_TYPE_DELTA = 'delta';

    protected $connection = null;
    var $indexChunkSize = 100;
    var $pageSize = 1000;
    var $indexName = null;
    var $typeName = null;
    var $esCommand = null;
    var $indexType = self::INDEX_TYPE_FULL;
    var $stats = [
        'total_documents' => 0, // total documents indexed
        'index_requests' => 0, // requests to index data on es
        'index_time' => 0, // sum of "took" from elastic
        'time' => 0, // time it took
    ];

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->connection = \Yii::$app->get('db');
    }

    /**
     * Get the minimum and maximum value from the table
     * @param string $table Table in "source" db
     * @param string $column column to get the min/max for
     * @return array
     * @author Wasim Aif <wasimasif@gmail.com>
     * @version 20160515
     */
    public function getMaxMin($table, $column) {

        $query = new \yii\db\Query();

        $rs = $query->select(['themin' => new \yii\db\Expression("MIN(" . $column . ")"), 'themax' => new \yii\db\Expression("MAX(" . $column . ")")])
                ->from($table)
                ->one($this->connection);

        return $rs;
    }

    /**
     * Indexes the data into Elastic search via bulk api. 
     * @param string $index Name of the index
     * @param string $type Type of the item in elastic
     * @param array $data Data to be indexed
     * @param array $options
     * @return array
     * @author Wasim Asif <wasimasif@gmail.com>
     * @since 20160515
     */
    public function esIndexBulk($index, $type, $data, $options = []) {

//        if ($this->esCommand === null):
            $this->esCommand = new \common\components\ElasticCommand(['db' => \Yii::$app->elasticsearch]);
//        endif;

        $data = array_chunk($data, $this->indexChunkSize);

        $this->esCommand->updateSettings((isset($options['setting_parrern']) ? $options['setting_parrern'] : $index), ['refresh_interval' => '-1']);

        $return = ['requests' => 0, 'took' => 0];
        foreach ($data as $chunk):
            $this->stats['index_requests'] ++;
            if($index === null):
                $result = $this->esCommand->bulkAll($chunk);
            else:
                $result = $this->esCommand->bulk($index, $type, $chunk);
            endif;
            $return['took']+= isset($result['took']) ? $result['took'] : 0;
            $return['requests'] ++;
        endforeach;
        unset($data);
        $this->esCommand->updateSettings((isset($options['setting_parrern']) ? $options['setting_parrern'] : $index), ['refresh_interval' => '1s']);
        
        return $return;
    }

    public function getLastIndexInfo($indexName, $options = []) {

        $query = new \yii\db\Query();

        $query->select(['*'])
                ->from(\common\models\IndexingLog::tableName())
                ->andWhere(['index_name' => $indexName, 'success' => 1])
                ->orderBy(['on_date' => SORT_DESC,'log_id'=>SORT_DESC])
                ->limit(1);

        $rs = $query->createCommand()->queryOne();
        
//        Utils::d($query->createCommand()->getRawSql(),1);
        $info = null;
        if ($rs !== false):
            $info['on_date'] = $rs['on_date'];
            $info['max_id'] = $rs['max_id'];
        endif;
        return $info;
    }
    
    public function getCount($table){
        $query = new \yii\db\Query();

        $query->select(['total'=>new \yii\db\Expression('COUNT(*)')])
                ->from($table);

        $rs = $query->createCommand()->queryOne();
        $total = 1;
        if ($rs !== false):
            $total = $rs['total'];
        endif;
        return $total;
    }

}
