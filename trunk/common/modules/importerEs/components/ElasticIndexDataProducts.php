<?php

namespace common\modules\importerEs\components;

use common\modules\importerEs\components;
use common\components\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ElasticIndexDataProducts extends ElasticIndexDataAbstract {

    var $idColumn = 'product_id';
    private $table = null;
//    private $customerTable = null;
//    private $indexPattern = 'clicks_*';

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->table = \common\models\Products::tableName();
//        $this->customerTable = \app\models\SpopCustomers::tableName();

        $this->indexName = 'ns_products';
        $this->typeName = 'product';
        $this->pageSize = 1000;
        $this->indexChunkSize = 200;
    }

    /**
     * Index the products data into es
     * @param array $options "size" to fetch record in one go
     * @author Wasim Aif <wasimasif@gmail.com>
     */
    public function indexData($options) {

        $startIndex = \Yii::getLogger()->getElapsedTime();

        $size = isset($options['size']) ? $options['size'] : $this->pageSize;
        $indexingType = isset($options['type']) ? $options['type'] : self::INDEX_TYPE_FULL;

//        echo $indexingType;
        $indexData = [];
//        $indexData['dateEnd'] = date('Y-m-d H:i:00');
        
        if ($indexingType == self::INDEX_TYPE_DELTA):
            $lastIndexing = $this->getLastIndexInfo($this->indexName);
//            Utils::d($lastIndexing,1);
            if ($lastIndexing == null):
                throw new \Exception("Please run full index before delta index");
            else:
                $indexData['dateStart'] = $lastIndexing['on_date'];
            endif;
            
            $totalCount = $this->getCountToIndex($indexData);
            $stats['max_id'] = 0;
            $this->pageSize = $totalCount;
            $pages = 1;
        else:
            $minMax = $this->getMaxMin($this->table, 'product_id');

//            if(isset($minMax['themin'])):
            $indexData['startId'] = $minMax['themin'];
            $indexData['endId'] = $minMax['themax'];
            $stats['max_id'] = $indexData['endId'];
//            endif;
            $totalCount = $indexData['endId'] - $indexData['startId'];
            $pages = $totalCount == 0 ? 0 : ceil($totalCount / $this->pageSize);
        endif;
        
//        Utils::d($indexData,1);
        
        $this->indexName = $this->indexName;
        $indexedTotal = 0;
        $stats = &$this->stats;
        
        $stats['on_date'] = date('Y-m-d H:i:00');
        
        for ($page = 1; $page <= $pages; $page++):

            if($indexingType == self::INDEX_TYPE_DELTA):
                $pass = ['dateStart'=>$indexData['dateStart']];
                $start = 0;
            else:
                $start = $indexData['startId'] + ( ($page - 1 ) * $this->pageSize);
                $end = $start + $this->pageSize;
                $pass = ['start' => $start, 'end' => $end];
            endif;
            

            $data = $this->loadData($pass);
//            Utils::d($data,1);

            echo "Indexing: " . $start . ' to ' . ($start + $this->pageSize).' Records: '. count($data) . PHP_EOL;
            echo "Memory: Now: " . ceil(memory_get_usage() / (1024 * 1024)) . " Peak:" . ceil(memory_get_peak_usage() / (1024 * 1024)) . PHP_EOL;
            $indexedTotal+= count($data);
            $stats['total_documents']+=count($data);
            try {
                $response = $this->esIndexBulk($this->indexName, $this->typeName, $data,['do_not_replica_settings'=>true]);
            } catch (\yii\elasticsearch\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode());
            }

            $stats['index_time']+=isset($response['took']) ? $response['took'] : 0;

        endfor;



        $stats['time'] = \Yii::getLogger()->getElapsedTime() - $startIndex;
        echo "Total Documents: " . $indexedTotal . PHP_EOL;

//        Utils::d($min);
    }

    //index data who are coming in last 5 min
     public function indexDataRecentlyUpdated($options) {
 $stats = &$this->stats;
    $data = $this->loadDataForRecentUpdate();
    
  if($data!=null){
     $stats['total_documents']+=count($data);
    try {
                $response = $this->esIndexBulk($this->indexName, $this->typeName, $data,['do_not_replica_settings'=>true]);
          
            } catch (\yii\elasticsearch\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode());
            }
  }else{
      echo "no data for indexing";
  }
     }

    private function loadData($options) {

//        Utils::d($this->categories,1);
        $query = new \yii\db\Query();
        $select = ['p.*'];
        $query->select($select)
                ->from($this->table . ' p')
                ->indexBy('product_id');
        
        if(isset($options['start']) && $options['end']):
            $condition['string'] = 'p.product_id BETWEEN :start AND :end';
            $condition['param'] = [':start'=>$options['start'],':end'=>$options['end']];
        elseif(isset($options['dateStart'])):
            $condition['string'] = 'p.updated >= :start';
            $condition['param'] = [':start'=>$options['dateStart']];
        endif;
        
        $query->andWhere($condition['string'],$condition['param']);
        

//        Utils::d($query->createCommand($this->connection)->getRawSql());

        $rs = $query->all($this->connection);
        
        $query = new \yii\db\Query();
        $select = ['id_product','image_path'];
        
        $ids = count($rs) > 0 ? array_keys($rs) : [0];
        
        $query->select($select)
                ->from(\common\models\ProductImage::tableName(). ' p')
                ->andWhere(['id_product'=>$ids])
                ->groupBy(['id_product'])
                ->orderBy('id ASC')->indexBy('id_product');
        
//        $query->andWhere($condition['string'],$condition['param']);
        
//        Utils::d($query->createCommand($this->connection)->getRawSql());

        $rsImages = $query->all($this->connection);
        
        
        $suppliers = \common\models\Suppliers::loadAll();
        $brands = \common\models\Brands::loadAll();
        $categories = \common\models\Categories::loadAll();
//        Utils::d($suppliers,1);
        
        
        $featuresModel = new \common\models\ProductFeature();
//Utils::d($rsImages,1);
        $data = [];
        foreach ($rs as $row):
            $row['id'] = intval($row['product_id']);
            $row['supplier_name'] = isset($suppliers[$row['store_id']]) ? $suppliers[$row['store_id']]['name'] : '';
            $row['supplier_image'] = isset($suppliers[$row['store_id']]) ? $suppliers[$row['store_id']]['image'] : '';
//            $productIds[] = $row['id'];
            $row['product_image'] = $row['image'];
            
            $row['image'] = isset($rsImages[$row['id']]) ? $rsImages[$row['id']]['image_path'] : "";
            
            $row['brands_folder_slug'] = isset($brands[$row['brands_brand_id']]) ? $brands[$row['brands_brand_id']]['folder_slug'] : '';
            
            if(isset($categories[$row['categories_category_id']])):
                $category = $categories[$row['categories_category_id']];
                $row['categories'] = [
                    'id' => $category['category_id'],
                    'name' => $category['category_name'],
                    'slug' => $category['slug'],
                    'folder_slug' => $category['folder_slug'],
			'preset' => $category['image_scrap_preset'],
                ];
            endif;
            
            
//            $features = $featuresModel->getFeaturesForFilter([$row['id']]);
//            if(isset($features[$row['id']])):
//                $row['features'] = $features[$row['id']];
//            endif;
            
            /**
             * Add filters
             */
            $filters = $featuresModel->getFeaturesFilter([$row['id']]);
            foreach($filters as $groupId => $filterData):
                $row['filter_'.$groupId] = $filterData;                
            endforeach;

	
  $query1 = new \yii\db\Query();
        $select = ['store_id','product_id'];
           $query1->select($select)
                ->from(\common\models\ProductsSuppliers::tableName(). ' p')
                ->andWhere(['product_id'=>$row['id']])
                ->andWhere(['instock'=>1]);
          
		//add available product supplier
       $supplierList = $query1->all($this->connection);
        if(count($supplierList)>0)
        { $row['stores']=[];
        foreach($supplierList as $k => $v):
                $row['stores'][] = [ 'store_id'=> $v['store_id'] ];                
            endforeach;
        }
 
            $data[] = $row;
        endforeach;

        return $data;
    }





    private function loadDataForRecentUpdate() {

//        Utils::d($this->categories,1);
        $query = new \yii\db\Query();
        $select = ['p.*'];
        $query->select($select)
                ->from($this->table . ' p')
                ->indexBy('product_id');

            $fiveminbeforetime = date('Y-m-d H:i:s', strtotime("-15 min"));
            $condition['string'] = 'p.updated >= :start';
            $condition['param'] = [':start'=>$fiveminbeforetime];
      
        $query->andWhere($condition['string'],$condition['param']);
        

//        Utils::d($query->createCommand($this->connection)->getRawSql());

        $rs = $query->all($this->connection);
        
        $query = new \yii\db\Query();
        $select = ['id_product','image_path'];
        
        if(count($rs) <=0){
            return null;
        }
        $ids = count($rs) > 0 ? array_keys($rs) : [0];
        
        $query->select($select)
                ->from(\common\models\ProductImage::tableName(). ' p')
                ->andWhere(['id_product'=>$ids])
                ->groupBy(['id_product'])
                ->orderBy('id ASC')->indexBy('id_product');
        
//        $query->andWhere($condition['string'],$condition['param']);
        
//        Utils::d($query->createCommand($this->connection)->getRawSql());

        $rsImages = $query->all($this->connection);
        
        
        $suppliers = \common\models\Suppliers::loadAll();
        $brands = \common\models\Brands::loadAll();
        $categories = \common\models\Categories::loadAll();
//        Utils::d($suppliers,1);
        
        
        $featuresModel = new \common\models\ProductFeature();
//Utils::d($rsImages,1);
        $data = [];
        foreach ($rs as $row):
            $row['id'] = intval($row['product_id']);
            $row['supplier_name'] = isset($suppliers[$row['store_id']]) ? $suppliers[$row['store_id']]['name'] : '';
            $row['supplier_image'] = isset($suppliers[$row['store_id']]) ? $suppliers[$row['store_id']]['image'] : '';
//            $productIds[] = $row['id'];
            $row['product_image'] = $row['image'];
            
            $row['image'] = isset($rsImages[$row['id']]) ? $rsImages[$row['id']]['image_path'] : "";
            
            $row['brands_folder_slug'] = isset($brands[$row['brands_brand_id']]) ? $brands[$row['brands_brand_id']]['folder_slug'] : '';
            
            if(isset($categories[$row['categories_category_id']])):
                $category = $categories[$row['categories_category_id']];
                $row['categories'] = [
                    'id' => $category['category_id'],
                    'name' => $category['category_name'],
                    'slug' => $category['slug'],
                    'folder_slug' => $category['folder_slug'],
			'preset' => $category['image_scrap_preset'],
                ];
            endif;
            
            
//            $features = $featuresModel->getFeaturesForFilter([$row['id']]);
//            if(isset($features[$row['id']])):
//                $row['features'] = $features[$row['id']];
//            endif;
            
            /**
             * Add filters
             */
            $filters = $featuresModel->getFeaturesFilter([$row['id']]);
            foreach($filters as $groupId => $filterData):
                $row['filter_'.$groupId] = $filterData;                
            endforeach;

	
  $query1 = new \yii\db\Query();
        $select = ['store_id','product_id'];
           $query1->select($select)
                ->from(\common\models\ProductsSuppliers::tableName(). ' p')
                ->andWhere(['product_id'=>$row['id']])
                ->andWhere(['instock'=>1]);
          
		//add available product supplier
       $supplierList = $query1->all($this->connection);
        if(count($supplierList)>0)
        { $row['stores']=[];
        foreach($supplierList as $k => $v):
                $row['stores'][] = [ 'store_id'=> $v['store_id'] ];                
            endforeach;
        }
 
            $data[] = $row;
        endforeach;

        return $data;
    }


    /**
     * Get names of the categories to be put in index
     * @param array $catIds int
     * dateStart, idStart
     * @return 
     */
    private function getCountToIndex($options = []) {

        $query = new \yii\db\Query();

        $query->select(['total' => new \yii\db\Expression('COUNT(*)')])
                ->from($this->table);

        if (isset($options['dateStart']) && isset($options['dateEnd'])):
            $query->andWhere("datetime >= :dateStart", ['dateStart' => $options['dateStart']]);
        elseif (isset($options['idStart'])):
            $query->andWhere("id >= :idStart", [':idStart' => $options['idStart']]);
        endif;
//        Utils::d($query->createCommand()->getRawSql(),1);
        $rs = $query->createCommand($this->connection)->queryOne();
        $total = 0;
        if ($rs !== false):
            $total = $rs['total'];
        endif;
        return $total;
    }

}
