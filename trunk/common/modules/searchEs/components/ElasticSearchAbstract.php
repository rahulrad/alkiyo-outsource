<?php

namespace common\modules\searchEs\components;

use yii\base\Component;
//use common\components\Utils;




class ElasticSearchAbstract extends Component {
    
    public $esCommand = null;
    public $index;
    public $type;

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->esCommand = new \common\components\ElasticCommand(['db' => \Yii::$app->elasticsearch]);
    }
    
    /**
     * Converts string words in *word* string.
     * String "foor bar" will become "*foo* *bar*".
     * @param string $term
     * @return string
     */
    public static function wildcardWords($term) {

        if (trim($term) == ""):
            return $term;
        endif;
        $words = explode(' ', $term);
        array_walk($words, 'trim');
        $newTerm = '*' . implode('* *', $words) . '*';
        return $newTerm;
    }

    /**
     * Make words of string pipe separated.
     * String "foor bar" will become "foo | bar".
     * @param string $term
     * @return string
     */
    public static function orWords($term) {

        if (trim($term) == ""):
            return $term;
        endif;
        $words = explode(' ', $term);
        array_walk($words, 'trim');
//        Util::debug(explode(' ',$term),1);
        $newTerm = implode(' | ', $words);
        return $newTerm;
    }

    /**
     * Converts string words to wildcard and or format.
     * String "foor bar" will become "*foo* | *bar*".
     * @param type $term
     * @return type
     */
    public static function orWildcardWords($term) {

        if (trim($term) == ""):
            return $term;
        endif;
        $term = self::wildcardWords($term);
        $term = self::orWords($term);

//        $newTerm = implode(' | ', $words);
        return $term;
    }
}
