<?php

namespace common\modules\searchEs\components;

use common\modules\searchEs\components;
use common\components\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ElasticSearchProducts extends ElasticSearchAbstract {

    var $allowedSort = ['relevance','popular', 'new', 'price_high', 'price_low', 'discount'];
    
    private $filterGroups = null;
    
    public function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * 
     * @param type $params
     * term
     */
    function find($params) {

        $term = $params['term'];

        $category = isset($params['category']) && is_array($params['category']) ? $params['category'] : [];
        $brands = isset($params['brand']) && is_array($params['brand']) ? $params['brand'] : [];
        $attributes = isset($params['attribute']) && is_array($params['attribute']) ? $params['attribute'] : [];
        $perPage = isset($params['perpage']) && intval($params['perpage']) > 0 ? intval($params['perpage']) : 20;
        $page = isset($params['page']) && intval($params['page']) > 0 ? intval($params['page']) : 1;
        $sort = isset($params['sort']) && in_array($params['sort'], $this->allowedSort) ? $params['sort'] : 'relevance';
        
        $priceMax = isset($params['price_max']) ? floatval($params['price_max']) : null;
        $priceMin = isset($params['price_min']) ? floatval($params['price_min']) : null;
        
        $filterIds = isset($params['filter_group_item']) && is_array($params['filter_group_item']) ? $params['filter_group_item'] : [];
        $storeIds = isset($params['store']) && is_array($params['store']) ? $params['store'] : [];

	  $products = isset($params['products']) && is_array($params['products']) ? $params['products'] : [];


        // do we need to return facets?
        $facets = isset($params['facets']) && $params['facets'] == true ? true : false;

        $model = new \common\models\NsProducts();
        $queryObj = new \yii\elasticsearch\Query();

        $queryObj->offset($perPage * ($page - 1))->limit($perPage);

        
        $groupFilters = $this->filterGroupValues($filterIds);
//        Utils::d($filterIds);
//        Utils::d($groupFilters,1);
        $mustFilter = [
            ["terms" => [ "active" => ['active']]]
        ];
        
       
	$mustFilter[] = ["terms" => [ "is_delete" => [0]]];
 
        if ($facets):
            $queryObj->addAggregation('facet_category_id', 'terms', ['field' => 'categories_category_id', 'size' => 50]);
            $queryObj->addAggregation('facet_brand_id', 'terms', ['field' => 'brands_brand_id', 'size' => 50]);
            $queryObj->addAggregation('facet_store', 'terms', ['field' => 'store_id', 'size' => 10]);

            // add complex aggrigations manually
            $queryObj->aggregations['facet_features'] = [
                'terms' => [
                    'field' => 'features.feature_id',
                    'size' => 10
                ],
                'aggregations' => [
                    "facet_value_id" => [
                        'terms' => [
                            'field' => 'features.value_id',
                            'size' => 5
                        ]
                    ]
                ]
            ];
            
            $filters = \backend\models\FilterGroups::loadAll();
            foreach($filters as $filter):
                $key = 'filter_'.$filter['id'];
                $queryObj->aggregations['facet_filter_'.$filter['id']] = [
                    'terms' => [
                        'field' => $key.'.group_id',
                        'size' => 5
                    ],
                    'aggregations' => [
                        "facet_item_id" => [
                            'terms' => [
                                'field' => $key.'.items.item_id',
                                'size' => 10,
                                'min_doc_count' => 0
                            ]
                        ]
                    ]
                ];
                
                // set filter as well
               if(isset($groupFilters[$filter['id']])):
                   $mustFilter[] = ["terms" => [ "filter_".$filter['id'].'.items.item_id' => $groupFilters[$filter['id']]]];
               endif;
            endforeach; 

        endif;

//        $queryObj->addAggregation('attribute', 'terms',['field'=>'brands_brand_id']);

       
        $queryObj->orderBy($this->getSort($sort));
       

        $queryObj->from($model->tableName(), $model->type());

        

        if (count($category) > 0):
            $mustFilter[] = ["terms" => [ "categories_category_id" => $category]];
        endif;

        if (count($brands) > 0):
            $mustFilter[] = ["terms" => [ "brands_brand_id" => $brands]];
        endif;


	 if(count($products)>0):
         $mustFilter[] = ["terms" => [ "product_id" => $products]];
         endif;
        
        if (count($storeIds) > 0):
                    $mustFilter[] = ["terms" => [ "stores.store_id" => $storeIds]];
         //   $mustFilter[] = ["terms" => [ "store_id" => $storeIds]];
        endif;

        if (count($attributes) > 0):
            $mustFilter[] = ["terms" => [ "features.value_id" => $attributes]];
        endif;
        
        if ($priceMax != null && $priceMax >= 0):
            $mustFilter[] = ["range" => [ "lowest_price" => ['lte'=>$priceMax]]];
        endif;

        if ($priceMin != null && $priceMin >= 0):
            $mustFilter[] = ["range" => [ "lowest_price" => ['gte'=>$priceMin]]];
        endif;


        if ($term != null) {
            $match = [
                "query" => $term,
                "fields" => ['product_name_autocomplete^3', 'model_number'],
		        "type" => 'phrase_prefix',
            ];
        }

        $must = [];
        if (isset($match)) {
            $must['multi_match'] = $match;
        }

        $filters = [
            "bool" => [
                "must" => $mustFilter
            ]
        ];

        $query = ['bool' => ['must' => $must, 'filter' => $filters]];

       // exit;

        $queryObj->query($query);

        $command = $queryObj->createCommand();

       // Utils::d(json_encode($command->queryParts) ,1);

        $records = $command->search();
//        Utils::d($records,1);

        $return = [
            'total' => $records['hits']['total'],
            'took' => $records['took'],
        ];


        $items = [];
        foreach ($records['hits']['hits'] as $row) {
            $item = $row['_source'];
            $id = $item['product_id'];
            $items[$id] = $item;
        }
        $return['items'] = $items;

        if ($facets):
             
            $this->filterGroups = \backend\models\FilterGroups::loadAll();
            // aggrigations
            $return['facets'] = $this->facets($records['aggregations']);
        endif;
//        Utils::d($return,1);
        return $return;
    }

    private function facets($data) {

        $return = [];

        foreach ($data as $key => $values):
            switch ($key):
                case "facet_category_id":
                    $return[$key] = $this->factCategory($values);
                    break;
                case "facet_brand_id":
                    $return[$key] = $this->factBrand($values);
                    break;
                case "facet_features":
                    $return[$key] = $this->factFeatures($values);
                    break;
                case "facet_store":
                    $return[$key] = $this->factStore($values);
                    break;
                default :
                    if(strpos($key, 'facet_filter_')!== false):
                        $facet = $this->factFilterGroup($values);
                        if(count($facet) > 0):
                            $return['filters'][$key] = $facet;
                        endif;
                        
                    endif;

                    break;

            endswitch;
        endforeach;
        return $return;
    }

    private function factCategory($data) {

        $return = $ids = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
            ];
            $ids[] = $item['key'];
        endforeach;

        $model = new \common\models\Categories();
        $query = new \yii\db\Query();

        $query->from($model->tableName())
                ->where(['category_id' => $ids])
                ->select(['category_id', 'category_name']);

        $items = $query->createCommand()->queryAll();
//         Utils::d($items,1);
        foreach ($items as $item):
            $return[$item['category_id']]['name'] = $item['category_name'];
        endforeach;

        return $return;
    }

    private function factBrand($data) {

        $return = $ids = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
            ];
            $ids[] = $item['key'];
        endforeach;

        $model = new \common\models\Brands();
        $query = new \yii\db\Query();

        $query->from($model->tableName())
                ->where(['brand_id' => $ids])
                ->select(['brand_id', 'brand_name']);

        $items = $query->createCommand()->queryAll();
//         Utils::d($items,1);
        foreach ($items as $item):
            $return[$item['brand_id']]['name'] = $item['brand_name'];
        endforeach;

        return $return;
    }
    
    private function factStore($data) {

        $return = $ids = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
            ];
            $ids[] = $item['key'];
        endforeach;

        $model = new \common\models\Suppliers();
        $query = new \yii\db\Query();

        $query->from($model->tableName())
                ->where(['id' => $ids])
                ->select(['id', 'name']);

        $items = $query->createCommand()->queryAll();
//         Utils::d($items,1);
        foreach ($items as $item):
            $return[$item['id']]['name'] = $item['name'];
        endforeach;

        return $return;
    }

    private function factFeatures($data) {

        $return = $ids = $valueIds = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
                'values' => $this->factValues($item['facet_value_id']),
            ];

            $ids[] = $item['key'];
        endforeach;

        $model = new \common\models\Features();
        $query = new \yii\db\Query();

        $query->from($model->tableName())
                ->where(['id' => $ids])
                ->select(['id', 'name']);

        $items = $query->createCommand()->queryAll();
//         Utils::d($items,1);
        foreach ($items as $item):
            $return[$item['id']]['name'] = $item['name'];
        endforeach;

//        Utils::d($return, 1);
        return $return;
    }
    
    private function factFilterGroup($data) {
//        Utils::d($data,1);
        $return = $ids = $valueIds = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
		    'sort' => isset($this->filterGroups[$item['key']]) ? $this->filterGroups[$item['key']]['sort_order'] : '0',
             
                'name' => isset($this->filterGroups[$item['key']]) ? $this->filterGroups[$item['key']]['title'] : '',
                'values' => $this->factFilterValues($item['facet_item_id']),
            ];

//            $ids[] = $item['key'];
        endforeach;

//        Utils::d($return, 1);
        return $return;
    }
    
    private function factFilterValues($data) {

       
        $return = $ids = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
            ];
            $ids[] = $item['key'];
        endforeach;

        $model = new \backend\models\FilterGroupItems();
        $query = new \yii\db\Query();

        $query->from($model->tableName())
                ->where(['id' => $ids])
                ->select(['id', 'title','sort_order']);

        $items = $query->createCommand()->queryAll();
//         Utils::d($items,1);
        foreach ($items as $item):
            $return[$item['id']]['name'] = $item['title'];
$return[$item['id']]['sort'] = $item['sort_order'];
        endforeach;

//        Utils::d($return,1);
        return $return;
    }

    private function factValues($data) {

        $return = $ids = [];

        foreach ($data['buckets'] as $item):
            $return[$item['key']] = [
                'key' => $item['key'],
                'count' => $item['doc_count'],
            ];
            $ids[] = $item['key'];
        endforeach;

        $model = new \common\models\FeatureValues();
        $query = new \yii\db\Query();

        $query->from($model->tableName())
                ->where(['id' => $ids])
                ->select(['id', 'value']);

        $items = $query->createCommand()->queryAll();
//         Utils::d($items,1);
        foreach ($items as $item):
            $return[$item['id']]['name'] = $item['value'];
        endforeach;

//        Utils::d($return,1);
        return $return;
    }
    
    /**
     * Get array which can be used as sorting for elastic object
     * @param string $key @see allowedSort
     * @return array
     */
    function getSort($key){
        // 'relevance','popular', 'new', 'price_high', 'price_low'
        switch ($key):
            case 'new':
                $return = ['instock'=> SORT_DESC ,'product_id' => SORT_DESC, '_score' => SORT_DESC];
                break;
            case 'popular':
                $return = ['instock'=> SORT_DESC ,'rating' => SORT_DESC, '_score' => SORT_DESC];
                break;
            case 'price_high':
                $return = ['instock'=> SORT_DESC ,'lowest_price' => SORT_DESC, '_score' => SORT_DESC];
                break;
            case 'price_low':
                $return = ['instock'=> SORT_DESC ,'lowest_price' => SORT_ASC, '_score' => SORT_DESC];
                break;
            case 'discount':
                	$return = ['instock'=> SORT_DESC ,'discount' => SORT_DESC, '_score' => SORT_DESC];
                	break;
            default :
                $return = ['instock'=> SORT_DESC ,'_score' => SORT_DESC];
                break;
        endswitch;
        
        return $return;
    }
    
    /**
     * 
     * @param array $filterIds with values like 23_9 (group_item)
     * @return array
     */
    private function filterGroupValues($filterIds){
        $result = []; 
        foreach($filterIds as $filterId):
            $tmp = explode('_', $filterId);
            if(count($tmp) == 2):
                $result[$tmp[0]][] = (int) $tmp[1];
            endif;
        endforeach;
        return $result;
    }

}
