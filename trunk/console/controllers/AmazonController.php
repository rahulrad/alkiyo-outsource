<?php

namespace console\controllers;
use yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;//
use backend\models\CategoryUrl;
use backend\models\Mobile;
use backend\models\Logs;
use common\components\AmazonECS;
use backend\models\ProductUrls;
use common\models\Categories;
use common\components\HttpService;
use common\components\Aws;


class AmazonController extends Controller {


	 public function actionTest() {
		 
        $aws = \Yii::$app->awssdk->getAwsSdk();
        $sqs = $aws->createSqs();
		$queueUrl = \Yii::$app->awssdk->extra['queueUrl'];

        try {
	
			$productUrls =  ProductUrls::find()->select(['id','url','store_id'])->where(['status' => 0])->one();
			//echo '<pre>'; print_r($productUrls); die;
            $response = $sqs->sendMessage([
                    'QueueUrl' => $queueUrl, 
                    'MessageBody' => yii\helpers\Json::encode($productUrls)
                ]);
				
			$result = $sqs->receiveMessage(array(
				'QueueUrl' => $queueUrl,
			));
			
			
			echo '<pre>'; print_r($result); die;
			
			/**foreach ($result->getPath('Messages//Body') as $messageBody) {
				// Do something with the message
				echo $messageBody;
			} **/
			
			$deleteResult = $sqs->deleteMessageBatch(array(
					// QueueUrl is required
					'QueueUrl' => $queueUrl,
					// Entries is required
					'Entries' => array(
						array(
							// Id is required
							'Id' => 'test',
							// ReceiptHandle is required
							'ReceiptHandle' => 'AQEBgF8TyfLRds6g69XJl+JE3v5sv+I7M4Y99XOBnBVN29NbpXJIGeHduMcN0h8E0hEfTwoHrh0WZvCBavvlPioD8Hg5olF2zFP1qWlsBpFAxcu8emLANToIpF5h/TjytiyExuYx1iILOB1fvZNWeg3g7U1Cd3lYu1dvrvr+DCte2WMb57H9OxHq/eOE8Iu4SQVBnKwSvdAwe1MnMlmkRYsh6gzAl2EmFVabc/WUionSDPUhop663Y96gyw/HkkMjaA2Fyj7beJ63h9e5MpCKTwDK9QTVTYdOhZilBziY9ozfLqU0jFL9qnayKs+YQNnPWfx+Jm4e+H+9Tsu1uBwHPnVWWGnm8vYdt69jA6O5p3rLNgoQuB7TjrpmU7AuCOqOQb+ioGuKPIivLXs6T/nEcr2Yg==',
						),
						// ... repeated
					),
				)); 
            echo '<pre>'; print_r($deleteResult); die;
        } catch (\Exception $e) {
            echo "SQS Not added: ".$e->getMessage();
        }
    }
    public function actionIndex() {
		echo phpinfo();
        echo "Hello world.";
    }
    
    public function actionHello() {
        echo "eeeee";exit;
    }
    
//	public function actionSaveCatUrl(){
//		$categories_data_model = new CategoriesData;
//		$categoriesDatas = CategoriesData::find()->where(['type' => 0,'store_id'=>5])->all();
//
//
//		 if(!empty($categoriesDatas)){
//				foreach($categoriesDatas as $categoriesData){
//
//				// $model = CategoriesData::find($categoriesData->id)->one();
//				 $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
//				 $model->type = 1;
//			     $model->save();
//
//				$count_products = $model->count_products;
//				$perpage = 10;
//				$list_page = $count_products / $perpage;
//				$pages = round($list_page);
//				$x  = 1;
//				$y = 1;
//
//				$categoryUrlModel = new CategoryUrl;
//				$countLastInsertUrls = $categoryUrlModel->getLastInsertCategoryUrl($categoriesData);
//				$pages = $pages + $countLastInsertUrls; // count pages aur last insert url for same category url
//				$i_value = ($countLastInsertUrls != 0) ? $countLastInsertUrls + 1 : 1;
//				for($i = $i_value; $i<=$pages; $i++){
//
//
//					$category_urls = 'amazon_api/index.php?category='.$categoriesData->url.'&sub_category='.$categoriesData->category_name.'&page_number='.$i;
//
//
//					 $category_url_model = new CategoryUrl;
//					 $category_url_model->status = 0;
//					 $category_url_model->store_name = 'amazon';
//					  $category_url_model->store_id = 5;
//					 $category_url_model->category_name = $categoriesData->category_name;
//					 $category_url_model->category_type = $categoriesData->category_type;
//					 $category_url_model->site_url = $category_urls;
//
//
//					$category_url_model->scraper_group =$y;
//							 if($x==10)
//							 {
//								$y++;
//								$x=0;
//							 }
//					$category_url_model->save();
//					// echo 'pass'. $category_url_model->category_name.'<br>';
//					$x++;
//				}
//
//			}
//			echo 'save category url for snapdeal'; die;
//		 }else{
//				$scraperLog = new Logs();
//				$scraperLog->saveScraperLog('amazon scraper','AmazonController','No Any Category Data Url for amazon','Output',__LINE__);
//				echo 'No Any Category Data for Save category Url for amazon'; die;
//		}
//
//	}

    public function actionSaveCatUrl()
    {
        $categories_data_model = new CategoriesData;
        $categoriesDatas = CategoriesData::find()->where(['type' => 0, 'store_id' => 5])->all();


        if (!empty($categoriesDatas)) {
            foreach ($categoriesDatas as $categoriesData) {

                // $model = CategoriesData::find($categoriesData->id)->one();
                $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
                $model->type = 1;
                $model->save();

                $category_url_model = new CategoryUrl;
                $category_url_model->status = 0;
                $category_url_model->store_name = 'amazon';
                $category_url_model->store_id = 5;
                $category_url_model->category_name = $categoriesData->category_name;
                $category_url_model->category_type = $categoriesData->category_type;
                $category_url_model->site_url = $model->url;
                $category_url_model->scraper_group = 1;
                $category_url_model->save();
            }
            echo 'save category url for snapdeal';
            die;
        } else {
            $scraperLog = new Logs();
            $scraperLog->saveScraperLog('amazon scraper', 'AmazonController', 'No Any Category Data Url for amazon', 'Output', __LINE__);
            echo 'No Any Category Data for Save category Url for amazon';
            die;
        }

    }


    public function actionScraperProductUrls($latest_product = 0)
    {

        $max_scraper_group = CategoryUrl::find()->where(['store_id' => 5])->orderBy(['scraper_group' => SORT_DESC])->one();

        if (!empty($max_scraper_group)) {

            for ($i = 1; $i <= $max_scraper_group->scraper_group; $i++) {

                $categories_url_model = new CategoryUrl;
                $categories_urls = CategoryUrl::find()->where(['status' => 0, 'store_id' => 5, 'scraper_group' => $i])->all();

                if (!empty($categories_urls)) {
                    foreach ($categories_urls as $categories_url) {

                        $objsnapdealProduct = new \backend\models\AmazonProductScrapper();

                        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
                        $siteName = 'http://www.amazon.com';
                        // for men shoes category
                        //$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
                        $url = $categories_url->site_url;
                        $categoryName = $categories_url->category_name;
                        $categoryType = $categories_url->category_type;
                        $scraper_group = $i;

                        //$objsnapdealProduct->saveProductUrls($categoryName, '', $categoryType, $latest_product,'snapdeal',$scraper_group);

                        $this->getProductFromCurl($categories_url);

                    }
                    //echo 'save products for flipkart'; die;
                }
            } //die('sadfasd');
        } else {
            $scraperLog = new Logs();
            $scraperLog->saveScraperLog('amazon scraper', 'AmazonController', 'No any category url for scrapping', 'Output', __LINE__);
            echo 'No Any URLs for scurpping';
            die;
        }

    }


    public function getProductFromCurl($categories_url)
    {
        $objHttpService = new \common\components\HttpService();
        $proxy = $objHttpService->getProxyIps();

        $curl = curl_init();
        curl_setopt_array($curl, array(
//							CURLOPT_URL => $categories_url->site_url.'?q=Connectivity_s%3A4G%20LTE%7CScreensize_s%3A4.0%20-%204.5%5E5.0%20-%205.5%7C&sort=plrty&brandPageUrl=&keyword=&searchState=categoryRedirected=mobiles-mobile-phones|previousRequest=true|serviceabilityUsed=false&pincode=&vc=&webpageName=categoryPage&campaignId=&brandName=&isMC=false&clickSrc=unknown&showAds=true&cartId=',
            CURLOPT_URL => $categories_url->site_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_PROXY => $proxy['proxy_url'],
            CURLOPT_PROXYUSERPWD => $proxy['proxy_user'] . ':' . $proxy['proxy_pwd'],
            //CURLOPT_PROXYTYPE=> CURLPROXY_SOCKS5,
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "Accept-Encoding: gzip, deflate, sdch, br",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
                "referer: https://www.snapdeal.com/products/mobiles-mobile-phones?sort=plrty&q=Connectivity_s%3A4G%20LTE%7CScreensize_s%3A4.0%20-%204.5%5E5.0%20-%205.5%7C",
                "user-agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);
        //	echo '<pre>curl_getinfo==>';print_r(curl_getinfo($curl));
        //	echo '<pre>curl_error==>';print_r(curl_error($curl));

        curl_close($curl);

        //	echo '<pre>proxyDetail=>'; print_r($proxy);
        //	echo '<pre>Server=>'; print_r($_SERVER);
        //	echo '<pre>ssss'; print_r($response); die;


        if ($err) {
            return 'error';
        } else {

            $objSnapDealProduct = new \backend\models\AmazonProductScrapper();
            $objSnapDealProduct->saveProductUrls($response, $categories_url);

        }


    }
 
 
  
		
public function actionScraper($latest_product=0) {
		
		$product_urls = ProductUrls::find()->where(['store_id' => 5,'status' => 0])->all();
		
			if(!empty($product_urls)){
					
					foreach($product_urls as $product_url){
						
						/**$categories_url_model = new CategoryUrl;
						$categories_urls= CategoryUrl::find()->where(['id_category_url' => $product_url->cateogry_url_id])->one(); **/
						
						$categories_urls = Categories::find()->where(['category_id' => $product_url->category_id])->one();
						$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
					
						$objflipkartProduct = new \backend\models\AmazonProductScrapper();
						$objflipkartProduct->saveProductData($product_url,$category_name);
						
					}
					
			}else{
					$scraperLog = new Logs();
					$scraperLog->saveScraperLog('amazon scraper','AmazonController','No any category url for scrapping','Output',__LINE__);
					echo 'No Any URLs for scurpping'; die;
			}
		
	
	}

 
 /** public function actionScraper($latest_product=0) {
			
		$categories_url_model = new CategoryUrl;
		$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 5,'scraper_group' => 1])->all();
		//echo '<pre>'; print_r($categories_urls); die;
		 if(!empty($categories_urls)){
			 foreach($categories_urls as $categories_url){
					
					$objamazonProduct = new \backend\models\AmazonProductScrapper();
					
					// $tools_componant = new \common\components\Tools();
					// $tools_componant->alertMail("Create a new brand named", "Create Brand", 'vinayverma158@gmail.com'); die;
					//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
					$siteName = 'http://www.amazon.com';
					// for men shoes category
					//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
					$url = $categories_url->site_url;
					$categoryName = $categories_url->category_name;
					$categoryType = $categories_url->category_type;
					//echo $categoryName.'<br>';
						$objamazonProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'amazon');
			
				
			}
			echo 'save products for snapdeal'; die;
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('amazon scraper','AmazonController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
	
	} **/
	
	/*public function actionScraper($latest_product=0) {
		
		$objSnapDealProduct = new \backend\models\SnapDealProductScrapper();
        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
        $siteName = 'http://www.snapdeal.com';
        // for men shoes category
        $url = $siteName . '/acors/json/product/get/search/57/0/48?q=&sort=plrty&brandPageUrl=&keyword=&vc=&webpageName=categoryPage&campaignId=&brandName=&isMC=false&clickSrc=';
        $categoryName = 'computer';
        $categoryType = 'computer';
		
//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

//        $arr_department_type = array('For' => 'Men');
        $objSnapDealProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'snapdeal');
		echo '<pre>'; print_r($objSnapDealProduct); die;
    }*/

}
