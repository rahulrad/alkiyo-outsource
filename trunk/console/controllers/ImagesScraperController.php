<?php

namespace console\controllers;
use yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\ProductsSuppliers;
use common\components\CImage;
use common\models\Products;
use common\models\Brands;
use common\models\Categories;
use common\models\ProductImage;


class ImagesScraperController extends Controller {

    public function actionIndex() {
        echo "Hello world.";
    }
    
   
	
	public function actionProduct(){
		
			$productsListing = Products::find()->select(['product_id','image','categories_category_id','brands_brand_id','image_path','scraper_image_status'])->where(['scraper_image_status' => 0])->all();
			//echo '<pre>'; print_r($productsListing); die;
			
			if(!empty($productsListing)){
					foreach($productsListing as $product){
						
						if(!empty($product->image_path)){
								
									$categoryDetail = Categories::findOne(['category_id'=>  $product->categories_category_id]);
									$brandDetail = Brands::findOne(['brand_id'=>  $product->brands_brand_id]);
									
									$path = Yii::getAlias('@frontend') .'/web/';
									
									
									$folder_path = $path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug;
									
									$thumImageSizes = array('150x200','300x400');
										foreach($thumImageSizes as $size){
											
											if (!file_exists($folder_path)) {
												mkdir($folder_path, 0755, true);
											}
											
											try{
												copy($product->image_path, $folder_path.'/'.$product->image);
												
												$thumImageSizes = array('150x200','300x400');
												foreach($thumImageSizes as $size){
												$sizeArr = explode('x',$size);
												
													if (!file_exists($folder_path.'/'.$size)) {
														mkdir($folder_path.'/'.$size, 0755, true);
													}
												$destFile = $folder_path.'/'.$size.'/'.$product->image;
												$mainImage = $folder_path.'/'.$product->image;
												
												$objImageResize = new \common\components\CImage();
												$response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
													
												}
											
											} catch (\Exception $e) {
													echo 'images is not copy: ' . $e->getMessage();
											}
											
											$productModel = $product;
											$productModel->scraper_image_status = 1;
											$productModel->save(false);
										
									}
									//unlink($image); // correct			
								echo $product->product_id.'<br>';
						}
						
					} 
			}else{
					
				die('No any product url for main image resize');
				
			}
			 
			
	}
	
	
	
	
public function actionProductImages(){
		
		/**$productsImagesListing = ProductImage::find()->select(['id','id_product','image','image_path','scraper_image_status'])->andWhere(['scraper_image_status' => 0])->all();
		
			if(!empty($productsImagesListing)){
				
				foreach($productsImagesListing as $product){
					
						if(!empty($product->image_path)){
								
									$productDetail = Products::find()->where(['product_id'=> $product->id_product])->one();
									$categoryDetail = Categories::findOne(['category_id'=>  $productDetail->categories_category_id]);
									$brandDetail = Brands::findOne(['brand_id'=>  $productDetail->brands_brand_id]);
									//echo '<pre>'; print_r($categoryDetail); die;
									$path = Yii::getAlias('@frontend') .'/web/';
									
									
									$folder_path = $path.'uploads/products/'.$categoryDetail->folder_slug.'/'.$brandDetail->folder_slug;
									
									
									$thumImageSizes = array('150x200','300x400');
										foreach($thumImageSizes as $size){
											
											if (!file_exists($folder_path)) {
												mkdir($folder_path, 0755, true);
											}
											
											//$image  = $path.'uploads/products_img/'.$product->image;
											try{
												copy($product->image_path, $folder_path.'/'.$product->image);
												//echo 'image=>'.$image.'<br>';
												$thumImageSizes = array('150x200','300x400');
												foreach($thumImageSizes as $size){
												$sizeArr = explode('x',$size);
												
													if (!file_exists($folder_path.'/'.$size)) {
														mkdir($folder_path.'/'.$size, 0755, true);
													}
												$destFile = $folder_path.'/'.$size.'/'.$product->image;
												$mainImage = $folder_path.'/'.$product->image;
												
												$objImageResize = new \common\components\CImage();
												$response = $objImageResize->imageResize($mainImage,$destFile,$sizeArr[1],$sizeArr[0],'jpg');
												
												
													
												}
											} catch (\Exception $e) {
												echo 'images is not copy: ' . $e->getMessage();
											}
											
											$productImagesModel = $product;
											$productImagesModel->scraper_image_status = 1;
											$productImagesModel->save(false);
											
										
										//die('pass');
									}
							//unlink($image); // correct			
						echo $product->id.'<br>';
						}
						
				}
			}else{
					
				die('No any product url for multi image resize');
				
			}
		**/	
		
}
	
	
	
	

}
