<?php
/**
 * Created by PhpStorm.
 * User: alliancetec.
 * Date: 26/01/17
 * Time: 2:15 PM
 */

namespace console\controllers;


use backend\models\ProductUrls;
use common\components\ProductExistsInMongoException;
use common\models\Brands;
use common\models\Categories;
use common\models\Products;
use common\models\ProductsSuppliers;
use PhpAmqpLib\Message\AMQPMessage;
use yii\base\Exception;
use yii\console\Controller;

class ProductUrlScrapperController extends Controller
{

    private static $AMAZON_STORE_ID = 5;


    public function actionCheckpid()
    {
        $result = exec("ps aux | grep bulk-scrap", $output, $return);
        if (!$return) {
            return count($output);
        } else {
            echo "PDF not created";
        }


    }

    /**
     * method to bulk import
     */
    public function actionBulkScrap()
    {
        exec("ps aux | grep bulk-scrap", $output, $return);
        if (!$return && count($output) < 5) {
            echo "running process at " . date('Y-m-d h:i:sa') . "\n/n";
            print_r($output);
            echo "\n/n";
            $productUrls = ProductUrls::getPendingProductsUrlToScrap();
            foreach ($productUrls as $productUrl) {
                if ($productUrl->store_id == self::$AMAZON_STORE_ID) {
                    sleep(rand(5, 13));
                }
                $categories_urls = Categories::find()->where(['category_id' => $productUrl->category_id])->one();
                $category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
                $objProductScraper = $this->getStoreScraper($productUrl->store_id);
                if (!is_null($objProductScraper)) {
                    try {
//save url
                        $objProductScraper->saveProductData($productUrl, $category_name);
                    } catch (ProductExistsInMongoException $e) {
                        error_log(var_export($e->getMessage(), true));
                        error_log(var_export($e->getTraceAsString(), true));
                        $productUrl->scrap_error = $e->getMessage();
                        $productUrl->scrap_attempt = 10;
                        $productUrl->save();
                    } catch (Exception $e) {
                        error_log(var_export($e->getMessage(), true));
                        error_log(var_export($e->getTraceAsString(), true));
                        $productUrl->scrap_error = $e->getMessage();
                        $productUrl->status = 0;
                        $productUrl->scrap_attempt = $productUrl->scrap_attempt + 1;
                        $productUrl->save();
                    }
                }
            }
        } else {

            echo "skipping process as earlier process is runningi at" . date('Y-m-d h:i:sa') . "/n\n";
            print_r($output);

            echo "\n/n";
        }

    }

    /**
     * @param $storeId
     * @return null
     */
    private function getStoreScraper($storeId)
    {
        if ($storeId == 1) {
            return new \backend\models\FlipKartProductScrapper();

        } elseif ($storeId == 3) {
            return new \backend\models\SnapDealProductScrapper();

        } elseif ($storeId == 5) {
            return new \backend\models\AmazonProductScrapper();

        } elseif ($storeId == 6) {
            return new \backend\models\JabongProductScrapper();

        } elseif ($storeId == 7) {
            return new \backend\models\EbayProductScrapper();
        } else {
            return null;
        }
    }

    public function receiveCallback(AMQPMessage $message)
    {
        $productSupplierId = $message->body;
        \Yii::$app->rabbit->sendACK($message);
        $productSupplier = ProductsSuppliers::find()->where(['id' => $productSupplierId])->one();
        if (!is_null($productSupplier)) {
            $product = $productSupplier->product;
            $productCategory = $product->categoriesCategory;
            $objProductScraper = $this->getStoreScraper($productSupplier->store_id);
            try {
                $objProductScraper->saveProductSupplierData($productSupplier, $productCategory->category_name);
            } catch (Exception $e) {
                error_log(var_export($e->getMessage(), true));
                error_log(var_export($e->getTraceAsString(), true));
            }
        }
    }


    public function actionRead()
    {
        $rabbit = \Yii::$app->rabbit;
        $rabbit->receive([$this, 'receiveCallback']);
    }

    public function actionProductImageScrap()
    {
        $products = Products::find()->where(['image_scrapped' => 0])->limit(10)->all();
        foreach ($products as $product) {
            print_r("scrapping image prouct id=" . $product->product_id);
            $brandDetail = Brands::find()->where(['brand_id' => $product->brands_brand_id])->one();
            $categoryDetail = Categories::find()->where(['category_id' => $product->categories_category_id])->one();
            $product->image = Products::saveProductImage($categoryDetail, $brandDetail, $product->image_path);
            $product->image_scrapped = 1;
            foreach ($product->productImages as $productImage) {
                $productImage->image = Products::saveProductImage($categoryDetail, $brandDetail, $productImage->image_path, "other");
                $productImage->save();
            }
            $product->save();
        }
    }

    public function actionImgScrap($categoryId = 0)
    {
        $query = Products::find()->where(['store_id' => self::$AMAZON_STORE_ID])->orderBy("product_id desc")->limit(3000);
        if ($categoryId > 0) {
            $query->andWhere(['categories_category_id' => $categoryId]);
        }
        $products = $query->all();

        foreach ($products as $product) {
            $category = $product->categoriesCategory;
            $brand = $product->brandsBrand;
            $objProductScraper = $this->getStoreScraper($product->store_id);
            try {
                $objProductScraper->updateProductImages($product, $category, $brand);
            } catch (Exception $e) {
                error_log(var_export($e->getMessage(), true));
                error_log(var_export($e->getTraceAsString(), true));
            }
        }
    }

}
