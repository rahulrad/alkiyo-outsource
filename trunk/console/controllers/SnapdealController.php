<?php
namespace console\controllers;
use yii;

use yii\console\Controller;
use yii\helpers\Console;
use common\models\Products;
use backend\models\CategoriesData;//
use backend\models\CategoryUrl;
use backend\models\Mobile;
use backend\models\Logs;
use backend\models\ProductUrls;
use common\models\Categories;
use common\components\HttpService;
use backend\models\SnapDealProductScrapper;
use common\models\ProductImage;

class SnapdealController extends Controller {

    public function actionIndex() {
		$mobile = new Mobile();
		$mobile->test();
        echo "Hello world.";
    }
    
    public function actionHello() {
        echo "eeeee";exit;
    }


    public function actionSaveCatUrl()
    {

        $categoriesDatas = CategoriesData::find()->where(['type' => 0, 'store_id' => 3])->all();
        if (!empty($categoriesDatas)) {
            foreach ($categoriesDatas as $categoriesData) {

                $model = CategoriesData::find()->where(['id' => $categoriesData->id])->one();
                $model->type = 1;
                $model->save();

                $category_url_model = new CategoryUrl;
                $category_url_model->status = 0;
                $category_url_model->store_name = 'snapdeal';
                $category_url_model->store_id = 3;
                $category_url_model->category_name = $categoriesData->category_name;
                $category_url_model->category_type = $categoriesData->category_type;
                $category_url_model->site_url = $model->url;
                $category_url_model->scraper_group = 1;
                $category_url_model->save();
            }
            echo 'save category url for snapdeal';
            die;
        } else {
            $scraperLog = new Logs();
            $scraperLog->saveScraperLog('snapdeal scraper', 'SnapdealController', 'No Any Category Data Url for snapdeal', 'Output', __LINE__);
            echo 'No Any Category Data for Save category Url for snapdeal';
            die;
        }


    }
	
	

  public function actionScraperProductUrls($latest_product=0) {
		
		$max_scraper_group = CategoryUrl::find()->where(['store_id' => 3])->orderBy(['scraper_group' => SORT_DESC])->one();
		
		if(!empty($max_scraper_group)){
				
				for($i = 1; $i <=  $max_scraper_group->scraper_group; $i++){
					
					$categories_url_model = new CategoryUrl;
					$categories_urls= CategoryUrl::find()->where(['status' => 0,'store_id' => 3,'scraper_group' => $i])->all();
					//echo '<pre>'; print_r($categories_urls); die;
					if(!empty($categories_urls)){
						 foreach($categories_urls as $categories_url){
								
								$objsnapdealProduct = new \backend\models\SnapDealProductScrapper();
								
								//Tools::killOtherProcess("php console.php jungleeproductscrapper new");
								$siteName = 'http://www.snapdeal.com';
								// for men shoes category
								//$url = $siteName . '/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=1&ajax=true&_=1460746573007';
								$url = $categories_url->site_url;
								$categoryName = $categories_url->category_name;
								$categoryType = $categories_url->category_type;
								$scraper_group = $i;
									
								//$objsnapdealProduct->saveProductUrls($categoryName, '', $categoryType, $latest_product,'snapdeal',$scraper_group);
								
								$this->getProductFromCurl($categories_url);
							
						}
						//echo 'save products for flipkart'; die;
					}
				} //die('sadfasd');
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('snapdeal scraper','SnapdealController','No any category url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
		
  }

  
 public function getProductFromCurl($categories_url){
						//echo  $categories_url->site_url.'?q=Connectivity_s%3A4G%20LTE%7CScreensize_s%3A4.0%20-%204.5%5E5.0%20-%205.5%7C&sort=plrty&brandPageUrl=&keyword=&searchState=categoryRedirected=mobiles-mobile-phones|previousRequest=true|serviceabilityUsed=false&pincode=&vc=&webpageName=categoryPage&campaignId=&brandName=&isMC=false&clickSrc=unknown&showAds=true&cartId='; die;
							
							$objHttpService = new \common\components\HttpService();
							$proxy = $objHttpService->getProxyIps();
							
							$curl = curl_init();
							curl_setopt_array($curl, array(
//							CURLOPT_URL => $categories_url->site_url.'?q=Connectivity_s%3A4G%20LTE%7CScreensize_s%3A4.0%20-%204.5%5E5.0%20-%205.5%7C&sort=plrty&brandPageUrl=&keyword=&searchState=categoryRedirected=mobiles-mobile-phones|previousRequest=true|serviceabilityUsed=false&pincode=&vc=&webpageName=categoryPage&campaignId=&brandName=&isMC=false&clickSrc=unknown&showAds=true&cartId=',
							CURLOPT_URL => $categories_url->site_url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 30,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => false,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_PROXY => $proxy['proxy_url'],
							CURLOPT_PROXYUSERPWD =>  $proxy['proxy_user'].':'.$proxy['proxy_pwd'],
							//CURLOPT_PROXYTYPE=> CURLPROXY_SOCKS5,
							CURLOPT_HTTPHEADER => array(
								"accept: */*",
								"accept-language: en-US,en;q=0.8",
								"Accept-Encoding: gzip, deflate, sdch, br",
								"cache-control: no-cache",
								"content-type: application/json",
								"postman-token: eddf1792-5624-cc13-585b-d9ec26718c9b",
								"referer: https://www.snapdeal.com/products/mobiles-mobile-phones?sort=plrty&q=Connectivity_s%3A4G%20LTE%7CScreensize_s%3A4.0%20-%204.5%5E5.0%20-%205.5%7C",
								"user-agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
							  ),
							));

							
							$response = curl_exec($curl);
							$err = curl_error($curl);
						//	echo '<pre>curl_getinfo==>';print_r(curl_getinfo($curl));
						//	echo '<pre>curl_error==>';print_r(curl_error($curl)); 
							
							curl_close($curl);
							
						//	echo '<pre>proxyDetail=>'; print_r($proxy);
						//	echo '<pre>Server=>'; print_r($_SERVER);
						//	echo '<pre>ssss'; print_r($response); die;
							
							
							
							if ($err) {
							  return 'error';
							} else {
								
								$objSnapDealProduct = new \backend\models\SnapDealProductScrapper();
								$objSnapDealProduct->saveProductUrls($response,$categories_url);
								
							}
	 
 }
 
 
 
  
  
  
  public function actionScraper($latest_product=0) {
			
		$product_urls = ProductUrls::find()->where(['store_id' => 3,'status' => 0])->all();
		if(!empty($product_urls)){
				
				foreach($product_urls as $product_url){
					$categories_urls= Categories::find()->where(['category_id' => $product_url->category_id])->one();
					$category_name = !empty($categories_urls) ? $categories_urls->category_name : '';
					$objsnapdealProduct = new \backend\models\SnapDealProductScrapper();
					$objsnapdealProduct->saveProductData($product_url,$category_name);
					
				}
				
		}else{
				$scraperLog = new Logs();
				$scraperLog->saveScraperLog('snapdeal scraper','SnapdealController','No any product url for scrapping','Output',__LINE__);
				echo 'No Any URLs for scurpping'; die;
		}
			
		
	}
	
	
	
 
 
	/*public function actionScraper($latest_product=0) {
		
		$objSnapDealProduct = new \backend\models\SnapDealProductScrapper();
        //Tools::killOtherProcess("php console.php jungleeproductscrapper new");
        $siteName = 'http://www.snapdeal.com';
        // for men shoes category
        $url = $siteName . '/acors/json/product/get/search/57/0/48?q=&sort=plrty&brandPageUrl=&keyword=&vc=&webpageName=categoryPage&campaignId=&brandName=&isMC=false&clickSrc=';
        $categoryName = 'computer';
        $categoryType = 'computer';
		
//        $arr_url = $objflipkartProduct->getAllPagingUrl($url);
//        $objflipkartProduct->saveAllPagingUrl($arr_url, $categoryName, $categoryType);

//        $arr_department_type = array('For' => 'Men');
        $objSnapDealProduct->saveProduct($categoryName, '', $categoryType, $latest_product,'snapdeal');
		echo '<pre>'; print_r($objSnapDealProduct); die;
    }*/
	

 

	public function actionGetMissingImage(){
		$products = Products::find()->select(['product_id','image_path','url','categories_category_id'])->where(['image_path' => '', 'store_id' => 3])->all();
		
		foreach($products as $product){
							//echo '<pre>product'; print_r($product);
							$categoryName = Categories::find()->select(['category_id','category_name'])->where(['category_id' => $product->categories_category_id])->one();
							$categoryName = $categoryName->category_name;
							$product_url = $product->url;
							
							$objSnapDeal_product_detail = new \backend\models\SnapDealProductDetailScrapper($product_url, $categoryName);
							$productDetail =  $objSnapDeal_product_detail->StoreDataInArray();
							//echo '<pre>productDetail'; print_r($productDetail);
							$productModel = $product;
							$productModel->image_path = $productDetail['imgpath'];
							$image_name = time().'.jpg';
							$productModel->scraper_image_status = 0;
							$productModel->image = $image_name;
							$productModel->save(false);
							
							
							if(!empty($productDetail['product_img_arr'])){
								$exitProductImages = ProductImage::find()->where(['id_product' => $product->product_id])->one();
								
								if(empty($exitProductImages)){
									$i = 1;
									foreach($productDetail['product_img_arr'] as $product_image){
										 $product_image_model = new ProductImage;
										 $image = str_replace('400x400','1100x1100',$product_image);
										 $image_name = time().$i.'.jpg';
										 $product_image_model->id_product = $product->product_id;
										 $product_image_model->image_path = $image;
										 $product_image_model->image = $image_name;
										 $product_image_model->scraper_image_status = 0;
										 $product_image_model->created = date('Y-m-d h:i:s');
										 $product_image_model->save();
									$i++;	
									}
								}
							}
				
				}
			}
 

}
