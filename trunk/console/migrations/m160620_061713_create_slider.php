<?php

use yii\db\Migration;

class m160620_061713_create_slider extends Migration
{
    public function up()
    {
       $this->addColumn('sliders', 'link', 'VARCHAR(255) NULL AFTER category_id');
    }

    public function down()
    {
        $this->dropTable('slider');
    }
}
