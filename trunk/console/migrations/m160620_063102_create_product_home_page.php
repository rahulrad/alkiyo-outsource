<?php

use yii\db\Migration;

class m160620_063102_create_product_home_page extends Migration
{
    public function up()
    {
        $this->addColumn('products', 'show_home_page', 'INT(1) NOT NULL DEFAULT 0 AFTER meta_desc');
    }

    public function down()
    {
        $this->dropTable('products');
    }
}
