<?php

use yii\db\Migration;

class m160620_142323_products extends Migration
{
    public function up()
    {
		$this->alterColumn('products', 'store_name','INT(11)');
    }

    public function down()
    {
        echo "m160620_142323_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
