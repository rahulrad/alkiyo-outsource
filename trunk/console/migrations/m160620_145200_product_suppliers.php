<?php

use yii\db\Migration;

class m160620_145200_product_suppliers extends Migration
{
    public function up()
    {
		$this->addColumn('products_suppliers', 'store_id','INT(11) NOT NULL DEFAULT 0 AFTER store_name');
    }

    public function down()
    {
        echo "m160620_145200_product_suppliers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
