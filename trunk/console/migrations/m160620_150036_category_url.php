<?php

use yii\db\Migration;

class m160620_150036_category_url extends Migration
{
    public function up()
    {
$this->addColumn('category_url', 'store_id','INT(11) NOT NULL DEFAULT 0 AFTER store_name');
    }

    public function down()
    {
        echo "m160620_150036_category_url cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
