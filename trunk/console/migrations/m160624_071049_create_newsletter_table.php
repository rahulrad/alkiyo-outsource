<?php

use yii\db\Migration;

class m160624_071049_create_newsletter_table extends Migration
{
    public function up()
    {
        $this->createTable('newsletters', [
            'id' => $this->primaryKey(),
			'email' => $this->string(100),
			'status' => $this->integer()->defaultValue(1),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('newsletters');
    }
}
