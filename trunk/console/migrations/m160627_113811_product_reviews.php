<?php

use yii\db\Migration;

class m160627_113811_product_reviews extends Migration
{
    public function up()
    {
		$this->createTable('product_reviews', [
            'id' => $this->primaryKey(),
            'user_name' => $this->string(100),
			'email' => $this->string(100),
			'product_id' => $this->integer(15),
			'review' => $this->text(),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160627_113811_product_reviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
