<?php

use yii\db\Migration;

class m160701_141404_product_supplier_add_price_status extends Migration
{
    public function up()
    {
		$this->addColumn('products_suppliers', 'price_status', 'INT(1)  NOT NULL DEFAULT 0 AFTER meta_desc');
    }

    public function down()
    {
        echo "m160701_141404_product_supplier_add_price_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
