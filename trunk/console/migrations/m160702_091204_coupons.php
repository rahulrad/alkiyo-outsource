<?php

use yii\db\Migration;

class m160702_091204_coupons extends Migration
{
    public function up()
    {
		$this->createTable('coupons', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
			'sub_title' => $this->string(255),
			'offer' => $this->string(255),
			'description' => $this->text(),
			'valid' => $this->string(25),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160702_091204_coupons cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
