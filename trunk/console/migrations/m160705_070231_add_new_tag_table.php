<?php

use yii\db\Migration;

class m160705_070231_add_new_tag_table extends Migration
{
    public function up()
    {
		$this->createTable('tags', [
            'id' => $this->primaryKey(),
            'tag' => $this->string(255),
			'type' => $this->string(50),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160705_070231_add_new_tag_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
