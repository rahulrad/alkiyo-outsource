<?php

use yii\db\Migration;

class m160705_154617_products_add_review_field extends Migration
{
    public function up()
    {
		$this->addColumn('products', 'reviews', 'VARCHAR(255) NULL AFTER rating');
    }

    public function down()
    {
        echo "m160705_154617_products_add_review_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
