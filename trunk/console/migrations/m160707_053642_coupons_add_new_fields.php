<?php

use yii\db\Migration;

class m160707_053642_coupons_add_new_fields extends Migration
{
    public function up()
    {
		$this->addColumn('coupons', 'offer_id', 'INT(11) NULL AFTER id');
		$this->addColumn('coupons', 'promo_id', 'INT(11) NULL AFTER offer_id');
		$this->addColumn('coupons', 'offer_type', 'VARCHAR(25) NULL AFTER offer');
		$this->addColumn('coupons', 'code', 'VARCHAR(50) NULL AFTER offer_type');
		$this->addColumn('coupons', 'category', 'VARCHAR(50) NULL AFTER code');
		$this->addColumn('coupons', 'offer_url', 'VARCHAR(255) NULL AFTER category');
		$this->addColumn('coupons', 'start_date', 'VARCHAR(50) NULL AFTER offer_url');
		$this->addColumn('coupons', 'expiry_date', 'VARCHAR(50) NULL AFTER start_date');
    }

    public function down()
    {
        echo "m160707_053642_coupons_add_new_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
