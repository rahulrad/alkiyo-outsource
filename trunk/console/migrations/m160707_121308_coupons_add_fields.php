<?php

use yii\db\Migration;

class m160707_121308_coupons_add_fields extends Migration
{
    public function up()
    {
		$this->addColumn('coupons', 'featured', 'INT(11) NULL AFTER expiry_date');
		$this->addColumn('coupons', 'exclusive', 'INT(11) NULL AFTER featured');
		$this->addColumn('coupons', 'ref_id', 'VARCHAR(255) NULL AFTER exclusive');
		$this->addColumn('coupons', 'link', 'VARCHAR(255) NULL AFTER ref_id');
		$this->addColumn('coupons', 'store_link', 'VARCHAR(255) NULL AFTER link');
    }

    public function down()
    {
        echo "m160707_121308_coupons_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
