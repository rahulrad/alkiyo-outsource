<?php

use yii\db\Migration;

class m160708_052217_deals_new_table extends Migration
{
    public function up()
    {
		$this->createTable('deals', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
			'product_id' => $this->integer(11),
			'discount' => $this->integer(11),
			'start_date' => $this->string(50),
			'end_date' => $this->string(50),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160708_052217_deals_new_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
