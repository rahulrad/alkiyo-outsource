<?php

use yii\db\Migration;

class m160711_101251_add_deals_urls extends Migration
{
    public function up()
    {
		$this->createTable('deal_urls', [
            'id' => $this->primaryKey(),
            'url' => $this->string(255),
			'store_id' => $this->integer(11),
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160711_101251_add_deals_urls cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
