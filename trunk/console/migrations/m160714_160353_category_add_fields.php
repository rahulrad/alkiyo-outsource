<?php

use yii\db\Migration;

class m160714_160353_category_add_fields extends Migration
{
    public function up()
    {
			$this->addColumn('categories', 'show_home_page', 'VARCHAR(255) NULL AFTER category_description');
			$this->addColumn('categories', 'sort_order', 'VARCHAR(255) NULL AFTER show_home_page');
    }

    public function down()
    {
        echo "m160714_160353_category_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
