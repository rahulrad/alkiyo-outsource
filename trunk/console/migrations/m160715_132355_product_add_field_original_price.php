<?php

use yii\db\Migration;

class m160715_132355_product_add_field_original_price extends Migration
{
    public function up()
    {
		$this->addColumn('products', 'original_price', 'INT(20) NULL AFTER lowest_price');
		$this->addColumn('products', 'rating_user_count', 'INT(20) NULL AFTER rating');
    }

    public function down()
    {
        echo "m160715_132355_product_add_field_original_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
