<?php

use yii\db\Migration;

class m160715_132424_product_suppiler_add_fields_original_price extends Migration
{
    public function up()
    {
		$this->addColumn('products_suppliers', 'original_price', 'INT(20) NULL AFTER price');
		$this->addColumn('products_suppliers', 'rating_user_count', 'INT(20) NULL AFTER rating');
    }

    public function down()
    {
        echo "m160715_132424_product_suppiler_add_fields_original_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
