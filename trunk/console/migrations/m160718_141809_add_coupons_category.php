<?php

use yii\db\Migration;

class m160718_141809_add_coupons_category extends Migration
{
    public function up()
    {
		$this->createTable('coupon_categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
			'description' => $this->text(),
			'meta_title' => $this->string(255),
			'meta_keyword' => $this->string(255),
			'meta_descrption' => $this->text(),
			'show_home' => $this->integer()->defaultValue(1),	
			'status' => $this->string(10)->defaultValue('active'),
			'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160718_141809_add_coupons_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
