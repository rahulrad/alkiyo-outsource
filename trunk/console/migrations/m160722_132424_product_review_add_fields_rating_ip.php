<?php

use yii\db\Migration;

class m160722_132424_product_review_add_fields_rating_ip extends Migration
{
    public function up()
    {
		$this->addColumn('product_reviews', 'rating', 'INT(20) NULL AFTER status');
                $this->addColumn('product_reviews', 'user_id', 'INT(20) NULL AFTER status');
                $this->addColumn('product_reviews', 'ip', 'VARCHAR(25) NULL AFTER status');
    }

    public function down()
    {
        echo "m160722_132424_product_review_add_fields_rating_ip cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
