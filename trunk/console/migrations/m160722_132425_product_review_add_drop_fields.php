<?php

use yii\db\Migration;

class m160722_132425_product_review_add_drop_fields extends Migration
{
    public function up()
    {
	$this->dropColumn('product_reviews', 'user_name');
        $this->dropColumn('product_reviews', 'email');
        $this->dropColumn('product_reviews', 'review');	
        $this->addColumn('product_reviews', 'description', 'VARCHAR(512) NULL AFTER id');
        $this->addColumn('product_reviews', 'title', 'VARCHAR(50) NULL AFTER id');
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
