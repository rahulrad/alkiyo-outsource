<?php

use yii\db\Migration;

class m160725_113811_product_reviews_rank extends Migration
{
    public function up()
    {
        $this->createTable('product_reviews_rank', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(15),
            'ip' => $this->string(100),
            'product_id' => $this->integer(15),
            'rank' => $this->integer(15),
            'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160725_113811_product_reviews_rank cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
