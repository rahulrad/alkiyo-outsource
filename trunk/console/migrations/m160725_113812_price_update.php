<?php

use yii\db\Migration;

class m160725_113812_price_update extends Migration
{
    public function up()
    {
        $this->createTable('price_update', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(15),
            'product_id' => $this->integer(15),
            'created' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m160725_113812_price_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
