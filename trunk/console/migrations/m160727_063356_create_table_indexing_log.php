<?php

use yii\db\Migration;

class m160727_063356_create_table_indexing_log extends Migration
{
    public function up()
    {
        $table = "indexing_log";
        $this->createTable($table, [
            'log_id' => $this->primaryKey()->unsigned() ,//"INT(10) UNSIGNED NOT NULL AUTO_INCREMENT",
            'index_name' => "VARCHAR(255) NOT NULL COMMENT 'Name of the index'",
            'on_date' => "DATETIME NOT NULL COMMENT 'Time when index ran'",
            'max_id' => "INT(10) UNSIGNED NOT NULL COMMENT 'Max id for document indexed in this run'",
            'memory' => "BIGINT(20) UNSIGNED NOT NULL COMMENT 'Peak memory usage'",
            'requests' => $this->integer(10)->notNull()->unsigned()->comment("Request to indexer"),
            'max_id' => $this->integer()->notNull()->unsigned(),
            'success' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '1=yes, 0-no'",
            'type'=> "ENUM('full','delta') NOT NULL COMMENT 'Full or delta'",
            'documents'=> $this->integer(10)->notNull()->unsigned()->comment("Number of documents indexed in this run"),
            'took_time'=> $this->integer(10)->notNull()->unsigned()->comment("Time took from Elasticsearch"),
            'total_time'=> $this->float()->notNull()->unsigned()->comment("Total time for indexing process"),
            'error_message'=> $this->string(5000)->null()->defaultValue(NULL)->comment("Error messages"),
        ],"ENGINE=InnoDB COMMENT='Store meta infromation about the indexing like time, id etc.'");
        
//        $this->addCommentOnTable($table, "Store meta infromation about the indexing like time, id etc.");
    }

    public function down()
    {
        echo "m160727_063356_create_table_indexing_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
