<?php

use yii\db\Migration;

class m160801_110253_category_add_new_fields extends Migration
{
    public function up()
    {
$this->addColumn('categories', 'api_icon', 'VARCHAR(255) NULL AFTER image');
    }

    public function down()
    {
        echo "m160801_110253_category_add_new_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
