<?php

use yii\db\Migration;

class m160801_123218_menus_add_field_show_on_app extends Migration
{
    public function up()
    {
$this->addColumn('menus', 'show_on_app', 'INT(1) NULL AFTER sort_order');
    }

    public function down()
    {
        echo "m160801_123218_menus_add_field_show_on_app cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
