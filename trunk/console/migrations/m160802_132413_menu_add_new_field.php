<?php

use yii\db\Migration;

class m160802_132413_menu_add_new_field extends Migration
{
    public function up()
    {
	$this->addColumn('menus', 'api_icon', 'VARCHAR(255) NULL AFTER image');
    }

    public function down()
    {
        echo "m160802_132413_menu_add_new_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
