<?php

use yii\db\Migration;

class m160804_092606_menu_add_new_field_image_name extends Migration
{
    public function up()
    {
		$this->addColumn('menus', 'image_name', 'VARCHAR(255) NULL AFTER show_on_app');
    }

    public function down()
    {
        echo "m160804_092606_menu_add_new_field_image_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
