<?php

use yii\db\Migration;

class m160805_123514_coupons_category_add_new_fields extends Migration
{
    public function up()
    {
			$this->addColumn('coupon_categories', 'image', 'VARCHAR(255) NULL AFTER name');
			$this->addColumn('coupon_categories', 'image_name', 'VARCHAR(255) NULL AFTER image');
    }

    public function down()
    {
        echo "m160805_123514_coupons_category_add_new_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
