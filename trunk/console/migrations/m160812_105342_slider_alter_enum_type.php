<?php

use yii\db\Migration;

class m160812_105342_slider_alter_enum_type extends Migration
{
    public function up()
    {
		$this->alterColumn('sliders', 'slider_type', "ENUM('home', 'category','default') AFTER image");
		$this->alterColumn('sliders', 'start_date', "DATE  AFTER link");
		$this->alterColumn('sliders', 'end_date', "DATE  AFTER start_date");
		
    }

    public function down()
    {
       
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
