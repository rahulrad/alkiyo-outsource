<?php

use yii\db\Migration;

class m160822_074212_product_add_field_update extends Migration
{
    public function up()
    {
		$this->addColumn('products', 'updated_price_date', 'DATETIME NULL AFTER created');
    }

    public function down()
    {
        echo "m160822_074212_product_add_field_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
