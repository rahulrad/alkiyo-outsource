<?php

use yii\db\Migration;

class m160824_055429_add_index_features extends Migration
{
    public function up()
    {
        $this->createIndex('idx_id_product',\common\models\ProductFeature::tableName(), 'id_product');
        $this->createIndex('idx_product_id',  \common\models\ProductsSuppliers::tableName(), 'product_id');
        $this->createIndex('idx_id_product', \common\models\ProductImage::tableName(), 'id_product');
        
    }

    public function down()
    {
        echo "m160824_055429_add_index_features cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
