<?php


use yii\db\Migration;

class m160827_093217_filter_group_item_add_field extends Migration
{
    public function up()
    {
			$this->addColumn('filter_group_items', 'feature_id', 'INT(11) NULL AFTER group_id');
    }

    public function down()
    {
        echo "m160827_093217_filter_group_item_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
