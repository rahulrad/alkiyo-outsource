<?php

use yii\db\Migration;

class m160829_114342_slider_add_new_fields extends Migration
{
    public function up()
    {
			
			$this->addColumn('slider_images', 'title', 'VARCHAR(255) NULL AFTER image');
			$this->addColumn('slider_images', 'status', 'INT(1) NOT NULL DEFAULT 0 AFTER link');
    }

    public function down()
    {
        echo "m160829_114342_slider_add_new_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
