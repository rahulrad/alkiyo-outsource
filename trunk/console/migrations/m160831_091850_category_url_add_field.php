<?php

use yii\db\Migration;

class m160831_091850_category_url_add_field extends Migration
{
    public function up()
    {
		$this->addColumn('category_url', 'scraper_group_type', 'INT(1) NOT NULL DEFAULT 0 AFTER scraper_group');
    }

    public function down()
    {
        echo "m160831_091850_category_url_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
