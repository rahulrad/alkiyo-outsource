<?php

use yii\db\Migration;

class m160901_063518_product_add_image_path_and_img_stats extends Migration
{
    public function up()
    {
		$this->addColumn('products', 'image_path', 'VARCHAR(255) NULL AFTER image');
		$this->addColumn('products', 'scraper_image_status', 'INT(1) NOT NULL DEFAULT 0 AFTER image_path');
    }

    public function down()
    {
        echo "m160901_063518_product_add_image_path_and_img_stats cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
