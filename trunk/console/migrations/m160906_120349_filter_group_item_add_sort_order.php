<?php

use yii\db\Migration;

class m160906_120349_filter_group_item_add_sort_order extends Migration
{
    public function up()
    {
		$this->addColumn('filter_group_items', 'sort_order', 'INT(11) NOT NULL DEFAULT 0 AFTER status');
    }

    public function down()
    {
        echo "m160906_120349_filter_group_item_add_sort_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
