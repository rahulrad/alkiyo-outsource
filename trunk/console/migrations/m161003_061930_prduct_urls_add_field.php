<?php

use yii\db\Migration;

class m161003_061930_prduct_urls_add_field extends Migration
{
    public function up()
    {
		$this->addColumn('product_urls', 'category_id', 'INT(11) NULL AFTER url');
    }

    public function down()
    {
        echo "m161003_061930_prduct_urls_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
