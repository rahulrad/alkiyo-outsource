<?php

use yii\db\Migration;

class m161010_102222_add_new_field_product_urls extends Migration
{
    public function up()
    {
$this->addColumn('product_urls', 'other', 'TEXT NULL AFTER status');
    }

    public function down()
    {
        echo "m161010_102222_add_new_field_product_urls cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
