<?php

use yii\db\Migration;

class m161025_072613_display_category_name extends Migration
{
    public function up()
    {
		$this->addColumn('categories', 'display_name', 'VARCHAR(255) NULL AFTER category_name');
    }

    public function down()
    {
        echo "m161025_072613_display_category_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
