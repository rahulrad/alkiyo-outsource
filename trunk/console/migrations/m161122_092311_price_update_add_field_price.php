<?php


use yii\db\Migration;

class m161122_092311_price_update_add_field_price extends Migration
{
    public function up()
    {
		$this->addColumn('price_update', 'alert_price', 'VARCHAR(255) NULL AFTER product_id');
    }

    public function down()
    {
        echo "m161122_092311_price_update_add_field_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
