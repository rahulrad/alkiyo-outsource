<?php

use yii\db\Migration;

class m161122_102150_price_update_add_field_send_mail extends Migration
{
    public function up()
    {
		 $this->addColumn('price_update', 'send_mail', 'INT(1) NOT NULL DEFAULT 0 AFTER alert_price');
    }

    public function down()
    {
        echo "m161122_102150_price_update_add_field_send_mail cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
