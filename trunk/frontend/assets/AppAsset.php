<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/app';
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/libraries/lib.css',
        'css/font-awesome.min.css',
        'css/jquery-ui.css',       
    	'css/jquery.mCustomScrollbar.css',
		'css/jquery-picZoomer.css',
		'css/owl.carousel.css',
        'css/layout.css',
        'css/style.css',
        'css/css/styles.css',
    	
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
 ];
    public $js = [
      	'js/jquery-2.2.3.min.js',
        'js/bootstrap.min.js',
        'js/jquery-ui.js',
        'js/jquery.easing.min.js',
    	'js/jquery.mCustomScrollbar.js',
		'js/owl.carousel.min.js',
		'js/jquery.picZoomer.js',
		'js/jquery.lazyload.js',
        'js/support.js',
		
    ];
    public $depends = [
        
    ];
}
