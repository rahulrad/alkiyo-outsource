<?php

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;

class CategoriesProducts extends Widget{
	public $productIds = [];
	
	public function init(){
		parent::init();
	}
	
	public function run(){
		return $this->render('@frontend/views/product/categoriesProductsWithoutBrands', ['productIds' => $this->productIds]);
	}
}
