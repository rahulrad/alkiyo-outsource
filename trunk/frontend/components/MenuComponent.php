<?php

namespace frontend\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use common\models\Menus;

class MenuComponent extends Component {

    /*public function menu() {
        $model = new Menus();
        $menus = Menus::getTopMenu();

        echo Yii::$app->controller->renderPartial('@frontend/views/site/menu', [
            'model' => $model, 'menus' => $menus,
        ]);
    }*/

	public function menu() {
        $model = new Menus();
        $menus = Menus::getTopMenu();
        $key = 'top_menu';
        $menuHTML = Yii::$app->cache->get($key);
        if($menuHTML == false){
        	$menuHTML = Yii::$app->controller->renderPartial('@frontend/views/site/menu', [
        			'model' => $model, 'menus' => $menus,
        	]);
        	Yii::$app->cache->set( $key, $menuHTML, ( 60 * 60 * 24 * 7 ) );
        }
        
        echo $menuHTML;
    }
    
    public function mobileMenu(){
    	$model = new Menus();
    	$menus = Menus::getTopMenu();
    	$key = 'mobile_top_menu';
    	$menuHTML = Yii::$app->cache->get($key);
    	if($menuHTML == false){
    		$menuHTML = Yii::$app->controller->renderPartial('@frontend/views/site/mobilemenu', [
    			'model' => $model, 'menus' => $menus,
    		]);
    		Yii::$app->cache->set( $key, $menuHTML, ( 60 * 60 * 24 * 7 ) );
    	}
    	
    	echo $menuHTML;
    }
}
