<?php
namespace frontend\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use common\models\Newsletters;

 
class NewsletterComponent extends Component
{
 public function newsletter()
 {
     $model = new Newsletters();
   echo Yii::$app->controller->renderPartial('@frontend/views/site/newsletter', [
                'model' => $model,
            ]);
   
//   Yii::$app->controller->render('newsletter', [
//                'model' => $model,
//            ]);
 }
}