<?php

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;

class RecentlyViewed extends Widget{
	public $productIds = [];
	
	public function init(){
		parent::init();
	}
	
	public function run(){
		return $this->render('@frontend/views/product/recentlyViewed', ['productIds' => $this->productIds]);
	}
}