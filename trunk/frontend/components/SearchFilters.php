<?php

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;

class SearchFilters extends Widget{
	public $features = null;
	
	public function init(){
		parent::init();
	}
	
	public function run(){
		return $this->render('@frontend/views/search/searchFilters', ['features' => $this->features]);
	}
}