<?php

namespace frontend\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;

class UserComponent extends Component {

    public function login() {
        $model = new LoginForm();
        return Yii::$app->controller->renderPartial('/user/login', [
            'model' => $model,
        ]);
    }
    
    public function signup() {
        $model = new SignupForm();
        return Yii::$app->controller->renderPartial('/user/signup', [
            'model' => $model,
        ]);
    }

}