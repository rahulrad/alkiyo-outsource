<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Categories;
use common\models\Products;
use common\models\Brands;
use common\modules\searchEs\components\ElasticSearchProducts;

/**
 * Deals controller
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays category landing page.
     *
     * @return mixed
     */
    public function actionIndex()
    {

	   $this->view->params['index'] = 'true';
        $request = Yii::$app->request;
        $slug = $request->get('slug');
        $brand_slug = $request->get('brand');
        $category = Categories::findOne(['slug' => $slug], ['status' => 'active']);
        
        if(empty($category)){
        	return $this->render('category', [
        			'category' => [],
        			'categorySliders' => [],
        			'breadcrumb'=> '',
        			'recordPerPage' => 0,
        			'products' => [],
        			'brands' => [],
        			'features' => [],
        			'subCategories' => [],
        			'arrBrands' => [],
        			'total' => 0,
        			'defaultMinPrice' => 0,
        			'defaultMaxPrice' => 0,
        	]);
        }
        
        $breadcrumb = Categories::createCategoryBreadcrumb($category->category_id);
        $subCategories = Categories::find()->where(['parent_category_id' => $category->category_id])->orderBy('sort_order ASC')->all();
        
        $search = new ElasticSearchProducts();
        $param = [];
        $sort = Yii::$app->request->get('sort', 'popular');
        $featureIds = Yii::$app->request->get('feature_ids', []);
        $arrCategory = Yii::$app->request->get('categoryIds', []);
        $arrBrands = Yii::$app->request->get('brands', []);
        $priceMax = Yii::$app->request->get('max_price');
        $priceMin = Yii::$app->request->get('min_price');
        
    	/*if(!Yii::$app->request->isAjax){
        	$brandIdOfMenu = $menuItem->brand_id;
        	$featuresIdsOfMenu = unserialize($menuItem->others);
        	if(!empty($featuresIdsOfMenu)){
        		foreach($featuresIdsOfMenu as $menuFeatureId){
        			$featureIds[] = $menuFeatureId['feature_id'];
        		}
        	}
        	if(!empty($brandIdOfMenu))
        		$arrBrands[] = $brandIdOfMenu;
        }*/
        $siblings = $category->getSiblings();
        $defaultMinPrice = 100;
        $defaultMaxPrice = 100000;
        if(!is_array($featureIds)){
        	$featureIds = [];
        }
        if(!in_array($sort, $search->allowedSort)){
        	$sort = 'popular';
        }
        
        if( ( $priceMax > 0 && $priceMin >= 0) && ($priceMin <= $priceMax)){
        	$param['price_max'] = $priceMax;
        	$param['price_min'] = $priceMin;
        }else {
        	$param['price_min'] = $defaultMinPrice;
        	$param['price_max'] = $defaultMaxPrice;
        }
        
        if(!empty($arrBrands) && is_string($arrBrands))
        	$arrBrands = explode(',', $arrBrands);
		else
        	$arrBrands = [];
        
        $totalRecords = 0;
        $param['term'] = null;
        $param['category'] = [$category->category_id]; // numeric array
        $param['brand'] = $arrBrands; // numeric array
//         $param['attribute'] = $featureIds; //numeric  array
        $param['filter_group_item'] = $featureIds; //numeric  array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', \Yii::$app->params['listingResults']);
        $param['sort'] = $sort;
        
        $param['facets'] = true;
        $result = $search->find($param);
        $brands = $features = $products = [];
        if(count($result['items']) > 0){
        	$products = $result['items'];
        	$brands   = $result['facets']['facet_brand_id'];
        	$features = $result['facets']['filters'];
        }
        if(Yii::$app->request->isAjax){
        	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return [
        		'products' => $products,
        		'brands' => $brands,
        		'features' => $features,
        		'listHtml' => $this->renderPartial('@frontend/views/search/productListing', ['products' => $products]),
        		'gridHtml' => $this->renderPartial('@frontend/views/search/productGridListing', ['products' => $products]),
        		'total' => $result['total'],
        		'pageNumber' => $param['page'],
        		'params' => $param,
        		'listingResults' => \Yii::$app->params['listingResults'],
        	];
        }
        
        return $this->render('category', [
        	'category' => $category,
        	'breadcrumb'=>$breadcrumb,
        	'recordPerPage' => \Yii::$app->params['listingResults'],
        	'products' => $result['items'],
        	'brands' => $brands,
        	'features' => $features,
        	'subCategories' => $subCategories,
        	'arrBrands' => $arrBrands,
        	'total' => $result['total'],
        	'defaultMinPrice' => $defaultMinPrice,
        	'siblings' => $siblings,
        	'defaultMaxPrice' => $defaultMaxPrice,
        ]);
    }
   
    public function actionTest()
    {
        if((Yii::$app->request->post('test'))){
            $test = "Ajax Worked!";
            // do your query stuff here
        }else{
            $test = "Ajax failed";
            // do your query stuff here
        }

        // return Json    
        return \yii\helpers\Json::encode($test);
    }
    
    /**
     * The function will search and filter product in a category
     * @return type
     */
    
    public function actionFilters()
    {
        
        $jsonArr =[];
        if (Yii::$app->request->isAjax){
        
            $rs = Categories::filterCategoryProducts(Yii::$app->request);
            foreach($rs as $product){
                $jsonArr['product_id'][] = $product['product_id'];
                $jsonArr['product_name'][] = $product['product_name'];
                $jsonArr['slug'][] = $product['slug'];
                $jsonArr['product_description'][] = $product['product_description'];
                $jsonArr['model_number'][] = $product['model_number'];
                $jsonArr['price'][] = $product['price'];
                $jsonArr['discount'][] = $product['discount'] > 0 ? $product['discount'] : 0;
                $jsonArr['rating'][] = $product['rating'];
                $jsonArr['url'][] = $product['url'];
                $jsonArr['lowest_price'][] = $product['lowest_price'];
                $jsonArr['highest_price'][] = $product['highest_price'];
                $p=Products::find()->where(['product_id' => $product['product_id']])->one();
                $jsonArr['image'][] = isset($p->productImages[0]) ? $p->productImages[0]->image_path : '';
              }
            // return Json    
            return \yii\helpers\Json::encode($jsonArr);
        }
    }
 
}
