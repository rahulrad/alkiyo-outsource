<?php
namespace frontend\controllers;

use Yii;
use common\models\Sliders;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Categories;
use common\models\Products;
use common\models\Brands;
use common\models\CouponCategories;
use common\models\Suppliers;
use common\models\Menus;
use backend\models\CouponsVendors;
use common\models\Coupons;

/**
 * Home controller
 */
class HomeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$categories = Categories::find()->where(['show_home_page' => '1'])->orderBy('sort_order ASC')->all();
        $brands =   Brands::find()->limit(20)->all();
        $rsTopDiscountedSuppliers = Suppliers::topDiscountedSuppliers();
        $couponCategories = CouponCategories::find()->where(['show_home' => '1'])->limit(4)->all();
		$totalProducts = \common\models\Products::getAllProductsCount();
		$couponsList=CouponsVendors::getPopularVendorsList();
		$latestCoupons=Coupons::getLastestCoupons();
        
        \Yii::$app->view->registerMetaTag([
        	'name' => 'description',
        	'content' => 'NayaShopi.in - Compare & Buy Online From Leading ECommerce Stores in India. There are '.$totalProducts.' Products Available to Choose From.',
        ]);
        \Yii::$app->view->registerMetaTag([
        	'name' => 'keyword',
        	'content' => 'NayaShopi.in, compare online prices, compare prices in India, compare and buy, online store price comparison, comparison shoppping in India',
        ]);
        if(YII_ENV_PROD){
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'og:title',
	        	'content' => 'NayaShopi.in - Indias Comparison Shopping Engine',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'og:site_name',
	        	'content' => 'NayaShopi.in',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'og:image',
	        	'content' => 'http://www.nayashopi.in/images/logo.png',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'og:url',
	        	'content' => 'http://www.nayashopi.in',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'og:type',
	        	'content' => 'website',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'og:description',
	        	'content' => 'NayaShopi.in - Compare & Buy Online From Leading ECommerce Stores in India. There are '.$totalProducts.' Products Available to Choose From.',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'fb:app_id',
	        	'content' => 'App_Id Created At Facebook',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'property' => 'fb:pages',
	        	'content' => 'FB PAGE ID',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'author',
	        	'content' => 'Raditya Web Technology',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'twitter:site',
	        	'content' => '@nayashopi.in',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'twitter:card',
	        	'content' => 'summary_large_image',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'twitter:creator',
	        	'content' => '@NayaShopi',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'twitter:title',
	        	'content' => 'NayaShopi.in - Indias Comparison Shopping Engine',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'twitter:description',
	        	'content' => 'NayaShopi.in - Compare & Buy Online From Leading ECommerce Stores in India. There are '.$totalProducts.' Products Available to Choose From.',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'twitter:image',
	        	'content' => 'http://www.nayashopi.in/images/logo.png',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'http-equiv' => 'content-language',
	        	'content' => 'en-in',
	        ]);
	        \Yii::$app->view->registerMetaTag([
	        	'name' => 'language',
	        	'content' => 'en',
	        ]);
    	}

		 $menus = Menus::getTopMenu();
        return $this->render('home' , [ 'categories' => $categories,
            'brands' => $brands, 'rsTopDiscountedSuppliers' => $rsTopDiscountedSuppliers,
            'couponCategories' => $couponCategories , 'menus'=>$menus,'couponList'=>$couponsList,'latestCoupons'=>$latestCoupons]  );
    }
    
    public function actionSlug(){
    	$request = Yii::$app->request;
        $slug = $request->get('slug');
    	$data = \backend\models\Pages::findOne(['slug' => $slug]);
    	if($data instanceof \backend\models\Pages)
    	return $this->render('slug', ['data' => $data]);
    	else throw new \yii\web\NotFoundHttpException();
    }
    
 
}
