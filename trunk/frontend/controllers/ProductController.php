<?php

namespace frontend\controllers;

use common\models\ActivityTracker;
use Yii;
use common\models\Products;
use common\models\FeatureGroups;
use common\models\ProductFeature;
use common\models\ReviewForm;
use common\models\ProductReviews;
use common\models\ProductReviewsRank;
use common\models\PriceUpdate;
use common\models\Menus;
use common\models\ProductsSuppliers;
use yii\web\Controller;

class ProductController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays Product detail page.
     *
     * @return mixed
     */
    public function actionIndex() {
        
        $request        = Yii::$app->request;
        $slug           = $request->get('slug');
        //get product from slug
        $product        = Products::findOne(['slug' => $slug, 'is_delete'=>0], ['product_status' => 'active']);
        if(empty($product))
        	throw new \yii\web\NotFoundHttpException();
        //get product category feature groups
        $featureGroups  = FeatureGroups::getFeatureAttributeByCategory($product->categories_category_id);
        //product features array
        $productFeatures = ProductFeature::getProductFeatureValues($product->product_id);
        //product review form model
        $reviewmodel    = new ReviewForm();
        //$starRating = new StarRating();
        $userIP         = Yii::$app->request->userIP;

       if(!$product->categoriesCategory->is_index){
        $this->view->params['index'] = 'false';
       }

	
        //else user id 0
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
        $product->trigger(Products::PRODUCT_VIEW);
        $productId      = $product->product_id;
        $ProductReview = new ProductReviews();
        $ProductReviews = $ProductReview->getReviews($userId, $userIP, $productId);
        $PriceUpdate = new PriceUpdate();
        $PriceUpdate    = $PriceUpdate->getUpdate($userId, $productId);
//        $product->queueProductSuppliers();
        $similarProducts = Products::getProductsInTwoPriceRanges($product->lowest_price, $product->lowest_price, $product->categories_category_id);
        
        $menuSlug           = 'laptop-pricelists'; //product-price-menu
        $categoryId         = $product->categories_category_id;
        $menuType           = 'left menu';
        //$menuCategoryTypeList   = Menus::getMenuCategoryTypeList($menuSlug, $categoryId, $menuType);
        Products::metaTags($product);
	$instock=1;
        if($product->categoriesCategory->compare == 1){
        	$view = 'productwithsupplier';
}
        else {
        	$view = 'productwithoutsupplier';
        
	$productsSuppliersModel = new ProductsSuppliers();
            $existProductSuppiler = $productsSuppliersModel::find()->where(['product_id' => $productId])->one();
            if (!is_null($existProductSuppiler)) {
                $instock = $existProductSuppiler->instock;
            }

}
        return $this->render($view, [
       		'product'           => $product,
       		'featureGroups'     => $featureGroups,
       		'productFeatures'   => $productFeatures,
       		'reviewmodel'       => $reviewmodel, 
       		'ProductReviews'    => $ProductReviews,
            'PriceUpdate'       => $PriceUpdate,
		'instock' => $instock,
            'userId'            => $userId,
       		'similarProducts'   => $similarProducts,
        	'featurez'          => \common\models\Features::getIsFilterFeatureOfProduct($product->categories_category_id, $product->product_id),
        ]);
    }
    
    /*
     * 
     */
    public function actionAddreview() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model          = new ReviewForm();
        $request        = \Yii::$app->getRequest();
        $message = [];
        if ($request->isPost && $model->load($request->post())) {
            $message['status'] = 200;
            $message['message'] = 'My message';
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }

    public function actionReviewpost() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ProductReviews = new ProductReviews();
        $userIP = \Yii::$app->request->userIP;
        //if user login user id
        //else user id 0
        
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
        
        
        $request        = \Yii::$app->getRequest();
        $requestt       = $request->post('ReviewForm');
        $message = [];
        
        if ($request->isPost) {
            
            $ProductReviews->title      = $requestt['title'];
            $ProductReviews->description= $requestt['description'];
            $ProductReviews->product_id = $requestt['product_id'];
            $ProductReviews->rating     = $requestt['rating'];
            $ProductReviews->user_id    = $userId;
            $ProductReviews->ip         = $userIP;
            $ProductReviews->status     = "InActive";
            $ProductReviews->created    = date('Y/m/d');
            $ProductReviews->save();
            $message['status'] = 200;
            $message['message'] = 'My message';
            
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }
    
    public function actionReviewrank() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ProductReviews = new ProductReviewsRank();
        $userIP = Yii::$app->request->userIP;
        //if user login user id
        //else user id 0
        $userId = 0;
        
        $request        = \Yii::$app->getRequest();
        $requestt       = $request->post('ReviewForm');
        $message = [];
        
        if ($request->isPost) {
            
            $ProductReviews->product_id = $requestt['product_id'];
            $ProductReviews->review_id  = $requestt['review_id'];
            $ProductReviews->user_id    = $userId;
            $ProductReviews->ip         = $userIP;
            $ProductReviews->rank       = $requestt['rank'];
            $ProductReviews->created    = date('Y/m/d');
            $ProductReviews->save();
            $message['status'] = 200;
            $message['message'] = 'My message';
            
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }
    
    public function actionPriceupdate() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $PriceUpdate = new PriceUpdate();
    	if (!\Yii::$app->user->isGuest) {
            $userId = \Yii::$app->user->id;
        }else{
        	$userId = 0;
        }
        
        $request        = \Yii::$app->getRequest();
        $requestt       = $request->post('ReviewForm');
        $message = [];
        
    	if ($request->isPost && $userId > 0) {
        	$product = Products::findOne([ 'product_id' => $requestt['product_id'] ]);
            $PriceUpdate->product_id = $product->product_id;
            $PriceUpdate->user_id    = $userId;
            $PriceUpdate->alert_price = $product->lowest_price;
            $PriceUpdate->created    = date('Y/m/d');
            $PriceUpdate->save();
            $message['status'] = 200;
            $message['message'] = 'My message';
        }else{
            $message['status'] = 404;
            $message['message'] = 'Error message';
        }
        return $message;
    }
    
    public function actionCompare(){
    
 $this->view->params['index'] = 'true';
	 
    	$request = Yii::$app->request;
    	$products = $request->get('products');
    	 
    	if(empty($products) && !is_array($products)){
    		return $this->goHome();
    	}
    	 
    	$data = Products::compareProducts($products);
    	 
    	if(empty($data)){
    		return $this->goHome();
    	}
    	return $this->render('compare', $data);
    }
    
    public function actionComparesearch() {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $term = Yii::$app->request->get('term');
        $category = Yii::$app->request->get('category') ? intval(Yii::$app->request->get('category')) : 0;

        $search = new \common\modules\searchEs\components\ElasticSearchProducts();
        
        $param = [];
        $param['term'] = $term; // string
        $param['category'] = $category; // numeric array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', 20);
        
        $param['sort'] = Yii::$app->request->get('sort', 'relevance');
        
        // return facets as well
        $param['facets'] = false;
        
        $records = $search->find($param);


        $items = [];
        foreach ($records['items'] as $row) {
        	$image = \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($row);
        	$items[] = [
                'icon' => $image,
                'label' => $row['product_name'],
                'value' => $row['product_name'],
                'slug' => $row['slug'],
            ];
        }

        return $items;
    }


    public function actionStoreOut($id)
    {
        $model = ProductsSuppliers::find()->where(['track_id' => $id])->one();
        if ($model != null) {
            $this->layout = "track";
            $sessionId = Yii::$app->session->id;
            $activityType = ActivityTracker::Activity_PRODUCT;
            $url = $model->url . $model->supplier->data_url;
            ActivityTracker::saveActivity($url, $sessionId, $activityType, $model->supplier->name);
            return $this->render("send_to_store", [
                'url' => $url
            ]);
        }
        throw new \yii\web\NotFoundHttpException();
    }
}
