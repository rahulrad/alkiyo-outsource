<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Menus;
use common\modules\searchEs\components\ElasticSearchProducts;


class SearchController extends Controller {
    
    public function actionProducts() {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       

	   $this->view->params['index'] = 'true'; 
        $search = new ElasticSearchProducts();
        
        $param = [];
        $param['term'] = Yii::$app->request->get('term', null); // string
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', 20);
        $param['sort'] = 'relevance';
        
        $result = $search->find($param);
        $products = [];
$products=Menus::searchData(Yii::$app->request->get('term', null));

        if(empty($result['items']) && empty($products))
        	return [];
        
        foreach ($result['items'] as $product){
        	$products[] = [
	'url' =>  \yii\helpers\Url::to(['product/index', 'slug'=>$product['slug'], 'category' => $product['categories']['slug']]),
        	
        		'icon' 	=> \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($product),
                'label' => $product['product_name'],
                'value' => $product['product_name'],
                'slug' 	=> $product['slug'],
        		'supplierName' => $product['supplier_name'],
        		'supplierImage' => $product['supplier_image'],
        		'lowest_price' => $product['lowest_price'],
        		'original_price' => $product['original_price'],
        	];
        }
        
        return $products;
    }
    
    public function actionIndex(){
	$this->view->params['index'] = 'true';
    	if(\Yii::$app->getRequest()->isPost)
    		return $this->goHome();
    	
//     	$post = \Yii::$app->request->post('SearchForm');
    	$name = \Yii::$app->request->get('term', null);
    	$search = new ElasticSearchProducts();
    	$param = [];
    	$sort = Yii::$app->request->get('sort', 'popular');
    	$featureIds = Yii::$app->request->get('feature_ids', []);
    	$arrCategory = Yii::$app->request->get('categoryIds', []);
    	$priceMax = Yii::$app->request->get('max_price');
    	$priceMin = Yii::$app->request->get('min_price');
    	
    	$defautlMinPrice = 100;
    	$defaultMaxPrice = 50000;
    	if(!is_array($featureIds)){
    		$featureIds = [];
    	}
    	if(!in_array($sort, $search->allowedSort)){
    		$sort = 'popular';
    	}
    	
    	if( ( $priceMax > 0 && $priceMin >= 0) && ($priceMin <= $priceMax)){
    		$param['price_max'] = $priceMax;
    		$param['price_min'] = $priceMin;
    	}else {
    		$param['price_min'] = $defautlMinPrice;
    		$param['price_max'] = $defaultMaxPrice;
    	}
        if(!empty($arrCategory) && is_string($arrCategory)){
        	$arrCategory = explode(',', $arrCategory);
        }else 
        	$arrCategory = [];
        
        $arrBrands = Yii::$app->request->get('brands', []);
        if(!empty($arrBrands) && is_string($arrBrands))
        	$arrBrands = explode(',', $arrBrands);
        else 
        	$arrBrands = [];
        
        $arrStore = Yii::$app->request->get('store', []);
        if(!empty($arrStore) && is_string($arrStore)):
        	$arrStore = explode(',', $arrStore);
        else:
        	$arrStore = [];
        endif;
        
        $totalRecords = 0;
        
        $param['term'] = $name; // string
        $param['category'] = $arrCategory; // numeric array
        $param['brand'] = $arrBrands; // numeric array
        $param['store'] = $arrStore; // numeric array
        $param['filter_group_item'] = $featureIds; //numeric  array
        $param['page'] = Yii::$app->request->get('page', 1); // numeric
        $param['perpage'] = Yii::$app->request->get('perpage', \Yii::$app->params['listingResults']);
        $param['sort'] = $sort;
        
        $param['facets'] = true;
        $result = $search->find($param);

//         if(empty($result['items'])){
//         	$this->goHome();
//         }
        
        $firstProduct = $brands = $features = $categories = [];
        foreach($result['items'] as $product){
        	$firstProduct = $product;
        	break;
        }
        $brands = $categories = $features = [];
        if(count($result['items']) > 0){
        	$brands = $result['facets']['facet_brand_id'];
        	$categories = $result['facets']['facet_category_id'];
        	$features = $result['facets']['filters'];
        }
        $products = empty($result['items']) ? [] : $result['items'];
        
//     	$breadcrumb = Categories::createCategoryBreadcrumb($firstProduct['categories_category_id']);
    	if(Yii::$app->request->isAjax){
    		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		return [
    			'categories' => $categories,
    			'products' => $products,
    			'brands' => $brands,
    			'features' => $features,
    			'term' => $name,
    			'listHtml' => $this->renderPartial('@frontend/views/search/productListing', ['products' => $products]),
        		'gridHtml' => $this->renderPartial('@frontend/views/search/productGridListing', ['products' => $products]),
        		'total' => $result['total'],
    			'pageNumber' => $param['page'],
    			'params' => $param,
    			'listingResults' => \Yii::$app->params['listingResults'],
    		];
    	}
    	return $this->render('search', [
    		'categories' => $categories,
    		'products' => $products,
    		'brands' => $brands,
    		'features' => $features,
    		'term' => $name,
    		'pageNumber' => $param['page'],
    		'total' => $result['total'],
    		'arrCategory' => $arrCategory,
    		'min_price' => $defautlMinPrice,
    		'max_price' => $defaultMaxPrice,
    	]);
    }

}
