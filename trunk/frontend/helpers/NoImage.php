<?php

namespace frontend\helpers;
use common\models\Products;

class NoImage{
	const noImageProduct = 'product';
	const noImageSlider  = 'slider';
	const noImageCategory = 'category';
	const size150x200 = '150x200';
	const size300x400 = '300x400';
	
	public static function getNoImageIfNoImageExists($image, $noImage = 'product'){
		

			if($noImage === self::noImageProduct){
			return !empty($image) ? $image : '/images/no_images/no-image-250X300.jpg';
		}elseif($noImage === self::noImageSlider){
			return !empty($image) ? $image : 'images/no_images/banner-960x300.jpg';
		}elseif($noImage === self::noImageCategory){
			return !empty($image) ? $image : 'images/no_images/no-image2--150X150.jpg';
		}else{
			return !empty($image) ? $image : 'images/no_images/advertise-310x156.jpg';;
		}	

}
	
 public static function getProductImageFullPathFromObject(\common\models\Products $image, $size = '150x200',$page="thumb"){
		
		 $presets = array(
            Products::IMAGE_PRESET_VERTICAL => array(
                "main" => array(
                    "thumb" => "60x125",
                    "product" => "150x300",
                    "zoom" => "450x900"
                ),
                "other" => array(
                    "product" => "150x300",
                    "zoom" => "450x900"
                )
            ),
            Products::IMAGE_PRESET_HORIZONTAL => array(
                "main" => array(
                    "thumb" => "180x125",
                    "product" => "250x170",
                    "zoom" => "750x540"
                ),
                "other" => array(
                    "product" => "250x170",
                    "zoom" => "750x540"
                )
            ),
        );

		 $imageName = !empty($image->image)?$image->image : '';	
		$brandSlug = $image->brand->folder_slug;
		$categorySlug = $image->category->folder_slug;
		$categoryPreset=$image->category->image_scrap_preset;
		$sizepath=$presets[$categoryPreset]['main'][$page];
		$imageNameArray=explode('.',$imageName);
		
	
			return '/uploads/products/'.$categorySlug.'/'.$brandSlug.'/'.$imageNameArray[0].'/'.$sizepath.'/'.$imageName;
	}

	
/*
public static function getProductImageFullPathFromObject(\common\models\Products $image, $size = '150x200'){
			return $imageName = !empty($image->productImages) ? $image->productImages[0]->image_path : '';	
	$brandSlug = $image->brand->folder_slug;
		$categorySlug = $image->category->folder_slug;
		$imageName = !empty($image->productImages) ? $image->productImages[0]->image : '';
		if($size !== 'full' && in_array($size, [self::size150x200, self::size300x400]))
			return \Yii::$app->params['productImageBaseUrl'].$categorySlug.'/'.$brandSlug.'/'.$size.'/'.$imageName;
		else 
			return \Yii::$app->params['productImageBaseUrl'].$categorySlug.'/'.$brandSlug.'/'.$imageName;
	}
*/
	public static function getProductImageFullPathFromBrandCategorySlug($brandSlug = '', $categorySlug = '', \common\models\ProductImage $image, $size = '150x200',$preset='vertical',$type="product"){
		
//		if(isset($image->image_path)){
//			return $image->image_path;
//		}


                 $presets = array(
            Products::IMAGE_PRESET_VERTICAL => array(
                "main" => array(
                    "thumb" => "60x125",
                    "product" => "150x300",
                    "zoom" => "450x900"
                ),
                "other" => array(
                    "product" => "150x300",
                    "zoom" => "450x900"
                )
            ),
            Products::IMAGE_PRESET_HORIZONTAL => array(
                "main" => array(
                    "thumb" => "180x125",
                    "product" => "250x170",
                    "zoom" => "750x540"
                ),
                "other" => array(
                    "product" => "250x170",
                    "zoom" => "750x540"
                )
            ),
        );

                 $imageName = !empty($image->image)?$image->image : '';
                $sizepath=$presets[$preset]['main'][$type];
                $imageNameArray=explode('.',$imageName);


                        return '/uploads/products/'.$categorySlug.'/'.$brandSlug.'/'.$imageNameArray[0].'/'.$sizepath.'/'.$imageName;


	
	}
	
	public static function getProductImageFullPathFromElasticResult(array $image, $size = '150x200',$page="thumb"){

		//if(array_key_exists('image_path',$image) && isset($image['image_path'])){
		//			return $image['image_path'];
		//}

		 $presets = array(
            Products::IMAGE_PRESET_VERTICAL => array(
                "main" => array(
                    "thumb" => "60x125",
                    "product" => "150x300",
                    "zoom" => "450x900"
                ),
                "other" => array(
                    "product" => "150x300",
                    "zoom" => "450x900"
                )
            ),
            Products::IMAGE_PRESET_HORIZONTAL => array(
                "main" => array(
                    "thumb" => "180x125",
                    "product" => "250x170",
                    "zoom" => "750x540"
                ),
                "other" => array(
                    "product" => "250x170",
                    "zoom" => "750x540"
                )
            ),
        );


$imageName = !empty($image['product_image'])?$image['product_image'] : '';
                $brandSlug =  $image['brands_folder_slug'];
                $categorySlug = $image['categories']['folder_slug'];
                $categoryPreset= $image['categories']['preset'];
                $sizepath=$presets[$categoryPreset]['main'][$page];
                $imageNameArray=explode('.',$imageName);


                        return '/uploads/products/'.$categorySlug.'/'.$brandSlug.'/'.$imageNameArray[0].'/'.$sizepath.'/'.$imageName;


		$brandSlug = $image['brands_folder_slug'];
		$categorySlug = $image['categories']['folder_slug'];
	
		if($size !== 'full' && in_array($size, [self::size150x200, self::size300x400]))
			return \Yii::$app->params['productImageBaseUrl'].$categorySlug.'/'.$brandSlug.'/'.$size.'/'.$image['product_image'];
		else
			return \Yii::$app->params['productImageBaseUrl'].$categorySlug.'/'.$brandSlug.'/'.$image['product_image'];
	}
}
