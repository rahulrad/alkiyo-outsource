<?php $rowsPrinted = 0;?>
<?php foreach($dealsData as $current):?>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padd-r7"> <a href="<?=$current->link?>">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="full-row dealbox  text-center">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <figure> <img alt="" class="img-responsive centerimg" src="<?=$current->image_path?>"> </figure>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                          <p class="discription"><?=$current->title?> <!-- <span>Free shiping</span>  --></p>
                          <p class="price"> Rs <?=number_format(doubleval($current->price),2)?>
                            <span> <?=number_format(doubleval($current->offer_price),2)?> </span> 
                            <strong> <?=$current->discount?> off </strong>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  </a>
                </div>
              <?php $rowsPrinted++;
              if($rowsPrinted === 4) break;?>
              <?php endforeach;?>
              <!-- Now to print images -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padd-l7 combinedbox">
                  <div class="full-row dealbox  text-center padd-b-none">
                    <div class="row">
                    <?php for($index = $rowsPrinted; $index < count($dealsData); $index++):?>
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="mini_prodect"> <img  itemprop="image" src="<?=$dealsData[$index]->image_path?>" class="img-responsive"> </div>
                      </div>
                    <?php endfor;?>
                    </div>
                  </div>
                  
                  <a href="javascript:void(0)" id="href<?=$supplier->id?>" class="dealsSupplier" data-page="<?=($pageNumber+1)?>" data-suppler="<?=$supplier->name?>">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="full-row dealbox  text-center">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                          <p class="show_more_price">Show  more <br>
                            <!-- <span>$38.00 $38.00 58% off</span><br> -->
                            <i aria-hidden="true" title="Copy to use angle-double-down" class="fa fa-fw"></i> </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  </a>
				</div>
          