<?php use frontend\helpers\NoImage;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\helpers\Html;

use yii\base\Widget;




$this->title = $couponVendor[0]['meta_title']; ?>
<div class="container">
          <div class="row visible-xs">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <ul class="mobile-view-menu">
                            <li class="filter"><i class="fa fa-filter"></i> Filter </li>
                        </ul>
                    </div>
          </div>

          <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd-r7 brand left-panel cashback_categories left-panel filter-in">
     
     
     <div class="filterbutton-row hidden-sm hidden-md hidden-lg" >
      
      <div class="col-xs-6 no-padd">
       <button type="button" class="btn btn-default btn-filter btn-block no-radius"> Cancel </button>
       </div>
      <div class="col-xs-6  no-padd">
  <button type="button" class="btn btn-danger btn-block  btn-filter no-radius"> Apply </button>
         </div>       
  
      </div>
<div class="visible-xs hidded-sm hidden-md hidden-lg mobilefilter">
      
      <span>All FIlters</span> 
      <span class="clearall"> Clear All</span>   
  
      </div>

      <div class="filter-blc hidden-xs">
            	<div class="cat-img-blc"><img src="<?php echo $couponVendor[0]['image']; ?>" alt="<?php echo $couponVendor[0]['title']; ?>"></div>
             <!--   <p><img src="images/cash.jpg" alt="logo"/> Flat 3.5% Cashback From Coupon Dunia</p>
                <div class="accash"><a href="#">Activate Cashback</a></div>-->
             
            </div>
    <?php if(count($category)>0){ ?> 
      <section class="brand brand-block">
        <div class="row lefty selected">
          <div class="col-xs-12 logo text-center">
            <h4 class="text-left">Filter Categories  <span class="hidden-xs"  id="clear_cat" style="cursor:pointer"> Clear  </span></h4>
            <div class="input-group searcbar hidden-xs">
              <input type="text" class="form-control" id="categories">
              <span class="input-group-addon "> <i class="fa fa-search"></i> </span> </div>
          </div>
        </div>
        <div class="row righty" style="display:block;"> 
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="full-row left-scrollblock  ">
              <?php foreach($category as $k => $v) {
          if(!empty($v['name'])){
              ?>
              <div class="form-group">
                <label class="checkbox-inline">
                  <input type="checkbox" class="category" <?=in_array( $v['category'] , $catArr) ? 'checked="checked"' : ''?>  name="category[]" value="<?php  echo $v['category'] ?>"/>
                  <?php  echo $v['name'] ?> </label>
                <span class="pull-right">  <?php  echo $v['count'] ?></span> 
                
                </div>

              <?php }} ?>
              
          </div>
        </div>
      </section>

      <?php } ?>
      <br/>
      <section class="discount brand brand-block" style="margin-top: 0px;
    margin-bottom: 0px;
    padding-bottom: 0px;">
        <div class="row lefty selected">
          <div class="col-xs-12 logo text-center">
            <h4 class="text-left"> Stores</h4>
            <div class="input-group searcbar hidden-xs">
              <input type="text" class="form-control " id="stores">
              <span class="input-group-addon "> <i class="fa fa-search"></i> </span> </div>
          </div>
        </div>
        <div class="row righty">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 brand">
            <div class="full-row left-scrollblock ">
              
  <?php foreach($storeList as $k => $v) {
              ?>
              <div class="form-group">
                <label class="checkbox-inline" style="padding-left:4px;">
                  <input  <?php if($couponVendor[0]['slug']==$k){echo "checked";}  ?> type="radio" class="storeslist" url="<?php  echo \yii\helpers\Url::to(['/deals/deals','merchant'=>$k]);?>"  name="stores[]" value="<?php  echo $v ?>">
                  <?php  echo $v?> </label>
                <span class="pull-right">  </span> 
                
                </div>

              <?php } ?>

             
            </div>
          </div>
        </div>
      </section>

     
     
      <!--gurup--> 
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padd-l7 coupon-casbackblock right-block">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h1 class="la-hdd"><?php echo $couponVendor[0]['title']; ?></h1>

                  <div id="dynamic_text" class="gs_readmore" >
                                        
                            <p itemprop="description"></p>

                            <p class="gray-font"><?php echo $couponVendor[0]['short_description'] ?></p>
                  </div>
                <div id="All" class="tab-pane active" role="tabpanel">
                    <?= ListView::widget(['dataProvider' => $dataProvider, 'options' => ['tag' => 'div', 'class' => 'list-wrapper', 'id' => 'list-wrapper'], 'layout' => "{items}\n{pager}", 'itemView' => '_list_item','pager' => [
                        'nextPageLabel' => 'Next Page',
                        'nextPageCssClass' => 'page-numbers',
                        'prevPageLabel' => 'Previous',
                        'prevPageCssClass' => 'pre page-numbers',
                        'maxButtonCount' => 3,
                        'pageCssClass' => 'page-numbers',
                        'activePageCssClass'=>'current',
                        
                    ],]); ?>
                    <p><?php echo $couponVendor[0]['description'] ?></p>        
           
                </div>
                                </div>
                  </div>
              
                            
            </div>
  <?php  if(count($popular) > 0){

           ?>

  <div id="shopingbrands" class="container-fluid no-padding shopingbrands visible-xs">

                <div class="">
                        <div class="section-header">
                                <h3>Popular Stores</h3>
                                 <a href="/coupons">View All</a>
                        </div>
                        <div class="">

      <?php foreach($popular as $c => $l){
      if($c>11){
        break;
      }
        ?>
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="brandbox">
                                                <a href="<?php  echo \yii\helpers\Url::to(['/deals/deals','merchant'=>$l['slug']]);?>" title="<?php echo $l['name']; ?>">
              <img src="<?php echo $l['image']; ?>" alt="<?php echo $l['name']; ?>" width="178" height="100">
                                                </a>
                                        </div>
        </div>
      <?php } ?>
                        </div>
                </div>

  </div>

  <?php } ?>


</div>
<!-- end of container -->
<script type="text/javascript">

if($( window ).width()<768){
   $('input[type=checkbox]').click(function(e){
 

        var length= $(this).parents('.left-scrollblock').find('input[type=checkbox]:checked').length;
        if(length>0){
          $(this).parents('.righty').parent().find('.lefty .popup__name-cnt').html('✓');
        }else{
          $(this).parents('.righty').parent().find('.lefty .popup__name-cnt').html('');
        }
      });

      $('.lefty').click(function(){
    $('.righty').hide();
    $(this).next().slideDown();
    $( ".lefty" ).each(function( index ) {
    $( this ).removeClass('selected') ;
  });
     
    $(this).addClass('selected');
  
  });

$('.clearall').click(function(){
 
 
  var url=document.location.protocol +"//"+ document.location.hostname + document.location.pathname;
   window.location.href=url;
});
}




function copytext(text) {
  var textArea = document.createElement("textarea");

  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.select();
  try {
    
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
  document.body.removeChild(textArea);
  $('#'+anchorid).text('COPIED');
}




jQuery(document).ready(function(){

  var clipboard = new Clipboard('.btn');


	clipboard.on('success', function (e) {
		console.log(e);
	});

	clipboard.on('error', function (e) {
		console.log(e);
	});

   <?php  if(isset($_GET['id'])) {  $id=$_GET['id'] ;?>
    jQuery('#offer-<?php echo  $id; ?>').trigger('click');
  <?php } ?>

	jQuery('body').on('click','.getcode',function(){
		var id = jQuery(this).attr('code');
		jQuery(this).html(id+'<i class="fa fa-spinner" aria-hidden="true"></i>');
    var url=jQuery(this).attr('link');
   // setTimeout(function(){ 
     // 	jQuery('.getcode').find('i').removeClass('fa-spinner');
      // window.open(url, '_blank');}, 3000);
    
    
	});
  jQuery('body').on('click','.codelink',function(){
		var url = jQuery(this).attr('data-dealurl');
    window.open(url,'_blank');
    window.location.href=jQuery(this).attr('link');
	
   // setTimeout(function(){ 
     // 	jQuery('.getcode').find('i').removeClass('fa-spinner');
      // window.open(url, '_blank');}, 3000);
    
    
	});

  

	jQuery('body').on('click','#clear_cat',function(){
$(".category").each(function() {
	     $(this).prop('checked', false);
	    });
      redirect();

  });


  jQuery('body').on('click','#clear_type',function(){
$(".typeList").each(function() {
	     $(this).prop('checked', false);
	    });
      redirect();

  });

  jQuery('body').on('click','#clear_store',function(){
$(".storeslist").each(function() {
	    	 $(this).prop('checked', false);
	     });
        redirect();

  });
  
  jQuery('body').on('click','#clear_offer',function(){

  $(".offerlist").each(function() {
	    $(this).prop('checked', false);
      
});
	       redirect();


  });

  



    	function getCommaSepratedStoreValues(){
		var values = [];
	  	$(".storeslist").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val() );
	     });

	    if(values.length > 0)
	    	return values.join('-');
    	return '';
	}


  	function getOfferValues(){
		var values = [];
	  	$(".offerlist").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val() );
	     });

	    if(values.length > 0)
	    	return values.join('-');
    	return '';
	}



  	function getTypeValues(){
		var values = [];
	  	$(".typeList").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val() );
	     });

	    if(values.length > 0)
	    	return values.join('-');
    	return '';
	}

	function getCommaSepratedCategoryValues(){
		var values = [];
		$(".category").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val());
	    });

	    if(values.length > 0)
	    	return values.join('-');
    	return '';
	}

  function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i=0; i<arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // in case params look like: list[]=thing1&list[]=thing2
      var paramNum = undefined;
      var paramName = a[0].replace(/\[\d*\]/, function(v) {
        paramNum = v.slice(1,-1);
        return '';
      });

      // set parameter value (use 'true' if empty)
      var paramValue = typeof(a[1])==='undefined' ? true : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      //paramValue = paramValue.toLowerCase();

      // if parameter name already exists
      if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === 'string') {
          obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === 'undefined') {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        }
        // if array index number specified...
        else {
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      }
      // if param name doesn't exist yet, set it
      else {
        obj[paramName] = paramValue;
      }
    }
  }

  return obj;
}

  function redirect(){

    <?php echo "var currentCont='/".Yii::$app->getRequest()->getPathInfo()."';"; ?>
var category=getCommaSepratedCategoryValues();
var store=getCommaSepratedStoreValues();
var offers=getOfferValues();
var paramsObject=getAllUrlParams(document.URL);

var params=[];
if(category)
params.push("category="+category);
if(paramsObject.id!=undefined)
params.push("id="+paramsObject.id);


window.location.href=currentCont+(params.length>0?"?"+params.join('&'):'');

  }

   

      $('#categories').on('keyup', function () {
            var value = this.value;  
            
            $('.brand div.form-group').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $('#stores').on('keyup', function () {
            var value = this.value;  
            
            $('.discount div.form-group').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

         $('#offer').on('keyup', function () {
            var value = this.value;  
            
            $('.newarrivals div.form-group').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

        $('#type').on('keyup', function () {
            var value = this.value;  
            
            $('.type div.form-group').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

        $('.btn-filter').click(function(){

   redirect();
})


if($(window).width()>768){
  	$(".left-scrollblock").mCustomScrollbar();

   $('input[type=checkbox]').click(function(){
        redirect();
      });

       $('.storeslist').click(function(){
       window.location.href=$(this).attr('url');
      });

       $('input[type=radio]').click(function(){
         if($(this).hasClass('storeslist')){
           return;
         }
        redirect();
      });




}

});
</script>

<style>
@media (max-width: 767px)
{
.filter-in {
display:none ;
}

}
</style>
