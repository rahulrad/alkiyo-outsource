<?php

use yii\helpers\Url;
$this->title = 'NayaShopi.in - Indias Comparison Shopping Engine';
?>
<div class="">
  <?=frontend\components\Slider::widget()?>

  
  <?php  if(count($couponList) > 0){
            
           ?> 

  <div id="shopingbrands" class="container-fluid no-padding shopingbrands">
		 
		<div class="">
			<div class="section-header">
				<h3>Popular Stores</h3>
				 <a href="/coupons">View All</a>
			</div>
			<div class="">

      <?php foreach($couponList as $c => $l){
      if($c>11){
        break;
      }
        ?>
				<div class="col-md-2 col-sm-3 col-xs-3">
					<div class="brandbox">
						<a href="<?php  echo \yii\helpers\Url::to(['/deals/deals','merchant'=>$l['slug']]);?>" title="<?php echo $l['name']; ?>">
              <img src="<?php echo $l['image']; ?>" alt="<?php echo $l['name']; ?>" width="178" height="100">
						</a>
					</div>
        </div>
      <?php } ?>
			</div>
		</div> 
		 
  </div>
  
  <?php } ?>


  <?=\frontend\components\CategoriesProducts::widget();?>







  
  <?php  if(count($latestCoupons) > 0){
    
    
    ?>


  <div id="deal-section" class="deal-section">
		 
		<div class="section-header">
			<h3>Popular Coupons &amp; Deals</h3>
			<a href="/coupons">View All</a>
			 
		</div>
		<div class="row">
      
    <?php foreach($latestCoupons as $lt => $mt){ ?>
			<div class="col-md-3 col-sm-3 col-xs-6 gth">
				<div class="dealbox">
					<div class="deal-thmbimg">
						<img style="cursor:pointer;" onclick="window.location.href='<?php  echo \yii\helpers\Url::to(['/deals/deals','merchant'=>$mt['slug']]);?>'" src="<?php echo $mt['image'] ?>" width="370" height="247" alt="<?php echo $mt['name'] ?>">
						 
					</div>
					<div class="deal-content">


 <h3><?php
            if($mt['type']==0){
echo "Coupon";
            }
            if($mt['type']==1){
            echo "CashBack";  
                          }
            
           if($mt['type']==2){
                            echo "Deal";
              }
                                        
            
            ?></h3>  
            <?php  $productName = strlen($mt['title']) > 25 ? substr($mt['title'],0, 35). '...' : $mt['title'];?> 
						<p><span><?php echo $productName ; ?></p>
						<span class="dic_off">
            <?php echo $mt['discount'] ?>
						</span>
  
            
            
            <a 
 
 style="cursor:pointer;" code="<?php echo  $mt['code']; ?>" link="<?php echo 
            Url::toRoute(['/deals/store-out', 'id' => md5($mt['id'])]);
            
            ?>"  data-toggle="modal" id="offer-<?php echo $mt['id'] ; ?>" data-target="#deal-<?php echo $mt['id'] ; ?>"   title="Get this deal" class="getdeal-btn codelink " >
							<span class="get-coupencode"> <?php echo $mt['code'] ?>&nbsp;</span>
							<span class="getdeal"> <?php echo $mt['call_to_action_button'] ?></span>
            </a>
            
            <div style="margin-top:10px;margin-left:10px;" class=" de_share addthis_inline_share_toolbox" data-media="https://www.nayashopi.in/images/Nayashopi.png"  class="addthis_inline_share_toolbox"  data-url="
    
    <?php 
    echo 
    Url::toRoute(['/deals/store-out', 'id' => md5($mt['id'])]);
     ?>" data-title="<?php echo  $mt['title']; ?>" data-description="<?php echo $mt['description']; ?>" ></div>
     
            
					
					</div>
				</div>
      </div>











 <div class="modal fade" id="deal-<?php echo    $mt['id']; ?>" tabindex="-1" role="dialog" >
        <div class="modal-dialog coupons login-model" role="document">
             <div class="modal-content" style="    background-color: #fff !important; background-image: none !important;">
                <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                      </button>
                </div>
                <div class="modal-body" style="padding-top:0px;"> 
                    <div class="pop-padding">
                       <div class="center-logo"><img src="/images/Nayashopi.png" alt="logo"></div>
                             <?php if(  $mt['type'] == 0){  ?>
	                                	<h3>Copy  the coupon code</h3>
	                                	<div class="code-blc">
	                                    	

                                          

                                          <input  style="color: #7cbf65;
    font-size: 17px;
    text-transform: uppercase;
    padding: 5px 15px;
    display: inline-block;    width: 167px;
    text-align: center;background: transparent;border:0px;
    letter-spacing: 5px;"  id="deal-c<?php echo $mt['id']; ?>" type ="text" value="<?php echo   $mt['code']; ?>">
		                                    	 <button  class="btn" data-clipboard-action="copy" data-clipboard-target="#deal-c<?php echo $mt['id']; ?>">Copy Code</button>
		                                    
                                    </div>
                              <?php } ?>


		                          <p>Go to  <a href="<?php echo 
            Url::toRoute(['/deals/store-out', 'id' => md5($mt['id'])]);
            
            ?>" > <?php echo $mt['name'] ?> </a>and Avail this offer</p>
                              <h3>SHARE THIS HAPPINESS</h3>
 
                              <div style="margin-top:10px;margin-left:10px;"  class="addthis_inline_share_toolbox" data-media="https://www.nayashopi.in/images/Nayashopi.png"  class="addthis_inline_share_toolbox"  data-url="
                                            <?php 
                                             // echo $urlOfDealsDetails;
                                            ?>" data-title="<?php echo  $mt['title']; ?>" data-description="<?php echo  $mt['description']; ?>" ></div>
                                            
                                            

                              </div>
	                          	<div class="gray-clor">	
	                                  	<div class="text-left">	
	                                        	<a href="#" class="code">Code</a>  Upto  <?php echo $mt['discount']; ?>  <span>Verified</span>

	                                          	<h3><?php echo  $mt['title']; ?></h3>
	                                              	<p><?php echo $mt['description']; ?></p>
                                  		</div>
	                                    	<div class="newsletter-block">
	                                            		<span>Subscribe to Nayashopi</span>
		                                              	<div class="news-blc">
                                                      <input type="text" value="" placeholder="Enter your Email ID"> 
                                                      <button>Subscribe</button>
                                                    </div>
		                                    </div>
                               </div>
                          </div>
    
                      </div>
                        </div>
</div>










      <?php } ?>
		</div>
		 
	</div>

  <?php } ?>








  <section class="mobilepricelist" style="display:none;">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 mobileview-colume ">
        <h1 itemprop="name"> <span> Mobile Phone Price Lists </span> </h1>
        <div class="full-row priceblock b-shadow">
          <ul class="row">
              <?php
           $priceMenu = \common\models\Menus::getOneLevelMenuItemsByType('Footer Mobile Phone Price Menu');
            if(count($priceMenu) > 0):
            foreach($priceMenu as $myMenu){
           ?>
              <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6"> <a href="<?=\yii\helpers\Url::to(['prettyurl/index', 'slug' => $myMenu->slug]);?>"> 
              <p> <?php echo $myMenu->title?></p>
              </a>
              </li>
              <?php }
            endif;?>
           </ul>
        </div>
      </div>
    </div>
  </section>
  
</div>

   
<?php 
     $this->registerJs( <<< EOT_JS_CODE
$(document).ready(function() {
  var clipboard = new Clipboard('.btn');
  
  
    clipboard.on('success', function (e) {
      console.log(e);
    });
  
    clipboard.on('error', function (e) {
      console.log(e);
    });
				
         $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    navigation:true,
navigationText: [
   "<div class='carousel-control-prev crsl-wdgt__prvs-btn '><</div>",
   "<div class='carousel-control-next  crsl-wdgt__next-btn'>></div>"
],
    responsive:{
        0:{
         items:2,
             nav:true
        },
        600:{
         items:2,
            nav:true
        },
        1000:{
          items:6,
            nav:true,
            loop:false
 			
        }
    }
});

$('.promo').hide();

 $('.owl-carousel-home').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    navigation:true,
navigationText: [
   "<div class='carousel-control-prev crsl-wdgt__prvs-btn '><</div>",
   "<div class='carousel-control-next  crsl-wdgt__next-btn'>></div>"
],
      itemsCustom : [
       [320,2],
        [768,2],
        [1000, 3],
        [1200,3],
        [1400, 3],
        [1600, 3]
      ],
      navigation : true
});
$('.promo').show();
		
        });
	

 jQuery('body').on('click','.codelink',function(){
          var url = jQuery(this).attr('link');
//          window.open(url,'_blank');
    setTimeout(function(){  window.open(url,'_blank'); }, 5000);        
          
        });


  
EOT_JS_CODE
);
?>
