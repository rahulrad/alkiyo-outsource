<?php 
use common\models\Sliders;
use frontend\helpers\NoImage;

if(empty($categoryId))	
	$sliders = Sliders::getHomePageSlider(0);
else
	$sliders = Sliders::getCategorySlider($categoryId);?>
<?php if(count($sliders)):?>
	  <?php if(empty($categoryId)):?>
      <div class=" main-banner">
      <?php else:?>
      <div class="mobile_slidbg full-row brdr ">
      <?php endif;?>
      <div id="myCarousel" class="carousel  slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <?php 
          $index = $i = 0;
          
          foreach($sliders as $slider):
          	$images[$index] = isset($slider->sliderImages) ? $slider->sliderImages : [];
          	foreach ($images[$index] as $image): ?>
            <div class="item <?=($i==0 ? 'active':'')?>">
              <a target="_blank" href="<?=empty($image->link) ? 'javascript:void(0)': $image->link?>"> 
                <img src="/<?=NoImage::getNoImageIfNoImageExists($image->image, NoImage::noImageSlider) ?>" alt="" style="height:233px; width: 100%;" />
              </a>
            </div>
          <?php $i++;
			endforeach;
			$index++;
          endforeach;?>
        </div>
        <div class="carousel-control-prev crsl-wdgt__prvs-btn " data-slide="prev">
    <
  </div>
  <div class="carousel-control-next  crsl-wdgt__next-btn"  data-slide="next">
   >
  </div>
        <ul class="nav nav-pills nav-justified">
          <?php $index = $i = 0;
          foreach($sliders as $slider):
          	foreach ($images[$index] as $image): ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="<?php echo ($i==0?'active':'')?>">
            <a href="<?=$image->link?>">
             
            </a>
          </li>
          <?php $i++; 
          	endforeach;
          	$index++;
          endforeach;?>
        </ul>
      </div>


      
      </div>
<?php endif;?>

 <?php  if(empty($categoryId) && !empty($dealsbanner) && count($dealsbanner)>0){ ?>

<div class="row">
        <div class="col-lg-9 col-sm-12  col-md-9 col-xs-12 " style="overflow:hidden;padding-right:0px; margin-right:0px;">
           <div class="spnsr-hdr">
                <div class="spnsr-hdr__txt">Hot Deals</div>
                <a class="spnsr-hdr__logo spnsr-gear__logo" href="/gear"></a>
            </div>
     <div class="promo clearfix owl-carousel-home carousel slide"  data-ride="carousel">
        
<?php foreach($dealsbanner as $k => $deals) { ?>

    <div class="item">
       <a rel="nofollow" target="_blank" href="<?php echo $deals->url; ?>" >
       <img  src="/img/slider/<?php echo $deals->file ?>" ></a>
    </div>


<?php } ?>
       
    
      </div>
      </div>
      </div>
       <?php }?>

