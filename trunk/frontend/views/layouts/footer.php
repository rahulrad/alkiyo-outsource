
<div class="container-fluid no-padding counter-section">
		<div class="">
			
				<div class="col-md-6 col-sm-6 col-xs-6 invite_box"> 

					<div class="earn_blck">
						
						<h3>Subscribe to our newsletter to save big!</h3>
						<h4>Subscribe -&gt; Get Amazing deals &amp; Coupons.</h4>
						<div class="input-group">
							<input type="text" placeholder="Enter Address" class="form-control">
							<span class="input-group-btn">
								<button type="button" title="Subscribe" class="btn">Send</button>
							</span>
						</div>
					</div>
				</div>
				 
				 
				<div class="col-md-6 col-sm-6 col-xs-6 app_box">
					<h4 style="color:#fff;font-szie:30px;">Ours Apps</h4>
					 <img src="https://www.nayashopi.in/images/app1.png" class="desktoapp">
					 <img src="https://www.nayashopi.in/images/app2.png" class="desktoapp">
					 
					 

				</div>
				<div class="clearfix"></div>
		</div>
	</div>
<?php $footerLinks = common\models\FooterLinks::find()->all();


  ?>
<footer class="footer-main">
  
<div class="footer-main-block">
			<!-- Container -->
			<div class="">
				<div class="rows">
					<div class="col-md-7">

						<aside class="col-md-4 col-sm-6 col-xs-6 ftr-widget widget_support">
						<h4 class="widget-title">Policy Info</h4>
						<ul>
							<li><a title="Privacy Policy" href="<?=yii\helpers\Url::to(['home/slug', 'slug' => 'privacy-policy'])?>">Privacy Policy</a></li>
							<li><a title="Disclaimer" href="<?=yii\helpers\Url::to(['home/slug', 'slug' => 'disclaimer'])?>">Disclaimer</a></li>
							
						</ul>
					</aside>
					<aside class="col-md-4 col-sm-6 col-xs-6 ftr-widget widget_support">
						<h4 class="widget-title">Company</h4>
						<ul>
							<li><a title="About Us" href="<?=yii\helpers\Url::to(['home/slug', 'slug' => 'about-us'])?>">About Us</a></li>
							<li><a title="Carrer" href="<?=yii\helpers\Url::to(['home/slug', 'slug' => 'career'])?>">Career</a></li>
						
						</ul>
					</aside>
					<aside class="col-md-4 col-sm-6 col-xs-6 ftr-widget widget_support">
						<h4 class="widget-title">Business</h4>
						<ul>
							<li><a title="Advertise with us" href="#">Advertise with us</a></li>
							<li><a title="Contact Us" href="<?=yii\helpers\Url::to(['site/contact', 'title' => 'contact'])?>">Contact Us</a></li>
						
						</ul>
					</aside>
					  </div>

					<div class="col-md-5">

					<aside class="col-md-4 col-sm-6 col-xs-6 ftr-widget widget_support">
						<h4 class="widget-title">Need help</h4>
						<ul>
							<li><a title="FAQ" href="<?=yii\helpers\Url::to(['home/slug', 'slug' => 'faq'])?>">FAQs</a></li>
							<li><a title="Live Chat" href="#">Live Chat</a></li>
						
						</ul>
					</aside>

					<aside class="col-md-8 col-sm-6 col-xs-6 ftr-widget widget_newsletter">
						<h4 class="widget-title">Subscribe</h4>
						<p>We care for our users, Subscribe to our newsletter and get the latest deals.</p>
						<div class="input-group">
							<input type="text" placeholder="Enter Address" class="form-control">
							<span class="input-group-btn">
								<button type="button" title="Subscribe" class="btn">Go</button>
							</span>
						</div>
					</aside>


					   
				</div>


					 
				</div>
				 
			</div><!-- Container /- -->
		</div>

  

  <div class="cat-links-wrapper">
			<ul>
      <?php foreach($footerLinks as $li => $vi){ ?>

        <li><a href="<?php echo $vi->link; ?>"><?php echo $vi->link_name; ?></a></li>
			

      <?php } ?>
        

			</ul>
		</div>




  
  <div class="copyright">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<p>Copyright ©2017 Raditya Web Technology Pvt. Ltd.   |   All Rights Reserved     </p>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 ftr-social">
								 
								<ul>
									<li><a title="Facebook" data-toggle="tooltip" target="_block" href="https://www.facebook.com/nayashopi">
										<i class="fa fa-facebook"></i></a></li>
									<li><a title="Twitter" data-toggle="tooltip" href="https://twitter.com/nayashopi" target="_block"><i class="fa fa-twitter"></i></a></li>
									<li><a title="Google Plus" data-toggle="tooltip" href="https://plus.google.com/108651457881845957439" target="_block"><i class="fa fa-google-plus"></i></a></li>
									 
								</ul>
							</div>
		
  </div>
</footer>

<span  class="page-scroll b-shadow"> <i class="fa fa-arrow-up"></i> </span> 
<!-- Modal -->
<div class="modal fade" id="login-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog login-model
  accessories" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 succes-point hidden-xs ">
            <figure class="text-center"> <img style="margin-top:10px;" src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/Nayashopi.png" alt=""> </figure>
            <ul>
              <li> <i class="sprite sprite-icon_delivery "></i>
                <p style="display: inline-block;
    margin-left: 10px;">Manage Your Orders <strong>Easily Track Orders, Create Returns</strong> </p>
              </li>
              <li> <i class="sprite sprite-icon_alert "></i>
                <p style="display: inline-block;
    margin-left: 10px;">Make Informed Decisions <strong>Get Relevant Alerts And Recommendations</strong> </p>
              </li>
              <li>  <i class="sprite sprite-icon_social "></i>
                <p style="display: inline-block;
    margin-left: 10px;">Engage Socially <strong>With Wishlists, Reviews, Ratings </strong> </p>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#login" aria-controls="home" role="tab" data-toggle="tab">LOGIN</a></li>
              <li role="presentation"><a href="#signup" aria-controls="profile" role="tab" data-toggle="tab">SIGN UP</a></li>
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="login">
                <div class="main"><?php
                  use dektrium\user\widgets\Login;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;

$model = \Yii::createObject(LoginForm::className());
// echo Login::widget();
	if (Yii::$app->user->isGuest): ?>
    <?php $form = ActiveForm::begin([
        'id'                     => 'login-widget-form',
        'action'                 => \yii\helpers\Url::to(['/user/security/login']),
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'validateOnBlur'         => false,
        'validateOnType'         => false,
        'validateOnChange'       => false,
    ]) ?>

    <?= $form->field($model, 'login')->textInput(['placeholder' => 'Username or Email']) ?>

    <?php //$form->field($model, 'password')->passwordInput(['placeholder' => 'Password']) ?>
	<?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', 'Password') . (true ? ' (' . Html::a(Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']) . ')' : '')) ?>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-primary btn-block']) ?>

    <?php ActiveForm::end(); ?>
<?php else: ?>
    <?= Html::a(Yii::t('user', 'Logout'), ['/user/security/logout'], [
        'class'       => 'btn btn-danger btn-block no-radius',
        'data-method' => 'post'
    ]) ?>
<?php endif ?>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p class="text-center"> Login with your social account </p>
                      </div>
                    </div>
                      <div class="row">
                          <?php $authAuthChoice = Connect::begin([
                              'baseAuthUrl' => ['/user/security/auth'],
                          ]); ?>
                          <?php foreach ($authAuthChoice->getClients() as $client): ?>
                              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                  <?php
                                  $class = "";
                                  $icon = "";
                                  if (strtolower($client->getName()) == "facebook") {
                                      $class = "btn btn-lg btn-primary btn-block no-radius";
                                      $icon = "facebook";
                                  } else if (strtolower($client->getName()) == "google") {
                                      $class = "btn btn-lg btn-danger btn-block no-radius";
                                      $icon = "google-plus";
                                  }
                                  ?>
                                  <?= $authAuthChoice->clientLink($client, '<i class="fa fa-' . $icon . '"></i> ' . ucwords($client->getName()), array('class' => $class)) ?>
                              </div>
                          <?php endforeach; ?>
                          <?php Connect::end(); ?>
                      </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="signup">
                <div class="main">
                  <!--<form role="form">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Enter Name" id="inputUsernameEmail">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control" placeholder="Enter Email Id"  id="inputPassword">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control" placeholder="Enter Password"  id="inputPassword">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn btn-primary btn-submit"> Log In </button>
                    </div>
                  </form>-->
                  <?php
                 use yii\helpers\Url;
                 use dektrium\user\models\RegistrationForm;
                 $model = Yii::createObject(RegistrationForm::className());
                 $form = ActiveForm::begin([
                   'id'                     => 'registration-form',
                   'action'                 => Url::to(['/user/registration/register']),
                   'enableAjaxValidation'   => true,
                   'enableClientValidation' => false,
               ]); ?>

                <?=$form->field($model, 'email') ?>

                <?=$form->field($model, 'username') ?>

                <?php if (false == false): ?>
                    <?=$form->field($model, 'password')->passwordInput() ?>
                <?php endif ?>

                <?=Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-primary btn-block no-radius']) ?>

                <?php ActiveForm::end(); ?>
                <p>&nbsp;</p>
                 <p>&nbsp;</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
