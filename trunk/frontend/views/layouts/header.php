<?php 
use yii\helpers\Html;?>

<div class="overlay2"></div>
<div class="feedback-side" data-target="#feedback" data-toggle="modal"> <img src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/msdropdown/icons/blank.gif" alt=""></div>
<header class="visible-xs"  data-offset-top="50" data-spy="affix" >
  <div class="container">
      <div class="row navbar-header mobile-header mobileview-hd">
        <div class="col-xs-12 logo ">
	      	<button type="button" id="nav-close" class="navbar-toggle navbar-toggle collapsed waves-effect waves-button"  aria-expanded="false"> 
                  <span class="sr-only">Toggle navigation</span> 
                  <span class="icon-bar"></span> 
                  <span class="icon-bar"></span> 
                  <span class="icon-bar"></span> 
           </button> 
           <a href="/"><img src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/mobile-logo.png" alt="" class="img-responsive"></a> 
        </div>
        <div class=" " style="padding-left:0px; padding-right:8px;"> 
           <ul class="mobile-login">
           <div class ="fa fa-search fa-4 pull-left"></div>
           <li class="dropdown" >
                  <a href="#" class="dropdown-toggle bdr-right" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ><i class="fa fa-user fa-4 "></i>
                  </a>
                     <ul class="dropdown-menu login-menu dropdown-menu-right" aria-labelledby="myaccount">
                         <li>
                            <div class="full-row topsection"> <a href="#" class="newuser" data-toggle="modal"  data-target="#login-box"> New user ? <strong>Register </strong> </a>
                               <div class="clearfix"></div>
                                <a href="#"  data-toggle="modal"  data-target="#login-box" class="btn btn-danger btn-block"> Login</a>
                            </div>
                          </li>
                  
                       </ul>
                </li>
              
               
                
             </ul>
        </div>
      </div>
      <div class="row mobileview-hd2">
        <div class="col-xs-12 text-center menu-xs-bg">
             <div class="">
                 <span class="">
                    <a href="javascript:void(0)" id="m-submitSearch"> 
                        <i class="fa fa-search"></i>
                    </a>
                  </span> 
			          <input type="text" class="form-control globalSearch" id="m-globalSearch" placeholder="Search your items..">
             </div>
        </div>
      </div>
    
      <div class="row">
        <div class="col-xs-12 ">
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
          </div>
        </div>
      </div>
  </div>
</header>

<div class="visible-lg visible-md visible-sm">
  <div class="overlay"></div>
  <header class="mainheader  header-main container-fluid no-padding ">
    <div class="container">
      <div class="logo_head">
   
        <div class="col-md-3  no-padding"> 
        <div class="navbar-header">
			<a href="/">
				<img src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/Nayashopi.png" alt="" class="img-responsive" />
			</a>
      </div>
		    </div>
         
          <div class="col-md-6 col-xs-6  no-padding mob-see search-block  ">
          <div class="pull-center">
          <div class="top_serch">
            <input type="text" class=" globalSearch" id='globalSearch' placeholder="Search milions of products from online stores">
           
            <button type="button" id="submitSearch">
            	<i class="fa fa-search"></i>
							</button>
           
        </div>
       </div>
        </div>
         <div class="col-md-3 col-xs-6 no-padding mob-see ">
        
        <div class="top_logins pull-right">
          <ul class="login ">
          
                  
       
            <li class="dropdown" >
            
            
            
          <a href="#" class="dropdown-toggle signin" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
          
          <i class="fa fa-unlock-alt "></i> 
           <span>  
           
            <?= Yii::$app->user->isGuest ? ' Sign in / Sign up ':Yii::$app->user->identity->username?>  </span> 
            
            
            <i class="fa fa-angle-down "> </i>
            
            
            </a>
             
             
             
             
              <ul class="dropdown-menu login-menu dropdown-menu-right" aria-labelledby="myaccount">
              
                <div><?php
                  
                  if(Yii::$app->user->isGuest):?>
                    <div class="full-row topsection"> <a href="#"  data-toggle="modal"  data-target="#login-box" class="btn btn-danger btn-block"> Login </a>
                      <div class="clearfix"></div>
                      <a href="#" class="newuser" data-toggle="modal"  data-target="#login-box"> New user ? <strong>Register </strong> </a> </div><?php 
                   else:?>
                      <div class="full-row topsection"><?=
                          Html::beginForm(['/user/security/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-danger btn-block']
            )
            . Html::endForm();?>
                          
                      
                      </div><?php
                      endif;?>
                </div>
              </ul>
            </li>
          
            
             
          </ul>
          </div>
        </div>
        </div>
     
    </div>
             <?php   Yii::$app->MenuComponent->menu(); ?>
     </header>
</div>


































<!-- feedback -->
<div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
     <h3> Feedback   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> </h3>
       
      </div>
      <div class="modal-body">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
           
            
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="main">
                  <form role="form">
                    <div class="form-group">
                      <input type="text" class="form-control no-radius" placeholder="Name" >
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control no-radius" placeholder="Email" >
                    </div>
                     <div class="form-group">
                      <input type="text" class="form-control no-radius" placeholder="Subject" >
                    </div>
                    
                      <div class="form-group">
                
                <textarea cols="" rows="" class="form-control no-radius" placeholder="Enter Message" ></textarea>
                    </div>
                    
                  </form>
                  <div class="form-group">
                   
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <a href="#" class="btn btn-lg btn-primary btn-block no-radius"> Submit </a> </div>
                    </div>
                  </div>
                </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if(Yii::$app->controller->id =="prettyurl"){ ?>
<!--compare box-->
<div class="cmpr-pnl-wrpr add-cmp-ml">
  <div class="sidebaroverlay"></div>
  <div class="compareouter">
      <div class="cmpr-pnl__close compare-side"> <img src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/msdropdown/icons/blank.gif" alt=""> </div>
  <div class="cmpr-pnl sctn sctn--sdbr cmpr-pnl-list cmpr-pnl-list clearfix ui-front cmpr-pnl--bx-shdw">


  <span class="btn-compare-close"> <i class="fa fa-close"></i></span>

    <div class="sctn__hdr clearfix">
      <div class="sctn__ttl">Compare Products</div>
    </div>
      <div class="full-row comparerow" >
        <div class="compare-product"> <img data-blankimage="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png"  class=" img-responsive " src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png" alt=""> </div>
        <div class="compare-box">
          <input disabled placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch1" class="form-control hugoSearch">
          </div>
      </div>
      <div class="full-row comparerow" >
        <div class="compare-product"> <img data-blankimage="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png" class=" img-responsive " src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png" alt=""> </div>
        <div class="compare-box">
          <input disabled placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch2" class="form-control hugoSearch">
          </div>
      </div>
      <div class="full-row comparerow hidden-xs" >
        <div class="compare-product"> <img data-blankimage="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png"  class=" img-responsive" src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png" alt=""> </div>
        <div class="compare-box">
		  <input disabled placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch3" class="form-control hugoSearch">              
        </div>
      </div>
      <div class="full-row comparerow hidden-xs" >
        <div class="compare-product"> <img data-blankimage="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png"  class=" img-responsive " src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/generic_product_image_small.png" alt=""> </div>
        <div class="compare-box">
          <input disabled placeholder="Enter product name" required type="text" name="compproducts[]" placeholder="Search your items.." id="hugoSearch4" class="form-control hugoSearch">
          </div>
      </div>
      
      <a class="btn btn-block " style="background-color:#ff8d3d;color:#fff;font-weight:bold;" id="compareproducts"  href="javascript:void();"><i class="fa fa-copy"></i> Compare </a> 
   
	<a href="javascript:void()" class="esearchmore visible-xs visible-sm hidden-md hidden-lg" style="float: left;
    width: 100%;
    margin-top: 18px;
    text-align: center;
    font-size: 16px;">Search More to Compare</a>
 </div>
    </div>
  </div>
<?php } ?>

<?php 
     $this->registerJs( <<< EOT_JS_CODE
$(document).ready(function() {
// -------for subdropdown-----------
	$(".dropdown-menu-xs").click(            
	function() {
	$('.dropdown-menu-xs ', this).stop( true, true ).fadeIn("fast");
	$(this).toggleClass('open');
	$('b', this).toggleClass("caret caret-up");                
	},
	function() {
	$('.dropdown-menu-xs', this).stop( true, true ).fadeOut("fast");
	$(this).toggleClass('open');
	$('b', this).toggleClass("caret caret-up");                
	});	
	
});
 $(document).ready(function () {
	
   $('.cmpr-pnl__close,.esearchmore').click(function (e) {



        $('.cmpr-pnl-wrpr').toggleClass("add-cmp-ml");
	 $('#cmpr-pnl__close').toggleClass("slide-right");   
	  $('.btn-compare-close').toggleClass("slide-right");            
               
            });
			
			
            $('.btn-compare-close').click(function (e) {
  
	  $('.cmpr-pnl-wrpr').toggleClass("add-cmp-ml");            
               
            });
 
 });
$(document).ready(function() {
/*$('.dropdown-menu-xs').on('click', function () {
    if(!$(this).siblings('.dropdown-menu-xs').hasClass('open')) {
       $(this).siblings('.dropdown-menu-xs').click();
       }
});*/

$(".navbar-toggle").click(function(){
//$( "body" ).addClass( "in" );
if ( $( "body" ).hasClass( "in" ))  $( "body" ).removeClass("in");
else $("body").addClass("in");

});




$(".overlay2").click(function(){
$(".navbar-collapse.collapse.in").removeClass("in");
$("body.in").removeClass("in");
$(this).hide();
});


  

});



EOT_JS_CODE
);
?>

<?php 
$url = \yii\helpers\Url::to(['product/comparesearch']);
$redirectUrl = \yii\helpers\Url::to(['product/compare']);
$source = <<< Source
	source: function( request, response ) {
        $.ajax( {
          url: "{$url}",
          dataType: "json",
          data: {
            term: request.term, category: jQuery('input[name="category_id"]').val()
          },
          success: function( data ) {
			// Handle 'no match' indicated by [ "" ] response
            response( data.length === 1 && data[ 0 ].length === 0 ? [] : data );
          }
        } );
	},
Source;

$script = <<< JS
jQuery(document).ready(function () {
    $('#rightcomparebox').on('click',function(e){
		e.preventDefault();
		window.location = '{$redirectUrl}' + getQueryStringForRightCompareBox();
	});
    jQuery( "#hugoSearch1" ).autocomplete({
      minLength: 2,
      {$source}
      focus: function( event, ui ) {
        jQuery( "#hugoSearch1" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        jQuery( "#hugoSearch1" ).val( ui.item.slug );
//         jQuery( "#hugoSearch-id" ).val( ui.item.value );
//         jQuery( "#hugoSearch-description" ).html( ui.item.slug );
//         jQuery( "#hugoSearch-icon" ).attr( "src", ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div><p>" + item.label + "</p><img style=\"width:80px;height:80px\" src=\"" + item.icon + "\" /></div>" )
        .appendTo( ul );
    };
    jQuery( "#hugoSearch2" ).autocomplete({
      minLength: 2,
      {$source}
	focus: function( event, ui ) {
        jQuery( "#hugoSearch2" ).val( ui.item.label );
        return false;
	},
      select: function( event, ui ) {
        jQuery( "#hugoSearch2" ).val( ui.item.slug );
//         jQuery( "#hugoSearch-id" ).val( ui.item.value );
//         jQuery( "#hugoSearch-description" ).html( ui.item.slug );
//         jQuery( "#hugoSearch-icon" ).attr( "src", ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div><p>" + item.label + "</p><img style=\"width:80px;height:80px\" src=\"" + item.icon + "\" /></div>" )
        .appendTo( ul );
    };
	jQuery( "#hugoSearch3" ).autocomplete({
      minLength: 2,
      {$source}
      focus: function( event, ui ) {
        jQuery( "#hugoSearch3" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        jQuery( "#hugoSearch3" ).val( ui.item.slug );
//         jQuery( "#hugoSearch-id" ).val( ui.item.value );
//         jQuery( "#hugoSearch-description" ).html( ui.item.slug );
//         jQuery( "#hugoSearch-icon" ).attr( "src", ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div><p>" + item.label + "</p><img style=\"width:80px;height:80px\" src=\"" + item.icon + "\" /></div>" )
        .appendTo( ul );
    };
	jQuery('.btn-group').click(function(){
		var queryString = getCompareQueryString();
		var location = "{$redirectUrl}" + queryString;
		window.location.href = location;
	});
    
    function getQueryStringForRightCompareBox(){
    	var queryString = '?';
		jQuery('input[name^="compproducts"]').each(function() {
	        console.log(jQuery(this).val());
			if(jQuery.trim(jQuery(this).val()).length != 0){
				queryString = queryString.concat('products[]=' +jQuery.trim(jQuery(this).val()) + '&');
			}
		});
    	return queryString;
	}
} );
JS;

$this->registerJs($script, \yii\web\View::POS_END);


