<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;


AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<?php if( Yii::$app->controller->id === "deals" || Yii::$app->controller->id === "home"){ ?>
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-592a9063893f738f"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.2/clipboard.min.js"></script> 
  

  
   <?php } ?>
   
    <meta charset="<?= Yii::$app->charset ?>">
        <link rel="shortcut icon" href="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/favicon.ico">
            <link rel="shortcut icon" type="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/images/favicon.ico" />

    <?php
        
        if(isset($this->params['index'])){
       ?>
       <META NAME="robots" CONTENT="noindex,nofollow"/>
       <?php
        }
         ?>
        
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
<meta name="theme-color" content="#0c4572" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WF3GT2P');</script>
<!-- End Google Tag Manager -->
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WF3GT2P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="">
<div >
<?php $this->beginBody() ?> 
<?php $this->beginContent('@app/views/layouts/header.php'); ?>
    <!-- You may need to put some content here -->
<?php $this->endContent(); ?>
<?php //$this->beginBody() ?>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('.dropdown-menu-child  a:not(a[href="#"])').on('click', function() {
    	self.location = jQuery(this).attr('href');
	});
	jQuery('#submitSearch').on('click', function(){
		var searchTerm = jQuery('#globalSearch').val();
		return false;
		if(searchTerm.length > 0)
			window.location = '<?=\Yii::$app->getUrlManager()->createUrl('search/index').'?term=' ?>'+searchTerm;
	});
	jQuery('#m-submitSearch').on('click', function(){
		return false;
		var searchTerm = jQuery('#m-globalSearch').val();
		if(searchTerm.length > 0)
			window.location = '<?=\Yii::$app->getUrlManager()->createUrl('search/index').'?term=' ?>'+searchTerm;
	});
	jQuery('#globalSearch').on('keypress',function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	return false;
	        jQuery('#submitSearch').trigger('click');
	    }
	});
	jQuery('#m-globalSearch').on('keypress',function(event){
		
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	return false;
	        jQuery('#m-submitSearch').trigger('click');
	    }
	});
<?php 
$url =  \yii\helpers\Url::to(['search/products']);

$source = <<< Source
source: function( request, response ) {
    $.ajax( {
      url: "{$url}",
      dataType: "json",
      data: {
        term: request.term
      },
      success: function( data ) {
		// Handle 'no match' indicated by [ "" ] response
        response( data.length === 1 && data[ 0 ].length === 0 ? [] : data );
      }
    } );
},
Source;

$script = <<< JS

jQuery( ".globalSearch" ).autocomplete({
    minLength: 2,
    source: "{$url}",
    focus: function( event, ui ) {
      jQuery( ".globalSearch" ).val( ui.item.label );
      return false;
    },
    select: function( event, ui ) {
      jQuery( ".globalSearch" ).val( ui.item.slug );
	  window.location =  ui.item.url;
      return false;
    }
  })
  .autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( item.label )
      .appendTo( ul );
  };
JS;

echo $script;?>
});
</script>
<div class="page_block margin10">
<div class="container-fluid no-padding photobanner">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>

        </div>

        
        <?php $this->beginContent('@app/views/layouts/footer.php'); ?>
        <?php $this->endContent(); ?>
        <?php $this->endBody() ?>

</div>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96463096-3', 'auto');
  ga('send', 'pageview');

</script>

</body>
<?php Yii::$app->MenuComponent->mobileMenu(); ?>
</html>
<?php $this->endPage() ?>
