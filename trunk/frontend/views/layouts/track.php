<!DOCTYPE html>
<html>
<head>

    <meta name="robots" content="noindex, nofollow">
    <meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>

    <style>

        body{
            font-family: 'Noto Sans', Arial, serif;
            font-weight: 400;
            -webkit-font-smoothing:antialiased;
            -moz-osx-font-smoothing:grayscale;
            line-height: 1.618em;
            background-size: cover;
        }
        h2{
            font-family: 'Noto Sans', Arial, serif;
            font-weight: 700;
            font-size:40px;
            line-height: 1.618em;
        }
        section{
            max-width:800px;
            margin:8% auto 1em auto;
            opacity: 0.8;
            filter: alpha(opacity=80); /* For IE8 and earlier */
            color:#222;
            padding:1em 5%;
            text-align: center;
        }

        a{
            color: #00CC66;
        }
        a:focus{
            outline:none;
            outline-offset:inherit;
        }
        @media (max-device-width: 1027px) {

            body{
                text-align:center;
                font-size:larger;
            }
            section{
                max-width: 90%;
            }

        }

        @media (max-device-width: 640px) {
            section{
                max-width: 97%;
            }

        }


    </style>
</head>
<body>

<?= $content ?>

</body>
</html>