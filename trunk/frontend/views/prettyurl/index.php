<?php

$request = Yii::$app->request;
        $slug = $request->get('slug', null);
if(isset($category->display_name))
$this->title = $menuItemObject->meta_title;
else 
	$this->title = 'No records to display.';

use frontend\helpers\NoImage;
$this->registerCssFile("/css/jquery.mCustomScrollbar.css");
?>
<style type="text/css">
  #loading {width: 50%;height: 80%; top: 150px; left: 300px; position: fixed;display: block;opacity: 0.7;background-color: #fff;z-index: 99; text-align: center;
}
#loading-content img{margin-top:40%}
.hide{
  display: none;
}
ul.featureList , ul.brandList{
   list-style-type: none;
}
</style>
<div class="container">
  <div class="row ">
    <div class="col-lg-12">
      <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li ><i class="fa fa-home"></i></li>
        	            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        	           
	 <?php              
					 echo  $menuItemObject->title; 
				
        	                   ?>
</li>
      </ol>
    </div>
  </div>

  <section class="innerpage ">

<?php if(isset($menuItemObject->banner_image) && !empty($menuItemObject->banner_image)){  ?>  
<img style="width:100%;margin-bottom:10px;" src="<?php echo $menuItemObject->banner_image; ?>"/>

<?php } ?>


    <div class="row">
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padd-l7 caregori_podectblog right-block catgories-lisitng mobileview-colume">
          <div class="row caregori_podectblog hidden-xs">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="panel panel-default">
          <div class="panel-footer bdr-btm">
           <h1 itemprop="name"> <?php  if($menuItemObject->is_popular){ echo $menuItemObject->title; }else{ echo "Buy ".$menuItemObject->title." at Lowest Price Online	"; } ?></h1>
           <div class="clearfix"></div>
           <?=frontend\components\Slider::widget(['categoryId' =>$category->category_id])?>
           </div>
           <div class="panel-body">
            <div class="mobile_slidbg full-row ">
            <!-- sub categories-->
           <!-- sub categories-->
           </div> </div> 
        </div>        </div>      </div>
        <div class="row hidden-xs">
          <div class="col-lg-12">
            <div class=" amh_row2 ">
 <div class="mobile_slidbg full-row ">
            <!-- sub categories-->
           <ul class="catgories-list">
             <?php foreach ($siblings as $subCategory): if(!$subCategory->show_on_mega_menu){continue;} ?>
             <li>
               <a href="<?php  echo \yii\helpers\Url::to(['prettyurl/index','slug'=>$subCategory->slug]);?>"> 
                     <img  itemprop="image" class="center-block " src="/<?=NoImage::getNoImageIfNoImageExists($subCategory->image, NoImage::noImageCategory) ?>" alt="<?=$subCategory->title?>" /> 
                 <p><?=$subCategory->title?></p>
               </a>
             </li>
             <?php endforeach;?>
           </ul>
           <!-- sub categories-->
           </div>         
<div id="dynamic_text" class="gs_readmore">
                       
          <p itemprop="description"><?php echo $menuItemObject->short_description;?></p>

          </div>
      </div>
          </div>
        </div>
        <section class="product-section">
     	  <!-- Tab Order Start Div -->
          <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tab-order">
            <div class="full-row tab-orderview paddt-none">           
             <div class="row visible-xs">
              <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
              <ul class="mobile-view-menu">
              <li class="filter"><i class="fa fa-filter"></i> Filter </li>
              <li> 
              <a href="#" data-toggle="modal" class="sortby-btn" data-target="#shortby"> <i class="fa fa-long-arrow-up"></i><i class="fa fa-long-arrow-down"></i> Sort By </a> </li>
                <!--<li class="dropdown"> <a href="#" class="dropdown-toggle" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-long-arrow-up"></i><i class="fa fa-long-arrow-down"></i> Short By </a>  
                <div class="dropdown-menu">
                <ul role="tablist" class="nav nav-tabs">
	 <li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="new" href="javascript:void(0);" aria-expanded="false">New Arrivals</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="discount" href="javascript:void(0);" aria-expanded="false">Discount</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_low" href="javascript:void(0);" aria-expanded="false">Lowprice</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_high" href="javascript:void(0);" aria-expanded="false">Highprice</a></li>
<li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="popular" href="javascript:void(0);" aria-expanded="false">Popularity</a></li>
            </ul>
                    </div>
                </li>-->
              </ul>
              </div>
              </div>
   <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pos-rel hidden-xs ">
            <h3 itemprop="name" class="pull-left"> <?php /*?><?php echo isset($category->display_name) ? $category->display_name :''?><?php */?>  <span  class="hidden-xs" style="font-size:12px"> Showing <?php if((int)\Yii::$app->params['listingResults']>(int)$total){echo $total;} else {echo \Yii::$app->params['listingResults'];}  ?> out of <?=$total?></span></h3> <ul role="tablist" class="nav nav-tabs centerfilter hidden-xs" id="sortParams">
<li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="new" href="javascript:void(0);" aria-expanded="false">New Arrivals</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="popular" href="javascript:void(0);" aria-expanded="false">Popularity</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="discount" href="javascript:void(0);" aria-expanded="false">Discount</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_low" href="javascript:void(0);" aria-expanded="false">Lowprice</a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_high" href="javascript:void(0);" aria-expanded="false">Highprice</a></li>
            </ul>
           
             <span class="view-type">
             <span> View  </span>
		<a href="#" id="gridView" class="active">  <i class="fa  fa-th"></i> </a>
              <a href="#" id="listView"><i class="fa fa-list"></i></a>
             </span>            </div> 
            </div>
			</div>    
			</div>
        </div>
          <!-- Tab Order Start Div End -->
        <!-- Product Row -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tab-content "> <!-- tab-content -->
             <?php // products will get populated here... ?>
             <!--main View-->
             <div id="mainContentDiv" class="tab-pane active" role="tabpanel">
                 <!--display:none listViewContents-->
                <div class="full-row no-columepadding" style="display:none" id="listViewContents">
                <?=$this->render('@frontend/views/search/productListing', ['products' => $products, 'menuItem'=>$menuItemObject])?>
                </div>
                <!-- display:none listViewContents --> 
                
                <!--display:block gridViewContents --> 
                  <div class="full-row no-columepadding" style="display:block" id="gridViewContents">
                	<?=$this->render('@frontend/views/search/productGridListing', ['products' => $products, 'menuItem'=>$menuItemObject])?>
                  </div>
                  <!--display:block gridViewContents--> 
                  
                 <div class="row">
                 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="errMsg" style="display:none"></div>
                 </div>
                 <?php if($total > $recordPerPage):?>
                 <div class="row">
                    <div class="colorred col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="javascript:void(0);" class="btn btn-block btn-loadmore bgnone" id="viewMore"> Show More <i class="fa fa-angle-down"></i> <i class="fa fa-angle-down"></i></a> </div>
                 </div>
                 <?php endif;?>
<div class="amh_row2">
                   <p itemprop="description"><?=$description?></p>

                   </>
             </div>
             <!--main View-->  
             
            </div> <!-- tab-content-->
            <input type="hidden" value="1" id="page" name="page">
          </div>
        </div>
        <!-- Product Row End -->
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd-r7 brand left-panel filter-in">
      <div class="filterbutton-row" style="display:none">
      
      <div class="col-xs-6 no-padd">
       <button type="button" class="btn btn-default btn-filter btn-block no-radius"> Cancel </button>
       </div>
      <div class="col-xs-6  no-padd">
  <button type="button" class="btn btn-danger btn-block  btn-filter no-radius"> Apply </button>
         </div>       
  
      </div>

 <div class="visible-xs hidded-sm hidden-md hidden-lg mobilefilter">
      
      <span>All FIlters</span> 
      <span class="clearall"> Clear All</span>   
  
      </div>
           <!-- Suppliers -->
        <?php $suppliers = \common\models\Suppliers::findAll(['is_delete' => 0]);?>
       <section class="brand brand-block" style="margin-bottom:4px;margin-top:0px;">
        <!-- Brand Search Box -->
        <div class="row lefty selected">
          <div class="col-xs-12 logo text-center ">
            <h4 class="text-left">
            Suppliers <span class="popup__name-cnt"></span> <span id="clear_suppliers"> Clear  </span>
            </h4>
            <div class="input-group searcbar hidden-xs" >
              <input type="text" class="form-control" name="txtSupplier" id="txtSupplier">
              <span class="input-group-addon "> <i class="fa fa-search"></i> </span>
            </div>
          </div>
        </div>
        <!-- Suppliers Listing -->
        <div class="row righty" style="display:block;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="full-row left-scrollblock txtSupplierlist" >
              <!-- Feature Item -->
              <?php foreach($suppliers as $supplier):?>
                  <div class="form-group">
                    <label  class="checkbox-inline">
                        <input type="checkbox" name="supplier_ids[]" value="<?=$supplier->id?>" <?=in_array($supplier->id, $supplierArr) ? 'checked="checked"' : ''?> class="suppliersValues" />
                         <?=$supplier->name?> 
                    </label>
                  </div>
              <!-- Feature Item -->
              <?php endforeach;?>
            </div>
          </div>
        </div>
       </section>
       <!-- End of Suppliers Section -->
       <!-- Suppliers -->

       
        <div class="row hidden-xs">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="price_renge_slider">
              <label for="amount">Min - Max</label>
              <p>
                <input type="text" style="border:0; color:#f6931f; font-weight:bold;" readonly id="amount">
              </p>
              <div id="slider-range"></div>
            </div>
          </div>
        </div>
      

       <section class="brand brand-block"  >
        <!-- Brand Search Box -->
        <div class="row lefty">
          <div class="col-xs-12 logo text-center ">
            <h4 class="text-left">
            Brands <span class="popup__name-cnt"></span><a href="#"> <span id="clear_brands"> Clear  </span></a>
            </h4>
            <div class="input-group searcbar hidden-xs" >
              <input type="text" class="form-control" name="txtBrand" id="txtBrand">
              <span class="input-group-addon "> <i class="fa fa-search"></i> </span>
            </div>
          </div>
        </div>
        <!-- Brands Listing -->
        <div class="row righty">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="full-row left-scrollblock brandlist" >
              <!-- Feature Item -->
              <?php
               if(count($brands)==1){  ?>
                 
              <?php foreach($allBrandsArr as $keyb=> $brand):?>
			
                  <div class="form-group">
                    <label  class="checkbox-inline" >
                        <input type="checkbox" name="brand_ids[]" value="<?=$keyb?>" class="brandsValues" <?=in_array($keyb,$arrBrands) ? 'checked="checked"' : ''?> />
                         <?=$brand?> 
                    </label>

                  </div>


           
              <!-- Feature Item -->
              <?php endforeach;?>
               
               
            <?php   }else{  ?>
              
       
              
              <?php foreach($brands as $brand):?>
			 <?php if($brand['count']>0) {?>
                  <div class="form-group">
                    <label  class="checkbox-inline" >
                        <input type="checkbox" name="brand_ids[]" value="<?=$brand['key']?>" class="brandsValues" <?=in_array($brand['key'],$arrBrands) ? 'checked="checked"' : ''?> />
                         <?=$brand['name']?> 
                    </label>

                  </div>
 <?php }  ?>

           
              <!-- Feature Item -->
              <?php endforeach;?>
                 <?php } ?>
            </div>
          </div>
        </div>
        </section>



        <!-- End of Brands Section -->
       <?=frontend\components\SearchFilters::widget(['features' => $features])?>
       <!--gurup--> 
      </div>
    </div>
  </section>

<?php  if(!empty($popular)) { ?>
<div class="Popular-block gtrr">
            <h3>Popular Price Lists</h3> 
            <ul>
 <?php foreach ($popular as $popCategory){ ?>


   <li><a href="<?php  echo \yii\helpers\Url::to(['prettyurl/index','slug'=>$popCategory->slug]);?>"><?php echo $popCategory->title ?></a> </li>

<?php } ?>

            </ul>
  
	</div>


<?php } ?>

<span class="hidden-xs">  
  <?=\frontend\components\RecentlyViewed::widget()?>
</span>
</div>
<!-- Newer version starts here -->
<style type="text/css">
  #loading {
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}
.hide{display: none;}  
ul.featureList , ul.brandList{
   list-style-type: none;  
}
</style>
<input type="hidden" name="pageNumber" id="pageNumber" value="1" />
<div id="loading" class="hide">
  <div id="loading-content"><img src="/images/ajax-gif-load.gif" /></div>
</div>

<div class="modal fade" id="shortby" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        Sort By
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body shortby-content">
      <ul role="tablist" class="nav nav-tabs centerfilter" id="sortParams">
              <li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="popular" href="javascript:void(0);" aria-expanded="false">  Popularity <i ><input checked  value="" type="radio"/></i> </a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="discount" href="javascript:void(0);" aria-expanded="false">Discount <i ><input  type="radio" value=""/></i> </a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_low" href="javascript:void(0);" aria-expanded="false">Lowprice <i ><input  type="radio" value=""/></i></a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="price_high" href="javascript:void(0);" aria-expanded="false">Highprice <i ><input  type="radio" value=""/></i></a></li>
              <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="new" href="javascript:void(0);" aria-expanded="false">New Arrivals <i ><input  type="radio" value=""/></i></a></li>
	  </ul>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
    var currentView = "list";
    var min_price = "<?=$defaultMinPrice?>";
    var max_price = "<?=$defaultMaxPrice?>";
    var page=1;
    var isHash=false;
    var str_brand_ids=commaSepratedCategoryIds = '';
    var sort = 'popular';
    $(document).ready(function() {
    	$( "#slider-range" ).slider({
    	      range: true,
    	      min: 0,
    	      max: max_price,
    	      values: [ <?=$defaultMinPrice?>, <?=$defaultMaxPrice?> ],
    	      slide: function( event, ui ) {
    	        $( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
    	      }
    	});
    	$( "#amount" ).val( "Rs." + $( "#slider-range" ).slider( "values", 0 ) +
    		      " - Rs." + $( "#slider-range" ).slider( "values", 1 ) );
    	$('#sortParams a').click(function(e) {
    		  $('#sortParams li.active').removeClass('active');
          $('#sortParams input').removeAttr('checked');
    		  var $this = $(this);
    		  if (!$this.parent('li').hasClass('active')) {
    		    $this.parent('li').addClass('active');
            $this.find('input').prop("checked", true)
;
    		  }
    		  sort = $this.attr('aria-controls');
    		  ajaxSearch();
          $('.close').trigger('click');
    		  e.preventDefault();
    	});
    <?php /*foreach($features as $feature):?>
        $('#txtFeature_<?=$feature['key'];?>').on('keyup', function () {
            var value = this.value;  
            $('#featureList_<?=$feature['key'];?> div').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $("#clear_<?=$feature['key'];?>").click(function(e){
            e.preventDefault();
           
            $('#txtFeature_<?=$feature['key'];?>').val('');
            $('#txtFeature_<?=$feature['key'];?>').keyup();
            $('#featureList_<?=$feature['key'];?> .featureValues').attr('checked', false); 
           	ajaxSearch();
        });
	<?php endforeach;*/?>
        $('#txtBrand').on('keyup', function () {
            var value = this.value;  
            
            $('.brandlist div.form-group').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $('#txtSupplier').on('keyup', function () {
            var value = this.value;  
            
            $('.txtSupplierlist div.form-group').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $("#clear_brands").click(function(e){
            e.preventDefault();
            $('#txtBrand').val('');
             $('#txtBrand').keyup();
             $('.brandsValues').attr('checked', false); 
                	ajaxSearch();
        });

        $("#clear_suppliers").click(function(e){
            e.preventDefault();
            $('#txtSupplier').val('');
             $('#txtSupplier').keyup();

             $('.suppliersValues').attr('checked', false); 
    	ajaxSearch();
        });
        
        $("#clickBtn").click(function(e){
            e.preventDefault();
        });
        
        $("#listView").click(function(e){
            e.preventDefault();
            currentView = "list";
            $(this).toggleClass('active');
            $("#gridView").toggleClass('active');
            $("#listViewContents").show();
            $("#gridViewContents").hide();
        });

        $("#gridView").click(function(e){
            e.preventDefault();
            currentView = "grid";
            $(this).toggleClass('active');
            $("#listView").toggleClass('active');
            $("#listViewContents").hide();
            $("#gridViewContents").show();
           
        });
        
        $(".featureValues").click(function(e){
           	ajaxSearch();
           
        });
        
        $(".brandsValues, .suppliersValues").click(function(e){
           	ajaxSearch();
        });
        
        $(document).ajaxStart(function(){
        	$("#loading").removeClass('hide');
        }).ajaxStop(function(){
            $("#loading").addClass('hide');
        });
        $( "#slider-range" ).slider({
             change: function( event, ui ) {
             min_price = $( "#slider-range" ).slider("values")[0];
             max_price = $( "#slider-range" ).slider("values")[1];
             if(isHash===false)
				ajaxSearch();
             }
		});

        $(".categoryValues").on('click',function(){
        	//ajaxSearch();
        	var Values = getCommaSepratedCategoryIds();
        	url = '<?php echo \Yii::$app->getUrlManager()->createUrl('search/?term='.\Yii::$app->request->get('term')) ?>';
        	if(Values.length == 0)
            	window.location = url;
        	else
            	window.location = url + '&categoryIds=' + Values;
        });
        $('#viewMore').on('click', function(){
        	var pagenum = parseInt($("#pageNumber").val()) + 1;
        	addResults(pagenum);
        });
        
        /*$(window).scroll(function(){
//         	console.log($(window).scrollTop() );
//             console.log($('#mainContentDiv').height() );
//             console.log($(window).height());
            //console.log($('#mainContentDiv').height() - $(window).height());
        	if ($(window).scrollTop() == ( $('#mainContentDiv').height() - ($('.affix').height() - 50 ) ) ){
        	  //if($(".pagenum:last").val() <= $(".rowcount").val()) {
        	    var pagenum = parseInt($("#pageNumber").val()) + 1;
        	    addResults(pagenum);
        	  //}
        	}
        });*/
     }); // end of document.ready 



    function getCategoryIdsArray(){
        var values = [];
     	$(".categoryValues").each(function() {
             if($(this).prop('checked')==true) 
                values.push($(this).val());
         });
        
     	return values;
 	}
    function getCommaSepratedCategoryIds(){
        var values = getCategoryIdsArray();
        if(values.length > 0)
        	return values.join(',');
    	return '';
	}

    function getFeaturesIdsArray(){
        var values = [];
    	$(".featureValues").each(function() {
            if($(this).prop('checked')==true) 
               values.push($(this).val());
        });
    	return values;
	}
	function getCommaSepratedBrandValues(){
		var values = [];
		$(".brandsValues").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val() );
	    });

	    if(values.length > 0)
	    	return values.join(',');
    	return '';
	}
	function getSuppliersValues(){
		var values = [];
		$(".suppliersValues").each(function() {
	    	if($(this).prop('checked')==true) 
	        	values.push($(this).val() );
	    });

    	return values;
	}

	function getSupplierValuesForQueryString(){
		var values = getSuppliersValues();
		supplierQueryString = '';
    if(values.length > 0){
			for(var i=0; i < values.length; i++){
				supplierQueryString += '&store[]=' +  values[i];
			}
		}
		return supplierQueryString;
	}

	function getFeatureValuesGetString(){
		features = getFeaturesIdsArray();
		getString = '';
		if(features.length > 0){
			for(var i=0; i < features.length; i++){
				getString += '&feature_ids[]=' +  features[i];
			}
		}

		return getString;
	}
	function addResults(pageNumber = 1){
		categoryIds = getCommaSepratedCategoryIds();
	    /*if(str_brand_ids.length > 0){
	    	str_brand_ids = str_brand_ids.substr(0, (str_brand_ids.length-1)  )
		}*/
		str_brand_ids = getCommaSepratedBrandValues();
		
		data = {
				'store[]': getSuppliersValues(),
	            'feature_ids[]' : getFeaturesIdsArray(),
	            min_price : min_price,
	            max_price : max_price,
	            page : pageNumber,
	            currentView : currentView,
	            brands: str_brand_ids,
	            sort: sort
	        };
		$.ajax({
            url: '<?php echo \Yii::$app->getUrlManager()->createUrl(\Yii::$app->request->get('slug')) ?>',
            type: 'GET',
            data: data,
            success: function(response) {
            	$("#listViewContents").append(response.listHtml);
                $("#gridViewContents").append(response.gridHtml);
                var currentlyShowing = response.listingResults*response.pageNumber;
                if(currentlyShowing > response.total){
               	 currentlyShowing = response.total;
               	 $('#viewMore').parent().hide();
                }
                $('h3.pull-left').html('<span style="font-size:12px">'+(currentlyShowing)+' out of ' + response.total + '</span>');
                $("#pageNumber").val((parseInt(response.pageNumber)));



if ($(window).width() >= 1023) {
$("img.lazy").lazyload({
    effect : "fadeIn"
});
}else{
 $('img.lazy').each(function(){
      $(this).attr('src',$(this).data('original'));
   });

}


            }
		});
	}

    function ajaxSearch(pageNumber = 1){
        page = pageNumber;
        str_brand_ids ="";
        str_brand_ids = getCommaSepratedBrandValues();
        data = {
        	'store[]': getSuppliersValues(),
            'feature_ids[]' : getFeaturesIdsArray(),
            min_price : min_price,
            max_price : max_price,
            page : page,
            currentView : currentView,
            brands: str_brand_ids,
            sort:sort
        };
    window.history.pushState('object or string', 'Title', '<?php echo \Yii::$app->getUrlManager()->createUrl(\Yii::$app->request->get('slug')) ?>?brands=' +str_brand_ids+getFeatureValuesGetString() + getSupplierValuesForQueryString());  $.ajax({
            url: '<?=\Yii::$app->getUrlManager()->createUrl(\Yii::$app->request->get('slug')) ?>',
            type: 'GET',
             data: data,
             success: function(response) {
                 $("#listViewContents").html('');
                 $("#gridViewContents").html('');
                 $("#pageNumber").val(1);
                 $("#listViewContents").html(response.listHtml);
                 $("#gridViewContents").html(response.gridHtml);
                 var currentlyShowing = response.listingResults*response.pageNumber;
                 if(currentlyShowing > response.total){
                	 currentlyShowing = response.total;
                	 
                 }
                 $('#viewMore').parent().show();
                 $('h3.pull-left').html('<span style="font-size:12px">'+(currentlyShowing)+' out of ' + response.total + '</span>');



		if ($(window).width() >= 1023) {
$("img.lazy").lazyload({
    effect : "fadeIn"
});
}else{
 $('img.lazy').each(function(){
      $(this).attr('src',$(this).data('original'));
   });

}
             }
         });
    } // end of ajax search
</script>
<script type="text/javascript">
$(document).ready(function(e){
	var pagewrap = $('.container').innerWidth();
	var leftpanel = $(".brand.left-panel").innerWidth();
	
	
	$(window).load(function(){
 if($( window ).width()>767){
    	$(".left-scrollblock").mCustomScrollbar();

  }
if($( window ).width()<768){
   $('input[type=checkbox]').click(function(){

        var length= $(this).parents('.left-scrollblock').find('input[type=checkbox]:checked').length;
        if(length>0){
          $(this).parents('.righty').parent().find('.lefty .popup__name-cnt').html('✓');
        }else{
          $(this).parents('.righty').parent().find('.lefty .popup__name-cnt').html('');
        }
      });

      $('.lefty').click(function(){
    $('.righty').hide();
    $(this).next().slideDown();
    $( ".lefty" ).each(function( index ) {
    $( this ).removeClass('selected') ;
  });
     
    $(this).addClass('selected');
  
  });

$('.clearall').click(function(){
 
 
  var url=document.location.protocol +"//"+ document.location.hostname + document.location.pathname;
   window.location.href=url;
});
}




	});




});
$(window).resize(function(){
	var pagewrap = $('.container').innerWidth();
	var leftpanel = $(".brand.left-panel").innerWidth();
	
});
</script>
