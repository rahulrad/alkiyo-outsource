<?php $categories = \common\models\Categories::find()->where(['show_home_page' => '1'])->orderBy('sort_order ASC')->all();?>
  <?php foreach ($categories as $category) {
  	$rsTopRatedBrands =$category->getTopRatedBrandsByCategory($category->category_id);
  	if(count($rsTopRatedBrands)> 0):
  	?>
  <section class="mobile" >
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 mobileview-colume ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h1 itemprop="name"> <?php echo $category->category_name;?> </h1>
            <ul class="nav nav-tabs pull-right hidden-xs" role="tablist">
                <?php
                
                
                $i=0;
                foreach($rsTopRatedBrands as $brand){
                   //$brand =  \common\models\Brands::find()->where(['brand_id' => $topBrand['brands_brand_id']])->one();
                   $i++;
                   ?>
               <li role="presentation" class="<?php echo ($i==1 ? 'active' : '') ;?>"><a href="#brand_<?php echo $brand->brand_id;?>_<?php echo $category->category_id;?>" aria-controls="snapdeal" role="tab" data-toggle="tab"><?php echo $brand->brand_name;?></a></li>
              <?php }?>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="tab-content">
            
             <?php
            //$rsTopRatedBrands =\common\models\Brands::getTopRatedBrandsByCategory($category->category_id);
            $i=0;
            foreach($rsTopRatedBrands as $brand){
               //$brand =  \common\models\Brands::find()->where(['brand_id' => $topBrand['brands_brand_id'] ])->one();
               $i++;
               ?> 
              <div role="tabpanel" class="tab-pane <?php echo ($i==1 ? 'active' : '') ;?>" id="brand_<?php echo $brand->brand_id;?>_<?php echo $category->category_id;?>">
              <div class="panel-body dealrow">
                <div class="row">
                <div class="owl-carousel">
                
                <?php  //$products =  $category->getProducts()->orderBy('rating DESC')->limit(6)->all();
                //$products =  $category->getProducts()->orderBy('rating DESC')->limit(6)->all();
                 $products =  $category->getProducts()->Where([ 'brands_brand_id' => $brand->brand_id , 'categories_category_id'=> $category->category_id ])->orderBy('rating DESC')->limit(6)->all();

                foreach($products as $product)
                  {
                  ?>
                    <div class="item">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
                    <div class="full-row dealbox mobile  text-center"> 
                   
                    
                    <a href="<?php  echo Yii::$app->getUrlManager()->getBaseUrl();?>product/<?php echo $product->slug; ?>"><figure> <img itemprop="image" alt="" src="/uploads/products/mobiles/apple/150x200/14714311812.jpg"> </figure>
                      <p class="discription"> <?php echo $product->product_name; ?></p>
                      
                      <div class="starbase">
                        <div class="star-rating" style="width:<?php echo $product->rating*20?>%"></div>
                        </div>
                      
                        
                        <!-- <p class="price"> <span><?=empty($product->lowest_price) ? '' : '<span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> ' . number_format($product->lowest_price,2)?> </span> <strong><?=empty($product->lowest_price) ? '' : '<span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> ' . number_format($product->lowest_price,2)?> </strong> -->
                        <p class="price"> <?=( (!empty($product['original_price']) && $product['lowest_price'] != $product['original_price'] ) ? '<span> <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> ' . number_format($product['original_price'],2) . ' </span>' : '') ?> <strong>  <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span>  
<span class="pricevalue" itemprop="lowPrice" content="Fill Product Price">
						<?=number_format($product['lowest_price'],2)?> <?php if($product->discount > 0 && !empty($product->lowest_price) ){ ?> </span>  <i> (<?php echo $product->discount;?>% off)</i> <?php }?></strong>
	        			<?php if((int)$product->lowest_price > 0):?>  </p>
                        <strong class="stroes"> <span> <?php echo count($product->suppliersProducts);?> Stores at <?php echo number_format($product->lowest_price,2);?> </span> </strong> 
                        <?php endif;?>
                      
                      </a> 
                    </div>
                  </div>
                  </div>
                          <?php } ?>
                
               
              
                
                </div>
                </div>
                
              </div>
            </div><!-- end samsung -->
            <?php } // end of brand loop ?>  
     
          </div>
        </div>
      </div>
    </div>
  </section>
    <?php endif; }?>
	
    
    <script type="text/javascript">
    $(document).ready(function() {

				
         $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    responsive:{
        0:{
         items:2,
             nav:true
        },
        600:{
         items:2,
            nav:true
        },
        1000:{
          items:6,
            nav:true,
            loop:false
 			
        }
    }
})

		
        });
    </script>
