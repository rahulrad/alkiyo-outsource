<?php
$this->title = 'Comparing Products';
$session = Yii::$app->session;

?>

<input type="hidden" name="category_id" value="<?=$comparasionProducts[0]->categories_category_id;?>" />
<input type="hidden" name="products[]" id="bensonSearch4" value="<?=$comparasionProducts[0]->slug;?>" />
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li ><i class="fa fa-home"></i></li>
                <li><a href="#">Mobile </a></li>
                <li><a href="#">Apple </a></li>
                <li class="active">Apple iPhone 6S Price in India </li>
            </ol>
        </div>
    </div>

<!-- Compare Box Start -->
<?php $image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[0])?>
<div class="row">
<div class="col-lg-3 col-xs-4 left_penal"></div>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 padd-l3 padd-r-9">
<div class="compare_item_row">
<!-- First Product Box Start -->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
<div class="compare_item_bg">
<div class="colose"><a href="#"><i class="fa fa-close" id="removeCompareProduct4"></i></a></div>
<img itemprop="image" class="img-responsive center-block" src="<?=$image?>">
<p><?=$comparasionProducts[0]->product_name?><br><span>Rs. <?=$comparasionProducts[0]->lowest_price?></span></p>
<div class="starbase">
<div class="star-rating" style="width:<?=$comparasionProducts[0]->rating*20?>%"></div>
</div>
</div>
</div>
<!-- First Product Box End -->
<?php if( !empty($comparasionProducts[1]) ):
$image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[1]);?>
<!-- Second Product Box Start -->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
<div class="compare_item_bg">
<div class="colose"><a href="#"><i class="fa fa-close" id="removeCompareProduct1"></i></a></div>
<img itemprop="image" class="img-responsive center-block" src="<?=$image?>">
<p><?=$comparasionProducts[1]->product_name?><br><span>Rs. <?=$comparasionProducts[1]->lowest_price?></span></p>
<div class="starbase">
<div class="star-rating" style="width:<?=$comparasionProducts[1]->rating*20?>%"></div>
</div>
<input type="hidden" name="products[]" id="bensonSearch1" value="<?=$comparasionProducts[1]->slug?>"/>
</div>
</div>
<?php else:?>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ">
<div class="compare_item_bg last text-center" style="padding:0px">
<div class="search_gurup">
<label>Product 2</label>
<div class="input-group ui-widget">
    <input type="text" name="products[]" placeholder="Search your items.." id="bensonSearch1" class="form-control bensonSearch">
    </div>
</div>
<a href="javascript:void(0)" id="btn1" class="btn btn-group">ADD TO COMPARE</a>
</div>

</div>
<?php endif;?>
<?php if( !empty($comparasionProducts[2]) ):
$image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[2]);?>
<!-- Third Product Box Start -->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs" >
<div class="compare_item_bg">
<div class="colose"><a href="#"><i class="fa fa-close" id="removeCompareProduct2"></i></a></div>
<img itemprop="image" class="img-responsive center-block" src="<?=$image?>">
<p><?=$comparasionProducts[2]->product_name?><br><span>Rs. <?=$comparasionProducts[2]->lowest_price?></span></p>
<div class="starbase">
<div class="star-rating" style="width:<?=$comparasionProducts[2]->rating*20?>%"></div>
</div>
<input type="hidden" name="products[]" id="bensonSearch2" value="<?=$comparasionProducts[2]->slug?>"/>
</div>
</div>
<?php else:?>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
<div class="compare_item_bg last text-center">
<div class="search_gurup">
<label>Product 3</label>
<div class="input-group ui-widget">

	<input type="text" name="products[]" placeholder="Search your items.." id="bensonSearch2" class="form-control bensonSearch">
 </div>
           
</div>
<a href="javascript:void(0)" id="btn2" class="btn btn-group">ADD TO COMPARE</a> 
</div>


</div>
<?php endif;?>
<?php if( !empty($comparasionProducts[3]) ):
$image = \frontend\helpers\NoImage::getProductImageFullPathFromObject($comparasionProducts[3]);?>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
<div class="compare_item_bg">
<div class="colose"><a href="#"><i class="fa fa-close" id="removeCompareProduct3"></i></a></div>
<img itemprop="image" class="img-responsive center-block" src="<?=$image?>">
<p><?=$comparasionProducts[3]->product_name?><br><span>Rs. <?=$comparasionProducts[3]->lowest_price?></span></p>
<div class="starbase">
<div class="star-rating" style="width:<?=$comparasionProducts[3]->rating*20?>%"></div>
</div>
<input type="hidden" name="products[]" id="bensonSearch3" value="<?=$comparasionProducts[3]->slug?>"/>
</div>
</div>
<?php else:?>
<!-- Fourth Product Box Start -->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
<div class="compare_item_bg last text-center">
<div class="search_gurup">
<label>Product 4</label>
<div class="input-group ui-widget">
	<input type="text" name="products[]" placeholder="Search your items.." id="bensonSearch3" class="form-control bensonSearch" />
    </div>
</div>
<a href="javascript:void" id="btn3" class="btn btn-group">ADD TO COMPARE</a> 
</div>

</div>
<?php endif;?>
</div>

</div>
</div>
<!-- Compare Box end -->
<div class="gernel">
<?php 
      foreach($productFeatureGroup as $featureGroup):
      	//echo $featureHTML;
		$featureHTML = '';?>
		<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_penal">
	<h3><?=$featureGroup->name?></h3>
	</div>
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
	<div class="right_penal hidden-xs" >
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
	
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
	
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
	
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
	
	</div>
	</div>
	</div>
	</div>
	  <?php foreach($featureGroup->features as $feature):
	  $firstProductFeature = !isset($productsFeatures[0]) ? '&nbsp;' :  (isset($productsFeatures[0][$feature->id]) ? '<h4>'.$productsFeatures[0][$feature->id].'</h4>' : '<h4>' . 'Not Available' . '</h4>');
	  $secondProductFeature = !isset($productsFeatures[1]) ? '&nbsp;' :  (isset($productsFeatures[1][$feature->id]) ? '<h4>'.$productsFeatures[1][$feature->id].'</h4>' : '<h4>' . 'Not Available' . '</h4>');
	  $thirdProductFeature = !isset($productsFeatures[2]) ? '&nbsp;' :  (isset($productsFeatures[2][$feature->id]) ? '<h4>'.$productsFeatures[2][$feature->id].'</h4>' : '<h4>' .'Not Available' . '</h4>');
	  $fourthProductFeature = !isset($productsFeatures[3]) ? '&nbsp;' :  (isset($productsFeatures[3][$feature->id]) ? '<h4>'.$productsFeatures[3][$feature->id].'</h4>' : '<h4>' .'Not Available' . '</h4>');
	  
	  $featureHTML = <<<FeatureHtml
		<div class="row">
			<div class="bdr_bottom"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 left_penal">
			<h4>{$feature->name}</h4>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
				<div class="right_penal full-row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
					{$firstProductFeature}
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
					{$secondProductFeature}
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 hidden-xs text-center">
					{$thirdProductFeature}
					</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center hidden-xs">
					{$fourthProductFeature}
					</div>
				</div>
			</div>
		</div>
FeatureHtml;
		echo $featureHTML;
?>

	  <?php endforeach;?>
<?php endforeach;
$url = \yii\helpers\Url::to(['product/comparesearch']);
$redirectUrl = \yii\helpers\Url::to(['product/compare']);
$source = <<< Source
	source: function( request, response ) {
        $.ajax( {
          url: "{$url}",
          dataType: "json",
          data: {
            term: request.term, category: jQuery('input[name="category_id"]').val()
          },
          success: function( data ) {
			// Handle 'no match' indicated by [ "" ] response
            response( data.length === 1 && data[ 0 ].length === 0 ? [] : data );
          }
        } );
	},
Source;

$script = <<< JS
jQuery(document).ready(function () {
    jQuery( "#bensonSearch1" ).autocomplete({
      minLength: 2,
      {$source}
      focus: function( event, ui ) {
        jQuery( "#bensonSearch1" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        jQuery( "#bensonSearch1" ).val( ui.item.slug );
//         jQuery( "#bensonSearch-id" ).val( ui.item.value );
//         jQuery( "#bensonSearch-description" ).html( ui.item.slug );
//         jQuery( "#bensonSearch-icon" ).attr( "src", ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( item.label )
        .appendTo( ul );
    };
    jQuery( "#bensonSearch2" ).autocomplete({
      minLength: 2,
      {$source}
	focus: function( event, ui ) {
        jQuery( "#bensonSearch2" ).val( ui.item.label );
        return false;
	},
      select: function( event, ui ) {
        jQuery( "#bensonSearch2" ).val( ui.item.slug );
//         jQuery( "#bensonSearch-id" ).val( ui.item.value );
//         jQuery( "#bensonSearch-description" ).html( ui.item.slug );
//         jQuery( "#bensonSearch-icon" ).attr( "src", ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( item.label )
        .appendTo( ul );
    };
	jQuery( "#bensonSearch3" ).autocomplete({
      minLength: 2,
      {$source}
      focus: function( event, ui ) {
        jQuery( "#bensonSearch3" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        jQuery( "#bensonSearch3" ).val( ui.item.slug );
//         jQuery( "#bensonSearch-id" ).val( ui.item.value );
//         jQuery( "#bensonSearch-description" ).html( ui.item.slug );
//         jQuery( "#bensonSearch-icon" ).attr( "src", ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( item.label )
        .appendTo( ul );
    };
	jQuery('.btn-group').click(function(){
		var queryString = getCompareQueryString();
		var location = "{$redirectUrl}" + queryString;
		window.location.href = location;
	});
    jQuery('.fa-close').click(function(){
    	var id = jQuery(this).prop('id');
    	var number = id.charAt(id.length - 1);
    	number = '#bensonSearch' + number;
    	jQuery(number).val('');
    	queryString = getCompareQueryString();
    	var location = "{$redirectUrl}" + queryString;
		window.location.href = location;
	});
    function getCompareQueryString(){
    	var queryString = '?';
		jQuery('input[name^="products"]').each(function() {
	        console.log(jQuery(this).val());
			if(jQuery.trim(jQuery(this).val()).length != 0){
				queryString = queryString.concat('products[]=' +jQuery.trim(jQuery(this).val()) + '&');
			}
		});
    	queryString += 'category_id='+jQuery('input[name="category_id"]').val();
    	return queryString;
	}
} );
JS;

$this->registerJs($script, \yii\web\View::POS_END);
?>

</div>
</div>
