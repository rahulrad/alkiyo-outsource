<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\base\Widget;
$this->title = $product->product_name." Lowest Prices Online in India As On ".date('d-m-y')." | NayaShopi.com";
?>

<div class="savnavigation_bg">
  <div class="container">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button aria-expanded="false" data-target="#bs-example-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a href="#" class="navbar-brand"><?php echo $product->category->category_name; ?>Mobiel &amp; Accesories</a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Best Seller<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Data Storage<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Lap Tops<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Computer Accessories<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Keyboard &amp; Mice<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Keyboard &amp; Mice<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Tablets<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Printers &amp; Ink<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Networking Device<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Desktop &amp; Monitors<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse --> 
      
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li ><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#"><?php echo $product->category->category_name?> </a></li>
        <li class="active"><?php echo $product->product_name; ?> Price in India </li>
      </ol>
    </div>
  </div>
</div>

<!--compare box-->
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center detail-screenview-colume">
      <div class="display_slider  ">
        <div id="myCarousel-product" class="carousel slide" data-ride="carousel"> 
          
          <!-- Wrapper for slides -->
          <div class="carousel-inner product-d-slider">
            <?php $j = 0;
                        foreach ($product->productImages as $product_image):
                            ?>
            <div class="item picZoomer <?php echo($j == 0 ? 'active' : ''); ?>"><img src="<?php echo $product_image->image_path; ?>" alt="<?php echo $product->product_name; ?> online price in India" title="<?php echo $product->product_name; ?> Prices"> </div>
            <?php $j++;
                        endforeach;
                        ?>
          </div>
          <ul class="nav nav-pills small-thumb">
            <?php
                        $i = 0;
                        foreach ($product->productImages as $product_image) :
                            ?>
            <li data-target="#myCarousel-product" data-slide-to="<?php echo $i; ?>" class="active"><a href="#"> <img src="<?php echo $product_image->image_path; ?>" alt=""> </a></li>
            <?php
    $i++;
    if ($i > 3)
        break;
endforeach;
?>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 left-padd-none detail-view-colume ">
      <div class="detail_blog-right">
        <h1 itemprop="name"><?php echo $product->product_name; ?> Best Price</h1>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="star_row full-row">
              <ul class="subsectioin">
                <li>
                  <div class="starbase">
                    <div class="star-rating" style="width:<?=$product->rating*20?>%"></div>
                  </div>
                </li>
                <li class="sold review"> <a href="#"> <?php echo count($product->productReviews); ?> Review </a> </li>
                <li class="sold review"> <a href="#"> <i class="fa fa-pencil"></i> Write a review </a> </li>
                <li class="sold review"> <a href="<?=yii\helpers\Url::to(['product/compare',]).'?products[]=' . $product->slug;?>"> <i class="fa fa-copy" ></i> &nbsp;Add to Compare </a> </li>
                <?php if( $userId > 0 ) : ?>
                <li class="sold review formId"> <a href="#"> <i class="fa fa-bell" ></i>
                  <?php if(count($PriceUpdate) == 0) : ?>
                  Price Drop Alert
                  <?php $form = ActiveForm::begin([
                                                    'id' => 'priceupdate-form',
                                                    'action' => Url::to(['product/priceupdate']),
                                                    ]); ?>
                  <?php echo $form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?>
                  <div id="formId">
                    <?= Html::button('Alert', ['class' => 'btn btn-primary', 'name' => 'login-button', 'type' => 'submit']) ?>
                  </div>
                  <?php ActiveForm::end(); ?>
                  <?php else: ?>
                  Will be alert you
                  <?php endif; ?>
                  </a> </li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="order_form">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 order-store padd-r-none ">
                  <div class="priceblock full-row"> <span class="pull-left">
                    <h3> <?php echo ( !empty($product->discount) ? '<del><span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> ' . $product->original_price . ' </del>' : '') ?> <?php echo ( !empty($product->discount) ? '<span>( ' . $product->discount . '% ) OFF </span>' : ''); ?> </h3>
                    <h2><span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span>
                     <span class="pricevalue" itemprop="lowPrice" content="<?=$product->lowest_price; ?>"> <?=$product->lowest_price; ?> </span>
                    </h2>
                    </span> <span class="pull-right"> <a href="<?=$product->url; ?>">
                    <button class="btn btn-blue" type="button">Go To Store</button>
                    </a> </span> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <ul class="colors">
                    <p>Color</p>
                    <li> <i class="black"></i> </li>
                    <li> <i class="darkblue"></i> </li>
                    <li> <i class="golden"></i> </li>
                  </ul>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <ul class="colors Storage">
                    <p>Storage</p>
                    <li>128 GB</li>
                    <li>64 GB</li>
                    <li>16 GB</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="Warranty_stetas">
                <p class="soldby"> <strong> Sold by :</strong> <img itemprop="image" alt="<?php echo $product->supplier->name; ?>" src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/<?php echo $product->supplier->image; ?>" alt="<?php echo $product->supplier->name; ?>" /> </p>
                <p><strong><?php echo $product->emi; ?> <i class="help"> ? </i> </strong><br>
                </p>
                <!--span>(Free delivery) </span></p-->
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <h4><strong>DELIVERY</strong>
                      <span><?php echo ($product->delivery ? $product->delivery : 'N/A'); ?></span></h4>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <h4><strong>RETURN Policy</strong>
                      <span><?php echo ($product->return_policy ? $product->return_policy : 'N/A'); ?></span></h4>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <h4><strong>CASH DELIVERY</strong>
                      <span><?php echo ($product->cod ? $product->cod : 'N/A'); ?> </span></h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 order-store bdrtop">
            <h4> Key Features </h4>
            <ul class="listfeature">
              <li><i class="fa fa-circle "></i> Excellent Features</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if (count($product->suppliersProducts) > 0) : ?>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 storedetail ">
      <div class="full-row ">
        <div class="panel panel-default product-detail">
          <div class="panel-heading filteroption">
            <h2 class="pull-left">Compare <?php echo $product->product_name; ?> Prices Online in India </h2>
            <span class="filter-option-right">
            <div class="funkyradio">
              <div class="funkyradio-default">
                <input type="checkbox" name="checkbox" id="cod" checked/>
                <label for="cod">COD</label>
              </div>
              <div class="funkyradio-default">
                <input type="checkbox" name="checkbox" id="emi" checked/>
                <label for="emi">EMI</label>
              </div>
              <div class="funkyradio-default">
                <input type="checkbox" name="checkbox" id="return_policy" checked/>
                <label for="return_policy">Return Policy</label>
              </div>
              <div class="funkyradio-default">
                <input type="checkbox" name="checkbox" id="offer" checked/>
                <label for="offer">Offer</label>
              </div>
            </div>
            </span>
            <div class="clearfix"></div>
          </div>
          <div class="panel-body" id="suppliersInfo">
            <?php 
    $myIndex = 0;
    foreach ($product->suppliersProducts as $supplier_product): 
    	$myIndex++;
    	$hasCOD = !empty($supplier_product->cod) ? 'hasCod' : '';
    	$hasEMI = !empty($supplier_product->emi) ? 'hasEmi' : '';
    	$hasDelivery = !empty($supplier_product->delivery) ? 'hasDelivery' : '';?>
            <div class="detail-row <?=$hasCOD.' '. $hasDelivery . ' '. $hasEMI?>" id="detail-row<?=$myIndex?>">
              <div class="row ">
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 dealer">
                  <div class="colume-outer">
                    <div class="colume-inner"> <img itemprop="image" src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/<?php echo $supplier_product->supplier->image; ?>" alt="<?php echo $supplier_product->supplier->name; ?> Offers For <?php echo $product->product_name ?>" title="<?php echo $product->product_name ?> Price At <?php echo $supplier_product->supplier->name ?>"> </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <div class="colume-outer">
                    <div class="colume-inner">
                      <ul class="buyconditioin buyoption" id="jqueryHeadache<?=$myIndex?>">
                        <li> <?php echo ($supplier_product->cod ? '<i class="fa  fa-check-circle"></i>'.$supplier_product->cod : '<i class="fa fa-close"></i> Not Available</li>');?>
                        <li> <?php echo ($supplier_product->delivery ? '<i class="fa  fa-check-circle"></i>'.$supplier_product->delivery : '<i class="fa fa-close"></i> Delivery Not Available</li>');?>
                        <li> <?php echo ($supplier_product->emi ? '<i class="fa  fa-check-circle"></i> EMI : '.$supplier_product->emi : '<i class="fa fa-close"></i> EMI Not Available</li>');?>
                        <li> <?php echo ($supplier_product->emi ? '<i class="fa  fa-check-circle"></i> Return Policy : '.$supplier_product->return_policy : '<i class="fa fa-close"></i> Return Policy Not Available</li>');?>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <div class="colume-outer">
                    <div class="colume-inner">
                      <p><strong>Color Variants </strong></p>
                      <ul class="colors text-center">
                        <li> <i class="black"></i> </li>
                        <li> <i class="darkblue"></i> </li>
                        <li> <i class="golden"></i> </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                  <div class="colume-outer">
                    <div class="colume-inner">
                      <h4> <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> <span class="pricevalue" itemprop="lowPrice" content="<?php echo $supplier_product->price; ?>"><?php echo $supplier_product->price; ?> <!--span> Free Shiping</span--> </span> </h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center"> <a href="<?php echo $supplier_product->url; ?>" class="btn btn-block btn-blue btn-store"> GO TO STORE </a>
                  <p class="offer-row"><strong> 1 Offers </strong> Available </p>
                </div>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 storedetail ">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 right-padd-none dealstab ddetailtab specifytab">
          <ul role="tablist" class="nav nav-tabs">
            <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="All" href="#technical" aria-expanded="true">Technical Specification</a></li>
            <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="profile" href="#discretion" aria-expanded="false"> Product Discription</a></li>
            <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="profile" href="#Reviews" aria-expanded="false"> Reviews </a></li>
          </ul>
          <div class="full-row tech-specification form-inline">
            <div class="tab-content">
              <div id="technical" class="tab-pane active" role="tabpanel">
                <div class="full-row tech-specification form-inline">
                  <h2> <?php echo $product->product_name; ?> Specification </h2>
                  <?php foreach ($featureGroups as $featureGroup): 
                            
                            $printFeatureGroup = '<div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <h3>'.$featureGroup->name.'</h3>
                                </div>
                            </div>';
                            $printFeature = false;
                            $featuresHTML = '';
                            foreach ($featureGroup->features as $feature): 
                            	if(isset($productFeatures[$feature->id])): 
                            	$printFeature = true;
                                $featuresHTML.= '<div class="tech-spec-row full-row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 first-colume ">'.$feature->name.'</div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 "> '. $productFeatures[$feature->id] .' </div>
                                </div>';
                            	endif;
                            endforeach; 
                            if($printFeature):
                                echo $printFeatureGroup . $featuresHTML;
                            endif;
                            ?>
                  <?php endforeach; ?>
                </div>
              </div>
              <div id="discretion" class="tab-pane" role="tabpanel">
                <h4> Discription </h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
              <div id="Reviews" class="tab-pane" role="tabpanel">
                <div class="full-row detailblock paddb bdr-none">
                  <h2><?php echo $product->product_name; ?> User Reviews <span  class="userreview"> <a href="#writereivew"> <i class="fa fa-edit"> </i> Write a Review </a> </span>
                    <div class="clearfix"></div>
                  </h2>
                  <div class="detail-row full-row">
                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                      <p class="review-colume"> <img  src="<?php //echo $supplier_product->supplier->image;?>" alt="<?php //echo $supplier_product->supplier->name;?>"> 
                      <span>
                      <div class="starbase">
                        <div class="star-rating" style="width:<?=$supplier_product->rating*20?>%"></div>
                      </div>
                      </span> <a href="<?php echo $supplier_product->url; ?>" target="_blank"> Read all review <i class="fa  fa-angle-double-right "></i></a>
                      </p>
                    </div>
                  </div>
                  <?php if (count($product->productReviews) > 0) : ?>
                  <?php foreach ($product->productReviews as $review_product): ?>
                  <div class="detail-row full-row paddb">
                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                      <div class="full-row">
                        <?php if (count($review_product->user) > 0) : ?>
                        <div class="userthumb" style="float:none;"> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/thumb_user.jpg" alt=""> </div>
                        <span class="user-detail"> <span> <strong><?php echo $review_product->user->username; ?></strong> <br>
                        <?= $supplier_product->created?>
                        </span> </span>
                        <div class="clearfix"></div>
                        <p class="color-blue"> 100% People Found <?php echo $review_product->user->username; ?> <br>
                          review helpul <span>
                          <?=$supplier_product->created?>
                          </span> </p>
                        <?php else: ?>
                        <div class="userthumb" style="float:none;"> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/thumb_user.jpg" alt=""> </div>
                        <span class="user-detail"> <span> <strong>Anonymous</strong> <br>
                        </span> </span>
                        <div class="clearfix"></div>
                        <p class="color-blue"> Anonymous <br>
                          review helpul <span> </span> </p>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12 review-detail">
                      <h4> Superp Product <span class="block">
                        <ul class="subsectioin">
                          <li>
                            <div class="starbase">
                              <div class="star-rating" style="width:<?= $review_product->rating *20?>%"></div>
                            </div>
                          </li>
                        </ul>
                        </span> </h4>
                      <p> <?php echo $review_product->description ?> </p>
                      <p>
                        <?php $form = ActiveForm::begin([
                                                'id' => 'review-form-rank',
                                                'action' => Url::to(['product/reviewrank']),
                                                ]); ?>
                        <?php echo $form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?> <?php echo $form->field($reviewmodel, 'review_id')->hiddenInput(['value'=> $review_product->id])->label(false); ?> <?php echo $form->field($reviewmodel, 'rank')->hiddenInput(['value'=> 1, 'class'=>'rank'])->label(false); ?> <span>Did you find this review helpul: <?php echo count($review_product->reviewsRank); ?></span>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reviewRank" id="reviewRank">
                        <?= Html::button('Yes', ['class' => 'btn btn-primary', 'name' => 'login-button', 'type' => 'submit']) ?>
                        <?= Html::button('No', ['class' => 'btn btn-primary', 'name' => 'login-button', 'type' => 'submit', 'id'=>'no']) ?>
                        <!--<a href="#"> Yes </a> <a href="#"> No </a>--> 
                      </div>
                      <?php ActiveForm::end(); ?>
                      </p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row"> <a class="btn btn-block btn-loadmore all-reivew" href="#"> View All Reviews <i class="fa fa-angle-down"></i> </a> </div>
                  </div>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
            <div class="advertise"> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/advertise/ADD.jpg" class="img-responsive" alt=""> </div>
            <section class="powerbank " >
                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12  logo text-center ">
                        <h4 class="text-left"> <strong>Product Price Lists</strong> </h4>
                        <ul class="pricelist">
                            <?php // foreach ($menuCategoryTypeList as $menuCategoryType): ?>
                                <li> <a href="<?php //echo $menuCategoryType->slug; ?>"> <?php //echo $menuCategoryType->title; ?></a> </li>
                            <?php //endforeach; ?>
                        </ul>
                    </div>
                </div>
            </section>
        </div>-->
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
      <section class="powerbank  marg-t0 " >
        <div class="row">
          <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12  logo text-center ">
            <h4 class="text-left"> <strong>Popular Mobile Price Lists</strong> </h4>
            <ul class="pricelist">
              <li> <a href="#"> Nokia Mobile Price List</a> </li>
              <li> <a href="#"> Sony Mobile Price List </a> </li>
              <li> <a href="#"> Lenovo Mobile Price List </a> </li>
              <li> <a href="#"> Ashush Mobile Price List</a> </li>
              <li> <a href="#"> Micromax Mobile Price List </a> </li>
              <li> <a href="#"> Philips Mobile Price List </a> </li>
              <li> <a href="#"> Lg Mobile Price List </a> </li>
              <li> <a href="#"> Samsung Mobile Price List </a> </li>
              <li> <a href="#"> Intex PMobile Price List </a> </li>
              <li> <a href="#"> Intex PMobile Price List </a> </li>
            </ul>
          </div>
        </div>
      </section>
      <section class="powerbank" >
        <div class="row">
          <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 logo text-center ">
            <h4 class="text-left"> <strong>Frequently Bought Together </strong> </h4>
            <div class="row marb15 trendrow">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12"> <img itemprop="image" class="img-responsive" alt="" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/memorycard.jpg"> </div>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 text-left">
                <p class="discription"> Momory Card</p>
                <p class="price"> <span>$42,383 </span></p>
                <a class="btn btn-default" href="#">BUY NOW </a> </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 logo text-center ">
            <div class="row marb15 trendrow">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12"> <img itemprop="image" class="img-responsive" alt="" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/powerbank.jpg"> </div>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 text-left">
                <p class="discription"> Power Bank</p>
                <p class="price"><span>$42,383 </span></p>
                <a class="btn btn-default" href="#">BUY NOW </a> </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 logo text-center ">
            <div class="row marb15 trendrow">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12"> <img itemprop="image" class="img-responsive" alt="" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/powerbank.jpg"> </div>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 text-left">
                <p class="discription"> Power Bank</p>
                <p class="price"> $42,383 <span>$42,383 </span></p>
                <a class="btn btn-default" href="#">BUY NOW </a> </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  </div>
  <div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margt15" id="writereivew">
      <div class="full-row detailblock detailblockDiv marg-b15 paddb form-horizontal">
        <?php 
                        if(count($ProductReviews) == 0){
                        $form = ActiveForm::begin([
                                'id' => 'review-form',
                                'action' => Url::to(['product/reviewpost']),
                                'enableAjaxValidation' => true,
                                'validationUrl' => Url::to(['product/addreview'])]); ?>
        <h2> Write a Review </h2>
        <div class="form-group">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <span class="block blockDiv"> <img width="30" height="30" id="1" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt=""> <img width="30" height="30" id="2" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt=""> <img width="30" height="30" id="3" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt=""> <img width="30" height="30" id="4" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt=""> <img width="30" height="30" id="5" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-outlined.svg" alt=""> <br>
            <br>
            <label>4/5 Rating s</label>
            </span> <?php echo $form->field($reviewmodel, 'rating')->hiddenInput(['class'=> 'rating', 'value'=> '4', 'id'=> 'rating'])->label(false); ?> </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?= $form->field($reviewmodel, 'title')->textInput(['class' => 'form-control','placeholder' => 'Title ( 20 Charector)', 'id' => 'user_name']) ?>
          <?php echo $form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?> </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?= $form->field($reviewmodel, 'description')->textArea(['cols' => '', 'rows' => '', 'class' => 'form-control','placeholder' => 'Description', 'id' => 'review']) ?>
        </div>
        <div class="form-group">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= Html::button('SUBMIT REVIEW', ['class' => 'btn btn-danger', 'name' => 'login-button', 'type' => 'submit']) ?>
          </div>
        </div>
        <?php ActiveForm::end(); 
                        }
                        else{
                            echo "<div>You have already comment on this product</div>";
                        }?>
      </div>
    </div>
  </div>
  <?=\frontend\components\RecentlyViewed::widget(['productIds' => [$product->product_id]])?>
  <section class="fashion">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h4 class="block-title "> Similar Range Product </h4>
            <div class="clearfix"></div>
          </div>
          <div class="tab-content">
            <div class="panel-body dealrow">
              <div class="row ">
              
                  <div class="owl-carousel">
                  
                      <?php 
                            foreach($similarProducts as $similar):?>
                                <div class="item">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="full-row dealbox mobile  text-center"> <?php echo ( $similar->discount ? '<div class="discount-badge2"> <span>' . $similar->discount . '%<strong> OFF</strong> </span></div>' : ''); ?> 
                        <a href="<?=\yii\helpers\Url::to(['product/index', 'slug' => $similar->slug, 'category' => $similar->category->slug])?>">
                          <figure> <img itemprop="image"  src="<?=empty($similar->productImages[0]->image_path) ? '' : $similar->productImages[0]->image_path?>" class="img-responsive"> </figure>
                          <p class="discription">
                            <?=$similar->product_name?>
                            <span> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                          <p class="price" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer"><strong><span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span>
                            <?=$similar->lowest_price?>
                            </strong><strong></strong> </p>
                          </a> </div>
                      </div>       
                       </div>
                      <?php 
                            endforeach;?>
                   
                
                  </div>
                <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="full-row dealbox mobile  text-center">
                                        <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                                        <a href="#">
                                            <figure> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/samsung_gallaxy.jpg" class="img-responsive"> </figure>
                                            <p class="discription">   Samsung Galaxy Note 5 Dual SIM  <span> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                                            <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                                        </a> </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="full-row dealbox mobile  text-center">
                                        <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                                        <a href="#">
                                            <figure> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/nokia_lumia.jpg" class="img-responsive"> </figure>
                                            <p class="discription">  Nokia Lumia 1020 (EOS) <span> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                                            <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                                        </a> </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="full-row dealbox mobile  text-center"> 
                                        <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                                        <a href="#">
                                            <figure> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/htc.jpg" class="img-responsive"> </figure>
                                            <p class="discription">  HTC one <span> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                                            <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                                        </a> </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="full-row dealbox mobile  text-center"> 
                                        <div class="discount-badge2"> <span> 40% <strong> OFF</strong></span> </div>
                                        <a href="#">
                                            <figure> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/product/samsung_gallaxy.jpg" class="img-responsive"> </figure>
                                            <p class="discription">  Samsung Galaxy Note 5 Dual SIM  <span> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                                            <p class="price"> <span>$42,383 </span> <strong> $42,383 </strong><strong> <span> 8 Stores at $42,383 </span> </strong> </p>
                                        </a> </div>
                                </div>--> 
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script>
    function func(id) {
        var rating = $( '#rating' ).val(id);
        var block = $('.blockDiv');
        block.empty();
        for(var i=1; i<=5; i++){
            if(id>=i){
                block.append('<img width="30" height="30" id="'+i+'" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt="">');
            }
            else{
                block.append('<img width="30" height="30" id="'+i+'" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-outlined.svg" alt="">');
            }
        }
        block.append('<br><br><label>'+id+'/5 Rating s</label>');
    }
$(document).ready(function(){
	$('.funkyradio input[type="checkbox"]').on('click', function(){
		var loop = ($('ul.buyconditioin').length);
		for(var index = 0; index < loop; index++){
			hasCod = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(1) i').hasClass('fa-check-circle'));
			hasEmi = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(2) i').hasClass('fa-check-circle'));
			hasDelivery = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(3) i').hasClass('fa-check-circle'));
			hasReturn = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(4) i').hasClass('fa-check-circle'));

			if(!hasCod && !hasEmi && !hasDelivery && !hasReturn){
				$('#detail-row'+loop).hide();
			}else
				$('#detail-row'+loop).show();
		}
	});

$('.detailblock').on('beforeSubmit', '#review-form', function () {
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            var detailblock = $('.detailblockDiv');
                detailblock.empty();
                detailblock.append('<div> Your Comment are successfully submit. </div>');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});

$( "#no" ).click(function() {
    var rating = $( '.rank' ).val(0);
});

$('.detailblock').on('beforeSubmit', '#review-form-rank', function () {
    
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            var detailblock = $('.reviewRank');
                detailblock.empty();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});

$('.formId').on('beforeSubmit', '#priceupdate-form', function () {
    
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            
            var detailblock = $('.formId');
                detailblock.empty();
                detailblock.append('<a href="#"> <i class="fa fa-bell" ></i> Will be alert you </a>');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});
});
</script>



<script type="text/javascript">
$(function() {
  $('.userreview a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top -100 }, 1000);
        return false;
      }
    }
  });
});



;
</script>


 <script type="text/javascript">
   $(document).ready(function() {

				
         $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    navigation:true,
navigationText: [
   "<div class='carousel-control-prev crsl-wdgt__prvs-btn '><</div>",
   "<div class='carousel-control-next  crsl-wdgt__next-btn'>></div>"
],
    responsive:{
        0:{
         items:2,
             nav:true
        },
        600:{
         items:5,
            nav:true
        },
        1000:{
          items:6,
            nav:true,
            loop:false
 			
        }
    }
})

		
        });
	
	
    </script>
<script type="text/javascript">
        $(function() {
            $('.picZoomer').picZoomer();

            $('.piclist li').on('click',function(event){
                var $pic = $(this).find('img');
                $('.picZoomer-pic').attr('src',$pic.attr('src'));
            });
        });
    </script>
