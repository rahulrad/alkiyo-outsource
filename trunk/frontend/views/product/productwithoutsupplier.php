<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\base\Widget;
use frontend\helpers\NoImage;
$this->title = $product->product_name." Lowest Prices online in India";
$brandSlug = $product->brand->folder_slug;
$categorySlug = $product->category->folder_slug;
$imagePreset = $product->category->image_scrap_preset;
?>
<div class="spe-tabs_outer">
        
         <div class="spe-tabs">
        <ul>
        <li><a class="specs" href="#">SPECIFICATIONS</a></li>           
        <li class="review"><a  href="#">REVIEWS</a></li>          
        <li class="alternative"><a  href="#">ALTERNATIVES</a></li>

<?php if(isset($product->product_description) && !empty($product->product_description)){ ?>
 <li class="desc"><a  href="#">DESCRIPTION</a></li>

<?php } ?>
        </ul>
    </div>
    </div>

    <br class="hidden-xs"/>
<div class="container">
  <div class="row hidden-xs">
    <div class="col-lg-12">    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item"  href="#"> <span itemprop="name"><i class="fa fa-home"></i> </span></a> <meta itemprop="position" content="1" /></li>
        <li  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo '/'.$product->category->slug ?>"><span itemprop="name"><?php echo $product->category->category_name?>  </span> </a> ><meta itemprop="position" content="2" /></li>
        <li  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><?=$product->product_name; ?>  <span itemprop="name"> Price in India </span> <meta itemprop="position" content="3" /> </li>
    </ol>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center fashion-detail-screenview-colume  "  itemscope itemtype="http://schema.org/Product">
      <div class="display_slider pos-rel ">
     <meta itemprop="name" content="<?=$product->product_name; ?>" value="<?=$product->product_name; ?>"/> 
      <div class="panel zoom-panel">
      <div class="panel-body picZoomer">
          <img itemprop="image"  lsrc="<?=NoImage::getProductImageFullPathFromObject($product,'full','zoom')?>"  src="<?=NoImage::getProductImageFullPathFromObject($product,'full','zoom')?>" class="img-responsive center-block">
      </div>

      <div class="panel-footer  hidden-xs">
        <ul class="piclist">
  		<?php foreach ($product->productImages as $product_image):?>

  			<li> <img lsrc="<?=NoImage::getProductImageFullPathFromBrandCategorySlug($brandSlug,$categorySlug, $product_image, 'full',$imagePreset,'zoom')?>" itemprop="image"  title="<?=$product->product_name; ?>" alt="<?=$product->product_name; ?> online price in India" src="<?=NoImage::getProductImageFullPathFromBrandCategorySlug($brandSlug,$categorySlug, $product_image, 'full',$imagePreset,'product')?>"> </li>
		<?php endforeach;?>

</ul>     </div>
        </div>
      </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 left-padd-none fashion-detail-view-colume fashiondetails mobileview-colume ">
      <div class="detail_blog-right detail_blog-right pro-det-right">
        <h1><?=$product->product_name?> Best Price</h1>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="star_row full-row rate-block">
              <ul class="subsectioin">
                <li>
                  
                <div class="starbase">
                        <div class="star-rating" style="width:<?php if(isset($product->rating)){ echo ($product->rating)*20; } ?>%"></div>
                      </div>
              
                


              <span>  <?=$product->rating?>/5 Ratings  </span>
                </li>
                <li class="sold review hidden-xs"> &nbsp;&nbsp;|&nbsp;&nbsp;<a href="#"> <i class="fa fa-pencil"></i> Write a review </a> </li>
                <li class="sold review hidden-xs"> &nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?=yii\helpers\Url::to(['product/compare',]).'?products[]=' . $product->slug;?>"> <i class="fa fa-copy"></i> &nbsp;Add to Compare </a> </li>
                <?php if( $userId > 0 ) : ?>
                <li class="sold review formId hidden-xs"> <a href="#"> <i class="fa fa-bell" ></i>
                  <?php if(count($PriceUpdate) == 0) : ?>
                  Price Drop Alert
                  <?php $form = ActiveForm::begin([
                                                    'id' => 'priceupdate-form',
                                                    'action' => Url::to(['product/priceupdate']),
                                                    ]); ?>
                  <?php echo $form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?>
                  <div id="formId">
                    <?= Html::button('Alert', ['class' => 'btn btn-primary', 'name' => 'login-button', 'type' => 'submit']) ?>
                  </div>
                  <?php ActiveForm::end(); ?>
                  <?php else: ?>
                  Price alert is activated on this product.
                  <?php endif; ?>
                  </a></li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="order_form ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 order-store padd-r-none "  itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">

			<meta itemprop="lowPrice"  content="<?=$product->lowest_price; ?>" value="<?=$product->lowest_price; ?>" />
                
      
      
      
      
      
      <div class=" full-row "  itemprop="offers" itemscope itemtype="http://schema.org/Offer"> 
                
      <div class="price-color">
             	<div class="price">
               <?php if(!empty($product->discount)):?>
                Best Price :                <?php echo ( !empty($product->discount) ? '<del style="font-size:12px"><span class="inline" itemprop="priceCurrency" content="INR">  </span> ' . $product->original_price . ' </del>' : '') ?> 
               <?php echo ( !empty($product->discount) ? '<span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> <span   content="'.$product->lowest_price.'"> '. $product->lowest_price.'</span> <span><span style="font-size:12px;">    ( ' . $product->discount . '% ) OFF </span>' : ''); ?> </span>
               </span>

               <?php else:?>
               <span class="inline" itemprop="priceCurrency" content="INR"> 
               <i class="fa fa-rupee"></i> </span> <?php if($product->upcoming){ echo "Launch Price"; }else{echo "Best Price :"; } ?> 
               <span><?=$product->lowest_price; ?>
              </span>
            
              <?php endif;?>
            
            
            </div>

                  </div>



                  
            <div class="ad-ph">

	<div class="ad-ph-sub">
            		<div class="ad-ph-sub-img"><img height="40" title="<?php echo $product->supplier->name; ?>" src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/<?php echo $product->supplier->image; ?>" alt="<?php echo $product->supplier->name; ?>"></div>
            		<div class="ad-ph-sub-text">
            			<div class="fl"><h3><i class="fa fa-rupee"></i><?=$product->lowest_price; ?></h3><span>Free Shipping</span></div>
                        <?php if (!is_null($product->suppliersProductWithLowestPrice)) { ?>
            			    <a rel ="nofollow" target="_blank" href="<?=$product->suppliersProductWithLowestPrice->storeLink;?>" class="goto">Go to Store <i class="fa fa-angle-right"></i></a>
                        <?php } ?>
            		</div>
            	</div>
              
            </div>
                 
				
                 </div>
                </div>
              </div>
            </div>
         
          </div>
        </div>
        <div class="off-blc">
            	<span>%</span><b>OFFERS</b>
            
<p>           
              <?php $offerList=explode('##',$product->offers);
foreach( $offerList as $ok => $ov){
echo $ov."<br/>";

}
?>
</p>

</div>
        </div>
      </div>
    </div>
  
<?php if(isset($product->product_description) && !empty($product->product_description)){ ?>
  <div class="review-block gray description">
    	 <h2><?=$product->product_name?> Price in india</h2>
            <p> <?=$product->product_description?></p>
    </div>

<?php } ?>


    <div class="container-fluid no-padding detail-section">

      <h3> <?php echo $product->product_name; ?> Specification </h3>
      
      <table width="100%" cellpadding="2" cellspacing="2" class="speci-table">
				<tbody>
      <?php foreach ($featureGroups as $featureGroup): 
                            $printFeatureGroup = '<tr><th colspan="2">
                                    '.$featureGroup->name.'</th>
                                </tr>
                            ';
                            $printFeature = false;
                            $featuresHTML = '';
                            foreach ($featureGroup->features as $feature): 
                            	if(isset($productFeatures[$feature->id])): 
                            	$printFeature = true;
                                $featuresHTML.= '<tr>
                                    <td>'.$feature->name.'</td>
                                    <td> '. $productFeatures[$feature->id] .' </td>
                                </tr>';
                            	endif;
                            endforeach; 
                            if($printFeature):
                                echo $printFeatureGroup . $featuresHTML;
                            endif;
                            ?>
                  <?php endforeach; ?>
                  </tbody></table>

    </div>
    


    


    <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 storedetail fashion_storedetail padd-t0 mobileview-colume">
   

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 right-padd-none  " style="margin-top:15px;">
      <div class="full-row detailblock marg-b15 paddb form-horizontal">
        <h2> Write a Review </h2>
        <div class="form-group">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <span class="block"> <img alt="" src="/images/star.jpg">
            <label><?=$product->rating?>/5 Rating</label>
            </span> 
        </div>
        </div>
        <?php
        if(count($ProductReviews) == 0){?>
        <div id="reviewblockDiv"><?php

        $form = ActiveForm::begin([
                                'id' => 'review-form',
                                'action' => Url::to(['product/reviewpost']),
                                'enableAjaxValidation' => true,
                                'validationUrl' => Url::to(['product/addreview'])]);?>
                <?=$form->field($reviewmodel, 'rating')->hiddenInput(['class'=> 'rating', 'value'=> '4', 'id'=> 'rating'])->label(false); ?>
                <div class="form-group cssHeadache">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?= $form->field($reviewmodel, 'title')->textInput(['placeholder' => 'Title ( 20 Charector)', 'id' => 'user_name']) ?>
          <?=$form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?>
          </div>
        </div>
        <div class="form-group cssHeadache">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($reviewmodel, 'description')->textArea(['cols' => '', 'rows' => '', 'class' => 'form-control','placeholder' => 'Description', 'id' => 'review']) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= Html::button('SUBMIT REVIEW', ['class' => 'btn btn-danger', 'name' => 'login-button', 'type' => 'submit']) ?>
          </div>
        </div><?php
        ActiveForm::end();?>
        </div><?php
        }else{
                echo "<div>You have already commented on this product</div>";
        }
        ?>
      </div>
    </div>
  </div>

  <section class="fashion margt15">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 mobileview-colume ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer alternatives">
          <h6 itemprop="name"> Similar Range Product  </h6>>
            <div class="clearfix"></div>
          </div>
                <div class="tab-content">
                <div class="panel-body dealrow">
          <div class="row">
              <div class="owl-carousel">
          <?php foreach($similarProducts as $similar):?>
          	<div class="item">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">

		<meta itemprop="lowPrice" content="<?=$similar->lowest_price; ?>"  value="<?=$similar->lowest_price?>"/>
                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"  class="full-row dealbox mobile  text-center"> 
                     <?php if ( $similar->discount ):?>
						<div class="discount-badge2"> <span><?=$similar->discount?>% <strong> OFF</strong> </span></div>
					 <?php endif;?>
                    <a href="<?=\yii\helpers\Url::to([ 'product/index', 'slug' => $similar->slug, 'category' => $similar->category->slug ])?>">
                    <figure> <img itemprop="image" itemprop="image" src="<?=NoImage::getProductImageFullPathFromObject($similar,'full','thumb')?>" class="img-responsive"> </figure>
                    <p class="discription">
                            <?=$similar->product_name?>
                            <span> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                          <p class="price"><strong><span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span>
                         <span class="pricevalue"  content="<?= floor($similar->lowest_price) ?>">   <?= floor($similar->lowest_price)?> </span>
                            </strong> <strong></strong> </p>
                      </a> </div>
                  </div>
                  </div>
          <?php endforeach;?>
                </div>  </div>
                </div>
                </div>
        </div>
      </div>
    </div>
  </section>
      </div> 
      
   </div>
      </div>

<style>
.picZoomer-pic-wp{
  height:auto !important;
}
</style>

<script>
    function func(id) {
        var rating = $( '#rating' ).val(id);
        var block = $('.blockDiv');
        block.empty();
        for(var i=1; i<=5; i++){
            if(id>=i){
                block.append('<img width="30" height="30" id="'+i+'" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt="">');
            }
            else{
                block.append('<img width="30" height="30" id="'+i+'" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-outlined.svg" alt="">');
            }
        }
        block.append('<br><br><label>'+id+'/5 Rating s</label>');
    }
$(document).ready(function(){
	$('.funkyradio input[type="checkbox"]').on('click', function(){
		var loop = ($('ul.buyconditioin').length);
		for(var index = 0; index < loop; index++){
			hasCod = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(1) i').hasClass('fa-check-circle'));
			hasEmi = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(2) i').hasClass('fa-check-circle'));
			hasDelivery = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(3) i').hasClass('fa-check-circle'));
			hasReturn = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(4) i').hasClass('fa-check-circle'));

			if(!hasCod && !hasEmi && !hasDelivery && !hasReturn){
				$('#detail-row'+loop).hide();
			}else
				$('#detail-row'+loop).show();
		}
	});
	$('.cssHeadache > div > div').removeClass('form-group');
$('.detailblock').on('beforeSubmit', '#review-form', function () {
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            var detailblock = $('#reviewblockDiv');
                detailblock.empty();
                detailblock.append('<div> Your comment was successfully submitted. </div>');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});

$( "#no" ).click(function() {
    var rating = $( '.rank' ).val(0);
});

$('.detailblock').on('beforeSubmit', '#review-form-rank', function () {
    
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            var detailblock = $('.reviewRank');
                detailblock.empty();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});

$('.formId').on('beforeSubmit', '#priceupdate-form', function () {
    
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            
            var detailblock = $('.formId');
                detailblock.empty();
                detailblock.append('<a href="#"> <i class="fa fa-bell" ></i> Will be alert you </a>');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});
});
</script>



<script type="text/javascript">
$(function() {
  $('.userreview a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top -100 }, 1000);
        return false;
      }
    }
  });
});



;
</script>


 <script type="text/javascript">
    $(document).ready(function() {
		
		
         $('.owl-carousel').owlCarousel({
    loop:true,
    margin:3,
    responsiveClass:true,
	navigation:true,
navigationText: [
   "<div class='carousel-control-prev crsl-wdgt__prvs-btn '><</div>",
   "<div class='carousel-control-next  crsl-wdgt__next-btn'>></div>"
],

    responsive:{
        0:{
            items:2,
          nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:6,
            nav:true,
            loop:false
        }
    }
})

//
//var dealrowInnerWeight = $('.dealrow').innerWidth();
//		//alert(dealrowInnerWeight);
//		$('.owl-stage').css('width',dealrowInnerWeight);
//   
//	
//	var dealrowInnerWeight = $('.owl-item.active').outerWidth();
//		//alert(dealrowInnerWeight);
//		$('.owl-item').css('width',dealrowInnerWeight - 2);
    });  
	
	
	
    </script>
<script type="text/javascript">
    

    $(function() {

		$.fn.picZoomer.defaults = {
                picWidth: 294,
                picHeight: 515,
                scale: 1,
                zoomerPosition: {top: '0px', left: '350px'}
                /*,
                zoomWidth: 320,
                zoomHeight: 320*/
        };

            $('.picZoomer').picZoomer();

            $('.piclist li').on('click',function(event){
                var $pic = $(this).find('img');
                $('.picZoomer-pic').attr('src',$pic.attr('lsrc'));
            });
        });
    </script>
