<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\base\Widget;
use frontend\helpers\NoImage;


$this->title = $product->product_name." Lowest Prices Online in India ";
$brandSlug = $product->brand->folder_slug;
$categorySlug = $product->category->folder_slug;
$imagePreset = $product->category->image_scrap_preset;

?>
<div class="container">
  <div class="row hidden-xs">
    <div class="col-lg-12">
    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item"  href="#"> <span ><i class="fa fa-home"></i> </span></a> <meta  content="1" /></li>
        <li  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo '/'.$product->category->slug ?>"><span ><?php echo $product->category->category_name?> </span> </a> <meta content="2" /></li>
        <li  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><?php echo $product->product_name; ?> <span itemprop="name"> Price in India </span> <meta  content="3" /> </li>
    </ol>
    </div>
  </div>
</div>

<div class="spe-tabs_outer">
        
         <div class="spe-tabs">
        <ul>
        <li><a class="specs" href="#">SPECIFICATIONS</a></li>           
        <li class="review"><a  href="#">REVIEWS</a></li>          
        <li class="alternative"><a  href="#">ALTERNATIVES</a></li>

           <?php if(isset($product->product_description) && !empty($product->product_description)){ ?>
 <li class="desc"><a  href="#">DESCRIPTION</a></li>    
<?php } ?>

    </ul>
    </div>
    </div>

    <br class="hidden-xs"/>
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center fashion-detail-screenview-colume " itemscope itemtype="http://schema.org/Product">
      <div class="display_slider pos-rel ">
<meta itemprop="name" content="<?=$product->product_name?>"/>
      <div class="panel zoom-panel">
      <div class="panel-body picZoomer">
          <img itemprop="image" src="<?=NoImage::getProductImageFullPathFromObject($product,'full','zoom')?>" class="img-responsive center-block">
      </div>
      <div class="panel-footer  hidden-xs">
        <ul class="piclist">
        <?php foreach ($product->productImages as $kyi => $product_image):   if($kyi>5) continue; ?>
  			<li> <img title = "<?php echo $product->product_name; ?>"  lsrc="<?=NoImage::getProductImageFullPathFromBrandCategorySlug($brandSlug,$categorySlug, $product_image, 'full',$imagePreset,'zoom')?>"  alt="<?php echo $product->product_name; ?> online price in India" src="<?=NoImage::getProductImageFullPathFromBrandCategorySlug($brandSlug,$categorySlug, $product_image, 'full',$imagePreset,'product')?>"> </li>
		<?php endforeach;?>
</ul>     </div>
        </div>
    </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 left-padd-none fashion-detail-view-colume fashiondetails mobileview-colume">
      <div class="detail_blog-right pro-det-right">
         <h1 itemprop="name"><?=$product->product_name?> Best Price</h1>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="star_row full-row rate-block">
              <ul class="subsectioin">
                <li>
 <div class="starbase">
                        <div class="star-rating" style="width:<?php if(isset($product->rating)){ echo ($product->rating)*20; } ?>%"></div>
                      </div>
               <span> <?=$product->rating?>/5</span> Ratings  
                </li>
                <li class="sold review hidden-xs"> <a href="#"> &nbsp;&nbsp;|&nbsp;&nbsp;<i class="fa fa-pencil"></i> Write a review </a> </li>
                <li class="sold review  hidden-xs"> <a href="<?=yii\helpers\Url::to(['product/compare',]).'?products[]=' . $product->slug;?>">&nbsp;&nbsp;|&nbsp;&nbsp; <i class="fa fa-copy"></i> &nbsp;Add to Compare </a> </li>
                <?php if( $userId > 0 ) : ?>
                <li class="sold review hidden-xs hidden-md hidden-sm hidden-lg formId"> <a href="#"> <i class="fa fa-bell" ></i>
                  <?php if(count($PriceUpdate) == 0) : ?>
                  Price Drop Alert
                  <?php $form = ActiveForm::begin([
                                                    'id' => 'priceupdate-form',
                                                    'action' => Url::to(['product/priceupdate']),
                                                    ]); ?>
                  <?php echo $form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?>
                  <div id="formId">
                    <?= Html::button('Alert', ['class' => 'btn btn-primary', 'name' => 'login-button', 'type' => 'submit']) ?>
                  </div>
                  <?php ActiveForm::end(); ?>
                  <?php else: ?>
                  Will be alert you
                  <?php endif; ?>
                  </a> </li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="order_form ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 order-store padd-r-none" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">

 <span style="display:none;" itemprop="lowPrice" content="<?=$product->lowest_price; ?>">  <?=$product->lowest_price; ?> </span>
                  <div class=" full-row " itemprop="offers" itemscope itemtype="http://schema.org/Offer"> 
                  <div class="price-color">
             	<div class="price">
               <?php if(!empty($product->discount)):?>
                Best Price :                <?php echo ( !empty($product->discount) ? '<del style="font-size:12px"><span class="inline" itemprop="priceCurrency" content="INR">  </span> ' . $product->original_price . ' </del>' : '') ?> 
               <?php echo ( !empty($product->discount) ? '<span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> <span   content="'.$product->lowest_price.'"> '. $product->lowest_price.'</span> <span><span style="font-size:12px;">    ( ' . $product->discount . '% ) OFF </span>' : ''); ?> </span>
               </span>

               <?php else:?>
               <span class="inline" itemprop="priceCurrency" content="INR"> 
               <i class="fa fa-rupee"></i> </span> <?php if($product->upcoming){ echo "Launch Price"; }else{echo "Best Price :"; } ?> 
               <span><?=$product->lowest_price; ?>
              </span>
            
              <?php endif;?>
            
            
            </div>
           
  </div>

<?php if($product->upcoming && $product->date_launch){ ?>
 <?php echo "<h3 class='lowetprice'>Launch Date : ".$product->date_launch."</h3>"; ?>
<?php }?>


<div class="ad-ph">

<?php foreach ($product->suppliersProducts as $supplier_product){ ?>
	<div class="ad-ph-sub">
            		<div class="ad-ph-sub-img"><img height="40" src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/<?php echo $supplier_product->supplier->image; ?>" alt="brand1"></div>
            		<div class="ad-ph-sub-text">
            			<div class="fl"><h3><i class="fa fa-rupee"></i><?php echo floor($supplier_product->price+0); ?></h3><span>Free Shipping</span></div>
            			<a rel="nofollow" target="_blank" href="<?=$supplier_product->storeLink;?>" class="goto"><?php if($supplier_product->instock) {echo "Go To Store";} else { echo "Sold Out"; }?> <i class="fa fa-angle-right"></i></a>
            		</div>
            	</div>

<?php } ?>

            
               
              
            </div>

</div>
                </div>
              </div>
            </div>
          
          </div>
        </div>
        <div class="off-blc">
            	<span>%</span><b>OFFERS</b>
                 

<p>
              <?php $offerList=explode('##',$product->offers); 
foreach( $offerList as $ok => $ov){
echo $ov."<br/>";

}
?>
</p>
            </div>
        </div>


      </div>
    </div>
  <?php if (count($product->suppliersProducts) > 0) : ?>
  <div class="row">
    <div class="hidden-xs col-lg-12 col-md-12 col-sm-12 col-xs-12 storedetail mobileview-colume ">
      <div class="full-row ">
      <div class="panel panel-default product-detail">
    <div class="panel-heading filteroption">
      
        <h2 class="pull-left"> <?=$product->product_name?> Price list in India </h2>
        <span class="filter-option-right hidden-xs">
          
        </span>
            <div class="clearfix"></div>
            </div>
        <div class="panel-body"><?php 
    $myIndex = 0;
    foreach ($product->suppliersProducts as $supplier_product): 
    	$myIndex++;
    	$hasCOD = !empty($supplier_product->cod) ? 'hasCod' : '';
    	$hasEMI = !empty($supplier_product->emi) ? 'hasEmi' : '';
    	$hasDelivery = !empty($supplier_product->delivery) ? 'hasDelivery' : '';?>
       <div class="detail-row <?=$hasCOD.' '. $hasDelivery . ' '. $hasEMI?>" id="detail-row<?=$myIndex?>">
          <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 dealer ">
            <div class="colume-outer">
             <div class="colume-inner" itemscope itemtype="http://schema.org/Order">
            <span itemprop="seller" itemscope itemtype="http://schema.org/Organization">
			<span itemprop="name">
			 <img src="<?php Yii::$app->getUrlManager()->getBaseUrl() ?>/<?php echo $supplier_product->supplier->image; ?>" alt="<?php echo $supplier_product->supplier->name; ?> Offers For <?php echo $product->product_name ?>" title="<?php echo $product->product_name ?> Price At <?php echo $supplier_product->supplier->name ?>" /> 
			</span>
			</span>
             </div>
             </div>
             </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  hidden-xs">
            <div class="colume-outer">
             <div class="colume-inner">
              <?php /*'<ul class="buyconditioin buyoption">
                <li> <i class="fa  fa-check-circle"></i> Cash on Delivery </li>
                <li> <i class="fa  fa-check-circle"></i> 4-6 Days Delivery </li>
                <li> <i class="fa fa-close"></i> EMI : Not Available </li>
                <li> <i class="fa fa-check-circle"></i> Return Policy :
                  15 Days </li>
              </ul>'*/;?>
              <ul class="buyconditioin buyoption" id="jqueryHeadache<?=$myIndex?>">
                 <li> <?php echo ($supplier_product->cod ? '<i class="fa  fa-check-circle"></i>'.$supplier_product->getCodDisplayString($product->categories_category_id)  : '<i class="fa fa-close"></i> Not Available</li>');?>
                 <li> <?php echo ($supplier_product->delivery ? '<i class="fa  fa-check-circle"></i>'.$supplier_product->getDeliveryDisplayString($product->categories_category_id) : '<i class="fa fa-close"></i> Delivery Not Available</li>');?>
                 <li> <?php echo ($supplier_product->emi ? '<i class="fa  fa-check-circle"></i> EMI : '. $supplier_product->getEmiDisplayString($product->categories_category_id) : '<i class="fa fa-close"></i> EMI Not Available</li>');?>
                 <li> <?php echo ($supplier_product->return_policy ? '<i class="fa  fa-check-circle"></i> Return Policy : '.$supplier_product->getReturnPolicyDisplayString($product->categories_category_id) : '<i class="fa fa-close"></i> Return Policy Not Available</li>');?>
			  </ul>
              </div></div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 hidden-xs">
            <div class="colume-outer">
             <div class="colume-inner">
               <p class="text-center  hidden-xs"><strong>Offers </strong></p>
               <p class="offer-row padd-t0 hidden-xs" itemprop="offers" itemscope itemtype="http://schema.org/Offer"> <span itemprop="availability" href="http://schema.org/InStock"><strong> <?php echo $supplier_product->offersCount; ?> Offers </strong> Available </span> </p>
              </div></div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 text-center ">
            <div class="colume-outer">
             <div class="colume-inner"itemprop="offers" itemscope itemtype="http://schema.org/Offer">
              <h4> <span itemprop="priceCurrency" class="inline" content="INR"> Rs. </span> <span  class="pricevalue" itemprop="price" content="<?php echo floor($supplier_product->price+0); ?>"><?php echo floor($supplier_product->price+0); ?> </span> <?php if($supplier_product->instock){   ?> <span >  <strong>  FREE </strong> Shiping </span><?php }else{?> <span >  <strong>  Out Of Stock</strong> </span><?php }?> </h4>
              </div>
            </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 text-center"> 
            <span class="pull-right">
            <div class="colume-outer">
              <div class="colume-inner">
           		<a href="<?=$supplier_product->storeLink;?>" rel="nofollow" target="_blank" id="gotostore-<?php echo $supplier_product->supplier->name ?>-<?php echo $product->product_name; ?>" target="_blank" class="btn btn-block btn-blue btn-store"> <?php if($supplier_product->instock){ echo "GO TO STORE"; } else{echo "Sold Out";}    ?>    </a>
              </div>
           </div>
           </span>
            </div>
          </div>
        </div>
        <?php endforeach?>
      </div>
      </div></div>
    </div>
  </div>
  <?php endif;?>

 <?php if(isset($product->product_description) && !empty($product->product_description)){ ?> 

<div class="review-block gray description">
    	 <h2><?=$product->product_name?> Price in india</h2>
            <p> <?=$product->product_description?></p>
    </div>




<?php } ?>

    <div class="container-fluid no-padding detail-section">

      <h3> <?php echo $product->product_name; ?> Specification </h3>
      
      <table width="100%" cellpadding="2" cellspacing="2" class="speci-table">
				<tbody>
      <?php foreach ($featureGroups as $featureGroup): 
                            $printFeatureGroup = '<tr><th colspan="2">
                                    '.$featureGroup->name.'</th>
                                </tr>
                            ';
                            $printFeature = false;
                            $featuresHTML = '';
                            foreach ($featureGroup->features as $feature): 
                            	if(isset($productFeatures[$feature->id])): 
                            	$printFeature = true;
                                $featuresHTML.= '<tr>
                                    <td>'.$feature->name.'</td>
                                    <td> '. $productFeatures[$feature->id] .' </td>
                                </tr>';
                            	endif;
                            endforeach; 
                            if($printFeature):
                                echo $printFeatureGroup . $featuresHTML;
                            endif;
                            ?>
                  <?php endforeach; ?>
                  </tbody></table>

    </div>
    







    <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 storedetail  mobileview-colume margb-none">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 mobileview-colume" style="padding-right: 0px;
    padding-top: 15px;">
        <div class="panel panel-default deal  dealofthe-day" style="margin-bottom:15px;">
          <div class="panel-footer  alternatives" >
             <h6 itemprop="name"> Similar Range Product  </h6>


            <div class="clearfix"></div>
          </div>
                <div class="tab-content">
                <div class="panel-body dealrow">
          <div class="row ">
            <div class="owl-carousel"><?php
                        foreach($similarProducts as $similar):?>
                                <div class="item">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="full-row dealbox mobile  text-center">
                  <?php echo ( $similar->discount ? '<div class="discount-badge2"> <span>' . $similar->discount . '%<strong> OFF</strong> </span></div>' : ''); ?>
                    <a href="<?=\yii\helpers\Url::to([ 'product/index', 'slug' => $similar->slug, 'category' => $similar->category->slug ])?>">
                    <figure> <img  src="<?=NoImage::getProductImageFullPathFromObject($similar,'full','product')?>"  class="img-responsive">
                    </figure>
                          <p class="discription">
                            <?=$similar->product_name?>
                            <span> <img itemprop="image" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span> </p>
                          <p class="price" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer"><strong><span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span>
                           <span class="pricevalue" itemprop="lowPrice" content="<?= floor($similar->lowest_price)?>"> <?=floor($similar->lowest_price)?> </span>
                          </strong> <strong></strong> </p>
                    </a>
                    </div>
                    </div>
                 </div><?php
            endforeach;?>
                     </div>
                </div>
                </div>
                        </div>
                </div>
                </div>
                </div>

      <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 right-padd-none ">
      <div class="full-row detailblock marg-b15 paddb form-horizontal" itemprop="review" itemscope itemtype="http://schema.org/Review">
        <h2> Write a Review </h2>
 <meta itemprop="author" content="nayashopi" />
        <div class="form-group">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
          <span class="block"> <div class="starbase">
                        <div class="star-rating" style="width:<?php if(isset($supplier_product)){ echo $supplier_product->rating*20; } ?>%"></div>
                      </div>
            <label><?=$product->rating?>/5 Rating</label>
            </span> </div>
        </div>
        <?php 
        if(count($ProductReviews) == 0){?>
        <div id="reviewblockDiv"><?php 
        $form = ActiveForm::begin([
                                'id' => 'review-form',
                                'action' => Url::to(['product/reviewpost']),
                                'enableAjaxValidation' => true,
                                'validationUrl' => Url::to(['product/addreview'])]);?>
		<?=$form->field($reviewmodel, 'rating')->hiddenInput(['class'=> 'rating', 'value'=> '4', 'id'=> 'rating'])->label(false); ?>
		<div class="form-group cssHeadache">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?= $form->field($reviewmodel, 'title')->textInput(['class' => 'form-control','placeholder' => 'Title ( 20 Charector)', 'id' => 'user_name']) ?>
          <?=$form->field($reviewmodel, 'product_id')->hiddenInput(['value'=> $product->product_id])->label(false); ?> 
          </div>
        </div>
        
        <div class="form-group cssHeadache">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($reviewmodel, 'description')->textArea(['cols' => '', 'rows' => '', 'class' => 'form-control','placeholder' => 'Description', 'id' => 'review']) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= Html::button('SUBMIT REVIEW', ['class' => 'btn btn-danger', 'name' => 'login-button', 'type' => 'submit']) ?>
          </div>
        </div><?php 
        ActiveForm::end();?>
        </div>
        <?php }else{
        	echo "<div>You have already comment on this product</div>";
        }
        ?>
        </div>
      </div>
    </div>


  </div>      
      
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs ">

	<section>
	<!-- First_Ad -->
	</section>

      
    </div>
       </div>
      </div>
<style>
.picZoomer-pic-wp{
  height:auto !important;
}
</style>
<script>
    function func(id) {
        var rating = $( '#rating' ).val(id);
        var block = $('.blockDiv');
        block.empty();
        for(var i=1; i<=5; i++){
            if(id>=i){
                block.append('<img width="30" height="30" id="'+i+'" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-filled.svg" alt="">');
            }
            else{
                block.append('<img width="30" height="30" id="'+i+'" class="rating" onclick="func(this.id)" src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star-outlined.svg" alt="">');
            }
        }
        block.append('<br><br><label>'+id+'/5 Rating</label>');
    }
$(document).ready(function(){
	$('.funkyradio input[type="checkbox"]').on('click', function(){
		var loop = ($('ul.buyconditioin').length);
		for(var index = 0; index < loop; index++){
			hasCod = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(1) i').hasClass('fa-check-circle'));
			hasEmi = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(2) i').hasClass('fa-check-circle'));
			hasDelivery = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(3) i').hasClass('fa-check-circle'));
			hasReturn = ($('#jqueryHeadache'+(index+1) + ' li:nth-child(4) i').hasClass('fa-check-circle'));

			if(!hasCod && !hasEmi && !hasDelivery && !hasReturn){
				$('#detail-row'+loop).hide();
			}else
				$('#detail-row'+loop).show();
		}
	});
	$('.cssHeadache > div > div').removeClass('form-group');

$('.detailblock').on('beforeSubmit', '#review-form', function () {
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            var detailblock = $('#reviewblockDiv');
                detailblock.empty();
                detailblock.append('<div> Your comment was successfully submitted. </div>');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});

$( "#no" ).click(function() {
    var rating = $( '.rank' ).val(0);
});

$('.detailblock').on('beforeSubmit', '#review-form-rank', function () {
    
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            var detailblock = $('.reviewRank');
                detailblock.empty();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});

$('.formId').on('beforeSubmit', '#priceupdate-form', function () {
    
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            
            var detailblock = $('.formId');
                detailblock.empty();
                detailblock.append('<a href="#"> <i class="fa fa-bell" ></i> Will be alert you </a>');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
    });
    return false;
});
});
</script>
<script type="text/javascript">
$(function() {
  $('.userreview a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top -100 }, 1000);
        return false;
      }
    }
  });
});

;
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:3,
    responsiveClass:true,
	    navigation:true,
navigationText: [
   "<div class='carousel-control-prev crsl-wdgt__prvs-btn '><</div>",
   "<div class='carousel-control-next  crsl-wdgt__next-btn'>></div>"
],
    responsive:{
        0:{
            items:2,
          nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
    }
})

//
//var dealrowInnerWeight = $('.dealrow').innerWidth();
//		//alert(dealrowInnerWeight);
//		$('.owl-stage').css('width',dealrowInnerWeight);
//   
//	
//	var dealrowInnerWeight = $('.owl-item.active').outerWidth();
//		//alert(dealrowInnerWeight);
//		$('.owl-item').css('width',dealrowInnerWeight - 2);
    });  
	
	
	
    </script>
<script type="text/javascript">
    

    $(function() {

if ($(window).width() >= 1023) {    
        $('.picZoomer').picZoomer();

            $('.piclist li').on('click',function(event){
                var $pic = $(this).find('img');
                $('.picZoomer-pic').attr('src',$pic.attr('lsrc'));
            });

}
        });
    </script>
