<?php use frontend\helpers\NoImage;?>
<?php if(is_array($productIds)):
		$products = \common\models\RecentlyViewedProducts::getRecentlyViewedProducts($productIds);?>
	<?php if(!empty($products)):?>
<section class="footwear">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
        <div class="panel panel-default deal  dealofthe-day">
          <div class="panel-footer">
            <h5 itemprop="name" style="text-align:left;"> Recently Viewed </h5>
            <div class="clearfix"></div>
          </div>
          <div class="tab-content">
			<div class="panel-body dealrow">
          	  <div class="row">
                  <div class="owl-carousel">
          	  <?php foreach($products as $product):
          	       $product = $product->product;?>
                    <div class="item"  itemscope="" itemtype="http://schema.org/Product">
<meta itemprop="name"  value="<?=$product->product_name?>" content="<?=$product->product_name?>" />
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">

<meta itemprop="priceCurrency" value="INR" content="INR"/>
<meta itemprop="lowPrice"  value="<?=$product->lowest_price?>" content="<?=$product->lowest_price?>" />
                    <div class="full-row dealbox mobile  text-center" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"> 
                        <?php if(!empty($product->discount)):?>
                        <div class="discount-badge2"> <span><?=$product->discount?>% <strong> OFF</strong></span> </div>
                        <?php endif;?>
                      <a href="<?=\yii\helpers\Url::to(['product/index', 'slug' => $product->slug, 'category' => $product->category->slug])?>">
                      <figure> <img itemprop="image" src="<?=NoImage::getProductImageFullPathFromObject($product,'full','zoom')?>" class="img-responsive"> </figure>
                      <p class="discription">  <?=strlen($product->product_name)> 36 ? substr($product->product_name, 0, 40). '...' : $product->product_name?>  </p>
					  <span class="text-center"> <img src=" <?php echo Yii::$app->getUrlManager()->getBaseUrl(true); ?>/images/star.jpg" alt=""> </span>
                      <p class="price"><strong><span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span><span class="pricevalue" content="Fill Product Price"> <?=$product->lowest_price?> </span></strong></p>
                      </a> 
                   </div>
                  </div>
                  </div>
              <?php endforeach;?>
				</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<script type="text/javascript">
$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:0,
	    responsiveClass:true,
		    navigation:true,
navigationText: [
   "<div class='carousel-control-prev crsl-wdgt__prvs-btn '><</div>",
   "<div class='carousel-control-next  crsl-wdgt__next-btn'>></div>"
],
	    responsive:{
	        0:{
	         items:2,
	             nav:true
	        },
	        600:{
	         items:2,
	            nav:true
	        },
	        1000:{
	          items:6,
	            nav:true,
	            loop:false
	 			
	        }
	    }
	});
});
</script>
	<?php endif;?>
<?php endif;?>
