<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'action' => 'site/login',
    'enableAjaxValidation' => true,
    'validationUrl' => '/index.php/site/login']); ?>