<?php if(empty($products)):?>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
		<strong>No record found...</strong>
	</div>
<?php else:?>
	<?php foreach($products as $product):?>
	

	<?php 
	   $categorySlug = \common\models\Categories::findOne(['category_id' => $product['categories_category_id']]);

	$url=\yii\helpers\Url::to(['product/index', 'slug'=>$product['slug'], 'category' => $categorySlug->slug]);
	if($menuItem->is_landing_page){
		$url=$product['url'];
	}
	
	?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  itemscope="" itemtype="http://schema.org/Product">

<meta itemprop="name" value='<?php echo addslashes($product['product_name']); ?>' content='<?php echo addslashes($product['product_name']); ?>'/>
          <div class="full-row product-colume"   itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">

<meta itemprop="lowPrice" value="<?php echo $product['lowest_price']; ?>" content="<?php echo $product['lowest_price']; ?>"/>
<meta itemprop="priceCurrency" value="INR" content="INR"/>
    <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
          <a class="btn-favourite" href="#"> <i class="fa fa-heart"></i> </a>
	<?php

 	    $productImage = \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($product);?>
	<a id="listpage-<?=$product['product_name']?>" title="<?=$product['product_name']?>" href="<?php echo $url; ?>">
			<figure><img itemprop="image" class="lazy"  data-original="<?=$productImage?>" alt="<?=$product['product_name']?>" /></figure>
            <p class="discription"><?=strlen($product['product_name'])> 36 ? substr( $product['product_name'], 0, 25). '...' :  $product['product_name']?> </p>
			  <div class="starbase">
                <div class="star-rating" style="width:<?=$product['rating']*20?>%"></div>
			  </div>
            <p class="price"><?php echo ( (!empty($product['original_price']) && $product['lowest_price'] != $product['original_price'] ) ? '<span>  <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span>' . number_format($product['original_price'],0) . ' </span>' : '') ?> <strong>  <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> <span class="pricevalue"><?=number_format($product['lowest_price'],0)?> </span>
	     
		    <?php if(!empty($product['discount'])) {?>
	        
	          <i style="font-size:12px;    color: #b5b4b4;">(<?=$product['discount']?>% OFF)</i>
	        
			<?php } ?>
			</strong></p>
			<strong class="stroes"> <span><?php if(array_key_exists('stores',$product)){ echo 	count($product['stores'])." stores at " . $product['lowest_price']; }else{echo "&nbsp;";}?> </span> </strong>
			</a>
          

						<div class="prdct-item__cmpr"> <label> <input name="<?php echo $product['product_name'] ?>" slug="<?php echo $product['slug'] ?>" type="checkbox" name="compare" class="prdct-item__cmpr-chkbx js-add-to-cmpr"> <span class="prdct-item__cmpr-img">Add to Compare</span> </label> </div>
            
           
          </div>
        </div>
</div>
	<?php endforeach;?>
<?php endif;?>
