<?php if(empty($products)):?>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<strong>No record found...</strong>
	</div>
<?php else:?>
	<?php foreach($products as $product):?>

	<?php 
	 $categorySlug = \common\models\Categories::findOne(['category_id' => $product['categories_category_id']]);
	$url=\yii\helpers\Url::to(['product/index', 'slug'=>$product['slug'], 'category' => $categorySlug->slug]);
	if($menuItem->is_landing_page){
		$url=$product['url'];
	}
	
	?>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"   itemscope="" itemtype="http://schema.org/Product" >

<meta itemprop="name" value='<?php echo addslashes($product['product_name']); ?>' content='<?php echo addslashes($product['product_name']); ?>'/>
<div class="full-row product-colume"  itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">

<meta itemprop="priceCurrency" value="INR" content="INR"/>

<meta itemprop="lowPrice" value="<?=$product['lowest_price']?>" content="<?=$product['lowest_price']?>"/>
	
           <a class="btn-favourite" href="#"> <i class="fa fa-heart"></i> </a>	<?php 
	   
	    $productName = strlen($product['product_name']) > 68 ? substr($product['product_name'], 0,65). '...' : $product['product_name'];
// 	    $productImage = \Yii::$app->params['productImageBaseUrl'] . $product['categories']['folder_slug'].'/'.$product['brands_folder_slug'].'/150x200/$product['product_image'].jpg';
		$productImage = \frontend\helpers\NoImage::getProductImageFullPathFromElasticResult($product);?>
		  <div class="full-row" style="cursor:pointer" >
	        <a id="listpage-<?=$product['product_name']?>" title="<?=$product['product_name']?>" href="<?php echo 	$url; ?>">
	          <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
	            <figure><img itemprop="image" class="lazy" data-original="<?=$productImage?>" alt="<?=$product['product_name']?>" title="<?=$product['product_name']?>" /></figure>
	          </div>
	        </a>
	        <div  class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left mobile-text-center"  itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
	          <a title="<?=$product['product_name']?>" href="<?php echo 	$url; ?>">
	     
					  <p class="discription text-left"><?=$productName?></p>        	  
						<div class="starbase">
                <div class="star-rating" style="width:<?=$product['rating']*20?>%"></div>
			  		</div>
	          <p class="price text-left"><?php echo ( (!empty($product['original_price']) && $product['lowest_price'] != $product['original_price'] ) ? '<span>  <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> <span class="pricevalue"  style="    color: #b5b4b4 !important;font-size:12px !important;" content="Fill Product Price"> ' . $product['original_price'] . ' </span> </span>' : '') ?> <strong> 
              <span class="inline" itemprop="priceCurrency" content="INR"> <i class="fa fa-rupee"></i> </span> <?=$product['lowest_price']?>
							
												<?php if(!empty($product['discount'])) {?>
										
											<i style="font-size:12px;    color: #b5b4b4;">(<?=$product['discount']?>% OFF)</i>
										
								<?php } ?>
							</strong> </p>
 <strong class="stroes"> <span><?php if(array_key_exists('stores',$product)){ echo       count($product['stores'])." stores at " . $product['lowest_price']; }else{echo "&nbsp;";}?> </span> </strong>
							</a>
	        		<div class="prdct-item__cmpr"> <label> <input name="<?php echo $product['product_name'] ?>" slug="<?php echo $product['slug'] ?>" type="checkbox" name="compare" class="prdct-item__cmpr-chkbx js-add-to-cmpr"> <span class="prdct-item__cmpr-img">Add to Compare</span> </label> </div>
        
			</div>
	      </div>
	      </div>
	    </div>
	<?php endforeach;?>
<?php endif;?>
