<?php $script = '';?>
<?php foreach($features as $key => $feature):?>
<?php foreach($feature as $searchFilter):?>
       <!-- Feature Start -->
       <section class="brand brand-block">
        <div class="row lefty">
              <div class="col-xs-12 logo text-center ">
                <h4 class="text-left"><?=$searchFilter['name'] ?><span class="popup__name-cnt"></span> <a href="#"> <span id="clear_<?=$searchFilter['key'] ?>"> Clear  </span></a></h4>
                <div class="input-group searcbar hidden-xs" >
                  <input type="text" class="form-control" name="txtFeature_<?=$searchFilter['key'] ?>" id="txtFeature_<?=$searchFilter['key'] ?>">
                  <span class="input-group-addon "> <i class="fa fa-search"></i> </span>
                                </div>
              </div>
        </div>
        <div class="row righty">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="full-row left-scrollblock " >
             <!-- Feature Item -->
             <div id="featureList_<?=$searchFilter['key'] ?>" class="featureList">
             <?php foreach($searchFilter['values'] as $feature_value):?>

	   <?php if($feature_value['count']>0) {?>
                                <div class="form-group">
                    <label  class="checkbox-inline" >
                        <input type="checkbox" name="featureValues[]" value="<?=$searchFilter['key'].'_'.$feature_value['key'];?>" class="featureValues">
                         <?=$feature_value['name'];?>
                    </label>
                </div>

  <?php } ?>
               <!-- Feature Item -->
             <?php endforeach;?>
             </div>
            </div>
          </div>
        </div>
       </section>
       <!-- Feature End -->
       <?php $script .= '$(\'#txtFeature_'.$searchFilter['key'].'\').on(\'keyup\', function () {
            var value = this.value;  
            $(\'#featureList_'.$searchFilter['key'].' div\').hide().each(function () {
                if ($(this).text().search( new RegExp(value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        
        $("#clear_'.$searchFilter['key'].'").click(function(e){
            e.preventDefault();
            $(\'#txtFeature_'.$searchFilter['key'].'\').val(\'\');
            $(\'#txtFeature_'.$searchFilter['key'].'\').keyup();
            $(\'#featureList_'.$searchFilter['key'].' .featureValues\').attr(\'checked\', false); 
            ajaxSearch();
        });';?>
<?php endforeach;?>
<?php endforeach;
echo '<script type="text/javascript">$(document).ready(function(){
  '.$script.'
});</script>';
