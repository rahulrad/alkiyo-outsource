(function (e) {
	"use strict";
	var t = {
		defaults: {
			speed: 200,
			collapsedHeight: 200,
			moreLink: '<a href="#">Read More >></a>',
			lessLink: '<a href="#">Close</a>',
			fade: !1,
			heightMargin: 20
		},
		_init: function (t, n) {
			n.heightMargin + n.collapsedHeight >= e(t).height() || (e(t).data("gsp_rm.settings", n).data("gsp_rm.expanded", 0).addClass("gs_readmore").css({
				height: n.collapsedHeight
			}), this._appendButtons(t))
		},
		_appendButtons: function (t) {
			var n = e(t).data("gsp_rm.settings"),
				i = e(n.lessLink).attr("data-gsrm-less", 1).addClass("hide"),
				r = e(n.moreLink).attr("data-gsrm-more", 1);
			n.fade ? (r = e("<p></p>").html(r[0].outerHTML).addClass("gs_readmore_fade"), e(t).append(r.on("click", function (n) {
				return function (o) {
					n._toggle(e(t), o, i, r)
				}
			}(this)))) : (e(t).after(i.on("click", function (n) {
				return function (o) {
					n._toggle(e(t), o, i, r)
				}
			}(this))), e(t).after(r.on("click", function (n) {
				return function (o) {
					n._toggle(e(t), o, i, r)
				}
			}(this))))
		},
		_toggle: function (e, t, n, i) {
			t.preventDefault();
			var r = this,
				o = e.data("gsp_rm.settings"),
				s = e.data("gsp_rm.expanded");
			s ? (e.animate({
				height: o.collapsedHeight
			}, o.speed), e.data("gsp_rm.expanded", 0), r._afterAnimation(o, n, i)) : e.animate({
				height: e[0].scrollHeight
			}, o.speed, function () {
				e.data("gsp_rm.expanded", 1).height("auto"), r._afterAnimation(o, n, i)
			})
		},
		_afterAnimation: function (e, t, n) {
			e.fade || t.toggleClass("hide"), n.toggleClass("hide")
		}
	};
	e.fn.gsp_readmore = function (n) {
		void 0 === n && (n = {}), n = e.extend({}, t.defaults, n), this.each(function () {
			t._init(this, n)
		})
	}
}(jQuery));

$(document).scroll(function () {
	var scroll = $(window).scrollTop();
	var height = $(window).height();

	if (!$('.spe-tabs').length) {
		/* set sticky menu */
		if (scroll >= height) {
			$(".menu-block").addClass("navbar-fixed-top animated fadeInDown").delay(2000).fadeIn();
		} else if (scroll <= height) {
			$(".menu-block").removeClass("navbar-fixed-top animated fadeInDown");
		} else {
			$(".menu-block").removeClass("navbar-fixed-top animated fadeInDown");
		}

	}

	if ($(this).scrollTop() >= 50) {
		/* If page is scrolled more than 50px */
		$("#back-to-top").fadeIn(200); /* Fade in the arrow */
	} else {
		$("#back-to-top").fadeOut(200); /* Else fade out the arrow */
	}


	if ($(this).scrollTop() >= 300) {
		$('.spe-tabs').addClass('fixed-tabs');
	}
	else {
		$('.spe-tabs').removeClass('fixed-tabs');
	}
});
$(document).ready(function (e) {

$('.alpha-blc a').on('click', function () {
            var value = this.text;  
            
            $('.brands-bloc li ').hide().each(function () {
                if ($(this).find('a').attr('title').search(new RegExp('^'+value, "i")) > -1) {
                     $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });


$('#allstores').on('click',function(){

$('.brands-bloc li ').show();
});


$('.avail-balance-block').click(function(){ $('.glow-bg,.overlay2').trigger('click')});

	// -------for top country-----------
	//$("#countries").msDropdown();

	// -------for dropdown-----------
	$(".dropdown").hover(
		function () {
			$('.dropdown-menu', this).stop(true, true).fadeIn("fast");
			$(this).toggleClass('open');
			$('b', this).toggleClass("caret caret-up");
		},
		function () {
			$('.dropdown-menu', this).stop(true, true).fadeOut("fast");
			$(this).toggleClass('open');
			$('b', this).toggleClass("caret caret-up");
		});
	// -------for subdropdown-----------
	$(".dropdown-submenu").hover(
		function () {
			$('.dropdown-menu-child', this).stop(true, true).fadeIn("fast");
			$(this).toggleClass('open');
			$('b', this).toggleClass("caret caret-up");
		},
		function () {
			$('.dropdown-menu-child', this).stop(true, true).fadeOut("fast");
			$(this).toggleClass('open');
			$('b', this).toggleClass("caret caret-up");
		});



	// -------for subdropdown end-----------

	$(".bs-dropdown-to-select-group:not(.homepagemenu)").hover(

		function () {
			// $('.overlay').toggle();
			$('.dropdown-menu', this).stop(true, true).fadeIn("fast");
			$(this).toggleClass('open');
			$('b', this).toggleClass("caret caret-up");
		},
		function () {
			$('.dropdown-menu', this).stop(true, true).fadeOut("fast");
			$(this).toggleClass('open');
			$('b', this).toggleClass("caret caret-up");
		});
	$(".main-menu:not(.homepagemenu)").hover(function (e) {
		$('.overlay').show();

	});
	$(".overlay").hover(function (e) {
		$(this).hide();

	});

	// -------for auto fil dropdown-----------
	$(function () {
		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
		];
		$(".search").autocomplete({
			source: availableTags
		});
	});
	// -------for menudropdown-----------

	$(document).on('click', '.bs-dropdown-to-select-group .dropdown-menu li', function (event) {
		var $target = $(event.currentTarget);
		$target.closest('.bs-dropdown-to-select-group')
			.find('[data-bind="bs-drp-sel-value"]').val($target.attr('data-value'))
			.end()
			.children('.dropdown-toggle').dropdown('toggle');
		$target.closest('.bs-dropdown-to-select-group')
			.find('[data-bind="bs-drp-sel-label"]').text($target.attr('data-value')); /*$target.text()*/
		return false;
	});



	//page top scroll
	//	   
	//				   $(function() {
	//    $('a.page-scroll').bind('click', function(evenat) {
	//        var $anchor = $(this);
	//        $('html, body').stop().animate({
	//            scrollTop: $($anchor.attr('href')).offset().top
	//        }, 1500, 'easeInOutExpo');
	//        event.preventDefault();
	//    });
	//});


});

$(window).load(function () {

if ($(window).width() > 767) {
	var prodetail = $('.detail_blog-right').innerHeight();
	$('.display_slider').css('min-height', prodetail + 2);
}
});

/*set menu*/
$(document).ready(function (e) {
	//for menu--------------------	
	//var pagewrap = $('.container').innerWidth();
	//var leftmenu = $(".menubig").innerWidth();
	//alert(pagewrap);
	//$('.dropdown-menu-child').css('width',pagewrap -leftmenu-350);
	//for menu--------end------------	

	//seting bannerimag 
	//var bannerheight = $('.addbanner').innerHeight();
	//$('.main-banner img').css('height',bannerheight-62);
	//seting bannerimag 



	//	var prodetail = $('.detail_blog-right').innerHeight();
	//	$('.display_slider').css('min-height',prodetail + 2 );
	//	$('.picZoomer-pic-wp').css('height',prodetail - 22 );





	var smldealbox = $('.smldealbox ').innerHeight();
	$('.bigdealbox').css('min-height', smldealbox * 2 + 18);

	var pagewrap = $('.container').innerWidth();
	//alert(pagewrap);
	$('.outer_box').css('width', pagewrap - 30);
});



//scrolltop------------------------
$(document).ready(function () {

	$(function () {

		$(document).on('scroll', function () {
			if ($(window).scrollTop() > 500) {
				$('.page-scroll').show();
			} else {
				$('.page-scroll').hide();
			}
		});

		$('.page-scroll').on('click', scrollToTop);
	});

	function scrollToTop() {
		verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = $('body');
		offset = element.offset();
		offsetTop = offset.top;
		$('html, body').animate({ scrollTop: offsetTop }, 500, 'linear');
	}
	//	mobile search
	$(".searcbbtn").click(function () {
		$(".searcbar").show('slow');
	});
	$(".closesearch").click(function () {
		$(".searcbar").hide('slow');
	});

});
//main crousel------------------------
$(document).ready(function () {
	var homepageCarousel = $('#myCarousel').carousel({
		interval: 4000

	});

	var clickEvent = false;
	$('#myCarousel').on('click', '.nav-pills a', function () {
		clickEvent = true;
		$('.nav-pills li').removeClass('active');
		$(this).parent().addClass('active');
	}).on('slid.bs.carousel', function (e) {
		if (!clickEvent) {
			var count = $('.nav-pills').children().length - 1;
			var current = $('.nav-pills li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if (count == id) {
				$('.nav-pills li').first().addClass('active');
			}
		}
		clickEvent = false;
	});

	$('div[data-slide="prev"]').click(function () {
		$('#myCarousel').carousel('prev');
	});

	$('div[data-slide="next"]').click(function () {
		$('#myCarousel').carousel('next');
	});


	//product detail------------------------
	$('#myCarousel-product').carousel({
		interval: false
	});

	var clickEvent = false;
	$('#myCarousel-product').on('click', '.nav-pills a', function () {
		clickEvent = true;
		$('.nav-pills li').removeClass('active');
		$(this).parent().addClass('active');
	}).on('slid.bs.carousel', function (e) {
		if (!clickEvent) {
			var count = $('.nav-pills').children().length - 1;
			var current = $('.nav-pills li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if (count == id) {
				$('.nav-pills li').first().addClass('active');
			}
		}
		clickEvent = false;
	});
});

$(window).resize(function () {
	//for menu--------------------	
	//var pagewrap = $('.container').innerWidth();
	//var leftmenu = $(".menubig").innerWidth();
	//$('.dropdown-menu-child').css('width',pagewrap -leftmenu-350 );
	//for menu--------------------	


	//seting bannerimag
	//var bannerheight = $('.addbanner').innerHeight();
	//$('.main-banner img').css('height',bannerheight-62);


	//seting bannerimag 
	var smldealbox = $('.smldealbox ').innerHeight();
	$('.bigdealbox').css('min-height', smldealbox * 2 + 18);

	var pagewrap = $('.container').innerWidth();
	//alert(pagewrap);
	$('.outer_box').css('width', pagewrap - 30);

});



(function ($) {

	$.fn.rating = function (method, options) {
		method = method || 'create';
		// This is the easiest way to have default options.
		var settings = $.extend({
			// These are the defaults.
			limit: 5,
			value: 0,
			glyph: "glyphicon-star",
			coloroff: "gray",
			coloron: "gold",
			size: "15px",
			cursor: "default",
			onClick: function () { },
			endofarray: "idontmatter"
		}, options);
		var style = "";
		style = style + "font-size:" + settings.size + "; ";
		style = style + "color:" + settings.coloroff + "; ";
		style = style + "cursor:" + settings.cursor + "; ";



		if (method == 'create') {
			//this.html('');	//junk whatever was there

			//initialize the data-rating property
			this.each(function () {
				attr = $(this).attr('data-rating');
				if (attr === undefined || attr === false) { $(this).attr('data-rating', settings.value); }
			})

			//bolt in the glyphs
			for (var i = 0; i < settings.limit; i++) {
				this.append('<span data-value="' + (i + 1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
			}

			//paint
			this.each(function () { paint($(this)); });

		}
		if (method == 'set') {
			this.attr('data-rating', options);
			this.each(function () { paint($(this)); });
		}
		if (method == 'get') {
			return this.attr('data-rating');
		}
		//register the click events
		this.find("span.ratingicon").click(function () {
			rating = $(this).attr('data-value')
			$(this).parent().attr('data-rating', rating);
			paint($(this).parent());
			settings.onClick.call($(this).parent());
		})

		function paint(div) {
			rating = parseInt(div.attr('data-rating'));
			div.find("input").val(rating); //if there is an input in the div lets set it's value
			div.find("span.ratingicon").each(function () { //now paint the stars

				var rating = parseInt($(this).parent().attr('data-rating'));
				var value = parseInt($(this).attr('data-value'));
				if (value > rating) { $(this).css('color', settings.coloroff); } else { $(this).css('color', settings.coloron); }
			})
		}

	};

}(jQuery));

$(document).ready(function () {

	$("#stars-default").rating();
	$("#stars-green").rating('create', { coloron: 'green', onClick: function () { alert('rating is ' + this.attr('data-rating')); } });
	$("#stars-herats").rating('create', { coloron: 'red', limit: 10, glyph: 'glyphicon-heart' });
});

function number_format(number, decimals, dec_point, thousands_sep) {
	// Strip all characters but numerical ones.
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}
//-----------------filter mobile view------------------------------------
$(document).ready(function () {

	var selectedchec = 4;
	if ($(window).width() <= 767) {
		selectedchec = 2;
	}


	$(".filter").click(function () {
		//alert("hi");
		$(".filter-in").toggleClass('filter-out');
		//$(".filter-in").animate({ left: '0'});
		//$(".overlay2").toggleClass('show');
		$(".filter-in").show();
		$(".filterbutton-row").show();
	});

	$(".btn-filter").click(function () {
		//alert("hi");
		$(".filter-in").toggleClass('filter-out');
		$(".filterbutton-row").hide();

	});


	$("body").on("change", ".js-add-to-cmpr", function () {
		var checkboxInstance = $(this);


		if ($('.cmpr-pnl-wrpr').hasClass('add-cmp-ml')) {
			$('.cmpr-pnl-wrpr').removeClass('add-cmp-ml');
		}

		var emptyTextBoxes = $('.cmpr-pnl-list').find('input:text').filter(function () { return this.value == ""; });

		if ($("input.js-add-to-cmpr:checked").length <= selectedchec) {
			if ($(this).prop("checked") == true) {
				var targetElement = $("#" + emptyTextBoxes[0].id);
				var targetElementImage = targetElement.parents('.comparerow').find('.compare-product img').offset();
				var slug = $(this).attr('slug');
				var name = $(this).attr('name');
				var imageOffset = $(this).parents('.product-colume').find('img').offset();
				$(this).parents('.product-colume').find('img').clone().addClass('product-clone').css({
					'left': imageOffset.left + 'px',
					'top': parseInt(imageOffset.top - $(window).scrollTop()) + 'px',
					'position': 'fixed',
					'display': 'none',
				}).appendTo($(this).parents('.product-colume').find('img').parent());
				var cart = $('nav .navbar-right strong').offset();


				$('.product-clone').animate({ top: parseInt(targetElementImage.top - $(window).scrollTop()) + 'px', left: targetElementImage.left + 'px', 'height': '0px', 'width': '0px' }, 400,
					function () {
						targetElement.parents('.comparerow').find('.compare-product img').attr('src', $(this).attr('src'));
						targetElement.val(checkboxInstance.attr('name'));
						targetElement.attr('slug', checkboxInstance.attr('slug'));
						$(this).remove();
					});
			} else {
				var slug = checkboxInstance.attr('slug');
				var objectInput = $(".cmpr-pnl-list input[slug=" + slug + "]");
				objectInput.val("");
				objectInput.attr('slug', '');
				objectInput.parents('.comparerow').find('.compare-product img').attr('src', '/images/generic_product_image_small.png');

			}

		} else {
			checkboxInstance.prop('checked', false);
			if (selectedchec > 0) {
				alert("Maximum " + selectedchec + " products compare at a time");
			}

		}

	});


	$('#compareproducts').click(function () {
		var notemptyTextBoxes = $('.cmpr-pnl-list').find('input:text').filter(function () { return this.value !== ""; });
		if (notemptyTextBoxes.length > 0) {
			var string = "";
			notemptyTextBoxes.each(function () {
				string += "&products[]=" + $("#" + this.id).attr('slug');
			});
			var params = string.substring(1, string.length);
			window.location.href = "/product/compare?" + params
		}
	});



});


//-----------------filter mobile view------------------------------------



/// for menu resize






$(document).ready(function () {
	if ($(window).width() >= 1023) {
		$("img.lazy").lazyload({
			effect: "fadeIn"
		});
	} else {
		$('img.lazy').each(function () {
			$(this).attr('src', $(this).data('original'));
		});

	}

 $(".desc a").click(function () {
                var top = $(".description").offset().top;
                var actualtop = top - 100;
                $('html, body').animate({
                        scrollTop: actualtop
                }, 500);
        });

	$(".review a").click(function () {
		var top = $("#user_name").offset().top;
		var actualtop = top - 100;
		$('html, body').animate({
			scrollTop: actualtop
		}, 500);
	});

	$(".alternative a").click(function () {
		var top = $(".alternatives").offset().top;
		var actualtop = top - 100;
		$('html, body').animate({
			scrollTop: actualtop
		}, 500);
	});




	$("a.specs").click(function () {
		var top = $(".detail-section").offset().top;
		var actualtop = top - 100;
		$('html, body').animate({
			scrollTop: actualtop
		}, 500);
	});




	if ($(window).width() >= 1300) {
		// for mainmenu
		var pagewrap = $('.container').innerWidth();
		var leftmenu = $(".menubig").innerWidth();
		$('.dropdown-menu-child').css('width', pagewrap - leftmenu - 350);
		// for mainmenu ends

	} else {
		// for mainmenu
		var pagewrap = $('.container').innerWidth();
		var leftmenu = $(".menubig").innerWidth();
		$('.dropdown-menu-child').css('width', pagewrap - leftmenu);
		// for mainmenu ends
	}


	$('#dynamic_text').gsp_readmore({
		speed: 500,
		collapsedHeight: 58,
		lessLink: '<a href="javascript:void(0)">Read Less >> </a>'
	});


	$('.fa-search').click(function () {
		$('.mobileview-hd2').slideToggle();
	});




});









jQuery(document).ready(function ($) {



$('.acc_section').click(function(){ $(this).find('.acc_content').slideToggle(); $(this).addClass('acc_active'); });
	//open/close mega-navigation
	$('.cd-dropdown-trigger').on('click', function (event) {
		event.preventDefault();
		toggleNav();
	});

	//close meganavigation
	$('.cd-dropdown .cd-close').on('click', function (event) {
		event.preventDefault();
		toggleNav();
	});

	//on mobile - open submenu
	$('.has-children').children('a').on('click', function (event) {
		//prevent default clicking on direct children of .has-children 
		event.preventDefault();
		var selected = $(this);
		selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');
	});

	//on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
	var submenuDirection = (!$('.cd-dropdown-wrapper').hasClass('open-to-left')) ? 'right' : 'left';
	$('.cd-dropdown-content').menuAim({
		activate: function (row) {
			$(row).children().addClass('is-active').removeClass('fade-out');
			if ($('.cd-dropdown-content .fade-in').length == 0) $(row).children('ul').addClass('fade-in');
		},
		deactivate: function (row) {
			$(row).children().removeClass('is-active');
			if ($('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row))) {
				$('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
				$(row).children('ul').addClass('fade-out')
			}
		},
		exitMenu: function () {
			$('.cd-dropdown-content').find('.is-active').removeClass('is-active');
			return true;
		},
		submenuDirection: submenuDirection,
	});

	//submenu items - go back link
	$('.go-back').on('click', function () {
		var selected = $(this),
			visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
		selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
	});

	function toggleNav() {
		var navIsVisible = (!$('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
		$('.cd-dropdown').toggleClass('dropdown-is-active', navIsVisible);
		$('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
		if (!navIsVisible) {
			$('.cd-dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
				$('.has-children ul').addClass('is-hidden');
				$('.move-out').removeClass('move-out');
				$('.is-active').removeClass('is-active');
			});
		}
	}


});


(function ($) {

	$.fn.menuAim = function (opts) {
		// Initialize menu-aim for all elements in jQuery collection
		this.each(function () {
			init.call(this, opts);
		});

		return this;
	};

	function init(opts) {
		var $menu = $(this),
			activeRow = null,
			mouseLocs = [],
			lastDelayLoc = null,
			timeoutId = null,
			options = $.extend({
				rowSelector: "> li",
				submenuSelector: "*",
				submenuDirection: "right",
				tolerance: 75, // bigger = more forgivey when entering submenu
				enter: $.noop,
				exit: $.noop,
				activate: $.noop,
				deactivate: $.noop,
				exitMenu: $.noop
			}, opts);

		var MOUSE_LOCS_TRACKED = 3, // number of past mouse locations to track
			DELAY = 300; // ms delay when user appears to be entering submenu

		/**
		 * Keep track of the last few locations of the mouse.
		 */
		var mousemoveDocument = function (e) {
			mouseLocs.push({ x: e.pageX, y: e.pageY });

			if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
				mouseLocs.shift();
			}
		};

		/**
		 * Cancel possible row activations when leaving the menu entirely
		 */
		var mouseleaveMenu = function () {
			if (timeoutId) {
				clearTimeout(timeoutId);
			}

			// If exitMenu is supplied and returns true, deactivate the
			// currently active row on menu exit.
			if (options.exitMenu(this)) {
				if (activeRow) {
					options.deactivate(activeRow);
				}

				activeRow = null;
			}
		};

		/**
		 * Trigger a possible row activation whenever entering a new row.
		 */
		var mouseenterRow = function () {
			if (timeoutId) {
				// Cancel any previous activation delays
				clearTimeout(timeoutId);
			}

			options.enter(this);
			possiblyActivate(this);
		},
			mouseleaveRow = function () {
				options.exit(this);
			};

		/*
		 * Immediately activate a row if the user clicks on it.
		 */
		var clickRow = function () {
			activate(this);
		};

		/**
		 * Activate a menu row.
		 */
		var activate = function (row) {
			if (row == activeRow) {
				return;
			}

			if (activeRow) {
				options.deactivate(activeRow);
			}

			options.activate(row);
			activeRow = row;
		};

		/**
		 * Possibly activate a menu row. If mouse movement indicates that we
		 * shouldn't activate yet because user may be trying to enter
		 * a submenu's content, then delay and check again later.
		 */
		var possiblyActivate = function (row) {
			var delay = activationDelay();

			if (delay) {
				timeoutId = setTimeout(function () {
					possiblyActivate(row);
				}, delay);
			} else {
				activate(row);
			}
		};

		/**
		 * Return the amount of time that should be used as a delay before the
		 * currently hovered row is activated.
		 *
		 * Returns 0 if the activation should happen immediately. Otherwise,
		 * returns the number of milliseconds that should be delayed before
		 * checking again to see if the row should be activated.
		 */
		var activationDelay = function () {
			if (!activeRow || !$(activeRow).is(options.submenuSelector)) {
				// If there is no other submenu row already active, then
				// go ahead and activate immediately.
				return 0;
			}

			var offset = $menu.offset(),
				upperLeft = {
					x: offset.left,
					y: offset.top - options.tolerance
				},
				upperRight = {
					x: offset.left + $menu.outerWidth(),
					y: upperLeft.y
				},
				lowerLeft = {
					x: offset.left,
					y: offset.top + $menu.outerHeight() + options.tolerance
				},
				lowerRight = {
					x: offset.left + $menu.outerWidth(),
					y: lowerLeft.y
				},
				loc = mouseLocs[mouseLocs.length - 1],
				prevLoc = mouseLocs[0];

			if (!loc) {
				return 0;
			}

			if (!prevLoc) {
				prevLoc = loc;
			}

			if (prevLoc.x < offset.left || prevLoc.x > lowerRight.x ||
				prevLoc.y < offset.top || prevLoc.y > lowerRight.y) {
				// If the previous mouse location was outside of the entire
				// menu's bounds, immediately activate.
				return 0;
			}

			if (lastDelayLoc &&
				loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {
				// If the mouse hasn't moved since the last time we checked
				// for activation status, immediately activate.
				return 0;
			}

			// Detect if the user is moving towards the currently activated
			// submenu.
			//
			// If the mouse is heading relatively clearly towards
			// the submenu's content, we should wait and give the user more
			// time before activating a new row. If the mouse is heading
			// elsewhere, we can immediately activate a new row.
			//
			// We detect this by calculating the slope formed between the
			// current mouse location and the upper/lower right points of
			// the menu. We do the same for the previous mouse location.
			// If the current mouse location's slopes are
			// increasing/decreasing appropriately compared to the
			// previous's, we know the user is moving toward the submenu.
			//
			// Note that since the y-axis increases as the cursor moves
			// down the screen, we are looking for the slope between the
			// cursor and the upper right corner to decrease over time, not
			// increase (somewhat counterintuitively).
			function slope(a, b) {
				return (b.y - a.y) / (b.x - a.x);
			};

			var decreasingCorner = upperRight,
				increasingCorner = lowerRight;

			// Our expectations for decreasing or increasing slope values
			// depends on which direction the submenu opens relative to the
			// main menu. By default, if the menu opens on the right, we
			// expect the slope between the cursor and the upper right
			// corner to decrease over time, as explained above. If the
			// submenu opens in a different direction, we change our slope
			// expectations.
			if (options.submenuDirection == "left") {
				decreasingCorner = lowerLeft;
				increasingCorner = upperLeft;
			} else if (options.submenuDirection == "below") {
				decreasingCorner = lowerRight;
				increasingCorner = lowerLeft;
			} else if (options.submenuDirection == "above") {
				decreasingCorner = upperLeft;
				increasingCorner = upperRight;
			}

			var decreasingSlope = slope(loc, decreasingCorner),
				increasingSlope = slope(loc, increasingCorner),
				prevDecreasingSlope = slope(prevLoc, decreasingCorner),
				prevIncreasingSlope = slope(prevLoc, increasingCorner);

			if (decreasingSlope < prevDecreasingSlope &&
				increasingSlope > prevIncreasingSlope) {
				// Mouse is moving from previous location towards the
				// currently activated submenu. Delay before activating a
				// new menu row, because user may be moving into submenu.
				lastDelayLoc = loc;
				return DELAY;
			}

			lastDelayLoc = null;
			return 0;
		};

		/**
		 * Hook up initial menu events
		 */
		$menu
			.mouseleave(mouseleaveMenu)
			.find(options.rowSelector)
			.mouseenter(mouseenterRow)
			.mouseleave(mouseleaveRow)
			.click(clickRow);

		$(document).mousemove(mousemoveDocument);

	};
})(jQuery);









	/// for menu resize











