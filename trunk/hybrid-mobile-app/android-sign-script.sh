# sudo ionic run android --prod --release

# keytool -genkey -v -keystore alcodes-android-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias alcodes-android-release-key
# No@reply@12
#keytool -genkey -v -keystore ns-android-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias ns-android-release-key 

echo "------------------- singing jar with jarsigner----------------"  &&

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ns-android-release-key.jks $1 ns-android-release-key && 

echo "------------------- zipalign apk----------------"  &&

~/Library/Android/sdk/build-tools/26.0.0/zipalign -v 4 $1 nayashopi.apk &&

echo "------------------- verifying apk----------------"  &&

~/Library/Android/sdk/build-tools/25.0.3/apksigner verify nayashopi.apk

