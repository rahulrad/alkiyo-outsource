import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { IonicStorageModule } from '@ionic/storage';
import { MenuService } from "./services/menu.service";
import { ApiBaseService } from "./services/service.api.base";
import { Common } from "./Utility/common";
import { HttpModule } from '@angular/http';
import { CategoryService } from './services/category.service';
import { StoreService } from './services/store.service';
import { ProductService } from './services/product.service';
import { CouponService } from './services/coupon.service';
import { TabsPage } from '../pages/tabs/tabs';
import { CategoryPage } from '../pages/categories/category';
import { CategoryProductsPage } from '../pages/products/category.product';
import { ProductModule } from './modules/product.module';
import { ProductFiltertPage } from '../pages/products/product.filter';
import { SharedModule } from './modules/shared.module';
import { UserSignIn } from '../pages/user/user.signin';
import { UserSignUp } from '../pages/user/user.signup';
import { UserForgotPassword } from '../pages/user/user.forgot.pass';
import { ProductSearch } from '../pages/products/product.search';
import { UserService } from './services/user.service';
import { GooglePlus } from '@ionic-native/google-plus';
import { ProductCompareWidget } from './components/product/product.compare.widget';
import { ProductCompareSearch } from '../pages/products/product.compare.search';
import { ProductCompare } from '../pages/products/product.compare';
import { Facebook } from '@ionic-native/facebook';
import { Network } from '@ionic-native/network';
import { BaseComponent } from './base.component';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    CategoryPage,
    CategoryProductsPage,
    ProductFiltertPage,
    UserSignIn,
    UserSignUp,
    UserForgotPassword,
    ProductSearch,
    ProductCompareWidget,
    ProductCompareSearch,
    ProductCompare,
    BaseComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    SharedModule.forRoot(),
    ProductModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    CategoryPage,
    CategoryProductsPage,
    ProductFiltertPage,
    UserSignIn,
    UserSignUp,
    UserForgotPassword,
    ProductSearch,
    ProductCompareSearch,
    ProductCompare
  ],
  providers: [
    StatusBar,
    SplashScreen,
    MenuService,
    CategoryService,
    StoreService,
    ProductService,
    CouponService,
    ApiBaseService,
    Common,
    UserService,
    GooglePlus,
    Facebook,
    Network,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule { }
