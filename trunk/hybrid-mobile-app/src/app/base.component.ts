import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Common } from './Utility/common';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Alert } from 'ionic-angular/components/alert/alert';

@Component({
    selector: 'base-component',
    templateUrl: 'base.html'
})
export class BaseComponent implements OnInit {

    constructor(
        public alertCtrl: AlertController,
    ) {


    }

    ngOnInit(): void {
        console.log("parent ngonint")
    }
    public handleErrors(message) {
        let alert: Alert = this.alertCtrl.create({
            message: message,
            buttons: ["Retry"]
        });
        alert.onDidDismiss(() => {
            this.ngOnInit();
        });
        alert.present();
    }



}

