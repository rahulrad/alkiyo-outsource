import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ApiConstant } from './../../Utility/api.constant';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

import { Common } from './../../Utility/common';

import { Component } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';
import { Loading } from 'ionic-angular/components/loading/loading';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { CouponService } from '../../services/coupon.service';
import { Coupon, CouponData } from '../../models/coupon';
import { ViewChild } from '@angular/core';
import { CouponModalComponent } from './couponmodal.component';
import { ProductSearch } from '../../../pages/products/product.search';


@Component({
  templateUrl: '../../../pages/coupon/couponlist.html'
})
export class CouponListComponent implements OnInit, OnDestroy {


   menuTitle: string;
   id_vendor: number;
  vendorName: string;
  private subscribe: Subscription;
  couponList: CouponData[];
  shownGroup: null;

  options: InAppBrowserOptions = {
    location: 'yes',//Or 'no' 
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls 
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'yes', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'pagesheet',//iOS only 
    fullscreen: 'yes',//Windows only    
  };


  constructor(private _couponService: CouponService, private utility: Common, public navParams: NavParams
    , public navCtrl: NavController, private modalCtrl: ModalController, private theInAppBrowser: InAppBrowser) {

  }

  public openWithSystemBrowser(url: string) {
    let target = "_system";
    this.theInAppBrowser.create(url, target, this.options);
  }
  public openWithInAppBrowser(url: string) {
    let target = "_blank";
    this.theInAppBrowser.create(url, target, this.options);
  }
  public openWithCordovaBrowser(url: string) {
    let target = "_self";
    this.theInAppBrowser.create(url, target, this.options);
  }


  ngOnInit() {
    this.id_vendor = this.navParams.get('id_vendor');
    this.vendorName = this.navParams.get('name');
    this.menuTitle = this.vendorName + " Deals & Offers";
    this.getCouponsList();



  }


  openmodal(data: CouponData) {
    let productFilterModal = this.modalCtrl.create(CouponModalComponent, {
      'coupon': data, 'name': this.vendorName
    });
    //productFilterModal.onDidDismiss(data => this.filterProducts(data));
    productFilterModal.present();
  }
  onSearch(event) {
    this.navCtrl.push(ProductSearch);
  }


  ngOnDestroy() {


  }

  getslider(count) {
    return ApiConstant.staticImagesDomain + ApiConstant.couponVendorMobileSliderImages + this.vendorName.toLowerCase() + "-" + count + ".png";
  }

  getImagePath(id) {
    return ApiConstant.staticImagesDomain + ApiConstant.couponCategoryMobileThumb + id + ".svg";
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };




  private getCouponsList() {
    let loader: Loading;

    loader = this.utility.showProgressBar({
      content: 'loading Deals',
      spinner: 'dots'
    });
    loader.present();
    this.subscribe = this._couponService.getCouponsListwithMerchant(this.id_vendor).subscribe(
      data => {

        this.couponList = data.data;

      },
      error => {

      },
      () => {
        this.utility.dismissProgressBar(loader);
      }
    );

  }

}

