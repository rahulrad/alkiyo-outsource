
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { CouponListComponent } from './couponList.component';
import { Common } from './../../Utility/common';

import { Component } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';
import { Loading } from 'ionic-angular/components/loading/loading';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { NavController } from 'ionic-angular/navigation/nav-controller';

import { Coupon, CouponData } from '../../models/coupon';
import { ViewChild } from '@angular/core';
import * as Clipboard from 'clipboard/dist/clipboard.min.js';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';


@Component({
  templateUrl: '../../../pages/coupon/couponmodal.html'
})
export class CouponModalComponent implements OnInit, OnDestroy {


  private menuTitle: string;
  private subscribe: Subscription;
  coupon: CouponData;
  code: string;
  clipboard: Clipboard;
  name: string;

  options: InAppBrowserOptions = {
    location: 'yes',//Or 'no' 
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls 
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'yes', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'pagesheet',//iOS only 
    fullscreen: 'yes',//Windows only    
  };


  constructor(private utility: Common, public navParams: NavParams
    , public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController, private theInAppBrowser: InAppBrowser, private socialSharing: SocialSharing) {
    this.clipboard = new Clipboard('#cpyBtn');
    this.clipboard.on('success', () => this.showMsg(toastCtrl));

  }

  public share(coupon: any) {
    this.socialSharing.share(coupon.description, coupon.title, null, coupon.redirectLink)
      .then((data) => {
        console.log(data);
      }).catch(error => {
        console.log(error);
      });
  }

  public openWithSystemBrowser(url: string) {
    let target = "_system";
    this.theInAppBrowser.create(url, target, this.options);
  }
  public openWithInAppBrowser(url: string) {
    let target = "_blank";
    this.theInAppBrowser.create(url, target, this.options);
  }
  public openWithCordovaBrowser(url: string) {
    let target = "_self";
    this.theInAppBrowser.create(url, target, this.options);
  }
  showMsg(toastCtrl: ToastController) {
    let toast = toastCtrl.create({
      message: 'Its copied to clipboard',
      duration: 1000,
      position: 'top'
    });
    toast.present();
    this.openWithInAppBrowser(this.coupon.redirectLink);
  }


  ngOnInit() {
    this.menuTitle = "Coupons";
    this.coupon = this.navParams.get('coupon');
    this.name = this.navParams.get('name');
    this.code = this.coupon.code;
  }


  ngOnDestroy() {


  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


}

