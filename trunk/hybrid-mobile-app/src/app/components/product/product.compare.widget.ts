import { Component } from "@angular/core";
import { ModalController } from "ionic-angular/components/modal/modal-controller";
import { ProductCompareSearch } from "../../../pages/products/product.compare.search";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { CompareProduct } from "../../models/product.compare";
import { ProductCompare } from "../../../pages/products/product.compare";



@Component({
    selector: "product-compare-widget",
    templateUrl: "../../../pages/product/product.compare.widget.html"
})
export class ProductCompareWidget {

    product1: CompareProduct = null;
    product2: CompareProduct = null;
    category: string = "";

    constructor(
        public modalCtrl: ModalController,
        public navCtrl: NavController
    ) {

    }

    selectFirstProduct() {
        console.log("fist");
        let productFilterModal = this.modalCtrl.create(ProductCompareSearch, {
            'category': this.category
        });
        productFilterModal.onDidDismiss(data => {
            this.product1 = data;

            if (data && data.hasOwnProperty('category')) {
                this.category = data.category;
            }
        });
        productFilterModal.present();
    }

    selectSecondProduct() {

        let productFilterModal = this.modalCtrl.create(ProductCompareSearch,
            {

                'category': this.category
            });
        productFilterModal.onDidDismiss(data => {
            this.product2 = data;
            if (data && data.hasOwnProperty('category')) {
                this.category = data.category;
            }
        });
        productFilterModal.present();
    }

    compare() {
        this.navCtrl.push(ProductCompare, {
            product1: this.product1,
            product2: this.product2,

            category: this.category,
        });
    }

}