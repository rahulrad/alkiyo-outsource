import { ProductReviewComponent } from './product.review.component';
import { ProductComponent } from './product.component';
import { ProductStoreComponent } from './product.store.component';
import { NavController } from 'ionic-angular';

import { Component, OnInit, Input } from '@angular/core';
import { ProductSpecsComponent } from './product.specs.component';


@Component({
    selector: '<product-segment>',
    templateUrl: '../../../pages/product/productsegment.html',

})
export class ProductSegmentComponent implements OnInit {
    @Input('activeTab') active: string;
    @Input() Obj: any;

    constructor(public navCtrl: NavController) {
        this.active = "overview";
    }

    ngOnInit() {


    }

     redirect(component: string) {


        if (component === 'store') {
            console.log(this.Obj);
            this.navCtrl.push(ProductStoreComponent, { 'obj': this.Obj },
                { animate: true, direction: 'forward', animation: 'ios-transition' }
            );
        }
        else if (component === 'specs') {
            this.navCtrl.push(ProductSpecsComponent, { 'obj': this.Obj },
                { animate: true, direction: 'forward', animation: 'ios-transition' }
            );
        }
        else if (component === 'overview') {
            this.navCtrl.push(ProductComponent, { 'obj': this.Obj },
                { animate: true, direction: 'forward', animation: 'ios-transition' }
            );
        }
        else {
            this.navCtrl.push(ProductReviewComponent, { 'obj': this.Obj },
                { animate: true, direction: 'forward', animation: 'ios-transition' }
            );

        }
    }


}