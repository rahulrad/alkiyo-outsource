export interface Category {
    display_name: string,
    slug: string
}