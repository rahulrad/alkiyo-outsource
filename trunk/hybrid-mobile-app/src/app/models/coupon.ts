export interface Coupon {
    name: string,
    image: string,
    offer_count: string,
    discount: string,
    selected: number,
    id: number
}

export interface CouponData {
    id: number,
    offer_id: number,
    promo_id: string,
    title: string,
    offer: string,
    offer_type: string,
    code: string,
    category: number,
    offer_url: string,
    start_date: string,
    expiry_date: string,
    merchant: number,
    merchant_logo_url: string,
    featured: number,
    exclusive: number,
    track_link: string,
    description: string,
    status: string,
    discount: string,
    verified_on: string,
    call_to_action_button: string;
    redirectLink: string;

}