export interface CompareProduct {
    label: string,
    icon: string,
    lowest_price: string,
    slug: string
}