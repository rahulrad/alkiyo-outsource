import { CouponListComponent } from './../components/coupon/couponList.component';


import { ProductReviewComponent } from './../components/product/product.review.component';

import { ProductSpecsComponent } from './../components/product/product.specs.component';
import { ProductStoreComponent } from './../components/product/product.store.component';
import { ProductSegmentComponent } from './../components/product/productsegment.component';

import { ErrorHandler, NgModule } from '@angular/core';
import { IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ProductComponent } from '../components/product/product.component';


import { ProductService } from '../services/product.service';
import { CommonModule } from '@angular/common';

import { SharedModule } from './shared.module';
import { CouponHomeComponent } from '../components/coupon/couponhome.component';
import { CouponModalComponent } from '../components/coupon/couponmodal.component';
import { IonicImageViewerModule } from 'ionic-img-viewer';


@NgModule({
  declarations: [
    ProductComponent,
    ProductSegmentComponent,
    ProductStoreComponent,
    ProductSpecsComponent,
    ProductReviewComponent,
    CouponHomeComponent,
    CouponListComponent,
    CouponModalComponent



  ],
  imports: [
    CommonModule,
    IonicModule.forRoot(ProductComponent),
    SharedModule.forRoot(),
    IonicImageViewerModule,



  ],
  entryComponents: [
    ProductComponent,
    ProductSegmentComponent,
    ProductStoreComponent,
    ProductSpecsComponent,
    ProductReviewComponent,
    CouponHomeComponent,
    CouponListComponent,
    CouponModalComponent

  ],
  providers: [
    ProductService,

  ]
})
export class ProductModule { }
