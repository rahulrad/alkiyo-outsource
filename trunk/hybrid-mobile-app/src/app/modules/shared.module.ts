
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { KeysPipe, ValuesPipe } from './../pipes/key.pipes';
import { IonicModule } from 'ionic-angular';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { IonRating } from '../components/shared/ion-rating/ion-rating';
import { CommonModule } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing';

import { Ionic2Rating } from 'ionic2-rating/dist/ionic2-rating';


@NgModule({
    imports: [CommonModule,
        IonicModule],
    declarations: [IonRating, Ionic2Rating, KeysPipe, ValuesPipe],
    entryComponents: [],
    exports: [IonRating, IonicModule, CommonModule, Ionic2Rating, KeysPipe, ValuesPipe],
    providers: [InAppBrowser, SocialSharing]
})
export class SharedModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: []
        };
    }
}