import { Injectable } from '@angular/core';
import { ApiBaseService } from "../services/service.api.base";
import { ApiConstant } from "../Utility/api.constant";
import { Observable } from "rxjs/Observable";
import { ApiResponse } from "../models/api.response";

@Injectable()
export class CategoryService {
    constructor(
        private baseService: ApiBaseService
    ) {

    }

    /**
     * menu list api
     */
    public getTopCategories(): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.topCategoriesUrl;
        return this.baseService.get(url, false);
    }
}