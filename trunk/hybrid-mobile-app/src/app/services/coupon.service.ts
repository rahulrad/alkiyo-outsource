import { Injectable } from '@angular/core';
import { ApiBaseService } from "../services/service.api.base";
import { ApiConstant } from "../Utility/api.constant";
import { Observable } from "rxjs/Observable";
import { ApiResponse } from "../models/api.response";

@Injectable()
export class CouponService {
    constructor(
        private baseService: ApiBaseService
    ) {

    }

    /**
     * 
     */
    public getTrendingDeals(): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.trendingDealsUrl;
        return this.baseService.get(url, false);
    }

    /**
     * 
     */
    public getCouponsAndCashbacks(full: number): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.couponsAndCashbacksUrl + "?full=" + full;
        return this.baseService.get(url, false);
    }


    public getCouponsListwithMerchant(id_merchant: number): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.getCouponsListwithMerchant + id_merchant;
        return this.baseService.get(url, false);
    }



}