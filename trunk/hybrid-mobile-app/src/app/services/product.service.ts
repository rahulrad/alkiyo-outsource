import { ApiConstant } from './../Utility/api.constant';
import { Injectable } from '@angular/core';
import { ApiBaseService } from "../services/service.api.base";
import { Observable } from "rxjs/Observable";

import { ApiResponse } from "../models/api.response";

@Injectable()
export class ProductService {
    constructor(
        private baseService: ApiBaseService

    ) {

    }

    /**
     * 
     * @param categoryId 
     * @param sort 
     */
    public getNewProducts(categoryId: string, sort: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.categoryWiseProductUrl;
        url = url.replace("{categoryId}", categoryId).replace("{sort}", sort);
        return this.baseService.get(url, false);
    }

    /**
     * 
     * @param categoryId 
     * @param sort 
     */
    public getPopularProducts(categoryId: string, sort: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.categoryWiseProductUrl;
        url = url.replace("{categoryId}", categoryId).replace("{sort}", sort);
        return this.baseService.get(url, false);
    }

    geCategoryWiseProducts(query: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.menuWiseProductUrl;
        url = url.replace("{query}", query);
        console.log(query);
        return this.baseService.get(url, false);
    }


    public getProductObject($product_id: any): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.getProductdata + $product_id;
        return this.baseService.get(url, false);
    }

    geCategoryWiseFilters(menuSlug: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.menuWiseFiltersUrl;
        url = url.replace("{slug}", menuSlug);
        return this.baseService.get(url, false);
    }



    public addUserReview(reveiwObj: any): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.reviewPostUrul;
        console.log(url);
        return this.baseService.post(url, JSON.stringify(reveiwObj));
    }


    public searchProductsByTerm(term: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.productSearchByTerm;
        url = url.replace("{term}", term);
        return this.baseService.get(url, false);
    }

    public searchCompareProductsByTerm(term: string, category: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.productCompareSearchByTerm;
        url = url.replace("{term}", term);
        url = url.replace("{category}", category);
        return this.baseService.get(url, false);
    }

    public getCompareProductsData(query: string): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.productCompareData;
        url = url.replace("{query}", query);
        return this.baseService.get(url, false);
    }
}