import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Loading } from 'ionic-angular';
import { Common } from '../../app/Utility/common';
import { ApiResponse } from '../../app/models/api.response';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiBaseService {
    constructor(
        private _httpService: Http,
        private commonUtility: Common
    ) {

    }

    /**
     * 
     * @param apiURL 
     * @param param 
     */
    public post(apiURL: string, param: string): Observable<ApiResponse> {
        let body = param;
        let headers = this.getAuthHeader();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        let options = new RequestOptions({
            headers: headers,
        });
        return this._httpService.post(apiURL, body, options)
            .timeout(10000)
            .map(res => {
                return res.json();
            }).catch(
            (res) => {
                this.handleHTTPError(res);
                return Observable.throw(res.json().error || 'Server error');
            });
    }

    /**
     * 
     * @param apiURL 
     * @param credential 
     * @param showLoader 
     */
    public get(apiURL: string, param?: any): Observable<ApiResponse> {
        let header = this.getAuthHeader();
        let options = new RequestOptions({ headers: header });
        return this._httpService.get(apiURL, options)
            .timeout(20000)
            .map(res => {
                return res.json();
            }).catch(
            (error) => {
                return this.handleHTTPError(error);
            });
    }

    /**
     * HTTP Errors handlor method common for all services with toast mssages
     */
    handleHTTPError(error: any) {
        switch (error.status != 200) {
            case error.status == 401:
                return Observable.throw("Authorization Required..");
            case error.status == 502 || error.status == 503 || error.status == 500:
                return Observable.throw("Bad Getway..");
            default:
                return Observable.throw("Error loading Data..");
        }
    }

    /**
     * 
     * @param username 
     * @param userpassword 
     */
    private getAuthHeader(): Headers {
        let headers = new Headers();
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS, POST, PUT, DELETE");
        headers.append("Access-Control-Allow-Headers", "Content-type,X-Requested-With,Origin,accept");
        headers.append("Content-type", 'application/json');
        return headers;
    }
}