import { Injectable } from '@angular/core';
import { ApiBaseService } from "../services/service.api.base";
import { ApiConstant } from "../Utility/api.constant";
import { Observable } from "rxjs/Observable";
import { ApiResponse } from "../models/api.response";

@Injectable()
export class StoreService {
    constructor(
        private baseService: ApiBaseService
    ) {

    }

    /**
     * 
     */
    public getTopStores(): Observable<ApiResponse> {
        let url: string = ApiConstant.baseDomain + ApiConstant.popularStoreUrl;
        return this.baseService.get(url, false);
    }
}