import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { ApiBaseService } from "./service.api.base";
import { ApiConstant } from "../Utility/api.constant";
import { ApiResponse } from "../models/api.response";

export class User {
    name: string;
    email: string;

    constructor(name: string, email: string) {
        this.name = name;
        this.email = email;
    }
}

@Injectable()
export class UserService {

    constructor(
        private baseService: ApiBaseService

    ) {

    }

    currentUser: User = null;

    public isGuest(): boolean {
        if (this.currentUser != null) {
            return false;
        } else {
            return true;
        }
    }

    public setCurrentUser(user: any) {
        this.currentUser = new User(user.username, user.email);
    }
    public login(credentials): Observable<ApiResponse> {
        if (credentials.email === null || credentials.password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            let url: string = ApiConstant.baseDomain + ApiConstant.userLogin;
            url = url.replace("{email}", credentials.email);
            url = url.replace("{password}", credentials.password);
            return this.baseService.get(url, false);
        }
    }

    public loginWithSocial(email: string): Observable<ApiResponse> {
        if (email === null) {
            return Observable.throw("Invalid credentials");
        } else {
            let url: string = ApiConstant.baseDomain + ApiConstant.userSocalLogin;
            url = url.replace("{email}", email);
            return this.baseService.get(url, false);
        }
    }

    public register(credentials): Observable<ApiResponse> {
        if (credentials.email === null || credentials.username === null || credentials.password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            let url: string = ApiConstant.baseDomain + ApiConstant.userRegister;
            url = url.replace("{email}", credentials.email);
            url = url.replace("{username}", credentials.username);
            url = url.replace("{password}", credentials.password);
            return this.baseService.post(url, JSON.stringify(credentials));
        }
    }

    public forgotPassword(credentials): Observable<ApiResponse> {
        console.log(credentials);
        if (credentials.email === null) {
            return Observable.throw("Please insert credentials");
        } else {
            let url: string = ApiConstant.baseDomain + ApiConstant.userForgetPassword;
            url = url.replace("{email}", credentials.email);
            return this.baseService.get(url, false);
        }
    }

    public getUserInfo(): User {
        return this.currentUser;
    }

    public logout() {
        return Observable.create(observer => {
            this.currentUser = null;
            observer.next(true);
            observer.complete();
        });
    }
}