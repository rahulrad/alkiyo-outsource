import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../app/services/menu.service';
import { ProductService } from '../../app/services/product.service';
import { Product } from '../../app/models/product';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Common } from '../../app/Utility/common';
import { Loading } from 'ionic-angular/components/loading/loading';
import { ProductComponent } from '../../app/components/product/product.component';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ProductFiltertPage } from './product.filter';
import { QueryParam } from '../../app/models/query.param';
import { query } from '@angular/core/src/animation/dsl';
import { ProductSearch } from './product.search';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { BaseComponent } from '../../app/base.component';

@Component({
    templateUrl: 'category.product.html'
})
export class CategoryProductsPage extends BaseComponent implements OnInit {

    menuSlug: string;
    menuTitle: string;
    products: Product[];
    grid: Array<Array<Product>>;
    page: number;
    filters: any;
    sorts: any;
    queryParams: QueryParam[];
    constructor(
        private productService: ProductService,
        public navCtrl: NavController,
        public navParams: NavParams,
        public utility: Common,
        public actionSheetCtrl: ActionSheetController,
        public modalCtrl: ModalController,
        public alertCtrl: AlertController
    ) {
        super(alertCtrl);
        this.products = [];
        this.page = 1;
        this.filters = [];
        this.sorts = [];
        this.queryParams = [];
        this.menuSlug = this.navParams.get("slug");
        this.menuTitle = this.navParams.get("title");
    }

    ngOnInit(): void {
        this.loadCategoryWiseFilters(this.menuSlug);
        let queryString: string = this.getFilterUrl();
        this.loadCategoryProducts(queryString, true);
    }

    loadCategoryWiseFilters(slug: string) {
        this.productService.geCategoryWiseFilters(this.menuSlug).subscribe(
            response => {
                this.filters = response.data.filters;
                this.sorts = response.data.sorts;
            },
            error => { }
        );
    }

    loadCategoryProducts(query: string, showLoader: boolean, infiniteScroll?: any) {
        let loader: Loading;
        if (showLoader) {
            loader = this.utility.showProgressBar({
                content: 'loading Products',
                spinner: 'dots'
            });
            loader.present();
        }
        this.productService.geCategoryWiseProducts(query).subscribe(

            response => {
                if (infiniteScroll) {
                    this.products = this.products.concat(response.data);
                } else {
                    this.products = response.data;
                }
                this.fillGrid();
                this.page++;
            },
            error => {
                if (showLoader) {
                    this.utility.dismissProgressBar(loader);
                }
                if (infiniteScroll) {
                    infiniteScroll.complete();
                }
                this.handleErrors(error);
            },
            () => {
                if (showLoader) {
                    this.utility.dismissProgressBar(loader);
                }
                if (infiniteScroll) {
                    infiniteScroll.complete();
                }
            }
        );
    }

    doInfinite(infiniteScroll) {
        setTimeout(() => {
            let queryString: string = this.getFilterUrl();
            this.loadCategoryProducts(queryString, false, infiniteScroll);
        }, 500);
    }

    openPage(slug) {
        this.navCtrl.push(ProductComponent, { 'slug': slug });
    }

    fillGrid() {

        let rowNum = 0;
        this.grid = Array(Math.ceil(this.products.length / 2));

        for (let i = 0; i < this.products.length; i += 2) {
            this.grid[rowNum] = Array(2);
            if (this.products[i]) {
                this.grid[rowNum][0] = this.products[i]
            }
            if (this.products[i + 1]) {
                this.grid[rowNum][1] = this.products[i + 1]
            }
            rowNum++;
        }
    }

    presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Sort By',
        });
        this.sorts.forEach(sort => {
            let button = {
                text: sort.name,
                role: sort.role,
                icon: sort.checked ? "checkmark" : null,
                handler: this.handleSort.bind(this, sort)
            };
            actionSheet.addButton(button);
        });
        actionSheet.present();
    }

    handleSort(sortBy) {
        this.sorts.forEach(element => {
            element.checked = false;
        });
        sortBy.checked = true;
        this.page = 1;
        let queryString: string = this.getFilterUrl();
        this.loadCategoryProducts(queryString, true);
    }
    displaySort() {
        this.presentActionSheet();
    }


    onSearch(event) {
        this.navCtrl.push(ProductSearch);
    }


    displayFilters() {
        let productFilterModal = this.modalCtrl.create(ProductFiltertPage, {
            filtersData: JSON.stringify(this.filters)
        });
        productFilterModal.onDidDismiss(data => this.filterProducts(data));
        productFilterModal.present();
    }

    private filterProducts(data) {
        if (data.isApplied) {
            this.filters = data.filtersData;
            this.page = 1;
            let queryString: string = this.getFilterUrl();
            this.loadCategoryProducts(queryString, true);
        }
    }

    private getFilterUrl() {
        this.queryParams = [];
        let queryArray: string[] = [];
        this._getMenuQueryParam();
        this._getPageQueryParam();
        this._getSortQueryParam();
        this._getFeaturesQueryParam();

        this.queryParams.forEach(query => {
            queryArray.push(encodeURIComponent(query.key) + '=' + encodeURIComponent(query.value));
        });
        return queryArray.join("&");
    }

    private _getMenuQueryParam() {
        this.queryParams.push({
            key: "slug",
            value: this.menuSlug
        });
    }
    private _getPageQueryParam() {
        this.queryParams.push({
            key: "page",
            value: this.page.toString()

        });
    }

    private _getSortQueryParam() {
        let sortBy = "new";
        this.sorts.forEach(element => {
            if (element.checked) {
                sortBy = element.key;
            }
        });
        this.queryParams.push({
            key: "sort",
            value: sortBy
        });
    }

    private _getFeaturesQueryParam() {
        this.filters.forEach(filter => {
            filter.values.forEach(filterValue => {
                if (filterValue.selected) {
                    this.queryParams.push({
                        key: filter.filterKey,
                        value: filterValue.filterVal,
                    });
                }
            });
        });
    }
}
