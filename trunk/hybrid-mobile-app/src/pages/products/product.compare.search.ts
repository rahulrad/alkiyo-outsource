import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Component, OnInit } from "@angular/core";
import { ProductService } from "../../app/services/product.service";
import { CategoryProductsPage } from "./category.product";
import { ProductModule } from "../../app/modules/product.module";
import { ProductComponent } from "../../app/components/product/product.component";
import { FormControl } from "@angular/forms";
import { ViewController } from "ionic-angular/navigation/view-controller";
import 'rxjs/add/operator/debounceTime';
import { NavController } from "ionic-angular/navigation/nav-controller";
import { HomePage } from '../home/home';

@Component({
    templateUrl: "./product.compare.search.html"
})
export class ProductCompareSearch implements OnInit {

     term: string;
     searchResults: any[];
    searching: boolean = false;
    searchControl: FormControl;
    category: string = '';


    constructor(
        private productService: ProductService,
        public viewCtrl: ViewController,
        public navParams: NavParams,
        private navCtrl: NavController,
    ) {
        this.searchResults = [];
        this.searchControl = new FormControl();
    }

    ngOnInit(): void {
        this.category = this.navParams.get("category");
    }

    goback() {

        this.navCtrl.parent.select(0);
    }
    ionViewDidLoad() {
        this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
            this.onSearch();
        });
    }

    onSearch() {
        this.productService.searchCompareProductsByTerm(this.term, this.category).subscribe(
            response => {
                this.searching = false;
                this.searchResults = response.data;
            }
        );
    }

    onSearchInput() {
        this.searching = true;
    }

    onProductSelect(result: any) {
        this.viewCtrl.dismiss(result);
    }

    dismiss() {
        this.viewCtrl.dismiss({

        });
    }

}