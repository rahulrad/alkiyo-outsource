import { Component, OnInit } from "@angular/core";
import { ProductService } from "../../app/services/product.service";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { CategoryProductsPage } from "./category.product";
import { ProductModule } from "../../app/modules/product.module";
import { ProductComponent } from "../../app/components/product/product.component";
import { Storage } from '@ionic/storage';
import { FormControl } from "@angular/forms";
import { Events } from "ionic-angular/util/events";
@Component({
    templateUrl: "./product.search.html"
})
export class ProductSearch implements OnInit {

     term: string;
     searchResults: any[];
    searching: boolean = false;
    searchControl: FormControl;
    recentSearch: any[] = [];

    constructor(
        private productService: ProductService,
        private navCtrl: NavController,
        private storage: Storage,
        private events: Events
    ) {
        this.searchResults = [];
        this.searchControl = new FormControl();
        events.subscribe('tab:popToRoot', (tab) => {
            this.navCtrl.popToRoot();
        });
    }

    ngOnInit(): void {
        //this.storage.clear();
        this.storage.forEach((value, key, index) => {
            let data = {
                'obj': { 'key': key, 'value': value }
            };

            console.log(data);
            this.recentSearch[this.recentSearch.length] = data;
        });

    }

    goback() {
        this.navCtrl.parent.select(0);
    }

    ionViewDidLoad() {
        this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
            this.onSearch();
        });
    }

    onSearch() {
        this.productService.searchProductsByTerm(this.term).subscribe(
            response => {
                this.searching = false;
                this.searchResults = response.data;
            }
        );
    }

    onSearchInput() {
        this.searching = true;
    }

    displayResultDetail(result: any) {
        if (result.isMenu) {
            this.navCtrl.push(CategoryProductsPage, {
                slug: result.slug,
                title: result.label
            });

            this.checkListInStoreage('0~' + result.slug, result.label);

        } else if (result.isProduct) {
            this.navCtrl.push(ProductComponent, {
                slug: result.slug,
                title: result.label
            });
            this.checkListInStoreage('1~' + result.slug, result.label);
        }
    }

    checkListInStoreage(slug: any, title: any) {
        console.log(slug);
        console.log(title);
        this.storage.set(slug, title);
    }

    redirect(search: any) {

        let result = search.obj.key.split("~");
        console.log(result[0]);
        if (result[0] === '0') {
            this.navCtrl.push(CategoryProductsPage, {
                slug: result[1],
                title: search.obj.value,
            });
        }
        if (result[0] === '1') {
            this.navCtrl.push(ProductComponent, {
                slug: result[1],
                title: search.obj.value,
            });
        }
    }
}