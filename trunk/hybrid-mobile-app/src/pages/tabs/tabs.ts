import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { CategoryPage } from '../categories/category';
import { UserSignIn } from '../user/user.signin';
import { ProductSearch } from '../products/product.search';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { UserService } from '../../app/services/user.service';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { ViewChild } from '@angular/core';
import { Tab } from 'ionic-angular/components/tabs/tab';
import { Events } from 'ionic-angular/util/events';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {

    tabs: any;
    @ViewChild('myTabs') tabRef: Tabs;
    constructor(
        private userService: UserService,
        private navCtrl: NavController,
        private events: Events
    ) {
        this.tabs = [
            {
                root: HomePage,
                title: "Home",
                icon: "home"
            },
            {
                root: CategoryPage,
                title: "Categories",
                icon: "list"
            },
            {
                root: ProductSearch,
                title: "Search",
                icon: "search"

            },
            {
                root: UserSignIn,
                title: "My Account",
                icon: "person"
            },
        ];

    }

    ngOnInit(): void {
        this.tabRef.ionChange.subscribe(
            tab => {
                this.events.publish('tab:popToRoot', tab);
            }
        );
    }

}
